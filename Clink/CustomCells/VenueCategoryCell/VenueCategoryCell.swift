//
//  VenueCategoryCell.swift
//  Clink
//
//  Created by Gunjan on 04/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import SDWebImage
class VenueCategoryCell: UICollectionViewCell {
    
    @IBOutlet var imgVenueCategory : UIImageView!
    @IBOutlet var selectionView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configureCell(objCategory:ClsMerchantCategory){        
            
//            = UIImage(named: IMG_PREFIX+pstrFileName)
        let imgTmp = UIImageView()
        imgTmp.sd_setImageWithURL(NSURL(string: objCategory.strImageURLSelected), placeholderImage: UIImage(named: "place_holder"))
        selectionView.hidden = false
        
    }
}
