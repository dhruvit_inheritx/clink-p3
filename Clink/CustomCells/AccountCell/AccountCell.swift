//
//  Accountself.swift
//  Clink
//
//  Created by Dhruvit on 22/10/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class AccountCell: UITableViewCell {
    
    @IBOutlet weak var lblDetailText: UILabel!
    @IBOutlet var txtField : UITextField!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var switchOnOff : UISwitch!
    @IBOutlet var btnInfo : UIButton!
    
    @IBOutlet weak var constraintImgViewWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintTxtFieldLeading: NSLayoutConstraint!

    @IBOutlet weak var constraintSwitchWidth: NSLayoutConstraint!
    
    @IBOutlet weak var constraintBtnInfoWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintBtnInfoTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var constraintImgViewLeading: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(pdictResponse:[String : AnyObject!], indexPath : NSIndexPath ){
        
        self.txtField.textColor = COLORTHEME
        self.txtField.tag = indexPath.row
        self.selectionStyle = .None
        
        self.lblDetailText.hidden = true
        self.lblDetailText.text = ""
        
        if self.constraintBtnInfoWidth != nil {
            self.constraintBtnInfoWidth.constant = 0
            self.constraintBtnInfoTrailing.constant = 0
            self.layoutIfNeeded()
        }

        if indexPath.section == 0
        {
            self.imgView.image = UIImage(named: "\(pdictResponse[KIMAGE]!)")
            self.txtField.placeholder = pdictResponse[KPLACEHOLDER] as? String
            
            self.txtField.enabled = true
            
            self.btnInfo.setTitle("?", forState: .Normal)
            self.btnInfo.userInteractionEnabled = true

            if pdictResponse[KPLACEHOLDER] as? String == "PASSWORD"{
                self.txtField.secureTextEntry = true
                self.btnInfo.hidden = false
                if self.constraintBtnInfoWidth != nil {
                    self.constraintBtnInfoWidth.constant = 33
                    self.constraintBtnInfoTrailing.constant = 15
                    self.layoutIfNeeded()
                }
            }
            
            self.txtField.text = pdictResponse.getValueIfAvilable(KTITLE) as? String
            
            if self.constraintImgViewWidth != nil{
                self.constraintImgViewWidth.constant = 29
                self.constraintTxtFieldLeading.constant = 15
                self.layoutIfNeeded()
            }
            
            if self.constraintSwitchWidth != nil{
                self.switchOnOff.hidden = true
                self.constraintSwitchWidth.constant = 0
                self.layoutIfNeeded()
            }
        }
        else if indexPath.section == 1{
            
            self.txtField.enabled = false
            
            self.imgView.image = UIImage(named: "\(pdictResponse[KIMAGE]!)")
            self.txtField.text = pdictResponse.getValueIfAvilable(KPLACEHOLDER) as? String
            
            if self.constraintImgViewWidth != nil{
                self.constraintImgViewWidth.constant = 29
                self.constraintTxtFieldLeading.constant = 15
                self.layoutIfNeeded()
            }
            
            if let objPaymentMethodDetails = pdictResponse.getValueIfAvilable(KPAYMENTMETHODDETAILS) as? ClsCreditCardDetails{
                
                if objPaymentMethodDetails.strAccountType == "bank" {
                    self.txtField.text = "Account *" + objPaymentMethodDetails.strLastFour
                }
                else {
                    self.txtField.text = "VISA CARD *" + objPaymentMethodDetails.strLastFour
                }
                
                if objPaymentMethodDetails.isDefaultPayment == false{
                    self.switchOnOff.on = false
                }
                else{
                    USERDEFAULTS.setDefaultPaymentDetailsObject(objPaymentMethodDetails)
                }
                
                if objPaymentMethodDetails.isDefaultTransfer == true && objPaymentMethodDetails.strAccountType == "debit" {
                    USERDEFAULTS.setDefaultTransferDetailsObject(objPaymentMethodDetails, forKey: KTRANSFERDETAILSDEBITCARD)
                }
            }
            
            if pdictResponse.getValueIfAvilable(KPLACEHOLDER) as? String == "ADD PAYMENT METHOD"{
                self.txtField.text = "ADD PAYMENT METHOD"
                
                self.switchOnOff.hidden = true
                self.constraintSwitchWidth.constant = 0
                self.layoutIfNeeded()
            }
            else{
                if self.constraintSwitchWidth != nil{
                    self.switchOnOff.hidden = false
                    self.constraintSwitchWidth.constant = 49
                    self.layoutIfNeeded()
                }
            }
        }
        else if indexPath.section == 2
        {
            self.txtField.enabled = false
            
            self.txtField.text = pdictResponse[KPLACEHOLDER] as? String

            self.lblDetailText.hidden = false
            
            if pdictResponse[KPLACEHOLDER]! as! String == "CLINK! BALANCE"{
                let strBalance = pdictResponse["detail"] as! String
                if strBalance == "0" {
                    self.lblDetailText?.text = "$" + strBalance
                    self.lblDetailText?.textColor = UIColor.lightGrayColor()
                }
                else{
                    self.lblDetailText?.text = "$" + strBalance
                    self.lblDetailText?.textColor = UIColor.blackColor()
                }
            }
            
            if let objPaymentMethodDetails = pdictResponse.getValueIfAvilable(KPAYMENTMETHODDETAILS) as? ClsCreditCardDetails{

                if objPaymentMethodDetails.strLastFour != "" {
                    
                    if objPaymentMethodDetails.strAccountType == "debit" {
                        self.txtField.text = "VISA CARD *" + objPaymentMethodDetails.strLastFour
                    }
                    else {
                        self.txtField.text = "Account *" + objPaymentMethodDetails.strLastFour
                    }
                }
                else{
                    self.txtField.text = pdictResponse[KPLACEHOLDER] as? String
                }
                
                if objPaymentMethodDetails.isDefaultTransfer == true{
                    USERDEFAULTS.setDefaultTransferDetailsObject(objPaymentMethodDetails, forKey: KTRANSFERDETAILSBANK)
                    self.switchOnOff.on = true
                }
                else{
                    self.switchOnOff.on = false
                }

                
                if objPaymentMethodDetails.strId == ""{
                    
                    if self.constraintSwitchWidth != nil{
                        self.switchOnOff.hidden = true
                        self.constraintSwitchWidth.constant = 0
                        self.layoutIfNeeded()
                    }
                }
                else{
                    if self.constraintSwitchWidth != nil{
                        self.switchOnOff.hidden = false
                        self.constraintSwitchWidth.constant = 49
                        self.layoutIfNeeded()
                    }
                }
            }

            if pdictResponse[KIMAGE]! as! String == "" {
                if self.imgView != nil{
                    self.imgView.image = nil
                }
                if self.constraintImgViewWidth != nil{
                    self.constraintImgViewWidth.constant = 0
                    self.constraintTxtFieldLeading.constant = 0
                    self.layoutIfNeeded()
                }
            }
            else{
                self.imgView.image = UIImage(named: "\(pdictResponse[KIMAGE]!)")
                if self.constraintImgViewWidth != nil{
                    self.constraintImgViewWidth.constant = 29
                    self.constraintTxtFieldLeading.constant = 15
                    self.layoutIfNeeded()
                }
            }
        }
        else if indexPath.section == 3
        {
            self.txtField.enabled = false
            
            self.txtField.text = pdictResponse["title"] as? String
            self.txtField.textColor = UIColor.blackColor()
            
            if self.imgView != nil{
                self.imgView.image = nil
            }
            if self.constraintImgViewWidth != nil{
                self.constraintImgViewWidth.constant = 0
                self.constraintTxtFieldLeading.constant = 0
                self.layoutIfNeeded()
            }
            
            if self.constraintSwitchWidth != nil{
                self.switchOnOff.hidden = false
                self.constraintSwitchWidth.constant = 49
                self.layoutIfNeeded()
            }
        }
        else if indexPath.section == 4 {
            
            self.btnInfo.setTitle("", forState: .Normal)
            self.btnInfo.setImage(UIImage(named: "arrow"), forState: .Normal)
            self.btnInfo.tintColor = COLORTHEME
            self.btnInfo.hidden = false
            self.btnInfo.userInteractionEnabled = false
            
            if self.constraintBtnInfoWidth != nil {
                self.constraintBtnInfoWidth.constant = 33
                self.constraintBtnInfoTrailing.constant = 10
                self.layoutIfNeeded()
            }
            
            self.txtField.enabled = false
            
            //            self.accessoryType = .DisclosureIndicator
            //            self.accessoryView = UIImageView(image: UIImage(named: "arrow"))
            
            self.txtField.text = pdictResponse[KTITLE] as? String
            
            if self.constraintImgViewWidth != nil{
                self.constraintImgViewWidth.constant = 0
                self.constraintTxtFieldLeading.constant = 0
                self.layoutIfNeeded()
            }
            
            if self.constraintSwitchWidth != nil{
                self.switchOnOff.hidden = true
                self.constraintSwitchWidth.constant = 0
                self.layoutIfNeeded()
            }
        }
        else
        {
            self.txtField.enabled = false
            
//            self.accessoryType = .DisclosureIndicator
//            self.accessoryView?.tintColor = COLORTHEME
            
//            self.btnInfo.setTitle(">", forState: .Normal)
//            self.btnInfo.hidden = false
//            
//            if self.constraintBtnInfoWidth != nil {
//                self.constraintBtnInfoWidth.constant = 33
//                self.constraintBtnInfoTrailing.constant = 15
//                self.layoutIfNeeded()
//            }

            self.txtField.text = pdictResponse[KTITLE] as? String
            
            if self.constraintImgViewWidth != nil{
                self.constraintImgViewWidth.constant = 0
                self.constraintTxtFieldLeading.constant = 0
                self.layoutIfNeeded()
            }
            
            if self.constraintSwitchWidth != nil{
                self.switchOnOff.hidden = true
                self.constraintSwitchWidth.constant = 0
                self.layoutIfNeeded()
            }
        }
    }
    
    override func prepareForReuse(){
        if self.imgView != nil{
            self.imgView.image = nil
        }
        self.textLabel?.text = ""
        self.txtField.placeholder = ""
        self.txtField.text = ""
        self.txtField.secureTextEntry = false
        self.txtField.keyboardType = .Default
        self.txtField.returnKeyType = .Done
        self.btnInfo.hidden = true
        self.btnInfo.setImage(nil, forState: .Normal)
        self.btnInfo.tintColor = COLORTHEME
        self.switchOnOff.on = true
    }
}
