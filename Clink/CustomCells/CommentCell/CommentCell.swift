//
//  CommentCell.swift
//  Clink
//
//  Created by Gunjan on 31/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblComment : UILabel!
    @IBOutlet var lblDateTime : UILabel!
    @IBOutlet var imgAmountBG : UIImageView!
    @IBOutlet var imgEmoLike : UIImageView!
    @IBOutlet var lblAmount : UILabel!
    
    @IBOutlet var constImgEmoLikeWidth : NSLayoutConstraint!
    @IBOutlet var constImgAmountLikeWidth : NSLayoutConstraint!
    
    @IBOutlet var lblNameInitialize : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configureCell(pdictResponse:ClsComment){
        
        lblUserName.text = pdictResponse.strUserName
        lblComment.text = pdictResponse.strComment
        let dateCreated = pdictResponse.strCreatedDate.getLocalDate(PHASE3FORMATE)
        lblDateTime.text = dateCreated.getFormatedString(PHASE3FORMATE)
        
        imgEmoLike.hidden = true
        imgAmountBG.hidden = true
        lblAmount.hidden = true
        constImgEmoLikeWidth.constant = CGFLOATZERO
        constImgAmountLikeWidth.constant = CGFLOATZERO
        
        self.configureInitials(pdictResponse)
        
        if pdictResponse.intEmojiType != INTZERO && pdictResponse.fltAmount == nil{
            imgEmoLike.hidden = false
            imgAmountBG.hidden = true
            imgEmoLike.image = UIImage(named: self.getImageName(pdictResponse.intEmojiType))
            lblAmount.hidden = true
            constImgEmoLikeWidth.constant = FLOATEMOJIWIDTH
            constImgAmountLikeWidth.constant = CGFLOATZERO
        }
        else if pdictResponse.intEmojiType == INTZERO && pdictResponse.fltAmount != nil && pdictResponse.fltAmount != FLOATZERO {
            imgEmoLike.hidden = true
            imgAmountBG.hidden = false
            lblAmount.hidden = false
            constImgEmoLikeWidth.constant = CGFLOATZERO
            constImgAmountLikeWidth.constant = FLOATAMOUNTWIDTH
            lblAmount.attributedText = (CommonAttributedString.getGiftFormattedString("$ \(pdictResponse.fltAmount)"))
        }
    }
    
    func configureEmoCell(pdictResponse:ClsComment){
        lblUserName.text = pdictResponse.strUserName
        lblDateTime.text = "\(pdictResponse.strCreatedDate)"
        imgEmoLike.image = UIImage(named: self.getImageName(pdictResponse.intEmojiType))
        self.configureInitials(pdictResponse)
    }
    
    func getImageName(intEmojiType : Int) -> String{
        var strImageName = STREMPTY
        switch intEmojiType{
        case EmoType.Rofl.rawValue:
            strImageName = IMGEMOROFL
            break
        case EmoType.Wow.rawValue:
            strImageName = IMGEMOWOW
            break
        case EmoType.Love.rawValue:
            strImageName = IMGEMOLOVE
            break
        case EmoType.Cheers.rawValue:
            strImageName = IMGEMOCHEERS
            break
        case EmoType.Claps.rawValue:
            strImageName = IMGEMOCLAP
            break
        default :
            strImageName = STREMPTY
        }
        return strImageName
    }
    
    func configureInitials(pdictResponse:ClsComment){
        if pdictResponse.strUserFirstName != "" && pdictResponse.strUserLastName != ""{
            if  ((pdictResponse.strUserFirstName.characters.first >= "a" && pdictResponse.strUserFirstName.characters.first <= "z" || pdictResponse.strUserFirstName.characters.first >= "A" && pdictResponse.strUserFirstName.characters.first <= "Z") && (pdictResponse.strUserLastName.characters.first >= "a" && pdictResponse.strUserLastName.characters.first <= "z" || pdictResponse.strUserLastName.characters.first >= "A" && pdictResponse.strUserLastName.characters.first <= "Z") ){
                lblNameInitialize.text = String(pdictResponse.strUserFirstName.characters.first!).uppercaseString + String(pdictResponse.strUserLastName.characters.first!).uppercaseString
            }
            else{
                lblNameInitialize.text = ""
                
            }
            
        }
        else if pdictResponse.strUserFirstName != "" && pdictResponse.strUserLastName == ""{
            if pdictResponse.strUserFirstName.characters.first >= "a" && pdictResponse.strUserFirstName.characters.first <= "z" || pdictResponse.strUserFirstName.characters.first >= "A" && pdictResponse.strUserFirstName.characters.first <= "Z"{
                lblNameInitialize.text = String(pdictResponse.strUserFirstName.characters.first!).uppercaseString
            }else{
                lblNameInitialize.text = ""
            }
            
        }
        else if pdictResponse.strUserFirstName == "" && pdictResponse.strUserLastName != ""{
            if pdictResponse.strUserLastName.characters.first >= "a" && pdictResponse.strUserLastName.characters.first <= "z" || pdictResponse.strUserLastName.characters.first >= "A" && pdictResponse.strUserLastName.characters.first <= "Z"{
                lblNameInitialize.text = String(pdictResponse.strUserLastName.characters.first!).uppercaseString
            }
            else{
                lblNameInitialize.text = ""
            }
            
        }
        else{
            lblNameInitialize.text = ""
        }
    }
}
