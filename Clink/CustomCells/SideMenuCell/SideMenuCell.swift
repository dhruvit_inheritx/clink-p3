//
//  SideMenuCell.swift
//  Clink
//
//  Created by Gunjan on 03/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var imgMenuIconConst: NSLayoutConstraint!
    @IBOutlet var imgMenuBG: UIImageView!
    @IBOutlet var imgMenuIcon: UIImageView!
    @IBOutlet var lblMenuTitle: UILabel!
    @IBOutlet var lblNotificationCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(pobjMenu:ClsMenu){
        
        lblMenuTitle.text = pobjMenu.strMenuTitle
        lblNotificationCount.hidden = (pobjMenu.intNotificationCount == INTZERO)
        lblNotificationCount.text = "\(pobjMenu.intNotificationCount)"
        lblMenuTitle.textColor = (pobjMenu.isSelected == true) ? kCellSelectionColor : kThemeDeselectionColor
        
        if pobjMenu.strImageNameSuffix != STREMPTY{            
        imgMenuIcon.image = UIImage(named:pobjMenu.strImageNameSuffix)

        }
    }
}
