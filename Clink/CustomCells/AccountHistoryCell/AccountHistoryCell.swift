//
//  AccountHistoryCell.swift
//  Clink
//
//  Created by Dhruvit on 05/12/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class AccountHistoryCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblStatus : UILabel!
    @IBOutlet var viewBack : UIView!
    @IBOutlet var lblStatusHeight : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCashOutAndAccountCell(objClinkDetails : ClsTransaction, enumAccountType : AccountHistoryControllerType) {
        
        var strToDisplay = ""
        
        if enumAccountType == AccountHistoryControllerType.CashOut {
            strToDisplay = "$\(objClinkDetails.strAmount.removeZerosAfterDecimal()) was transfered to Chase *\(objClinkDetails.strToAccountLast4)."
        }
        else {
            strToDisplay = "$\(objClinkDetails.strAmount.removeZerosAfterDecimal()) received from \(objClinkDetails.strSenderName)."
            
            if objClinkDetails.strSenderId != nil {
                
                if objClinkDetails.strSenderId == USERDEFAULTS.getUserDetailsObject().strId {
                    
                    if objClinkDetails.strReceiverId == USERDEFAULTS.getUserDetailsObject().strId {
                        strToDisplay = "$\(objClinkDetails.strAmount.removeZerosAfterDecimal()) was transfered to Chase *\(objClinkDetails.strToAccountLast4)."
                    }
                    else {
                        strToDisplay = "$\(objClinkDetails.strAmount) sent to \(objClinkDetails.strReceiverName)."
                    }
                }
                else {
                    strToDisplay = "$\(objClinkDetails.strAmount) received from \(objClinkDetails.strSenderName)."
                }
            }
        }
        
        lblTitle.text = strToDisplay
        lblTitle.numberOfLines = 0
        lblTitle.lineBreakMode = .ByWordWrapping
        //        lblTitle.sizeToFit()
        self.backgroundColor = UIColor.groupTableViewBackgroundColor()
        viewBack.layer.cornerRadius = 5
        
        lblStatusHeight.constant = 0
    }
    
    func configureCell(objClinkDetails : ClsClinkDetails, enumAccountType : AccountHistoryControllerType) {
    
        var strToDisplay = ""

        if enumAccountType == AccountHistoryControllerType.Donations {
            strToDisplay = "$\(objClinkDetails.strAmount.removeZerosAfterDecimal()) was donated to \(objClinkDetails.strReceiverName)."
        }
        else if enumAccountType == AccountHistoryControllerType.Redeemed {
            
            strToDisplay = "$\(objClinkDetails.strAmount.removeZerosAfterDecimal()) redeemed at merchant."
            
            if objClinkDetails.objMarchantDetails != nil  && objClinkDetails.objMarchantDetails != "" {
                if objClinkDetails.objMarchantDetails.strName != "" {
                    strToDisplay = "$ \(objClinkDetails.strAmount.removeZerosAfterDecimal()) redeemed at $\(objClinkDetails.strMerchantName)."
                }
            }
        }
        else if enumAccountType == AccountHistoryControllerType.Saved {
            if objClinkDetails.strMerchantName != nil {
                if objClinkDetails.strMerchantName != ""  {
                    strToDisplay = "\(objClinkDetails.strSenderName) sent you $\(objClinkDetails.strAmount) to \(objClinkDetails.strMerchantName)."
                }
                else{
                    strToDisplay = "\(objClinkDetails.strSenderName) sent you $\(objClinkDetails.strAmount) to merchant."
                }
            }
            else{
                strToDisplay = "\(objClinkDetails.strSenderName) sent you $\(objClinkDetails.strAmount)."
            }
        }
        else if enumAccountType == AccountHistoryControllerType.Sent {
            if objClinkDetails.strMerchantName != nil {
                if objClinkDetails.strMerchantName != "" {
                    strToDisplay = "Sent $\(objClinkDetails.strAmount) to \(objClinkDetails.strReceiverName) to \(objClinkDetails.strMerchantName)."
                }
                else {
                    strToDisplay = "Sent $\(objClinkDetails.strAmount) to \(objClinkDetails.strReceiverName) to merchant."
                }
            }
            else{
                strToDisplay = "Sent $\(objClinkDetails.strAmount) to \(objClinkDetails.strReceiverName)."
            }
            
            if objClinkDetails.isReceipientRead == true {
                lblStatus.text = "Received"
            }
            else {
                lblStatus.text = "Pending"
            }
        }
        else {
            strToDisplay = "$\(objClinkDetails.strAmount) received from \(objClinkDetails.strSenderName)."
        }

        lblTitle.text = strToDisplay
        lblTitle.numberOfLines = 0
        lblTitle.lineBreakMode = .ByWordWrapping
//        lblTitle.sizeToFit()
        self.backgroundColor = UIColor.groupTableViewBackgroundColor()
        viewBack.layer.cornerRadius = 5

        if enumAccountType == .Sent{
            lblStatusHeight.constant = 17
        }
        else{
            lblStatusHeight.constant = 0
        }
    }
    
    override func prepareForReuse() {
//        lblTitle.text = ""
        lblTitle.numberOfLines = 0
        lblTitle.lineBreakMode = .ByWordWrapping
//        lblTitle.sizeToFit()
        self.backgroundColor = UIColor.groupTableViewBackgroundColor()
        viewBack.layer.cornerRadius = 5
    }
    
}
