//
//  PaymentMethodCell.swift
//  Clink
//
//  Created by Dhruvit on 11/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class PaymentMethodCell: UITableViewCell {
    
    @IBOutlet var btnAdd : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(pdictResponse:[String : AnyObject!]){
        
        self.selectionStyle = .None
        
        var strTitle = pdictResponse.getValueIfAvilable(KTITLE) as? String
        self.btnAdd.setTitle(strTitle, forState: .Normal)
        
        if let objPaymentMethodDetails = pdictResponse.getValueIfAvilable(KPAYMENTMETHODDETAILS) as? ClsCreditCardDetails{
            
            if objPaymentMethodDetails.strLastFour != ""{
                if objPaymentMethodDetails.strAccountType == "bank" {
                    strTitle = "Account *" + objPaymentMethodDetails.strLastFour
                }
                else {
                    strTitle = "VISA CARD *" + objPaymentMethodDetails.strLastFour
                }
            }
            
            self.btnAdd.setTitle(strTitle, forState: .Normal)
            
            if objPaymentMethodDetails.isDefaultPayment == true{
                USERDEFAULTS.setDefaultPaymentDetailsObject(objPaymentMethodDetails)
            }
        }
    }
    
//    override func prepareForReuse(){
//        self.btnAdd.setTitle("", forState: .Normal)
//        self.btnAdd.backgroundColor = UIColor.clearColor()
//    }
}
