//
//  VenueCell.swift
//  Clink
//
//  Created by Gunjan on 08/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import EDStarRating

let MAXRATING : Int = 5
let DISPLAYMODE : UInt = 1
let FZERO : Float = 0.0
class VenueCell: UITableViewCell {
    
    @IBOutlet var lblVenueName : UILabel!
    @IBOutlet var lblVenueAddress : UILabel!
    @IBOutlet var lblVenueDetails : UILabel!
    @IBOutlet var imgVenue : UIImageView!
    @IBOutlet var btnShowDetails : UIButton!
    @IBOutlet var lblVenueURL : UILabel!
    @IBOutlet var lblVenuePhone : UILabel!
    @IBOutlet var icnURLWidthLayoutConst : NSLayoutConstraint!
    @IBOutlet var ratingView : EDStarRating!
    @IBOutlet var ratingViewHeightLayoutConst : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configureCell(pdictResponse:ClsVenue ){
        
        lblVenueName.text = pdictResponse.strName
        
        if pdictResponse.arrLocation.count > 0{
            lblVenueAddress.text = pdictResponse.arrLocation[0].strAddressLine1 + pdictResponse.arrLocation[0].strAddressLine1
            lblVenuePhone.text = pdictResponse.arrLocation[0].strPhone
        }
        ratingView.hidden = false
        if pdictResponse.arrCategoryName[0] == KCATEGORYCHARITIES
        {
            ratingView.hidden = false
            lblVenueURL.text = pdictResponse.strURL
            ratingView.hidden = true
        }
        else
        {
            lblVenueURL.text = pdictResponse.strURL
            
            let paragraphStyle = NSMutableParagraphStyle()
            let titleText1 = String()
            paragraphStyle.lineSpacing = LINESPACING
            let attrString = NSMutableAttributedString(string: titleText1)
            attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(INTZERO, attrString.length))
            lblVenueURL.attributedText = attrString
            
            if pdictResponse.strURL == nil{
                lblVenueURL.text = pdictResponse.strDescription
                
                if icnURLWidthLayoutConst != nil {
                    icnURLWidthLayoutConst.constant = 0
                }
                
            }
            else{
                lblVenueURL.text = pdictResponse.strURL
                
                if icnURLWidthLayoutConst != nil {
                    icnURLWidthLayoutConst.constant = 16
                }
            }
            
            lblVenueURL.text = pdictResponse.strDescription
            lblVenueDetails.text = pdictResponse.strDescription
            lblVenueURL.textAlignment = NSTextAlignment.Left
            
            ratingView.starImage = UIImage(named: IMGSTAR)
            ratingView.starHighlightedImage = UIImage(named: IMGSTARSELECTED)
            ratingView.maxRating = MAXRATING;
            ratingView.editable = false;
            ratingView.displayMode = DISPLAYMODE;
            
            if pdictResponse.floatRating != nil{
                ratingView.rating = pdictResponse.floatRating
                ratingView.hidden = false
                ratingView.horizontalMargin = CGFLOATZERO
            }
            else{
                ratingView.hidden = true
            }
        }
        
        lblVenueDetails.text = pdictResponse.strDescription
        
        let paragraphStyle1 = NSMutableParagraphStyle()
        var titleText : String = String()
        titleText = pdictResponse.arrLocation[0].strAddressLine1 + "\n" + pdictResponse.arrLocation[0].strCity + ", " + pdictResponse.arrLocation[0].strState + " " + pdictResponse.arrLocation[0].strZip
        paragraphStyle1.lineSpacing = LINESPACINGTWO
        let attrString1 = NSMutableAttributedString(string: titleText)
        attrString1.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle1, range:NSMakeRange(INTZERO, attrString1.length))
        lblVenueAddress.attributedText = attrString1
        lblVenueAddress.textAlignment = NSTextAlignment.Left
        
        lblVenuePhone.text = pdictResponse.arrLocation[0].strPhone
        
        imgVenue.sd_setImageWithURL(NSURL(string: pdictResponse.strImgThumbnail), placeholderImage: UIImage(named: IMGPLACEHOLDER))
        
    }
    
    //    override func prepareForReuse(){
    //        self.imgVenue.file?.cancel()
    //        self.imgVenue.image = nil
    //        
    //    }
}
