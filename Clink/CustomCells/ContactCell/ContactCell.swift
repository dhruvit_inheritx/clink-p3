//
//  ContactCell.swift
//  Clink
//
//  Created by Gunjan on 08/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblPhone: UILabel!
    @IBOutlet var btnSelection : UIButton!
    @IBOutlet var lblNameInitialize: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(pobjContacts:ClsContact){
        
        if pobjContacts.strFirstName != "" && pobjContacts.strLastName != ""{
            if  ((pobjContacts.strFirstName.characters.first >= "a" && pobjContacts.strFirstName.characters.first <= "z" || pobjContacts.strFirstName.characters.first >= "A" && pobjContacts.strFirstName.characters.first <= "Z") && (pobjContacts.strLastName.characters.first >= "a" && pobjContacts.strLastName.characters.first <= "z" || pobjContacts.strLastName.characters.first >= "A" && pobjContacts.strLastName.characters.first <= "Z") ){
                lblNameInitialize.text = String(pobjContacts.strFirstName.characters.first!).uppercaseString + String(pobjContacts.strLastName.characters.first!).uppercaseString
            }
            else{
                lblNameInitialize.text = ""
                
            }
            
        }
        else if pobjContacts.strFirstName != "" && pobjContacts.strLastName == ""{
            if pobjContacts.strFirstName.characters.first >= "a" && pobjContacts.strFirstName.characters.first <= "z" || pobjContacts.strFirstName.characters.first >= "A" && pobjContacts.strFirstName.characters.first <= "Z"{
                lblNameInitialize.text = String(pobjContacts.strFirstName.characters.first!).uppercaseString
            }else{
                lblNameInitialize.text = ""
            }
            
        }
        else if pobjContacts.strFirstName == "" && pobjContacts.strLastName != ""{
            if pobjContacts.strLastName.characters.first >= "a" && pobjContacts.strLastName.characters.first <= "z" || pobjContacts.strLastName.characters.first >= "A" && pobjContacts.strLastName.characters.first <= "Z"{
                lblNameInitialize.text = String(pobjContacts.strLastName.characters.first!).uppercaseString
            }
            else{
                lblNameInitialize.text = ""
            }
            
        }
        else{
            lblNameInitialize.text = ""
        }
        
        lblUserName.text = "\(pobjContacts.strFirstName) \(pobjContacts.strLastName)"
        
        if let strPhone = pobjContacts.strPhone{
            lblPhone.text = strPhone
        }
        else{
            lblPhone.text = " "//--
        }
        if let strEmail = pobjContacts.strEmail{
            lblEmail.text = strEmail
        }
        else{
            lblEmail.text = " "//--
        }
                
    }
    
    //MARK:- Returns Different Colour for background of profile image
    func pickColor(alphabet: Character) -> UIColor {
        let alphabetColors = [0x5A8770, 0xB2B7BB, 0x6FA9AB, 0xF5AF29, 0x0088B9, 0xF18636, 0xD93A37, 0xA6B12E, 0x5C9BBC, 0xF5888D, 0x9A89B5, 0x407887, 0x9A89B5, 0x5A8770, 0xD33F33, 0xA2B01F, 0xF0B126, 0x0087BF, 0xF18636, 0x0087BF, 0xB2B7BB, 0x72ACAE, 0x9C8AB4, 0x5A8770, 0xEEB424, 0x407887]
        let str = String(alphabet).unicodeScalars
        let unicode = Int(str[str.startIndex].value)
        if 65...90 ~= unicode {
            let hex = alphabetColors[unicode - 65]
            return UIColor(red: CGFloat(Double((hex >> 16) & 0xFF)) / 255.0, green: CGFloat(Double((hex >> 8) & 0xFF)) / 255.0, blue: CGFloat(Double((hex >> 0) & 0xFF)) / 255.0, alpha: 1.0)
        }
        return UIColor.blackColor()
    }

    
}
