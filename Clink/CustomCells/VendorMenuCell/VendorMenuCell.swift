//
//  VendorMenuCell.swift
//  Clink
//
//  Created by Gunjan on 05/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class VendorMenuCell: UITableViewCell {
    
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDescription : UILabel!
    @IBOutlet var lblAmount : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func configureCell(pdictResponse:ClsVendorMenu){
        
        lblTitle.text = pdictResponse.strTitle
        lblDescription.text = pdictResponse.strDescription
        lblAmount.text = "$ " + pdictResponse.strAmount
    }
}
