//
//  ClinkDetailCell.swift
//  Clink
//
//  Created by Gunjan on 04/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClinkListCell: UITableViewCell , UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imgClink: UIImageView!
    @IBOutlet var lblToUserName: UILabel!
    @IBOutlet var lblFromUserName: UILabel!
    @IBOutlet var thankyouView: UIView!
    
    @IBOutlet weak var btnSayThanksWidthConstraint: NSLayoutConstraint!
    //    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var imgisRead : UIImageView!
    @IBOutlet var btnShowDetails : UIButton!
    @IBOutlet var btnSayThankyou : UIButton!
    @IBOutlet var imgThankyou : UIImageView!
    
    @IBOutlet var badgeLike : UILabel!
    @IBOutlet var badgeComment : UILabel!
    
    @IBOutlet var btnLike : UIButton!
    @IBOutlet var btnComment : UIButton!
    @IBOutlet var btnGift : UIButton!
    @IBOutlet var lblGiftAmount : UILabel!
    @IBOutlet var collectionViewEmailCC: UICollectionView!
    
    @IBOutlet var scrolToview: UIScrollView!
    @IBOutlet var scrolFromview: UIScrollView!
    @IBOutlet var imgNewMsg : UIImageView!
    
    var objCurrentClink:ClsClinkDetails!
    
    var intCurrentPage : Int = 1
    var intLastPage : Int! = 0
    
    var arrEmailCC = [ClsContact]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(pobjClinkList:ClsClinkDetails , pclinkType : ClinkType){
        
        self.activityIndicator.startAnimating()
        if let strImgUrl = pobjClinkList.strImageUrl {
            imgClink.sd_setImageWithURL(NSURL(string: strImgUrl), completed: { (image, error, SDImageCacheType, url) -> Void in
                
                if (image != nil){
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        
        objCurrentClink = pobjClinkList
        
        badgeComment.layer.cornerRadius = BADGECORNERRADIOUS
        badgeLike.layer.cornerRadius = BADGECORNERRADIOUS
        
        lblToUserName.text = pobjClinkList.strReceiverName
        badgeComment.layer.cornerRadius = BADGECORNERRADIOUS
        badgeLike.layer.cornerRadius = BADGECORNERRADIOUS
        lblFromUserName.text = pobjClinkList.strSenderName
        
        badgeLike.text = "\(pobjClinkList.intLikeCount)"
        badgeComment.text = "\(pobjClinkList.intCommentCount)"
        
        scrolFromview.contentSize = CGSizeMake(lblFromUserName.intrinsicContentSize().width, LBLTOFROMHEIGHT)
        scrolToview.contentSize = CGSizeMake(lblToUserName.intrinsicContentSize().width, LBLTOFROMHEIGHT)
        
        
        let dateCreated = pobjClinkList.strCreatedDate.getLocalDate(PHASE3FORMATE)
//        print("Date Created :- \(dateCreated)")
        lblDate.text = Helper.getDateDifferenceforDate(dateCreated, andDate: NSDate())
//        imgNewMsg.hidden = pobjClinkList.hasNewMessage
        imgisRead.hidden = pobjClinkList.isRead
        if(pclinkType != ClinkType.Thankyou)
        {
            if (pobjClinkList.strOriginalClinkId?.isEmptyString() == false && pobjClinkList.objReplyClink == nil)
            {
                // Download only if its not available
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(pobjClinkList.strOriginalClinkId,nil).URLRequest, isBackgroundCall:true, pAlertView: self, pViewController: nil, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        pobjClinkList.objReplyClink = ClsClinkDetails(pdictResponse: response as! [String : AnyObject])
                        if let strImgUrl = pobjClinkList.objReplyClink.strImageUrl {
                            self.imgThankyou.sd_setImageWithURL(NSURL(string: strImgUrl), placeholderImage: nil)
                            self.imgThankyou.hidden = false
                        }
                        else{
                            self.imgThankyou.image = nil
                            self.imgThankyou.hidden = true
                        }
                        self.btnSayThankyou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
                    }
                    else{
                        if pclinkType == ClinkType.Sent{
                            self.btnSayThankyou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                        }
                        else{
                            self.btnSayThankyou.setImage(UIImage(named: IMGBTNTY), forState: .Normal)
                        }
                        
                        self.imgThankyou.image = nil
                        self.imgThankyou.hidden = true
                    }
                })
            }
            else if pobjClinkList.objReplyClink != nil{
                if let strImgUrl = pobjClinkList.objReplyClink.strImageUrl {
                    self.imgThankyou.sd_setImageWithURL(NSURL(string: strImgUrl), placeholderImage: nil)
                }
                else{
                    self.imgThankyou.image = nil
                    self.imgThankyou.hidden = true
                }
                self.imgThankyou.hidden = false
                self.btnSayThankyou.userInteractionEnabled = true
                btnSayThankyou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
            }
            else{
                
                if pclinkType == ClinkType.Sent{
                     self.btnSayThankyou.userInteractionEnabled = false
                    self.btnSayThankyou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                }
                else if pobjClinkList.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId
                {
                    self.btnSayThankyou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                    self.btnSayThankyou.userInteractionEnabled = false
                }
                else{
                    self.btnSayThankyou.userInteractionEnabled = true
                    self.btnSayThankyou.setImage(UIImage(named: IMGBTNTY), forState: .Normal)
                }
                
                self.imgThankyou.image = nil
                self.imgThankyou.hidden = true
            }
            
            if pclinkType == ClinkType.Sent{
//                imgisRead.hidden = !pobjClinkList.hasNewActivity
            }
        }
        
        thankyouView.backgroundColor = kThemeColor     
        
        
//        if pobjClinkList.strReceiverId != USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) && pobjClinkList.strReceiverId != KDEFAULTUSERID
//        {
            
//            if pobjClinkList.strRefrenceClinkId == KNOTAVIALABLE
//            {
//                btnSayThankyou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
//                btnSayThankyou.hidden = false
//                btnSayThankyou.userInteractionEnabled = false
//            }
//            else{
//                btnSayThankyou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
//            }
            
//        }
        if var intGiftAmount = Int(pobjClinkList.strGiftPlusAmount.removeSpecialChars("")){
            if let intTotalAmount = Int(pobjClinkList.strAmount.removeSpecialChars("")){
                intGiftAmount += intTotalAmount
            }
            let strTotalAmount = "\(intGiftAmount)"
//            strTotalAmount.removeLastCharIfExist("0")
//            strTotalAmount.removeLastCharIfExist(".")
            if intGiftAmount <= 0{
                lblGiftAmount.attributedText = NSAttributedString(string: "")
            }
            else{
                lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strTotalAmount)")
            }
            
        }
        else if let intTotalAmount = Int(pobjClinkList.strAmount.removeSpecialChars(".")){
            //            let fltTotalAmount = Float(pobjClinkList.strAmount.removeSpecialChars("."))!
            let strAmount = "\(intTotalAmount)"
//            strAmount.removeLastCharIfExist("0")
//            strAmount.removeLastCharIfExist(".")
            if intTotalAmount <= 0{
                lblGiftAmount.attributedText = NSAttributedString(string: "")
            }
            else{
                lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strAmount)")
            }
        }
        if let isLiked = pobjClinkList.isLiked{
            btnLike.selected = isLiked
        }
        collectionViewEmailCC.delegate = self
        collectionViewEmailCC.dataSource = self
        arrEmailCC = pobjClinkList.arrCCUser
        self.collectionViewEmailCC.reloadData()
    }
    
    func configureFeedCell(pobjClinkList:ClsClinkDetails , pclinkType : ClinkType){
        
        self.activityIndicator.startAnimating()
        if let strImgUrl = pobjClinkList.strImageUrl {
            imgClink.sd_setImageWithURL(NSURL(string: strImgUrl), completed: { (image, error, SDImageCacheType, url) -> Void in
                
                if (image != nil){
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        
//        imgNewMsg.hidden = pobjClinkList.hasNewMessage
        lblToUserName.text = pobjClinkList.strReceiverName
        badgeComment.layer.cornerRadius = BADGECORNERRADIOUS
        badgeLike.layer.cornerRadius = BADGECORNERRADIOUS
        lblFromUserName.text = pobjClinkList.strSenderName
        
        scrolFromview.contentSize = CGSizeMake(lblFromUserName.intrinsicContentSize().width, LBLTOFROMHEIGHT)
        scrolToview.contentSize = CGSizeMake(lblToUserName.intrinsicContentSize().width, LBLTOFROMHEIGHT)
        
        
        badgeLike.text = "\(pobjClinkList.intLikeCount)"
        badgeComment.text = "\(pobjClinkList.intCommentCount)"
        
        let dateCreated = pobjClinkList.strCreatedDate.getLocalDate(PHASE3FORMATE)
//        print("Date Created :- \(dateCreated)")
        lblDate.text = Helper.getDateDifferenceforDate(dateCreated, andDate: NSDate())
        
        if var intGiftAmount = Int(pobjClinkList.strGiftPlusAmount.removeSpecialChars("")){
            if let intTotalAmount = Int(pobjClinkList.strAmount.removeSpecialChars("")){
                intGiftAmount += intTotalAmount
            }
            let strTotalAmount = "\(intGiftAmount)"
//            strTotalAmount.removeLastCharIfExist("0")
//            strTotalAmount.removeLastCharIfExist(".")
            if intGiftAmount <= 0{
                lblGiftAmount.attributedText = NSAttributedString(string: "")
            }
            else{
                lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strTotalAmount)")
            }
        }
        else if let intTotalAmount = Float(pobjClinkList.strAmount.removeSpecialChars("")){
//            let fltTotalAmount = Float(pobjClinkList.strAmount.removeSpecialChars("."))!
            let strAmount = "\(intTotalAmount)"
//            strAmount.removeLastCharIfExist("0")
//            strAmount.removeLastCharIfExist(".")
            if intTotalAmount <= 0{
                lblGiftAmount.attributedText = NSAttributedString(string: "")
            }
            else{
            lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strAmount)")
            }
        }
        
        if let isLiked = pobjClinkList.isLiked{
            btnLike.selected = isLiked
        }
        collectionViewEmailCC.delegate = self
        collectionViewEmailCC.dataSource = self
        
        arrEmailCC = pobjClinkList.arrCCUser
        self.collectionViewEmailCC.reloadData()
        
    }
    
    // MARK: - CollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrEmailCC.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cellIdentifier:String = CELLEMAILCC;
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier,forIndexPath:indexPath) as! EmailCCCell
        if indexPath.row < arrEmailCC.count - 1{
            cell.lblEmailCCName.text = "\((arrEmailCC[indexPath.row].strFullName)!),"
        }
        else{
            cell.lblEmailCCName.text = arrEmailCC[indexPath.row].strFullName
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
        let str:String = "\((arrEmailCC[indexPath.row].strFullName)!),"
        let calCulateSizze: CGSize = str.sizeWithAttributes([NSFontAttributeName: UIFont (name: FONTGOTHAMMEDIUM, size: FONTSIZE14)!])
        return CGSizeMake(calCulateSizze.width+1,COLLECTIONHEIGHT)
    }
    
    override func prepareForReuse(){
        self.imgClink.sd_cancelCurrentImageLoad()
        self.imgClink.image = nil
        if self.imgThankyou != nil{
            self.imgThankyou.sd_cancelCurrentImageLoad()
            self.imgThankyou.image = nil
            self.imgThankyou.hidden = true
            self.imgThankyou.image = UIImage(named: NOIMAGE)
        }
        
    }
}

class ClinkGridCell: UICollectionViewCell {
    @IBOutlet var imgVwClink: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnDetails: UIButton!
}
