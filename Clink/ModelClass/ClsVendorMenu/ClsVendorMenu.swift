//
//  ClsVendorMenu.swift
//  Clink
//
//  Created by Gunjan on 05/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

/**
 This Class was implemented to store Vendor Menu Details.
 
 like:
 
 * id
 * title
 * description
 * amount
 
 etc.
 
 */
class ClsVendorMenu: NSObject {
    var strTitle:String = EMPTYSTRING
    var strDescription:String = EMPTYSTRING
    var strAmount:String = EMPTYSTRING
    var strVenueId:String = EMPTYSTRING
    
    init(pobjVendorMenu:[String : AnyObject]) {
        
        strTitle = pobjVendorMenu[KTITLE] as! String
        strDescription = pobjVendorMenu[KDESCRIPTION] as! String
        strAmount = pobjVendorMenu[KAMOUNT] as! String
        strVenueId = pobjVendorMenu[KVENUEID] as! String
    }
}
