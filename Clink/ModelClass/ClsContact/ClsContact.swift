//
//  ClsContact.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright (c) 2015 Gunjan. All rights reserved.
//

import Foundation
import UIKit

class ClsContact :NSObject{
    
    var strId: String! = EMPTYSTRING
    var strFirstName: String! = EMPTYSTRING
    var strLastName: String! = EMPTYSTRING
    var strEmail: String! = EMPTYSTRING
    var strPhone: String! = EMPTYSTRING
    var isClinkUser: Bool! = false
    var strUserid: String! = EMPTYSTRING
    var isSelected: Bool! = false
    var strFullName: String! = EMPTYSTRING
    var isUnRemovable: Bool! = false
    
    override init(){
        
    }
    // Class Constructor
    init(pClinkId : String, pfirstName: String, plastName: String, pemail: String, pphone: String,pisClinkUser: Bool,pisSelected: Bool,puserid: String!,isUnRemovable : Bool) {
        self.strId = pClinkId
        self.strFirstName = pfirstName
        self.strLastName = plastName
        self.strEmail = pemail
        self.strPhone = pphone
        self.isClinkUser = pisClinkUser
        self.strUserid = puserid
        self.isSelected = pisSelected
        self.strFullName = pfirstName + plastName
        self.isUnRemovable = isUnRemovable
    }
    
    init(pdicResponse : [String : AnyObject]) {
        self.strId = pdicResponse.getValueIfAvilable(KUUID) as? String
        self.strFirstName = pdicResponse.getValueIfAvilable(KFIRSTNAME) as? String
        self.strLastName = pdicResponse.getValueIfAvilable(KLASTNAME) as? String
        self.strEmail = pdicResponse.getValueIfAvilable(KEMAIL) as? String
        self.strPhone = pdicResponse.getValueIfAvilable(KPHONE) as? String
        
        if let clinkUserId = pdicResponse.getValueIfAvilable(KUSERUUID) as? String{
            self.isClinkUser = !clinkUserId.isEmptyString()
            self.strUserid = clinkUserId
        }
        
        if let selected = pdicResponse.getValueIfAvilable(KISSELCTED)?.boolValue{
            self.isSelected = selected
        }
        else{
            self.isSelected = false
        }
        self.isUnRemovable = false
        self.strFullName = self.strFirstName + " " + self.strLastName
    }
    
    required init(coder aDecoder: NSCoder)
    {
            self.strId = aDecoder.decodeObjectForKey(KUUID) as? String
            self.strFirstName = aDecoder.decodeObjectForKey(KFIRSTNAME) as? String
            self.strLastName = aDecoder.decodeObjectForKey(KLASTNAME) as? String
            self.strEmail = aDecoder.decodeObjectForKey(KEMAIL) as? String
            self.strPhone = aDecoder.decodeObjectForKey(KPHONE) as? String
            
            if let clinkUserId = aDecoder.decodeObjectForKey(KUSERUUID) as? String{
                self.isClinkUser = !clinkUserId.isEmptyString()
                self.strUserid = clinkUserId
            }
            
            if let selected = aDecoder.decodeObjectForKey(KISSELCTED)?.boolValue{
                self.isSelected = selected
            }
            else{
                self.isSelected = false
            }
            self.isUnRemovable = false
            self.strFullName = self.strFirstName + " " + self.strLastName
        
    }
    
    func encodeWithCoder(_aCoder: NSCoder) {
        
        _aCoder.encodeObject(self.strId , forKey: KUUID)
        _aCoder.encodeObject(self.strFirstName , forKey: KFIRSTNAME)
        _aCoder.encodeObject(self.strLastName , forKey: KLASTNAME)
        _aCoder.encodeObject(self.strEmail , forKey: KEMAIL)
        _aCoder.encodeObject(self.strPhone , forKey: KPHONE)
//        _aCoder.encodeObject(self.isClinkUser , forKey: KLASTACTIVITY)
        _aCoder.encodeObject(self.strUserid , forKey: KUSERUUID)
        _aCoder.encodeObject(self.isSelected , forKey: KISSELCTED)
        _aCoder.encodeObject(self.isUnRemovable , forKey: KISUNREMOVABLE)
    }
}
