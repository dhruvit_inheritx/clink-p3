//
//  ClsContactGroup.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright (c) 2015 Gunjan. All rights reserved.
//

import Foundation
import UIKit

/**
 This Class was implemented to store and differentiate user's contact details
 
 like:
 
 * Non Clink Users
 * Clink Users
 
 etc.
 
 */

class ClsContactGroup :NSObject{
    
//    var id: String!
    var arrOtherContacts:[ClsContact]!
    var arrClinkContacts:[ClsContact]!
    
}
