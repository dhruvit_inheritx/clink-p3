//
//  ClsComment.swift
//  Clink
//
//  Created by Gunjan on 31/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

/**
 This Class was implemented to store details of comments of Clink.
 
 like: 
 
 * id
 * comment
 * commenters id
 * commenter name
 * clink id
 
 etc.
 */

class ClsComment: NSObject {
    
    var strId : String! = EMPTYSTRING
    var strClinkId : String! = EMPTYSTRING
    var strComment : String! = EMPTYSTRING
    var strLikeCount : String! = EMPTYSTRING
    var strUserId : String! = EMPTYSTRING
    var strUserName : String! = EMPTYSTRING
    var strUserFirstName : String! = EMPTYSTRING
    var strUserLastName : String! = EMPTYSTRING
    
    var strCreatedDate : String! = EMPTYSTRING
    var strUpdatedDate : String! = EMPTYSTRING

    var intEmojiType : Int! = 0
    var fltAmount : Float!
    
    init(pdictResponse:[String : AnyObject]) {
        
        strId = pdictResponse.getValueIfAvilable(KUUID) as? String
        strClinkId = pdictResponse.getValueIfAvilable(KUUID) as? String
        strComment = pdictResponse.getValueIfAvilable(KCOMMENT) as? String
        
        let userDetails = pdictResponse.getValueIfAvilable(KUSER) as! [String : AnyObject]
        strUserId = userDetails.getValueIfAvilable(KUUID) as? String
        strUserFirstName = userDetails.getValueIfAvilable(KFIRSTNAME) as? String
        strUserLastName = userDetails.getValueIfAvilable(KLASTNAME) as? String
        strUserName = strUserFirstName + " " + strUserLastName
        
        
        intEmojiType = pdictResponse.getValueIfAvilable(KEMOJITYPE) as? Int
        fltAmount = pdictResponse.getValueIfAvilable(KGIFTPLUSAMOUNT) as? Float
        
        
        if let pUpdatedDate = pdictResponse.getValueIfAvilable(KUPDATEDAT) {
            strUpdatedDate = pUpdatedDate as! String
        }
        
        guard let pCreatedDate = pdictResponse.getValueIfAvilable(KCREATEDAT) else {
            return
        }
        strCreatedDate = pCreatedDate as! String
    }
}