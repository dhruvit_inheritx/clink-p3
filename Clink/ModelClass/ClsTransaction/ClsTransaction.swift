//
//  ClsTransaction.swift
//  Clink
//
//  Created by Dhruvit on 15/12/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClsTransaction: NSObject {
    
    var strId : String! = EMPTYSTRING
    var strAmount : String! = EMPTYSTRING
    var strClinkId : String! = EMPTYSTRING
    var strData : String! = EMPTYSTRING
    var strDescription : String! = EMPTYSTRING
    var strType : String! = EMPTYSTRING
    var strStatus : String! = EMPTYSTRING
    var strReferenceNumber : String! = EMPTYSTRING
    var strOperationType : String! = EMPTYSTRING
    var dictFromUser : [String : AnyObject]!
    var dictToUser : [String : AnyObject]!
    var dictFromAccount : [String : AnyObject]!
    var dictToAccount : [String : AnyObject]!
    var strCreatedAt : String! = EMPTYSTRING
    var strUpdatedAt : String! = EMPTYSTRING
    
    var strSenderId : String! = EMPTYSTRING
    var strSenderFirstName : String! = EMPTYSTRING
    var strSenderLastName : String! = EMPTYSTRING
    var strSenderName : String! = EMPTYSTRING
    
    var strReceiverId : String! = EMPTYSTRING
    var strReceiverFirstName : String! = EMPTYSTRING
    var strReceiverLastName : String! = EMPTYSTRING
    var strReceiverName : String! = EMPTYSTRING

    var strFromAccountId : String! = EMPTYSTRING
    var strFromAccountType : String! = EMPTYSTRING
    var strFromAccountLast4 : String! = EMPTYSTRING

    var strToAccountId : String! = EMPTYSTRING
    var strToAccountType : String! = EMPTYSTRING
    var strToAccountLast4 : String! = EMPTYSTRING

    override init(){
        
        strId = ""
        strAmount = ""
        strClinkId = ""
        strData = ""
        strDescription = ""
        strType = ""
        strStatus = ""
        strReferenceNumber = ""
        strOperationType = ""
        dictFromUser = Dictionary()
        dictToUser = Dictionary()
        dictFromAccount = Dictionary()
        dictToAccount = Dictionary()
        strCreatedAt = ""
        strUpdatedAt = ""

        strSenderId = ""
        strSenderFirstName = ""
        strSenderLastName = ""
        strSenderName = ""
        
        strReceiverId = ""
        strReceiverFirstName = ""
        strReceiverLastName = ""
        strReceiverName = ""
        
        strFromAccountId = ""
        strFromAccountType = ""
        strFromAccountLast4 = ""
        
        strToAccountId = ""
        strToAccountType = ""
        strToAccountLast4 = ""
    }
    
    init(pdictResponse : [String : AnyObject])
    {
        self.strId = pdictResponse.getValueIfAvilable(KUUID) as? String
        self.strAmount = pdictResponse.getValueIfAvilable(KAMOUNT) as? String
        self.strClinkId = pdictResponse.getValueIfAvilable("clink_uuid") as? String
        self.strData = pdictResponse.getValueIfAvilable("data") as? String
        self.strDescription = pdictResponse.getValueIfAvilable("description") as? String
       
        self.strType = pdictResponse.getValueIfAvilable("type") as? String
        self.strStatus = pdictResponse.getValueIfAvilable("status") as? String
        self.strReferenceNumber = pdictResponse.getValueIfAvilable("reference_number") as? String
        self.strOperationType = pdictResponse.getValueIfAvilable("operation_type") as? String
        
        var dictFrom : [String : AnyObject]! = Dictionary()
        dictFrom = pdictResponse.getValueIfAvilable("from") as? [String : AnyObject]
        dictFromUser = dictFrom.getValueIfAvilable("user") as? [String : AnyObject]
        strSenderId = dictFromUser.getValueIfAvilable(KSENDERID) as? String
        strSenderFirstName = dictFromUser.getValueIfAvilable(KSENDERFIRSTNAME) as? String
        strSenderLastName = dictFromUser.getValueIfAvilable(KSENDERLASTNAME) as? String
        strSenderName = strSenderFirstName + " " + strSenderLastName
        
        dictFromAccount = dictFrom.getValueIfAvilable(KACCOUNT) as? [String : AnyObject]
        strFromAccountId = dictFromAccount.getValueIfAvilable(KUUID) as? String
        strFromAccountType = dictFromAccount.getValueIfAvilable("type") as? String
        strFromAccountLast4 = dictFromAccount.getValueIfAvilable("last4") as? String

        var dictTo : [String : AnyObject]! = Dictionary()
        dictTo = pdictResponse.getValueIfAvilable("to") as? [String : AnyObject]
        dictToUser = dictTo.getValueIfAvilable("user") as? [String : AnyObject]
        strReceiverId = dictToUser.getValueIfAvilable(KRECEIVERID) as! String
        strReceiverFirstName = dictToUser.getValueIfAvilable(KRECEIVERFIRSTNAME) as? String
        strReceiverLastName = dictToUser.getValueIfAvilable(KRECEIVERLASTNAME) as? String
        strReceiverName = strReceiverFirstName + " " + strReceiverLastName
        
        dictToAccount = dictTo.getValueIfAvilable(KACCOUNT) as? [String : AnyObject]
        strToAccountId = dictToAccount.getValueIfAvilable(KUUID) as? String
        strToAccountType = dictToAccount.getValueIfAvilable("type") as? String
        strToAccountLast4 = dictToAccount.getValueIfAvilable("last4") as? String

        self.strCreatedAt = pdictResponse.getValueIfAvilable(KCREATEDTIME) as? String
        self.strUpdatedAt = pdictResponse.getValueIfAvilable(KUPDATEDTIME) as? String
    }
    
}
