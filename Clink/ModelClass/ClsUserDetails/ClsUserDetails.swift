//
//  ClsUserDetails.swift
//  Clink
//
//  Created by Gunjan on 19/09/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation


/**
 This Class was implemented to store User Details of Clink Users.
 
 like:
 
 * id
 * first name
 * last name
 * email
 * phone number
 
 etc.

 */

class ClsUserDetails: NSObject, NSCoding  {
    
    var strId : String! = EMPTYSTRING
    var strClinkAccountId : String! = EMPTYSTRING
    var strFirstName : String! = EMPTYSTRING
    var strLastName : String! = EMPTYSTRING
    var strEmail : String! = EMPTYSTRING
    var strPhone : String! = EMPTYSTRING
    var strDOB : String! = EMPTYSTRING
    var isPasswordResetRequired : Bool! = false
    var isActive : Bool! = false
    var isVerified : Bool! = false
    var strLastLoginAt : String! = EMPTYSTRING
    var strBalance : String! = EMPTYSTRING
    var intLikeCount : Int! = 0
    var intCommentCount : Int! = 0
    var arrRoles : [String] = Array()
    var arrAssociates : [String] = Array()
    var dicSession : ClsSessionDetails!
    var strCreatedAt : String! = EMPTYSTRING
    var strUpdatedAt : String! = EMPTYSTRING
    var intSentCount : Int! = 0
    var intReceivedCount : Int! = 0
    var intLoginCount : Int! = -1
    
    override init(){
//        init empty values
    }
    
    init(pdicResponse : [String : AnyObject])
    {
        self.strId = pdicResponse.getValueIfAvilable(KUUID) as? String
        self.strClinkAccountId = pdicResponse.getValueIfAvilable(KCLINKACCOUNTUUID) as? String
        self.strFirstName = pdicResponse.getValueIfAvilable(KFIRSTNAME) as? String
        self.strLastName = pdicResponse.getValueIfAvilable(KLASTNAME) as? String
        self.strEmail = pdicResponse.getValueIfAvilable(KEMAIL) as? String
        self.strPhone = pdicResponse.getValueIfAvilable(KPHONE) as? String
        self.strDOB = pdicResponse.getValueIfAvilable(KDOB) as? String
        self.isPasswordResetRequired = pdicResponse.getValueIfAvilable(KISPSEDRESETREQUIRED)?.boolValue
        self.isActive = pdicResponse.getValueIfAvilable(KISACTIVE)?.boolValue
        self.isVerified = pdicResponse.getValueIfAvilable(KISVERIFIED)?.boolValue
        self.strLastLoginAt = pdicResponse.getValueIfAvilable(KLASTLOGINTIME) as? String
        self.strBalance = pdicResponse.getValueIfAvilable(KBALANCE) as? String
        self.intLikeCount = pdicResponse.getValueIfAvilable(KLIKECOUNT)?.integerValue
        self.intCommentCount = pdicResponse.getValueIfAvilable(KCOMMENTCOUNT)?.integerValue
        self.arrRoles = (pdicResponse[KROLES] as? [String])!
//        self.arrAssociates = (pdicResponse[KASSOCIATES] as? [String])!
        
        self.dicSession = ClsSessionDetails(pdicResponse: pdicResponse[KSESSION] as! [String : AnyObject])
        self.strCreatedAt = pdicResponse.getValueIfAvilable(KCREATEDTIME) as? String
        self.strUpdatedAt = pdicResponse.getValueIfAvilable(KUPDATEDTIME) as? String
        self.intSentCount = pdicResponse.getValueIfAvilable(KSENTCOUNT)?.integerValue
        self.intReceivedCount = pdicResponse.getValueIfAvilable(KRECEIVEDCOUNT)?.integerValue
        self.intLoginCount = pdicResponse.getValueIfAvilable(KLOGINCOUNT)?.integerValue
    }
    
    required init(coder aDecoder: NSCoder)
    {
        self.strId = aDecoder.decodeObjectForKey(KUUID) as? String
        self.strClinkAccountId = aDecoder.decodeObjectForKey(KCLINKACCOUNTUUID) as? String
        self.strFirstName = aDecoder.decodeObjectForKey(KFIRSTNAME) as? String
        self.strLastName = aDecoder.decodeObjectForKey(KLASTNAME) as? String
        self.strEmail = aDecoder.decodeObjectForKey(KEMAIL) as? String
        self.strPhone = aDecoder.decodeObjectForKey(KPHONE) as? String
        self.strDOB = aDecoder.decodeObjectForKey(KDOB) as? String
        self.isPasswordResetRequired = aDecoder.decodeObjectForKey(KISPSEDRESETREQUIRED)?.boolValue
        self.isActive = aDecoder.decodeObjectForKey(KISACTIVE)?.boolValue
        self.isVerified = aDecoder.decodeObjectForKey(KISVERIFIED)?.boolValue
        self.strLastLoginAt = aDecoder.decodeObjectForKey(KLASTLOGINTIME) as? String
        self.strBalance = aDecoder.decodeObjectForKey(KBALANCE) as? String
        self.intLikeCount = aDecoder.decodeObjectForKey(KLIKECOUNT)?.integerValue
        self.intCommentCount = aDecoder.decodeObjectForKey(KCOMMENTCOUNT)?.integerValue
        self.arrRoles = (aDecoder.decodeObjectForKey(KROLES) as? [String])!
        self.arrAssociates = (aDecoder.decodeObjectForKey(KASSOCIATES) as? [String])!
        self.dicSession = (aDecoder.decodeObjectForKey(KSESSION) as! ClsSessionDetails)
        self.strCreatedAt = aDecoder.decodeObjectForKey(KCREATEDTIME) as? String
        self.strUpdatedAt = aDecoder.decodeObjectForKey(KUPDATEDTIME) as? String
        self.intSentCount = aDecoder.decodeObjectForKey(KSENTCOUNT)?.integerValue
        self.intReceivedCount = aDecoder.decodeObjectForKey(KRECEIVEDCOUNT)?.integerValue
        self.intLoginCount = aDecoder.decodeObjectForKey(KLOGINCOUNT)?.integerValue
    }
    func encodeWithCoder(_aCoder: NSCoder) {
        _aCoder.encodeObject(self.strId , forKey: KUUID)
        _aCoder.encodeObject(self.strClinkAccountId, forKey: KCLINKACCOUNTUUID)
        _aCoder.encodeObject(self.strFirstName , forKey: KFIRSTNAME)
        _aCoder.encodeObject(self.strLastName , forKey: KLASTNAME)
        _aCoder.encodeObject(self.strEmail , forKey: KEMAIL)
        _aCoder.encodeObject(self.strPhone , forKey: KPHONE)
        _aCoder.encodeObject(self.strDOB , forKey: KDOB)
        _aCoder.encodeObject(self.isPasswordResetRequired , forKey: KISPSEDRESETREQUIRED)
        _aCoder.encodeObject(self.isActive , forKey: KISACTIVE)
        _aCoder.encodeObject(self.isVerified , forKey: KISVERIFIED)
        _aCoder.encodeObject(self.strLastLoginAt , forKey: KLASTLOGINTIME)
        _aCoder.encodeObject(self.strBalance , forKey: KBALANCE)
        _aCoder.encodeObject(self.intLikeCount , forKey: KLIKECOUNT)
        _aCoder.encodeObject(self.intCommentCount , forKey: KCOMMENTCOUNT)
        _aCoder.encodeObject(self.arrRoles ,forKey : KROLES)
        _aCoder.encodeObject(self.arrAssociates ,forKey : KASSOCIATES)
        _aCoder.encodeObject(self.dicSession ,forKey : KSESSION)
        _aCoder.encodeObject(self.strCreatedAt , forKey: KCREATEDTIME)
        _aCoder.encodeObject(self.strUpdatedAt , forKey: KUPDATEDTIME)
        _aCoder.encodeObject(self.intSentCount , forKey: KSENTCOUNT)
        _aCoder.encodeObject(self.intReceivedCount , forKey: KRECEIVEDCOUNT)
        _aCoder.encodeObject(self.intLoginCount , forKey: KLOGINCOUNT)
    }
}

class ClsSessionDetails: NSObject {
    var id : String! = EMPTYSTRING
    var strIpAddress : String! = EMPTYSTRING
    var strLastActivity : String! = EMPTYSTRING
    var dicPayload : [String : AnyObject] = Dictionary()
    
    init(pdicResponse : [String : AnyObject])
    {
        self.id = pdicResponse.getValueIfAvilable(KID) as? String
        self.strIpAddress = pdicResponse.getValueIfAvilable(KIPADDRESS) as? String
        self.strLastActivity = pdicResponse.getValueIfAvilable(KLASTACTIVITY) as? String
        self.dicPayload = (pdicResponse.getValueIfAvilable(KPAYLOAD) as? [String : AnyObject])!
        
    }
    
    required init(coder aDecoder: NSCoder)
    {
        self.id = aDecoder.decodeObjectForKey(KID) as? String
        self.strIpAddress = aDecoder.decodeObjectForKey(KIPADDRESS) as? String
        self.strLastActivity = aDecoder.decodeObjectForKey(KLASTACTIVITY) as? String
        self.dicPayload = (aDecoder.decodeObjectForKey(KPAYLOAD) as? [String : AnyObject])!
    }
    
    func encodeWithCoder(_aCoder: NSCoder) {
        
        _aCoder.encodeObject(self.id , forKey: KID)
        _aCoder.encodeObject(self.strIpAddress , forKey: KIPADDRESS)
        _aCoder.encodeObject(self.strLastActivity , forKey: KLASTACTIVITY)
        _aCoder.encodeObject(self.dicPayload , forKey: KPAYLOAD)
        
    }
}