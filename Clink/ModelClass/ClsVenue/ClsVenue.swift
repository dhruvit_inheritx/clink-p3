//
//  ClsVenue.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright (c) 2015 Gunjan. All rights reserved.
//

import Foundation
import UIKit

/**
 This Class was implemented to store Venue Details.
 
 like:
 
 * id
 * name
 * description
 * category
 * location
 
 etc.
 
 */
class ClsVenue :NSObject{
    
    var id: String! = EMPTYSTRING
    var strName: String! = EMPTYSTRING
    var strDescription: String! = EMPTYSTRING
    var arrCategoryName: [String]!
    var arrLocation: [ClsLocation]! = Array()
    var arrPartners : [String]!
    var strImgThumbnail:String! = EMPTYSTRING
    var strImgScreen:String! = EMPTYSTRING
    var strURL: String! = EMPTYSTRING
    var floatRating: Float! = 0.0
    var strContactName : String! = EMPTYSTRING
    var strEmail : String! = EMPTYSTRING
    var arrPromotions : [ClsPromotion] = Array()
    var objDefaultContact: ClsContact! = ClsContact()
    var strCategory : String! = EMPTYSTRING
    var strImgSmallIcon : String! = EMPTYSTRING
    
    // Class Constructor
    init(pdictResponse:[String : AnyObject!]) {
        self.id = pdictResponse.getValueIfAvilable(KUUID) as? String
        self.strName = pdictResponse.getValueIfAvilable(KNAME) as? String
        self.arrCategoryName = pdictResponse.getValueIfAvilable(KCATEGORIES) as! [String]
        if arrCategoryName.count > 0{
            self.strCategory = arrCategoryName.first
        }
        
        if let arrLoacation = pdictResponse[KLOCATIONS]
        {
            for location in arrLoacation as! [AnyObject]{
                if location.count > 0{
                    self.arrLocation.append(ClsLocation(pdicLocation: location as! [String : AnyObject]))
                }
                //needs update as per data
            }
        }
        
        if let arrPromo = pdictResponse[KPROMOTIONS]
        {
            for promo in arrPromo as! [AnyObject]{
                if promo.count > 0{
                    self.arrPromotions.append(ClsPromotion(pdictPromotion: promo as! [String : AnyObject]))
                }
                //needs update as per data
            }
            
        }
        if let dictDefaultUser = pdictResponse.getValueIfAvilable(KDEFAULTUSER) as? [String : AnyObject]{
            self.objDefaultContact = ClsContact(pdicResponse:dictDefaultUser)
            print("\(self.objDefaultContact.strFullName!)")
        }
        
        self.arrPartners = pdictResponse[KPARTNERS] as! [String]
        self.strDescription = pdictResponse.getValueIfAvilable(KDESCRIPTIONSMALL) as? String
        self.strURL = pdictResponse.getValueIfAvilable(KWEBSITEURL) as? String
        self.floatRating = pdictResponse.getValueIfAvilable(KSTARRATING) as? Float
        if let strImage = pdictResponse.getValueIfAvilable(KLOGOTHUMBIMAGE) as? String {
            self.strImgThumbnail = strImage
        }
        if let strImage = pdictResponse.getValueIfAvilable(KLOGOSCREENIMAGE) as? String {
            self.strImgScreen = strImage
        }
        if let strImage = pdictResponse.getValueIfAvilable(KSMALLLOGOIMAGE) as? String {
            self.strImgSmallIcon = strImage
        }
        self.strContactName = pdictResponse.getValueIfAvilable(KCONTACTNAME) as? String
        self.strEmail  = pdictResponse.getValueIfAvilable(KEMAIL) as? String
    }
    
}

class ClsPromotion :NSObject{
    
    var strId: String! = EMPTYSTRING
    
    // Class Constructor
    init(pdictPromotion:[String : AnyObject!]) {
        self.strId = pdictPromotion.getValueIfAvilable(KUUID) as? String
    }
    
}
