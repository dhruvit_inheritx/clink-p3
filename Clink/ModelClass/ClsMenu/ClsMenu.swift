//
//  ClsMenu.swift
//  Clink
//
//  Created by Gunjan on 03/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClsMenu: NSObject {

    var strMenuTitle:String = EMPTYSTRING
    var intNotificationCount:Int = INTZERO
    var strImageNameSuffix:String = EMPTYSTRING
    var objSelector:Selector!   // This is the selector method which will be called when this menu is selected.
    var isSelected:Bool = false
    
    init(pstrTitle:String, pstrImageName:String, pintNotificationCount:Int, pactionMehod:Selector) {
        
        strMenuTitle = pstrTitle
        strImageNameSuffix = pstrImageName
        intNotificationCount = pintNotificationCount
        objSelector = pactionMehod
    }
}