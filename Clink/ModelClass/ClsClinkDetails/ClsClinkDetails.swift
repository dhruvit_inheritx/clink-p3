//
//  ClsClinkDetails.swift
//  Clink
//
//  Created by Gunjan on 04/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

enum ClinkType : Int{
    case Sent = 1, Received = 2, Thankyou = 3, Gift = 4
}

enum ClinkControllerType : Int{
    case Normal = 1,ThankYou = 2, Gift = 3
}

enum EmoType : Int{
    case NAN = 0, Rofl = 1, Wow = 2, Love = 3, Cheers = 4, Claps = 5
}

class ClsClinkDetails: NSObject {
    
    var strAmount: String! = "0"
    var arrCCUser:[ClsContact]!
    var intCommentCount: Int! = INTZERO
    var strSenderId: String! = ""
    var strSenderName: String! = ""
    var strSenderFirstName: String! = ""
    var strSenderLastName: String! = ""
    var objSenderUser : ClsContact!
    var strGiftPlusClinkId: String! = ""
    var strImageUrl:String! = ""
    var isPrivate:Bool!
    var isRead:Bool!
    var isReceipientRead:Bool!
    var isSent:Bool!
    var intLikeCount: Int! = INTZERO
    var strMerchantId: String! = ""
    var strMerchantName : String! = ""
    var strOriginalClinkId: String! = ""
    var strPartnerId:String! = ""
    var strReceiverId: String! = ""
    var strReceiverName: String! = ""
    var strReceiverFirstName: String! = ""
    var strReceiverLastName: String! = ""
    var objReceiverUser : ClsContact!
    var strId: String! = ""
    var isLiked:Bool!
    
    var strCreatedDate: String! = ""
    var strUpdatedDate: String! = ""
    var arrThankyouClink:[String]!
    
    var dictFromUser : [String : AnyObject]!
    var dictToUser : [String : AnyObject]!
   
    var objReplyClink : ClsClinkDetails!
    
    var strGiftPlusAmount: String! = ""
    var objMarchantDetails : ClsVenue!
    
    var arrAssociatedClinkId:[String]!
    var strDonationURL : String?
    var hasNewMessage : Bool!
    
    override init(){
        strAmount = "0"
        arrCCUser = Array()
        arrThankyouClink = Array()
        intCommentCount = INTZERO
        strSenderId = ""
        strSenderName = ""
        strSenderFirstName = ""
        strSenderLastName = ""
        strGiftPlusClinkId = ""
        strImageUrl = ""
        isPrivate = false
        isRead = false
        isReceipientRead = false
        isSent = false
        intLikeCount = INTZERO
        strMerchantId = ""
        strMerchantName = ""
        strOriginalClinkId = ""
        strPartnerId = ""
        strReceiverId = ""
        strReceiverName = ""
        strReceiverFirstName = ""
        strReceiverLastName = ""
        strId = ""
        
        strCreatedDate = ""
        strUpdatedDate = ""
        
        dictFromUser = Dictionary()
        dictToUser = Dictionary()
        
        strGiftPlusAmount = ""
        arrAssociatedClinkId = Array()
        hasNewMessage = false
//        objReplyClink = ClsClinkDetails()
    }
    
    init(pdictResponse:[String : AnyObject]){

        strAmount = pdictResponse.getValueIfAvilable(KAMOUNT) as? String
        strAmount = strAmount.removeSpecialChars(".")
        
        if let arrCCUserDetails = pdictResponse.getValueIfAvilable(KCCUSER) as? [AnyObject]{
            arrCCUser = Array()
            for dicUserDetails in arrCCUserDetails{
                let objContact = ClsContact(pdicResponse: dicUserDetails as! [String : AnyObject])
                objContact.isUnRemovable = true
                arrCCUser.append(objContact)
            }
        }
        
        arrAssociatedClinkId = Array()
        
        if let arrStrThankyouClink = pdictResponse.getValueIfAvilable(KTHANKYOUCLINKS) as? [String]{
            arrThankyouClink = Array()
            arrThankyouClink = arrStrThankyouClink
            arrAssociatedClinkId = arrThankyouClink
            if arrThankyouClink.count > 0{
                strOriginalClinkId = arrThankyouClink.first
            }
        }
        
        if let arrStrGiftClink = pdictResponse.getValueIfAvilable(KGIFTCLINKS) as? [String]{
            for strGiftClink in arrStrGiftClink{
                arrAssociatedClinkId.append(strGiftClink)
            }
        }
        
//        strOriginalClinkId = pdictResponse.getValueIfAvilable(KORIGINALCLINKUUID) as? String
        
//        if strGiftPlusClinkId != nil {
//            if strGiftPlusClinkId != "" {
//                arrAssociatedClinkId.append(strGiftPlusClinkId)
//            }
//        }
        
        intCommentCount = pdictResponse.getValueIfAvilable(KCOMMENTCOUNT) as? Int

        dictFromUser = pdictResponse.getValueIfAvilable(KFROMUSER) as? [String : AnyObject]
        strSenderId = dictFromUser.getValueIfAvilable(KSENDERID) as? String
        strSenderFirstName = dictFromUser.getValueIfAvilable(KSENDERFIRSTNAME) as? String
        strSenderLastName = dictFromUser.getValueIfAvilable(KSENDERLASTNAME) as? String
        strSenderName = strSenderFirstName + " " + strSenderLastName
        
        strGiftPlusClinkId = pdictResponse.getValueIfAvilable(KGIFTPLUSCLINKUUID) as? String
        strImageUrl = pdictResponse.getValueIfAvilable(KIMAGE) as? String
        isPrivate = pdictResponse.getValueIfAvilable(KISPRIVATE)?.boolValue
        isRead = pdictResponse.getValueIfAvilable(KISREAD)?.boolValue
        isReceipientRead = pdictResponse.getValueIfAvilable(KISRECEIPIENTREAD)?.boolValue
        isSent = pdictResponse.getValueIfAvilable(KISSENT)?.boolValue
        isLiked = pdictResponse.getValueIfAvilable(KISLIKED)?.boolValue
        
        intLikeCount = pdictResponse.getValueIfAvilable(KLIKES) as? Int

        strMerchantId = pdictResponse.getValueIfAvilable(KMERCHANTUUID) as? String
        strMerchantName = pdictResponse.getValueIfAvilable(KMERCHANTNAME) as? String
        strPartnerId = pdictResponse.getValueIfAvilable(KPARTNERUUID) as? String
        
        dictToUser = pdictResponse.getValueIfAvilable(KTOUSER) as? [String : AnyObject]
        strReceiverId = dictToUser.getValueIfAvilable(KRECEIVERID) as! String
        strReceiverFirstName = dictToUser.getValueIfAvilable(KRECEIVERFIRSTNAME) as? String
        strReceiverLastName = dictToUser.getValueIfAvilable(KRECEIVERLASTNAME) as? String
        strReceiverName = strReceiverFirstName + " " + strReceiverLastName
        
        objSenderUser = ClsContact(pdicResponse: dictFromUser)
        objReceiverUser = ClsContact(pdicResponse: dictToUser)
        
        strId = pdictResponse.getValueIfAvilable(KUUID) as! String
        strGiftPlusAmount = pdictResponse.getValueIfAvilable(KGIFTPLUSAMOUNT) as? String
        strGiftPlusAmount = strGiftPlusAmount.removeSpecialChars(".")
        
        strDonationURL = pdictResponse.getValueIfAvilable(KDONATIONURL) as? String
        if pdictResponse.getValueIfAvilable(KHASNEWMSG)?.boolValue != nil{
            hasNewMessage = pdictResponse.getValueIfAvilable(KHASNEWMSG)?.boolValue
        }
        else{
            hasNewMessage = false
        }
        
        guard let pCreatedDate = pdictResponse.getValueIfAvilable(KCREATEDAT) else {
            return
        }
        strCreatedDate = pCreatedDate as! String
        
        guard let pUpdatedDate = pdictResponse.getValueIfAvilable(KUPDATEDAT) else {
            return
        }
        strUpdatedDate = pUpdatedDate as! String
        
    }
    
    func getReplyClink(pAlertView: UIView, pViewController: UIViewController){
        
        if strOriginalClinkId.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > INTZERO && strOriginalClinkId.isEmptyString() == false && strOriginalClinkId != EMPTYSTRING {
        
            if self.objReplyClink == nil {
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(strOriginalClinkId,nil).URLRequest, isBackgroundCall : true, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        let objClinkDetail = ClsClinkDetails(pdictResponse: response as! [String : AnyObject])
                        self.objReplyClink = objClinkDetail
                        // Cache object once its downlaoded so next time it won't need to make call.
                    }
                })
            }
        }
    }
    
    func getMarchantDetails(pAlertView: UIView, pViewController: UIViewController){
        
         if strMerchantId.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > INTZERO && strMerchantId.isEmptyString() == false{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetVenueWithId(self.strMerchantId).URLRequest, isBackgroundCall:true, pAlertView: pAlertView, pViewController: pViewController) { (success, result) -> () in
                if success == true{
                    let objMarchantDetails = ClsVenue(pdictResponse: result as! [String : AnyObject])
                    self.objMarchantDetails = objMarchantDetails
                }
            }
        }
    }
    
}
