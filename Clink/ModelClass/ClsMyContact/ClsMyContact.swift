//
//  ClsContact.swift
//  SequrKiwi
//
//  Created by Gunjan on 07/03/16.
//  Copyright (c) 2015 Gunjan. All rights reserved.
//

import Foundation
import UIKit

class ClsMyContact :NSObject{
    
//    var id: String!
    var UserId: String!
    var arrEmail: [String]!
    
    // Class Constructor
    init( puserId: String, parrEmail: [String]) {
        self.UserId = puserId
        self.arrEmail = parrEmail
    }
}
