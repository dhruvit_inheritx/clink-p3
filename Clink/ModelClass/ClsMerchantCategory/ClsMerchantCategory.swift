//
//  ClsMerchantCategory.swift
//  Clink
//
//  Created by Gunjan on 13/09/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
class ClsMerchantCategory: NSObject {
    
    var strTitle:String! = EMPTYSTRING
    var strName:String! = EMPTYSTRING
    var strImageURL:String! = EMPTYSTRING
    var strImageURLSelected:String! = EMPTYSTRING
    
    init(pdicCategory : [String : AnyObject])
    {
        self.strTitle = pdicCategory.getValueIfAvilable(KTITLESMALL) as! String
        self.strName = pdicCategory.getValueIfAvilable(KNAME) as! String
        if let imgUrl = pdicCategory.getValueIfAvilable(KICONOFFIMAGE) as? String
        {
            self.strImageURL = imgUrl
        }
        if let imgUrlSelected = pdicCategory.getValueIfAvilable(KICONONIMAGE) as? String
        {
            self.strImageURLSelected = imgUrlSelected
        }
        
    }
}