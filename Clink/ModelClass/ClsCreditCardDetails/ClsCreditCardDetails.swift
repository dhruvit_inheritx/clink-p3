//
//  ClsCreditCardDetails.swift
//  Clink
//
//  Created by Dhruvit on 09/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClsCreditCardDetails: NSObject {
    
    var strId : String! = EMPTYSTRING
    var strUserId : String! = EMPTYSTRING
    var strAccountType : String! = EMPTYSTRING
    var strIAV : String! = EMPTYSTRING
    var isDefaultPayment : Bool! = false
    var isDefaultTransfer : Bool! = false
    var isVarified : Bool! = false
    var strLastFour : String! = EMPTYSTRING
    var strProcessorId : String! = EMPTYSTRING
    var strToken : String! = EMPTYSTRING
    var strCreatedAt : String! = EMPTYSTRING
    var strUpdatedAt : String! = EMPTYSTRING
    var strBankVarifyUrl : String! = EMPTYSTRING
    
    override init(){
        //        init empty values
    }
    
    init(pdictResponse : [String : AnyObject])
    {
        self.strId = pdictResponse.getValueIfAvilable(KUUID) as? String
        self.strUserId = pdictResponse.getValueIfAvilable(KUSERUUID) as? String
        self.strAccountType = pdictResponse.getValueIfAvilable(KACCOUNTTYPE) as? String
        self.strIAV = pdictResponse.getValueIfAvilable(KIAV) as? String
        self.isDefaultPayment = pdictResponse.getValueIfAvilable(KISDEFAULTPAYMENT) as? Bool
        self.isDefaultTransfer = pdictResponse.getValueIfAvilable(KISDEFAULTTRANSFER) as? Bool
        self.isVarified = pdictResponse.getValueIfAvilable(KISVERIFIED) as? Bool
        self.strLastFour = pdictResponse.getValueIfAvilable(KLASTFOUR) as? String
        self.strProcessorId = pdictResponse.getValueIfAvilable(KPROCESSORUUID) as? String
        self.strToken = pdictResponse.getValueIfAvilable(KTOKEN) as? String
        self.strCreatedAt = pdictResponse.getValueIfAvilable(KCREATEDTIME) as? String
        self.strUpdatedAt = pdictResponse.getValueIfAvilable(KUPDATEDTIME) as? String
        self.strBankVarifyUrl = pdictResponse.getValueIfAvilable("bank_verify_url") as? String
    }
    
    required init(coder aDecoder: NSCoder)
    {
        self.strId = aDecoder.decodeObjectForKey(KUUID) as? String
        self.strUserId = aDecoder.decodeObjectForKey(KUSERUUID) as? String
        self.strAccountType = aDecoder.decodeObjectForKey(KACCOUNTTYPE) as? String
        self.strIAV = aDecoder.decodeObjectForKey(KIAV) as? String
        self.isDefaultPayment = aDecoder.decodeObjectForKey(KISDEFAULTPAYMENT) as? Bool
        self.isDefaultTransfer = aDecoder.decodeObjectForKey(KISDEFAULTTRANSFER) as? Bool
        self.isVarified = aDecoder.decodeObjectForKey(KISVERIFIED) as? Bool
        self.strLastFour = aDecoder.decodeObjectForKey(KLASTFOUR) as? String
        self.strProcessorId = aDecoder.decodeObjectForKey(KPROCESSORUUID) as? String
        self.strToken = aDecoder.decodeObjectForKey(KTOKEN) as? String
        self.strCreatedAt = aDecoder.decodeObjectForKey(KCREATEDTIME) as? String
        self.strUpdatedAt = aDecoder.decodeObjectForKey(KUPDATEDTIME) as? String
        self.strBankVarifyUrl = aDecoder.decodeObjectForKey("bank_verify_url") as? String
    }
    
    func encodeWithCoder(_aCoder: NSCoder)
    {
        _aCoder.encodeObject(self.strId , forKey: KUUID)
        _aCoder.encodeObject(self.strUserId , forKey: KUSERUUID)
        _aCoder.encodeObject(self.strAccountType , forKey: KACCOUNTTYPE)
        _aCoder.encodeObject(self.strIAV , forKey: KIAV)
        _aCoder.encodeObject(self.isDefaultPayment , forKey: KISDEFAULTPAYMENT)
        _aCoder.encodeObject(self.isDefaultTransfer , forKey: KISDEFAULTTRANSFER)
        _aCoder.encodeObject(self.isVarified , forKey: KISVERIFIED)
        _aCoder.encodeObject(self.strLastFour , forKey: KLASTFOUR)
        _aCoder.encodeObject(self.strProcessorId , forKey: KPROCESSORUUID)
        _aCoder.encodeObject(self.strToken , forKey: KTOKEN)
        _aCoder.encodeObject(self.strCreatedAt , forKey: KCREATEDTIME)
        _aCoder.encodeObject(self.strUpdatedAt , forKey: KUPDATEDTIME)
        _aCoder.encodeObject(self.strBankVarifyUrl, forKey: "bank_verify_url")
    }
    
}
