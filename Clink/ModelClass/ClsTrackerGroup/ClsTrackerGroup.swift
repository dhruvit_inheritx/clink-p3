//
//  ClsTrackerGroup.swift
//  Clink
//
//  Created by Gunjan on 04/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClsTrackerGroup: NSObject {
    
    var strId : String! = EMPTYSTRING
    var strName : String! = EMPTYSTRING
    var strCreatedDate : String! = EMPTYSTRING
    var strUpdatedDate : String! = EMPTYSTRING
    
    
    init(pdictResponse:[String : AnyObject]) {
        
        strId = pdictResponse.getValueIfAvilable(KUUID) as? String
        strName = pdictResponse.getValueIfAvilable(KNAME) as? String
        
        if let pUpdatedDate = pdictResponse.getValueIfAvilable(KUPDATEDAT) {
            strUpdatedDate = pUpdatedDate as! String
        }
        
        guard let pCreatedDate = pdictResponse.getValueIfAvilable(KCREATEDAT) else {
            return
        }
        strCreatedDate = pCreatedDate as! String
    }
    required init(coder aDecoder: NSCoder)
    {
        self.strId = aDecoder.decodeObjectForKey(KUUID) as? String
        self.strName = aDecoder.decodeObjectForKey(KNAME) as? String
        
        if let pUpdatedDate = aDecoder.decodeObjectForKey(KUPDATEDAT) {
            strUpdatedDate = pUpdatedDate as! String
        }
        
        guard let pCreatedDate = aDecoder.decodeObjectForKey(KCREATEDAT) else {
            return
        }
        strCreatedDate = pCreatedDate as! String
    }
    
    
    func encodeWithCoder(_aCoder: NSCoder) {
        
        _aCoder.encodeObject(self.strId , forKey: KUUID)
        _aCoder.encodeObject(self.strName , forKey: KNAME)
        _aCoder.encodeObject(self.strUpdatedDate , forKey: KUPDATEDAT)
        _aCoder.encodeObject(self.strCreatedDate , forKey: KCREATEDAT)
        
    }
}
