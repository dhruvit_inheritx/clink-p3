//
//  ClsLocation.swift
//  Clink
//
//  Created by Gunjan on 13/09/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
class ClsLocation: NSObject {
    
    var id : String! = EMPTYSTRING
    var strAddressLine1 : String! = EMPTYSTRING
    var strAddressLine2 : String! = EMPTYSTRING
    var strCity : String! = EMPTYSTRING
    var strState : String! = EMPTYSTRING
    var strZip : String! = EMPTYSTRING
    var strPhone : String! = EMPTYSTRING
    
    init(pdicLocation : [String : AnyObject])
    {
        self.id = pdicLocation.getValueIfAvilable(KUUID) as? String
        self.strAddressLine1 = pdicLocation.getValueIfAvilable(KADDRESS1) as? String
        self.strAddressLine2 = pdicLocation.getValueIfAvilable(KADDRESS2) as? String
        self.strCity = pdicLocation.getValueIfAvilable(KCITY) as? String
        self.strState = pdicLocation.getValueIfAvilable(KSTATE) as? String
        self.strZip = pdicLocation.getValueIfAvilable(KZIP) as? String
        self.strPhone = pdicLocation.getValueIfAvilable(KPHONE) as? String
    }
}