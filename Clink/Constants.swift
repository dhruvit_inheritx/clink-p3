//
//  Constants.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

//PARSE Spec

let PARSEAPPID = "uifmKnV21UZEPxztK5flkOMfqFUiM4XJH0vMX2QE"
let PARSECLIENTKEY = "wsJx0PHp3oUKD4xLexeUkrQWcu6bg2UmvWPaeKlz"

//API Spec

let APIKEY = "ba1f0ea830c460cc23c8c195196ced76"
let APISECRET = "secret"
//Production: https://www.clink.nyc/api/
//Test: https://qa.clink.nyc/api/
let KBASEURL = "https://qa.clink.nyc/api"

//Font
let kDescriptionFont = UIFont(name: kAppFont, size: 12)

//Build
let BUILD = "27"
let INCLUDEACCOUNT = true

//Theme color
let kThemeSelectionColor = UIColor(red: 81/255, green: 206/255, blue: 164/255, alpha: 1)
let kThemeDeselectionColor = UIColor(red: 29/255, green: 29/255, blue: 38/255, alpha: 1)
let kThemeMainLabelColor = UIColor(red: 185/255, green: 150/255, blue: 9/255, alpha: 1)
let kThemeDestructiveColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
let kThemeColor = UIColor(red: 159/255, green: 136/255, blue: 93/255, alpha: 1)
let kCellSelectionColor = UIColor(red: 193/255, green: 157/255, blue: 63/255, alpha: 1)
let kCellSelectionLabelColor = UIColor .whiteColor()
let kAppFont = "Avenir"
let KCOMMENTEVENCOLOR = UIColor(colorLiteralRed: 247/255, green: 245/255, blue: 240/255, alpha: 1.0)
let KCOMMENTODDCOLOR = UIColor(colorLiteralRed: 243/255, green: 240/255, blue: 233/255, alpha: 1.0)
let COLORTHEME = UIColor(red: 179/255, green: 140/255, blue: 49/255, alpha: 1.0)
let LOGOUTCOLORTHEME = UIColor(red: 138/255, green: 130/255, blue: 102/255, alpha: 1)
let KFONTTHEMENEW = UIColor(red: 179.0/255.0, green: 160.0/255.0 , blue: 113.0/255.0 , alpha: 1.0)

//Parameters Name

let kLoadFeedData = "FeedData"

//Cell Identifire

let kVerifyDetailCellIdentifire = "VerifyDetailCell"
let CELLEMAILCC = "EmailCCCell"
let CELLCONTACTCC = "MyContactCell"
let CELLCOMMENTT = "commentCell"
let CELLEMOJI = "emojiOnlyCell"
let CELLVENUEFOODCELL = "venueCell"
let CELLVENUEDONATECELL = "venueCell1"
let CELLVENUECATEGORYCELL = "venueCategoryCell"
let CELLVENDORMENUCELL = "vendorMenuCell"
let CELLCLINKGRID = "clinkGridCell"

//Appdelegate
let APPDELEGATE = UIApplication.sharedApplication().delegate as! AppDelegate

// UserDefault
let USERDEFAULTS = NSUserDefaults.standardUserDefaults()

// NotificationCenter
let NOTIFICATIONCENTER = NSNotificationCenter.defaultCenter()

// UserDefault Keyword
let kUserDetail = "UserDetail"
let kUserProperty = "UserProperty"
let kISUSERLOGGEDIN = "isUserLoggedIn"
let kUserToken = "UserToken"
let KRECEIVEDCLINKSELECTED = "isReceivedClink"
let KCONTACTARRAY = "KContactArray"

//Attributed String
let ATTSTRDRAMALEAGUEA = "Drama League "
let ATTSTRB = "A donation was made to "
let ATTSTRC = "by the sender. Would you like to match their donation?"
let ATTSTRHORACEMANNSCHOOLA = "Horace Mann School "
let ATTSTRTWOFORONE = "2 for 1 "
let ATTSTRHAPPYHOURE = "CLINK includes exclusive happy hour "
let ATTSTRTSPECIAL = "special."

// Message
let APPNAME = "CLINK!"
let ALERTBLANKPHONENUMBER = "Please enter mobile number."
let ALERTBLANKEMAILADDRESS = "Please enter email address."
let ALERTBLANKEMAILFORGOT = "Please enter email address to reset password"
let ALERTBLANKPASSWORD = "Please enter password."
let ALERTBLANKCONFIRMPASSWORD = "Please enter confirm password."
let ALERTBLANKDOB = "Please enter Date of Birth."
let ALERTBLANKFNAME = "Please enter firstname."
let ALERTBLANKLNAME = "Please enter lastname."
let ALERTBLANKUSERNAME = "Please enter username."
let ALERTBLANKPHONE = "Please enter phone number"
let ALERTBLANKTEXTANDEMAIL = "Please select email or text to sent code"
let ALERTVERIFYACCOUNTEMAIL = "Verification mail sent to your email address. Please verify your account to continue using Clink!"
let ALERTPASSWORDVALIDATIONDESC = "Your password must be at least 8 characters long and contains at least 1 uppercase character,1 symbol and a combined of numbers and letters. "
let ALERTMOBILETENDEGITNUMBER = "Please enter valid mobile number."
let ALERTBLANKCURRENTPASSWORD = "Please enter current password."
let ALERTBLANKNEWPASSWORD = "Please enter new password."
let ALERTBLANKREENTERPASSWORD  = "Please enter confirm password."
let ALERTINVALIDEMAIL = "Please enter valid email address."
let ALERTBLANKAGE = "Please enter age."
let ALERTPASSWORDMISMATCH = "Password and confirm password does not matched."
let ALERTPASSWORDLENGTH = "Password must be at least 6 characters"
let ALERTBLANKREGISTRATIONCODE = "Please enter registration code"
let ALERTINVALIDEOREXPIREDPROMOCODE = "Promo code is either invalid or expired"
let ALERTEMAILSENTTO = "An email containing information on how to reset your password has been sent to "
let ALERTEMAILALREADYTAKEN = "Email id %@ is already registered with Clink!"
let ALERTINVITATIONCOMMINGSOON = "Invitation to contact is coming soon."
let ALERTTONONCLINKER = "Can not add non clink user as receiver in To field."
let ALERTALREADYLIKEDPOST = "This post is already liked by you"
let ALERTCONFIRMLOGOUT = "Are you sure you want to log out?"
let ALERTCONTACTPERMISIONDNIED = "User has denied contact permission. You can enable it from settings."
let ALERTPLEASEWAITTILLLOADINGIMAGE = "Please wait till loading image"
let ALERTFAILEDTOGETCLINK = "Failed to get Clink! post"
let ALERTFAILEDTOGETTYCLINK = "Failed to load Thank you Clink! response"
let ALERTPLEASEWAITTILLLOADINGTYIMAGE = "Please wait till loading image"
let ALERTPROMOCODEISINVALID = "Promo code is invalid"
let ALERTCONFIRMCLOSINGCLINK = "Are you sure that you would like to close current CLINK!?"
let ALERTINTERNETCONNECTION = "Internet connection is not available"
let ADDATLEASTONERECIPIENT = "Add at least one recipient to send CLINK!"
let ALERTCONFIRMADDGIFTAMOUNT = "Do you want to add a gift amount to the clink?"
let ALERTAREYOUSUREYOUWANTTOCLEAR = "Are you sure you want to clear the screen?"
let ALERTIMAGESAVED = "Image saved in photos."
let ALERTFAILDTOSETVENDOR = "Failed to set default vendor"
let ALERTBLANKCOMMENTEMO = "Please either enter text or select emoji to make a comment"

// Button title
let BTNOKTITLE = "OK"
let BTNRESETTITLE = "Reset"
let BTNREQUESTTITLE = "Request"
let BTNCANCELTITLE = "Cancel"
let BTNYESTITLE = "Yes"
let BTNNOTITLE = "No"

//FONTS
let FONTGOTHAMMEDIUM = "GothamMedium"
let FONTGOTHAMBOLD = "GothamBold"
let FONTSIZE13 : CGFloat = 13.0
let FONTSIZE14 : CGFloat = 14.0
let FONTSIZE15 : CGFloat = 15.0
let FONTSIZE17 : CGFloat = 17.0
let FONTSIZE18 : CGFloat = 18.0

//IMAGE NAMES
let IMGBTNHM = "btn_venue_hm"
let IMGBTNDL = "btn_venue_dl"
let IMGBTNHP = "btn_venue_hp"
let IMGBTNDONATEBIG = "btn_donate_big"
let IMGBTNDONATESMALL = "btn_donate_small"
let IMGBTNSENDCLINKSMALL = "btn_sendClink_small"
let PRIVATEPOPUPVIEW = "PrivatePopUpView"
let FTUPOPUPVIEW = "FTUPopUpView"
let IMGICONHM = "icn_small_hm"
let IMGICONDL = "icn_small_dl"
let IMGICONHP = "icn_small_hp"
let IMGTXTTY = "text_say_thank_you"
let IMGNAN = "bsnl"
let IMGBTNTYSMALL = "btn_ty_small"
let IMGICONMENUFEED = "icn_menu_Feed"
let IMGBTNAMOUNT = "btn_amount"
let IMGTXTADDTOCLINK = "txt_add_to_clink"
let IMGVENUEBG = "btnVbg"
let IMGTXTSAYTY = "txt_say_thankyou"
let IMGICNLOGO = "icn_logo"
let IMGVENDORBG = "icn_vendor_bg"
let IMGTXTSENDCLINK = "txt_sendClink"
let IMGICNLOCATION = "icn_location_n"
let IMGEMOROFL = "emo_rofl"
let IMGEMOWOW = "emo_wow"
let IMGEMOLOVE = "emo_love"
let IMGEMOCHEERS = "emo_cheers"
let IMGEMOCLAP = "emo_clap"
let IMGSTAR = "star"
let IMGSTARSELECTED = "starhighlighted"
let IMGVIEWINGCLINKSENT = "viewing-clink-sent"
let IMGHOWTOSAYTY = "How_To_Say_TY"
let IMGBTNTY = "btn_thank_you"
let NOIMAGE = "nana"
let IMGTXTWAITINGON = "txt_waiting_on"
let IMGBTNMENU = "btn_menu"
let IMGBTNBACK = "icn_back"
let IMGBTNVENUE = "btn_venue"
let IMGICNSENTTITLE = "icn_sent_title"
let IMGICNINBOX = "icn_inbox"
let IMGBANNERDRAMA = "banner_drama"
let IMGBANNERHM = "banner_hm"
let IMGICNCONTRIBUTEDISABLED = "icn_contribute_disabled"
let IMGICNPENCIL = "icn_pencil"
let IMGUNREADDOT = "UnreadDot"
let IMGHOUNDSBG = "bg_venue_menu_main"
let IMGLOGOGOLD = "logo_gold"

// Strings
let STREMPTY = ""
let STRYEARAGO = "year ago"
let STRMONTHAGO = "month ago"
let STRWEEKAGO = "week ago"
let STRDAYSAGO = "days ago"
let STRDAYAGO = "day ago"
let STRHOURSAGO = "hours ago"
let STRHOURAGO = "hour ago"
let STRMINUTESAGO = "minutes ago"
let STRMINUTEAGO = "minute ago"
let STRFEWSECONDSAGO = "few seconds ago"
let STRHELP = "HELP"
let STRSUPPORT = "SUPPORT"
let STRTERMSCONDITIONS = "TERMS & CONDITIONS"
let STRABOUTUS = "ABOUT US"
let KSELECTEDSCREENTYPE = "selectedScreenType"
let NOTDEFINED = "NDF"
let STRSENT = "  SENT"
let STRRECIEVED = "  RECEIVED"
let STRTO = "TO"
let STRFROM = "FROM"
let STRHOME = "  HOME"
let STREMPTYNAME = "Empty Name"
let STRNOTCHOOSEN = "Not choosen"
let STRCLINKUSERSECTIONTITLE = "WE CLINK!"
let STRCONTACTSECTIONTITLE = "CLINK! ME"
let STRCOMMENTPLACEHOLDER = "Write a comment..."
let STRCONTACTS = "CONTACTS"
let STRCOPYFRIENDS = "COPY FRIENDS"
let STRADDRESSCLINK = "ADDRESS CLINK!"
let STRAPPETIZERS = "APPETIZERS"
let STRSAYTY = "  SAY THANK YOU"
let STRADDTOCLINK = "  ADD TO CLINK!"
let STRSENDCLINK = "  SEND CLINK!"
let STRFIND = "Find"
let STRCHOOSEIMAGE = "Select image from"
let STRCAMERA = "Camera"
let STRGALLERY = "Gallery"
let STRDRAMAEMAIL = "dramaleague@clink.nyc"
let STRHMEMAIL = "donations@horacemann.org"
let STRSTOPCLINKING = "Stop\nCLINKING"
let STRKEEPCLINKING = "Keep\nCLINKING"
let STRDESCTYCLINK = "Thank you Clink!"
let STRDESCGIFTCLINK = "New Gift Amount"
let STRSELECTEDIMAGE = "_selected"

let STR0 = "0"
let STR1 = "1"
let STR2 = "2"
let STR3 = "3"
let STR4 = "4"
let STR5 = "5"
let STR6 = "6"
let STR7 = "7"
let STR8 = "8"
let STR9 = "9"
let STRPOINT = "."

let STRGIFTADDED = "Added Gift"
let STRGIFTADDEDSUCCESS = "Gift comment successfully"
let STRGIFTADDEDFAIL = "Gift comment faild"
let STRCLINKSENTSUCCESS = "Clink sent successfully"
let STRUPDATEFAIL = "Error updating field"

let LIKECOMMENTTEXT = "❤️"
let LIKECOMMENTTEXTSUCCESS = "Like comment successfully"
let LIKECOMMENTTEXTFAILED = "Like comment failed"
let LIKESUCCESS = "Like sent successfully"
let GIFTSENTESUCCESS = "Gift sent successfully"
let LOGINSUCCESS = "Log In Success"
let LOGINFAILED = "Invalid login parameters"
let PRMOCODEFOUND = "Promo code found"
let EMAILSENTSUCCESS = "Email send successfully"
let SIGNUPSUCCESS = "SignUp Success"
let SOMETHINGWENTWRONG = "Opps!!!...   Something went wrong"
let LOGOUTSUCCESS = "Logout Success"

let INVALIDSESSION = "Invalid session token. Please login again."

let AUTOFTUDESC = "AUTO FTU SENT CLINK!"
let AUTOFTURECDESC = "AUTO FTU RECEIVED CLINK!"
let STRCLINKTEAM = "CLINK Team"
let STRFAILEDTOGETFTU = "Failed to generate FTU CLINK!"

// Venues
let VENUEDRAMALEAGUE = "Drama League"
let VENUEHORACEMANN = "Horace Mann School"
let VENUEHOUNDSPUB = "HOUNDSTOOTH PUB"
let VENUEHOUNDSPUBSMALL = "Houndstooth Pub"
let STRHORACEID = "QWS4Q6D0VV"
let STRHORACE = "Horace"
let STRMANN = "Mann"
let VENUEDRAMALEAGUEPROMO = "DL07122016"
let VENUEHORACEMANNPROMO = "HM07122016"
let STRDLID = "BHN7T9mjyN"
let STRDRAMA = "Drama"
let STRLEAUGE = "League"
let STRPHONE = "0000000000"

//Other
let BUNDLESHORTVERSION = "CFBundleShortVersionString"
let EMAILREGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let PHONECHARS = "+0123456789"
let DateFormate = "EEEE, HH:MM "
let KDATEFORMATER = "hh:mm a MM-dd-yyyy"
let UTCFROMFORMATE = "yyyy-MM-dd HH:mm:ss ZZZ"
let PHASE3FORMATE = "yyyy-MM-dd HH:mm:ss"
let UTCNEWFROMFORMATE = "yyyyMMdd'T'HHmmss"
let KCATEGORYRESTAURANTS = "restaurants"
let KCATEGORYSHOPPING = "shopping"
let KCATEGORYHEALTH = "health"
let KCATEGORYPETS = "pets"
let KCATEGORYCHARITIES = "charities"
let KCATEGORYHERBAL = "services"
let KNOTAVIALABLE = "NA"
let RECORDSPERPAGE = 20

// Keys
let KPUSERNAME = "username"
let KUSERNAME = "UserName"
let KUSERID = "UserId"
let KCLINKID = "ClinkId"
let KCREATEDDATE = "CreatedDate"
let KEMOJITYPE = "emoji_type_id"
let KPROMOCODE = "PromoCode"
let KPROMOCODESMALL = "promocode"
let KEXPIRYDATE = "ExpiryDate"
let KADDITIONALAMOUNT = "AdditionalAmount"
let KTITLE = "Title"
let KDESCRIPTION = "Description"
let KDESCRIPTIONSMALL = "description"
//let KAMOUNT = "Amount"
let KVENUEID = "VenueId"
let IMG_PREFIX = "icon_"
let KVENUENAME = "VenueName"
let KCATEGORYNAME = "CategoryName"
let KCATEGORIES = "categories"
let KLOCATIONS = "locations"
let KPROMOTIONS = "promotions"
let KDEFAULTUSER = "default_user"
let KPARTNERS = "partners"
let KWEBSITEURL = "website_url"
let KSTARRATING = "star_rating"
let KLOGOTHUMBIMAGE = "logo_thumbnail_image"
let KLOGOSCREENIMAGE = "logo_screenpage_image"
let KSMALLLOGOIMAGE = "small_logo_image"
let KCONTACTNAME = "contact_name"
//let KPHONE = "phone"
let KADDRESS = "Address"
let KURL = "URL"
let KPAYMENTMETHODS = "PaymentMethods"
let KOFFER = "Offer"
let KRATING = "Rating"
//let KIMAGE = "Image"

// MARK:- Get All Clink Comment Response Keys

let KCOMMENT = "comment"
let KCLINKCOMMENT = "clinkcomment"
let KCLINKUUID = "clink_uuid"

// MARK:- Clink ActivityFeed Response Keys

//let KCLINKUUID = "uuid"
let KRECEIVERID = "uuid"
let KSENDERID = "uuid"
let KRECEIVERFIRSTNAME = "firstname"
let KRECEIVERLASTNAME = "lastname"
let KSENDERFIRSTNAME = "firstname"
let KSENDERLASTNAME = "lastname"
let KHASNEWMSG = "has_msg"
let KISREAD = "is_read"
let KISRECEIPIENTREAD = "is_receipient_read"
let KISSENT = "is_sent"
let KISLIKED = "is_liked"
let KISPRIVATE = "is_private"
let KOTHERUSERID = "OtherUserId"
let KOTHERUSERNAME = "OtherUserName"
let KSHOULDSENDNOTIFICATION = "shouldSendNotification"
let KLIKES = "like_count"
let KMERCHANTUUID = "merchant_uuid"
let KMERCHANTNAME = "merchant_name"
let KORIGINALCLINKUUID = "original_clink_uuid"
let KTHANKYOUCLINKS = "thankyou_clinks"
let KGIFTCLINKS = "giftplus_clinks"
let KPARTNERUUID = "partner_uuid"
let KTOUSER = "to_user"
let KFROMUSER = "from_user"
let KCOMMENTCOUNT = "comment_count"
let KCCUSER = "cc_users"
let KAMOUNT = "amount"
let KIMAGE = "image"
let KGIFTPLUSCLINKUUID = "giftplus_clink_uuid"
let KGIFTPLUSAMOUNT = "giftplus_amount"
let KCREATEDAT = "created_at"
let KUPDATEDAT = "updated_at"

let KCLINK = "clink"
let KCURRENTPAGE = "current_page"
let KLASTPAGE = "last_page"
let KPAGE = "page"
let KPERPAGE = "per_page"
let KTOTALCOUNT = "total_count"

let KREFERENCECLINKID = "ReferenceClinkId"
let KCOMMENTS = "Comments"
let KCLINKTYPE = "ClinkType"
let KCLINKSENT = "ClinkSent"
let KUSERSTONOTIFY = "UsersToNotify"
let KUPDATED = "Updated"
let KHASNEWACTIVITY = "HasNewActivity"
let ISEMAILVERIFIED = "emailVerified"
let KFBFIRSTNAME = "first_name"
let KFBLASTNAME = "last_name"
//let KEMAIL = "email"
let KMEGRAPHME = "me"
let KPUBLICPROFILE = "public_profile"
let KPUBLISHACTION = "publish_actions"
let KACCOUNTNUMBER = "accountnumber"
let KFIRSTTIMESENT = "isFirstTimeSent"
let KFIRSTTIMERECEIVED = "isFirstTimeReceived"

let KDEFAULTUSERID = "K2NKiCGF3a"
let KDEFAULTUSERID2 = "ktV1lrVEgV"
let KDEFAULTUSERIDFEED = "vd2uqEvfyr"

let KMOBILENUMBER = "MobileNumber"
let KUSER = "user"
let KVALIDVERSION = "ValidVersion"
let KSHOWHELPOPUP = "showHelpPopUp"
let KFETCHEDCLINKID = "fetchedClinkId"
let KSELECTED = "_select"

// MARK:- P3 - New

let KAUTHORIZATION = "Authorization"
let KUSERDETAILS = "userDeatils"
let KSESSIONID = "sid"
let KDEVICETOKEN = "device_token"
let KNONCE = "nonce"
let KTIMESTAMP = "timestamp"
let KMESSAGES = "messages"
let KMESSAGE = "message"
let KCODE = "code"
let KDATA = "data"
//let KFIRSTNAME = "firstname"
//let KLASTNAME = "lastname"
let KTOKEN = "token"
let KSOURCE = "source_name"
let KSOURCEFACEBOOK = "facebook"
let KSOURCETWITTER = "twitter"
let KPASSWORD = "password"
let KCONFIRMPASS = "password_confirm"

let KDATEOFBIRTH = "dob"
let KSID = "sid"
let KFBUSERID = "id"
let KISSELCTED = "isSelected"
let KISUNREMOVABLE = "isUnRemovable"
let KUUID = "uuid"
let KADDRESS1 = "address1"
let KADDRESS2 = "address2"
let KCITY = "city"
let KSTATE = "state"
let KZIP = "zip"

let KTITLESMALL = "title"
let KNAME = "name"
let KICONOFFIMAGE = "icon_off_image"
let KICONONIMAGE = "icon_on_image"

let KFIRSTNAME = "firstname"
let KLASTNAME = "lastname"
let KFULLNAME = "fullname"
let KEMAIL = "email"
let KPHONE = "phone"
let KUSERUUID = "user_uuid"
let KDOB = "dob"
let KISPSEDRESETREQUIRED = "is_pw_reset_required"
let KISACTIVE = "is_active"
let KISVERIFIED = "is_verified"
let KLASTLOGINTIME = "lastlogin_at"
let KBALANCE = "clink_balance"
let KLIKECOUNT = "clink_like_count"
let KSENTCOUNT = "clink_sent_count"
let KRECEIVEDCOUNT = "clink_received_count"
let KLOGINCOUNT = "login_count"
//let KCOMMENTCOUNT = "clink_comment_count"
let KROLES = "roles"
let KASSOCIATES = "associates"
let KSESSION = "session"
let KCREATEDTIME = "created_at"
let KUPDATEDTIME = "updated_at"
let KID = "id"
let KIPADDRESS  = "ip_address"
let KPAYLOAD  = "payload"
let KLASTACTIVITY  = "last_activity"

let KCONTACT = "contact"
let KISFTUHELPSHOWN = "isFTUHelpShown"
let KSEARCH = "search"
let KTRACKERGROUP = "trackergroup"
let KTRACKERGROUPUUID = "tracker_group_uuid"
let ALERTTITLEPASSWORDREQUIRENMENTS = "Password Requirements"
let KDONATIONURL = "donation_url"

let IMGPLACEHOLDER = "place_holder"
let KFILTERSTPES = "filters[type]"
let KFILTERSMIN = "filters[min]"


let EMPTYSTRING = ""
let SESSIONEXPIRED = 401
let REQUESTSUCCESS = 100
let REQUESTRESULTNOTFOUND = 400
let ALERTUNDERDEVLOPMENT = "This feature is under development."

//Parse Classes

let CLSCLINKDETAILS = "ClinkDetails"
let CLSCOMMENTDETAILS = "CommentDetails"
let CLSLIKEDETAILS = "LikeDetails"
let CLSVENUEDETAILS = "VenueDetails"
let CLSMENUDETAILS =  "MenuDetails"
let CLSVERSIONVALIDATOR =  "VersionValidator"
let CLSGIFTDETAILS = "GiftDetails"
let CLSPROMOCODEDETAILS = "PromoCodeDetails"
let CLSCUSTOMECAMERAVC = "CustomeCameraVC"

// MARK:- Story Board

let AUTHENTICATIONSTORYBOARD = "Authentication"
let MAINSTORYBOARD = "Main"
let ACCOUNTSTORYBOARD = "Accounts"

//controller
let CONTROLLERTANDC = "TermsConditionsController"
let CONTROLLERLOGIN = "LoginVC"
let CONTROLLERPRIVATEPOPUP = "PrivatePopUpView"
let CONTROLLERFTUPOPUP = "FTUPopUpView"
let CONTROLLERHELPPOPUP = "helpPopUp"
let CONTROLLERACCOUNTNAVIGATION = "accountnavigation"


//BASE URL
let KAPPBASEURL = "https://clink.parseapp.com/"

//Segues

let SEGUETOVENUEDETAILS = "segueToVenueDetails"
let SEGUETOHOUNDSDETAILS = "segueToHoundsDetails"
let SEGUETOSENDNEWCLINK = "segueToSendNewClink"
let SEGUETOTHREADVIEW = "segueToThreadView"
let SEGUETOCLINKDETAILS = "segueToClinkDetails"
let SEGUETOSELECTMYCONTACT = "segueToSelectMyContacts"
let SEGUETOSELECTVENUE = "segueToSelectVenue"
let SEGUETOAMOUNT = "segueToAmount"
let SEGUETOMYCONTACT = "SegueToMyContact"
let SEGUETOVENUE = "segueToVenue"
let SEGUETOTERMS = "segueToTerms"	
let SEGUETOABOUTUS = "segueToAboutUs"
let SEGUETONEWGROUP = "segueToCreateGroup"
let SEGUEBANKINFO = "seguebankinfo"
let SEGUETOADDGIFTVIEW = "segueToAddGift"
let SEGUELOADCONTROLLERS = "LoadContollers"
let SEGUECARDINFO = "segueCardinfo"
let SEGUEADDBANK = "segueaddbank"
let SEGUETOOFFERPREVIEW = "segueToOfferPreview"
let UNWINDTOUPDATEREADCOUNT = "unwindToUpdateReadCount"
let UNWINDTOFEEDSCREEN = "unwindToFeedScreen"
let UNWINDTORELOADDATA = "unwindToReloadData"
let UNWINDTOGIFT = "unwindToGiftClink"
let UNWINDTOSENDNEWCLINK = "unwindToSendNewClink"
let SEGUETOSAVECLINK = "segueToSaveClink"
let SEGUETOPREVIEWCLINK = "segueToPreviewClink"
let SEGUECENTERCONTAINMENT = "CenterContainment"
let SEGUESHOWSENTRECIVEDCLINK = "showSentRecivedClink"
let SEGUESENDNEWCLINK = "sendNewClink"
let SEGUETOSAVESENDNEWCLINK = "SaveToSendClinkSegue"
let SEGUETOSENDCLINKCANCEL = "CancelToSendClinkSegue"

//P3

let SEGUECLINKBALANCE = "segueClinkBalance"
let SEGUECASHOUTCLINKBALANCE = "segueCashOutClinkBalance"
let SEGUETOGRID = "segueToGridView"
let SEGUEACCOUNTHISTORY = "segueToAccountHistory"
let CELLACCOUNTHISTORY = "AccountHistoryCell"

//Extra

let FILEHELP = "help"
let FILESUPPORT = "support"
let FILEPRIVACYTERMS = "privacy"
let FILEABOUTUS = "aboutus"

let MSGINVALIDVERSION = "This version of Clink! is too old. Please download latest version to continue using Clink!"
let MSGVERIFYEMAIL = "Please verify your email address to continue using Clink!"

let SHOUDCOMMENT = false
let MAXIMAGESIZE = 2999999

//PUSH NOTIFICATION
let KTYPE = "type"
let KNOTIFICAITONTYPERECEIVE = "clink_receive"
let KNOTIFICAITONTYPELIKE = "clink_like"
let KNOTIFICAITONTYPECOMMENT = "clink_comment"
let KNOTIFICATIONTYPEBANKVERIFIED = "bank_verified"
let KNOTIFICATIONTYPEEMAILVERIFIED = "email_verified"

//NOTIFICATION
let GETDEFAULTPAYMENTMETHODDETAILS = "GetDefaultPaymentMethodDetails"
let RELOADPAYMENTMETHODS = "ReloadPaymentMethods"
let UPDATERECEIVEDCOUNT = "UpdateReceivedNotificationCount"
let UPDATESENTCOUNT = "UpdateSentNotificationCount"
let KNAVIGATETORECEIVEDLIST = "navigateToReceivedClinkListVC"
let KNAVIGATETOSENDLIST = "navigateToSentClinkListVC"
let TXTFETURECOMINGSOON = "This feature is coming soon"
let DESCTHANKYOU = "Thank you Clink!"
let TXTTESTCLINK = "This is test Clink!"
let STRUTC = "UTC"

//Animation Duration

let ANIMATIONDURATION = 0.3
let SPLASHANIMATIONDURATION = 1.2
let SPLASHDELAY : NSTimeInterval = 0.0
let COLORPICKERANIMATION : NSTimeInterval = 0.0
let LOGOUTACTIONDELAY : NSTimeInterval = 0.05
let UNDOACTIONDELAY : NSTimeInterval = 0.1

//Magic Numbers

let  CGFLOATZERO : CGFloat = 0.0
let FLOATZERO : Float = 0.0
let INTZERO : Int = 0
let FLOATONE : CGFloat = 1.0
let FLOATEMOJIWIDTH : CGFloat = 45.0
let FLOATAMOUNTWIDTH : CGFloat = 64.0
let FLOATMAXHEIGHT : CGFloat = 5000.0
let FLOATCOLORDEVIDER : CGFloat = 255.0
let DOUBLECOLORDEVIDER : Double = 255.0
let KEYBOARDMAXHEIGHT : CGFloat = 290.0
let LINESPACING : CGFloat = 1.8
let LINESPACINGTWO : CGFloat = 2.0
let LBLTOFROMWIDTH : CGFloat = 24.0
let LBLFROMWIDTH : CGFloat = 42.0
let BADGECORNERRADIOUS : CGFloat = 10.0
let LBLTOFROMHEIGHT : CGFloat = 21.0
let COLLECTIONHEIGHT : CGFloat = 21.0
let COMMENTHEIGHT : CGFloat = 17.0
let INTCOMMENTHEIGHT : Int = 17
let MENUIMAGEWIDTH : CGFloat = 36.0
let MENUCELLWITHIMAGEHEIGHT : CGFloat = 48.0
let MENUCELLNORMALHEIGHT : CGFloat = 33.0
let HOMECELLSUBSTRATOR : CGFloat = 200.0
let HOMECELLIPHONE6P : CGFloat = 516
let HOMECELLIPHONE6 : CGFloat = 418
let HOMECELLEXPECTEDHEIGHT : CGFloat = 467
let CLINKLISTCELL : CGFloat = 294
let COMMENTCELLWITHIMAGEHEIGHT : CGFloat = 48.0
let INTCOMMENTWIDTHTOSUBSTRACT : CGFloat = 98.0
let TXTVIEWMAXHEIGHT : Int32 = 70
let TXTVIEWMAXP1HEIGHT : Float = 71.0
let TXTVIEWMINP1HEIGHT : Float = 35.0
let TXTVIEWMINHEIGHTCONST : CGFloat = 60.0
let TXTVIEWMAXEXTRAHEIGHT : Float = 10.0
let COLLECTIONCCNAMEHEIGHT : CGFloat = 22.0
let VENUELISTCELLHEIGHT : CGFloat = 200.0
let VENUECATEGORYCELLHEIGHT : CGFloat = 65
let VENUECATEGORYCELLWIDTHSUBS : CGFloat = 50
let VENUEMENULISTCELLHEIGHT : CGFloat = 50.0
let VENUEMENUTITLECELLWIDTHSUBS : CGFloat = 100.0
let VENUEMENUTITLEHEIGHT : CGFloat = 35.0
let VENUEMENUTITLEXPOS : CGFloat = 8.0
let VENUEMENUTITLEYPOS : CGFloat = 5.0
let COLORPICKERWIDTHCONST : CGFloat = 222
let MAXXPOSCLORPICKER : CGFloat = 113.5
let MAXYPOSCLORPICKER : CGFloat = 19.5
let EDITINGTOOLHEIGHT : CGFloat = 45.0
let VENUEBUTTONHEIGHT : CGFloat = 43.0
let BTNSENDCLINK : CGFloat = 50.0
let EDITINGTOOLWIDTH : CGFloat = 225.0
let PRIVATEPOPUPCORNERRADIUS : CGFloat = 8.0
let FTUPOPUPCORNERRADIUS : CGFloat = 8.0
let HELPPOPUPCORNERRADIUS : CGFloat = 5.0
let CONTACTLISTCELLHEIGHT : CGFloat = 52.0
let CONTACTLISTCELLWIDTHSUBS : CGFloat = 48.0
let CONTACTLISTLABELXPOS : CGFloat = 24.0
let CONTACTLISTLABELYPOS : CGFloat = 8.0
let CONTACTLISTLABELHEIGHT : CGFloat = 25.0
let MAXCOMPRESSIONQUALITY : CGFloat = 1.0
let PHONEMAXLENTH : Int = 10
let KINTCONTACTMAX : Int = 5000

//ERROR CODE

let ERR202 : Int = 202
let ERR203 : Int = 203
let ERR209 : Int = 209
let ERR101 : Int = 101

//EDGE

let TXTVIEWCOMMENTINSET = UIEdgeInsetsMake(0, 4, 0, 5)
let TXTVIEWCOMMENTSCROLLINSET = UIEdgeInsetsMake(5, 2, 5, 0)
let BTNVENUEUIMAGEINSETDEFAULT = UIEdgeInsetsMake(0, 1,0,1)

let BTNVENUEUIMAGEINSETCUSTOME = UIEdgeInsetsMake(10, 120,10,10)

let BTNVENUEUTITLEINSETCUSTOME = UIEdgeInsetsMake(5,-35,5,5)
let BTNVENUEUIMAGEINSETZERO = UIEdgeInsetsMake(0, 0,0,0)

//QUEUE

let qualityOfServiceClass = QOS_CLASS_BACKGROUND
let BACKGROUNDQUEUE = dispatch_get_global_queue(qualityOfServiceClass, 0)


let KACCOUNT = "account"
let KACCOUNTTYPE = "account_type"
let KIAV = "iav"
let KISDEFAULTPAYMENT = "is_default_payment"
let KISDEFAULTTRANSFER = "is_default_transfer"
let KLASTFOUR = "last4"
let KPROCESSORUUID = "processor_uuid"

let CONTROLLERCLINKDETAIL = "ClinkDetailsController"
let CONTROLLERSELECTPAYMENTMETHOD = "SelectPaymentViewController"
let CONTROLLERSELECTPAYMENTMETHODFTU = "showFTUAccount"
let KPLACEHOLDER = "placeholder"
let CONTROLLERCONFIRMPASSWORDPOPUP = "ConfirmPasswordPopUpView"
let CONTROLLERSAVEPROFILEPOPUP = "SaveProfilePopUpView"
let CONTROLLERDEFAULTPAYMENTPOPUP = "DefaultPaymentMethodPopUp"
let KPAYMENTMETHODDETAILS = "paymentMethodDetails"
let CELLACCOUNTSWITCH = "AccountCellSwitch"
let CELLACCOUNTSWITCHICON = "AccountCellSwitchIcon"
let SEGUEADDPAYPAL = "segueaddpaypal"
let SEGUEPAYMENTMETHOD = "seguepaymentmethod"
let CARDMAXLENGTH : Int = 16
let KTRANSFERDETAILSBANK = "transferDetailsBank"
let KTRANSFERDETAILSDEBITCARD = "transferDetailsDebitCard"
let ALERTFILLALLFIELDS = "Please enter required fields."
let KPAYMENTDETAILS = "paymentDetails"
let KCLINKACCOUNTUUID = "clink_account_uuid"
let KDETAIL = "detail"
let CELLPAYMENTMETHOD = "PaymentMethodCell"

let PUSHNOTIFICATIONON = "PushNotificationOn"
