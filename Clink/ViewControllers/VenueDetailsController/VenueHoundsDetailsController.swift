//
//  VenueHoundsDetailsController.swift
//  Clink
//
//  Created by Gunjan on 18/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource
import HDCollectionDataSource

class VenueHoundsDetailsController: UIViewController,UICollectionViewDelegate,UIWebViewDelegate {
    
    //    MARK:- Gloable variable
    var isVenueSelecting : Bool!
    var objSelectedVenue : ClsVenue!
    var arrVendorMenu : [ClsVendorMenu]!
    var objPromoDetails : ClsPromotion!
    
    //    MARK:- IBOutlet
    @IBOutlet var btnBack : UIButton!
    @IBOutlet weak var webviewLoader: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var promoViewHeightConst : NSLayoutConstraint!
    @IBOutlet var imgBanner : UIImageView!
    @IBOutlet var imgHondsBG : UIImageView!
    @IBOutlet var imgPromoBanner : UIImageView!
    @IBOutlet var webViewDetails : UIWebView!
    @IBOutlet var btnAddPromo : UIButton!
    
     // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil && objSelectedVenue != nil{
            let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.PartnerDetails.rawValue ,"merchantId" : objSelectedVenue.strName]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
        }

        SideMenuRootController.rightSwipeEnabled = false
        arrVendorMenu = Array()
        if self.objSelectedVenue.strName == VENUEHOUNDSPUBSMALL{
            self.imgHondsBG.image = UIImage(named:IMGHOUNDSBG)
            self.imgHondsBG.backgroundColor = UIColor.whiteColor()
        }
        else{
            self.imgHondsBG.image = UIImage(named:NOIMAGE)
            self.imgHondsBG.backgroundColor = UIColor(red: 231.0/255.0, green: 231.0/255.0, blue: 231.0/255.0, alpha: 1)
        }
        if isVenueSelecting == nil{
            isVenueSelecting = false
        }
        self.activityIndicator.hidesWhenStopped = true
        self.webviewLoader.hidesWhenStopped = true
        if objPromoDetails != nil && objPromoDetails.strId == objSelectedVenue.arrPromotions.first?.strId{
            btnAddPromo.selected = true
        }
        else{
            btnAddPromo.selected = false
        }
        if let merchantBannerUrl = objSelectedVenue.strImgScreen{
            self.activityIndicator.startAnimating()
            self.imgBanner.sd_setImageWithURL(NSURL(string: merchantBannerUrl), placeholderImage: nil, completed: { (image, error, SDImageCacheType, url) -> Void in
                if image != nil{
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        self.getPromotionalData()
    }
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSENDNEWCLINK{
            let objNewClinkVC = segue.destinationViewController as! SendNewClinkController
            objNewClinkVC.objSelectedVenue = self.objSelectedVenue
            objNewClinkVC.objPromoDetails = self.objPromoDetails
            objNewClinkVC.shouldPopToVenue = true
        }
    }
    
    // MARK: - Custom Methods
    
    
    func getPromotionalData(){
        
        if objSelectedVenue.arrPromotions.count > 0 && objSelectedVenue.arrPromotions.first?.strId != nil{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetPromotions((objSelectedVenue.arrPromotions.first?.strId)!).URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController: self) { (success, response) -> () in
                if success == true{
                    let dicResponse = response as! [String : AnyObject]
                    dicResponse.nullsRemoved
                    self.objPromoDetails = self.objSelectedVenue.arrPromotions.first
                    self.setUpPromoData(dicResponse)
                }
            }
        }
    }
    
    func setUpPromoData(dicResponse : [String : AnyObject]){
        if let promoUrl = dicResponse.getValueIfAvilable("promo_url"){
            webViewDetails.loadRequest(NSURLRequest(URL: NSURL(string: promoUrl as! String)!, cachePolicy: NSURLRequestCachePolicy.ReturnCacheDataElseLoad, timeoutInterval: 30))
        }
        if let promoImgUrl = dicResponse.getValueIfAvilable("promo_image"){
            self.activityIndicator.startAnimating()
            self.imgPromoBanner.sd_setImageWithURL(NSURL(string: promoImgUrl as! String), placeholderImage: nil, completed: { (image, error, SDImageCacheType, url) -> Void in
                if image != nil{
                    self.activityIndicator.stopAnimating()
                }
            })
        }
        
    }
    
    // MARK: - Webview delegate
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        webviewLoader.stopAnimating()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        webviewLoader.stopAnimating()
    }
    func webViewDidStartLoad(webView: UIWebView) {
        webviewLoader.startAnimating()
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    @IBAction func btnAddPromo(sender: UIButton) {
        
        if isVenueSelecting == true{
            if sender.selected == false{
                sender.selected = !sender.selected
                self.objPromoDetails = self.objSelectedVenue.arrPromotions[0]
            }
            self.performSegueWithIdentifier(UNWINDTOSENDNEWCLINK, sender: sender)
        }
        else{
            self.performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: nil)
        }
    }
    
    @IBAction func btnClosePromo(sender: AnyObject) {
        UIView.animateWithDuration(ANIMATIONDURATION) { () -> Void in
            self.promoViewHeightConst.constant = CGFLOATZERO
        }
    }
}
