//
//  VenueDetailsController.swift
//  Clink
//
//  Created by Gunjan on 05/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource
import HDCollectionDataSource

class VenueDetailsController: UIViewController,UIWebViewDelegate {
    
    //    MARK:- Gloable variable
    var isVenueSelecting : Bool!
    var objSelectedVenue : ClsVenue!
    var isAlreadySelected : Bool!
    var strSelctedVenue : String!
    
    //    MARK:- IBOutlet
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnContribute : UIButton!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblURL : UILabel!
    @IBOutlet var imgBanner : UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var webViewDetails : UIWebView!
    @IBOutlet weak var webviewLoader: UIActivityIndicatorView!
    
     // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil && objSelectedVenue != nil{
            let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.PartnerDetails.rawValue ,"merchantId" : objSelectedVenue.strName]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
        }
        
        SideMenuRootController.rightSwipeEnabled = false
        if isVenueSelecting == nil{
            isVenueSelecting = false
        }
        
        if strSelctedVenue != nil{
            if strSelctedVenue == objSelectedVenue.strName{
                btnContribute.setImage(UIImage(named: IMGICNCONTRIBUTEDISABLED), forState: .Normal)
                btnContribute.userInteractionEnabled = false
            }
        }
        self.activityIndicator.hidesWhenStopped = true
        self.webviewLoader.hidesWhenStopped = true
        self.setUpData()
    }
    
    override func viewWillAppear(animated: Bool) {
//        SideMenuRootController.rightSwipeEnabled = false
    }
    
    override func viewDidDisappear(animated: Bool) {
//        SideMenuRootController.rightSwipeEnabled = true
    }
    
    // MARK: - Webview delegate
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        webviewLoader.stopAnimating()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        webviewLoader.stopAnimating()
    }
    func webViewDidStartLoad(webView: UIWebView) {
        webviewLoader.startAnimating()
    }
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSENDNEWCLINK{
            let objNewClinkVC = segue.destinationViewController as! SendNewClinkController
            objNewClinkVC.objSelectedVenue = self.objSelectedVenue
            objNewClinkVC.objToContact = self.objSelectedVenue.objDefaultContact
            objNewClinkVC.shouldPopToVenue = true
        }
    }
    
    // MARK: - Custom Methods
    func setUpData(){
        
        if objSelectedVenue != nil{
            
            lblTitle.text = objSelectedVenue.strName
            lblURL.text = objSelectedVenue.strURL
            self.view.layoutIfNeeded()
            
//            if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEDRAMALEAGUE){
//                imgBanner.image = UIImage(named: IMGBANNERDRAMA)
//            }
//            else if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEHORACEMANN){
//                imgBanner.image = UIImage(named: IMGBANNERHM)
//            }
            
//            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetAllPromotion().URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
//                
//                if success == true{
//                    print("Success")
//                }
//                else{
//                    print("Error")
//                }
//                
//            })
        }
        
        self.getPromotionalData()
    }
    
    func getPromotionalData(){
        
        if objSelectedVenue.arrPromotions.count > 0 && objSelectedVenue.arrPromotions.first?.strId != nil{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetPromotions((objSelectedVenue.arrPromotions.first?.strId)!).URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController: self) { (success, response) -> () in
                
                if success == true{
                    let dicResponse = response as! [String : AnyObject]
                    self.setUpPromoData(dicResponse)
                }
            }
        }
    }
    
    func setUpPromoData(var dicResponse : [String : AnyObject]){
        dicResponse = dicResponse.nullsRemoved
        if let promoUrl = dicResponse.getValueIfAvilable("promo_url"){
            webViewDetails.loadRequest(NSURLRequest(URL: NSURL(string: promoUrl as! String)!, cachePolicy: NSURLRequestCachePolicy.ReturnCacheDataElseLoad, timeoutInterval: 30))
        }
        if let promoImage = dicResponse.getValueIfAvilable("promo_image"){
            self.activityIndicator.startAnimating()
            self.imgBanner.sd_setImageWithURL(NSURL(string: promoImage as! String), placeholderImage: nil, completed: { (image, error, SDImageCacheType, url) -> Void in
                if image != nil{
                    self.activityIndicator.stopAnimating()
                }
            })
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnAddVenue(sender: AnyObject) {
        if isVenueSelecting == true{
            self.performSegueWithIdentifier(SEGUETOSAVECLINK, sender: nil)
        }
        else{
            self.performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: nil)
        }
    }
    
    @IBAction func btnVenueURLCliked(sender: AnyObject) {

    }
}
