//
//  AddGiftController.swift
//  Clink
//
//  Created by Gunjan on 17/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class AddGiftController: UIViewController {
    
    var objSelectedClinkDetail : ClsClinkDetails!
    var imgSelected : UIImage!
    var selectedClinkType : ClinkType!
    var intAmount = INTZERO
    
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var btnVenue : UIButton!
    @IBOutlet var btnAmount : UIButton!
    @IBOutlet var btnSendClink : UIButton!
    @IBOutlet var btnTo : UIButton!
    @IBOutlet var btnCC : UIButton!
    @IBOutlet var btnBack : UIButton!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setUpUI()
    }
    override func viewWillAppear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = false
    }
    
    override func viewDidDisappear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpUI(){
        if imgSelected != nil
        {
            imgView.image = imgSelected
        }
        if(objSelectedClinkDetail != nil){
            btnTo.setTitle(objSelectedClinkDetail.strSenderName, forState: .Normal)
           
            /*if objSelectedClinkDetail.strVenueName.isEqualToIgnoringCase(VENUEDRAMALEAGUE) {
                btnVenue.setImage(UIImage(named: IMGBTNDL), forState: UIControlState.Normal)
                btnSendClink.setImage(UIImage(named: IMGBTNDONATESMALL), forState: UIControlState.Normal)
            }
            else if objSelectedClinkDetail.strVenueName.isEqualToIgnoringCase(VENUEHORACEMANN){
                btnVenue.setImage(UIImage(named: IMGBTNHM), forState: UIControlState.Normal)
                btnSendClink.setImage(UIImage(named: IMGBTNDONATESMALL), forState: UIControlState.Normal)
            }
            else if objSelectedClinkDetail.strVenueName.isEqualToIgnoringCase(VENUEHOUNDSPUB){
                btnVenue.setImage(UIImage(named: IMGBTNHP), forState: UIControlState.Normal)
                btnSendClink.setImage(UIImage(named: IMGBTNSENDCLINKSMALL), forState: UIControlState.Normal)
            }
            else{
                btnVenue.setImage(UIImage(named: IMGBTNVENUE), forState: UIControlState.Normal)
                btnSendClink.setImage(UIImage(named: IMGBTNSENDCLINKSMALL), forState: UIControlState.Normal)
            }
            */
//            if objSelectedClinkDetail.intAmount > INTZERO
//            {
//                btnAmount.selected = true
//                btnAmount.setTitle("$ \(objSelectedClinkDetail.intAmount) +", forState:
//                    UIControlState.Normal)
//                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString(btnAmount.titleLabel?.text), forState: .Normal)  
//
//            }
//            else{
//                btnAmount.selected = true
//                btnAmount.setTitle("$ \(objSelectedClinkDetail.intAmount) +", forState:
//                    UIControlState.Normal)
//                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString(btnAmount.titleLabel?.text), forState: .Normal)  
//
//            }
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOAMOUNT{
            let objAmountVC = segue.destinationViewController as! AmountInputController
            objAmountVC.intAmount = intAmount
            objAmountVC.isGifting = true
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackCliked(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnAddGiftCliked(sender: AnyObject) {
        if objSelectedClinkDetail != nil{
             Helper.showHud(self.view)
//            let currentUser = USERDEFAULTS.boolForKey(kISUSERLOGGEDIN)
//            var currentUserName = PFUser.currentUser()?.objectForKey(KFIRSTNAME) as! String
//            let strLastName = PFUser.currentUser()?.objectForKey(KLASTNAME) as! String
//            currentUserName = currentUserName + " " + strLastName
            
//            let clinkId = objSelectedClinkDetail.strId;
            
            var arrUserToNotify : [String] = Array()
            if objSelectedClinkDetail.strSenderId != nil{
                arrUserToNotify.append(objSelectedClinkDetail.strSenderId)
            }
            if objSelectedClinkDetail.strReceiverId != nil{
                arrUserToNotify.append(objSelectedClinkDetail.strReceiverId)
            }
      
            /*ClinkDetailsManager.sharedInstance.addGiftToClink(clinkId, pUserId: currentUser!, pUserName: currentUserName, pUsersToNotify: arrUserToNotify, pAdditionalAmount: intAmount, pCompletionBlock: { (success, response) -> () in
            if success == true{
                Helper.hideHud(self.view)
                self.navigationController?.popViewControllerAnimated(false)
            }
            else{
                Helper.hideHud(self.view)
                let error = response as! NSError
                Helper.showAlert(error.localizedDescription, pobjView: self.view)
            }
        })*/
    }
    }
    
    @IBAction func btnEnterAmountCliked(sender: AnyObject) {
        self.performSegueWithIdentifier(SEGUETOAMOUNT, sender: nil)
    }
    @IBAction func saveToGiftVC(segue:UIStoryboardSegue) {
        if(segue.sourceViewController.isKindOfClass(AmountInputController))
        {
            let objAmountInputVC = segue.sourceViewController as! AmountInputController
            intAmount = Int(objAmountInputVC.lblAmount.text!)!
            if intAmount > INTZERO {
                btnAmount.selected = true
                btnAmount.setTitle("$ \(objSelectedClinkDetail.strAmount) + $ " + objAmountInputVC.lblAmount.text!, forState:
                    UIControlState.Normal)
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(objSelectedClinkDetail.strAmount) + $ " + objAmountInputVC.lblAmount.text!,withColor: KFONTTHEMENEW), forState: .Normal)
            }
            else{
                btnAmount.selected = false
                btnAmount.setTitle("$ \(objSelectedClinkDetail.strAmount) + $", forState:
                    UIControlState.Normal)
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(objSelectedClinkDetail.strAmount) + $ " + objAmountInputVC.lblAmount.text!,withColor: KFONTTHEMENEW), forState: .Normal)

            }
        }
    }
}
