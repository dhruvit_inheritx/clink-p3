//
//  AccountHistoryController.swift
//  Clink
//
//  Created by Dhruvit on 05/12/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

enum AccountHistoryControllerType : Int {
    case Redeemed = 1, Saved = 2, Sent = 3, Donations = 4, ClinkAccount = 5, CashOut = 6
}

class AccountHistoryController: UIViewController,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var txtSearch : UITextField!
    @IBOutlet var tblAccountHistory : UITableView!
    @IBOutlet var lblNotFound : UILabel!
    
    var objHDTableDataSource : HDTableDataSource!
    
    var arrayHistory : [AnyObject] = Array()
    var arraySection : [AnyObject] = Array()
    
    var intCurrentIndex : Int!
    var intCurrentPage : Int = 1
    var intLastPage : Int! = 0
    
    var accountHistoryControllerType : AccountHistoryControllerType!
    
    var strFilterType = ""
    var dicToPass : [String : AnyObject!] = Dictionary()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        strFilterType = "saved"
        
        if accountHistoryControllerType == .Saved {
            lblTitle.text = "CLINK! SAVED"
//            arrayHistory = arraySaved
            strFilterType = "saved"
        }
        else if accountHistoryControllerType == .Sent {
            lblTitle.text = "CLINK! SENT"
//            arrayHistory = arraySent
            strFilterType = "sent"
            dicToPass[KFILTERSMIN] = 1
        }
        else if accountHistoryControllerType == .Donations {
            lblTitle.text = "DONATIONS"
//            arrayHistory = arrayDonations
            strFilterType = "donations"
            dicToPass[KFILTERSMIN] = 1
        }
        else if accountHistoryControllerType == .ClinkAccount {
            lblTitle.text = "CLINK! ACCOUNT"
            //            arrayHistory = arrayClinkAccount
            strFilterType = "account"
            dicToPass[KFILTERSMIN] = 1
        }
        else if accountHistoryControllerType == .CashOut {
            lblTitle.text = "CASH OUT"
            //            arrayHistory = arrayCashOut
            strFilterType = "cashout"
        }
        else {
            lblTitle.text = "CLINK! REDEEMED"
            //            arrayHistory = arrayRedeemed
            strFilterType = "redeemed"
        }
        dicToPass[KFILTERSTPES] = strFilterType

        
        tblAccountHistory.rowHeight = UITableViewAutomaticDimension
        tblAccountHistory.rowHeight = 50
        tblAccountHistory.separatorStyle = .None
        tblAccountHistory.tableFooterView = UIView()
        tblAccountHistory.backgroundColor = UIColor.groupTableViewBackgroundColor()
        self.view.backgroundColor = UIColor.groupTableViewBackgroundColor()
        self.getClinkTransactionDetails(strFilterType, pRecordsPerPage: RECORDSPERPAGE, pCurrentPage: self.intCurrentPage)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func configureTableView(){
        
        objHDTableDataSource = HDTableDataSource(items: arrayHistory, cellIdentifier: CELLACCOUNTHISTORY, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! AccountHistoryCell
            
            if self.accountHistoryControllerType == AccountHistoryControllerType.CashOut || self.accountHistoryControllerType == AccountHistoryControllerType.ClinkAccount {
                let objItem = item as! ClsTransaction
                objCell.selectionStyle = .None
                objCell.configureCashOutAndAccountCell(objItem,enumAccountType: self.accountHistoryControllerType)
            }
            else {
                let objItem = item as! ClsClinkDetails
                objCell.selectionStyle = .None
                objCell.configureCell(objItem,enumAccountType: self.accountHistoryControllerType)
            }
        })
        
        objHDTableDataSource.sectionItemBlock = {(objSection:AnyObject!) -> [AnyObject]! in
            
            if self.accountHistoryControllerType == AccountHistoryControllerType.CashOut {
                let dicSection = objSection as! [ClsTransaction]
                return dicSection
            }
            else {
                let dicSection = objSection as! [ClsClinkDetails]
                return dicSection
            }
        }
        
        objHDTableDataSource.arrSections = arrayHistory
        tblAccountHistory.dataSource = objHDTableDataSource
        tblAccountHistory.delegate = self
        tblAccountHistory.reloadData()
    }
    
    // MARK: - Custom Methods
    func loadMoreClinks(){
        intCurrentPage += 1
        dicToPass[KFILTERSTPES] = strFilterType
        self.getClinkTransactionDetails(strFilterType, pRecordsPerPage: RECORDSPERPAGE, pCurrentPage: self.intCurrentPage)
    }
    
    func getClinkTransactionDetails(strFilterType : String , pRecordsPerPage : Int , pCurrentPage : Int) {
        
        var requestParam : [String : AnyObject] = [KFILTERSTPES :strFilterType,KPERPAGE : "\(pRecordsPerPage)", KPAGE : "\(pCurrentPage)"]

        if accountHistoryControllerType == AccountHistoryControllerType.Sent {
            requestParam["min"] = "1"
        }
        
        if accountHistoryControllerType == AccountHistoryControllerType.Redeemed {
            
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.getClinkRedeemHistory(requestParam).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) in
                if success == true
                {
                    let dicResponse = response as! [String : AnyObject]
                    self.intLastPage = dicResponse.getValueIfAvilable(KLASTPAGE) as? Int
                    if let arrClinks = dicResponse[KCLINK] as? [AnyObject]
                    {
                        
                        for objClinkDetails in arrClinks {
                            let objClinkDetail = ClsClinkDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                            
                            self.arrayHistory.append([objClinkDetail])
                        }
                        self.lblNotFound.hidden = true
                    }
                    if self.arrayHistory.count <= 0{
                        self.arrayHistory.removeAll()
                        self.lblNotFound.hidden = false
                    }
                    self.configureTableView()
                }
                else{
//                    self.arrayHistory.removeAll()
//                    self.lblNotFound.hidden = false
//                    self.configureTableView()
                }
            }
        }
        else if accountHistoryControllerType == AccountHistoryControllerType.Donations || accountHistoryControllerType == AccountHistoryControllerType.Saved || accountHistoryControllerType == AccountHistoryControllerType.Sent {
            
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.getClinkAccountHistory(requestParam).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) in
                if success == true
                {
                    let dicResponse = response as! [String : AnyObject]
                    self.intLastPage = dicResponse.getValueIfAvilable(KLASTPAGE) as? Int
                    if let arrClinks = dicResponse[KCLINK] as? [AnyObject]
                    {
                        for objClinkDetails in arrClinks {
                            let objClinkDetail = ClsClinkDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                            
                            self.arrayHistory.append([objClinkDetail])
                        }
                        self.lblNotFound.hidden = true
                    }
                    if self.arrayHistory.count <= 0{
                        self.arrayHistory.removeAll()
                        self.lblNotFound.hidden = false
                    }
                    self.configureTableView()
                }
                else{
//                    self.arrayHistory.removeAll()
//                    self.lblNotFound.hidden = false
//                    self.configureTableView()
                }
            }
        }
        else {
            
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.getClinkTransactionHistory(requestParam).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) in
                if success == true
                {
                    let dicResponse = response as! [String : AnyObject]
                    self.intLastPage = dicResponse.getValueIfAvilable(KLASTPAGE) as? Int
                    if let arrClinks = dicResponse["transaction"] as? [AnyObject]
                    {
                        for objTransactionDetails in arrClinks {
                            let objTransactionDetail = ClsTransaction(pdictResponse: objTransactionDetails as! [String : AnyObject])
                            self.arrayHistory.append([objTransactionDetail])
                        }
                        self.lblNotFound.hidden = true
                    }
                    if self.arrayHistory.count <= 0{
                        self.arrayHistory.removeAll()
                        self.lblNotFound.hidden = false
                    }
                    self.configureTableView()
                }
                else{
//                    self.arrayHistory.removeAll()
//                    self.lblNotFound.hidden = false
//                    self.configureTableView()
                }
            }
        }
    }
    // MARK: - TableView Delegate
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.accountHistoryControllerType == AccountHistoryControllerType.Saved || self.accountHistoryControllerType == AccountHistoryControllerType.Sent {
            
            let storyboard = UIStoryboard(name: MAINSTORYBOARD, bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERCLINKDETAIL) as? ClinkDetailsController
            let arrClinks : [ClsClinkDetails] = arrayHistory[indexPath.section] as! [ClsClinkDetails]
            vc?.objSelectedClinkDetail = arrClinks[indexPath.row]
            vc?.isFromHistoryScreen = true
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        else {
            Helper.showCommingsoonAlert()
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
        
        if indexPath.section == (arrayHistory.count - 1) && intCurrentPage < intLastPage {
            
            self.loadMoreClinks()
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        let lblTitle = UILabel()
        lblTitle.frame = CGRectMake(20, 1, UIScreen.mainScreen().bounds.size.width - 40, 25)
        lblTitle.font = UIFont.systemFontOfSize(FONTSIZE13)
        
        if accountHistoryControllerType == AccountHistoryControllerType.CashOut || accountHistoryControllerType == AccountHistoryControllerType.ClinkAccount {
            let arrClinkDetails = arrayHistory[section] as! [ClsTransaction]
            let objClinkDetails = arrClinkDetails.first
            lblTitle.text = "\(objClinkDetails!.strCreatedAt)"
            let dateCreated = objClinkDetails?.strCreatedAt.getLocalDate(PHASE3FORMATE)
            lblTitle.text = Helper.getDateDifferenceforDate(dateCreated!, andDate: NSDate())
        }
        else {
            let arrClinkDetails = arrayHistory[section] as! [ClsClinkDetails]
            let objClinkDetails = arrClinkDetails.first
            lblTitle.text = "\(objClinkDetails!.strCreatedDate)"
            let dateCreated = objClinkDetails?.strCreatedDate.getLocalDate(PHASE3FORMATE)
            lblTitle.text = Helper.getDateDifferenceforDate(dateCreated!, andDate: NSDate())
        }
        
//        lblTitle.text = "Date"
        lblTitle.textColor = UIColor.darkGrayColor()

        viewHeader.addSubview(lblTitle)

        return viewHeader
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 25
    }
    
    @IBAction func backClicked() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
