//
//  ClinkBalanceViewController.swift
//  Clink
//
//  Created by Dhruvit on 16/09/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClinkBalanceViewController: UIViewController {

    @IBOutlet weak var balanceView: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblInfo: UILabel!

    var objUserDetail : ClsUserDetails!
    var objTransferDetail : ClsCreditCardDetails = ClsCreditCardDetails()
    var arrayTransferMethodDetails : [ClsCreditCardDetails] = []

    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        
        objUserDetail = USERDEFAULTS.getUserDetailsObject()
        lblAmount.text = objUserDetail.strBalance//.removeZerosAfterDecimal()
        lblAmount.text = lblAmount.text?.stringByReplacingOccurrencesOfString(",", withString: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    // MARK: - Get All Bank
    func getAllBank(){
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchBank(["search":""]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        if objCreditCardDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultTransferDetailsObject(objCreditCardDetail, forKey: KTRANSFERDETAILSBANK)
                        }
                        
                        arrayCreditCardDetails.append(objCreditCardDetail)
                    }
                }
                
                if arrayCreditCardDetails.count > 0{
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        self.arrayTransferMethodDetails.append(objCreditCardDetail)
                    }
                }
                self.getPaymentDetail()
            }
            else{
                self.getPaymentDetail()
            }
        }
    }
    
    func getPaymentDetail(){
        
        objTransferDetail = USERDEFAULTS.getDefaultTransferDetailsObjectForKey(KTRANSFERDETAILSBANK)
        
        if objTransferDetail.strId == "" {
            if self.arrayTransferMethodDetails.count <= 0 {
                Helper.showAlert("No Account found to transfer CLINK! balance. Please, add Account first.", pobjView: self.view)
            }
            else
            {
                for objPayment in self.arrayTransferMethodDetails {
                    
                    objTransferDetail = objPayment
                    
                    USERDEFAULTS.setDefaultPaymentDetailsObject(objTransferDetail)
                    USERDEFAULTS.synchronize()
                }
                
                self.performSegueWithIdentifier(SEGUECASHOUTCLINKBALANCE, sender: self)
            }
        }
        else {
            self.performSegueWithIdentifier(SEGUECASHOUTCLINKBALANCE, sender: self)
        }
    }
    
    @IBAction func cashOut(sender: AnyObject) {
        
        objTransferDetail = USERDEFAULTS.getDefaultTransferDetailsObjectForKey(KTRANSFERDETAILSBANK)
        
        if objTransferDetail.strId == "" {
            self.getAllBank()
        }
        else {
            self.performSegueWithIdentifier(SEGUECASHOUTCLINKBALANCE, sender: self)
        }
    }
    
    @IBAction func cancelClicked(sender: AnyObject) {
        
        objTransferDetail = USERDEFAULTS.getDefaultTransferDetailsObjectForKey(KTRANSFERDETAILSBANK)

        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == SEGUECASHOUTCLINKBALANCE {
            let objCashOutVC = segue.destinationViewController as! CashOutViewController
            objCashOutVC.obkTransferDetail = self.objTransferDetail
        }
    }
    
}
