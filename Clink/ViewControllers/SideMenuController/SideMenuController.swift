
//
//  SideMenuController.swift
//  Clink
//
//  Created by Gunjan on 03/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

class SideMenuController: UIViewController, UITableViewDelegate {

     //    MARK:- Gloable variable
    var objHDTableDataSource:HDTableDataSource!
    var arrMenuGroups:[AnyObject]!
    weak var objSelectedMenu:ClsMenu!
    var intSelectedRow : Int = INTZERO
    var selectedScreenType : screenType!
    var previousSelectedIndex : NSIndexPath!
    
    //    MARK:- IBOutlet
    @IBOutlet var lblUsernName: UILabel!
    @IBOutlet var tblMenu: UITableView!
    @IBOutlet var lblVersion: UILabel!
    
    // MARK: - Viewcontroller Cycle
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        UIApplication.sharedApplication().statusBarHidden = true
        
        self.configureName()
        APPDELEGATE.objSideMenu = self
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
//        UIApplication.sharedApplication().statusBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeOnce()
        // Do any additional setup after loading the view.
    }
    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
    
    // MARK: - Initializations
    
    func initializeOnce(){
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        if arrMenuGroups == nil {
            arrMenuGroups = Array()
        }
        self.configureName()
        addStaticData()
        configureTable()
        
        lblVersion.text = APPDELEGATE.getVersionString()
        
        NOTIFICATIONCENTER.addObserver(self, selector: #selector(SideMenuController.getUserProfile), name: UPDATERECEIVEDCOUNT, object: nil)
        
//        NOTIFICATIONCENTER.addObserver(self, selector: "getUserProfile", name: UPDATESENTCOUNT, object: nil)
        
    }
    
    func configureName(){
        if (USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true){
            
            let currentUserFName = USERDEFAULTS.getUserDetailsObject().strFirstName
            let currentUserLName = USERDEFAULTS.getUserDetailsObject().strLastName
            lblUsernName.text = currentUserFName + " " + currentUserLName
        }
    }
    
    func addStaticData(){
        prepareFirstSection()
        self.setMenuSelectedItem(0)
    }
    
    func setMenuSelectedItem(intIndex : Int){
        
        intSelectedRow = intIndex
        let objMenuGroup = arrMenuGroups[0] as! ClsMenuGroup
        
        let arrFilteredGroup = objMenuGroup.arrMenus.filter {
            $0.isSelected.boolValue == true
        }
        arrFilteredGroup.first?.isSelected = false
        previousSelectedIndex = NSIndexPath(forRow: intIndex, inSection: 0)
        let objMenu = objMenuGroup.arrMenus[intIndex]
        objMenu.isSelected = true
        objSelectedMenu = objMenu
        tblMenu.reloadData()
    }
    
    func prepareFirstSection(){
    
        let objMenuGroup = ClsMenuGroup()
        objMenuGroup.arrMenus = Array()
        
        var objMenu = ClsMenu(pstrTitle: "HOME", pstrImageName: IMGICONMENUFEED, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showHomeScreen))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "SEND CLINK!", pstrImageName: "icn_menu_Send", pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showSendNewClink))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "RECEIVED", pstrImageName: "icn_menu_Received", pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showReceivedClink))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "SENT", pstrImageName: "icn_menu_Sent", pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showSentClink))
        objMenuGroup.arrMenus.append(objMenu)
        
        let timer = NSTimer.scheduledTimerWithTimeInterval(120, target: self, selector: #selector(SideMenuController.getUserProfile), userInfo: nil, repeats: true)
        timer.fire()
        
        
        objMenu = ClsMenu(pstrTitle: "Contacts", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showMyContacts))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Partners", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showVenues))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Promotions", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showCommingsoonAlert as (SideMenuController) -> () -> ()))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Account", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showAccount))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Help", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showHelp))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Support", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showSupport))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "Privacy, T & C", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showTermsCondition))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "About us", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.showAbooutUs))
        objMenuGroup.arrMenus.append(objMenu)
        
        objMenu = ClsMenu(pstrTitle: "LOGOUT", pstrImageName: STREMPTY, pintNotificationCount:INTZERO, pactionMehod: #selector(SideMenuController.logoutUser))
        objMenuGroup.arrMenus.append(objMenu)
        
        arrMenuGroups.append(objMenuGroup)
    }
  
    
    func configureTable(){
        
        objHDTableDataSource = HDTableDataSource(items: nil, cellIdentifier: "SideMenuCell", configureCellBlock: { (cell, item, indexPath) -> Void in
           
            let objTmpGroup = self.arrMenuGroups[indexPath.section] as! ClsMenuGroup
            let objTmpMenu = objTmpGroup.arrMenus[indexPath.row]
            let objCell:SideMenuCell = cell as! SideMenuCell
            
            objCell.configureCell(objTmpMenu)
            if indexPath.row < 4 || indexPath.row == (objTmpGroup.arrMenus.count - 1){
                if(objTmpMenu.isSelected == true){
                    objCell.lblMenuTitle.textColor = kCellSelectionLabelColor
                    objCell.backgroundColor = kCellSelectionColor
                    objCell.imgMenuBG.hidden = true
                    let strImageName = objTmpMenu.strImageNameSuffix + STRSELECTEDIMAGE
                    objCell.imgMenuIcon.image = UIImage(named: strImageName)
                }
                else{
                     objCell.lblMenuTitle.textColor = kCellSelectionColor
                    objCell.backgroundColor = kCellSelectionLabelColor
                    objCell.imgMenuBG.hidden = false
                }
                objCell.lblNotificationCount.textColor = objCell.lblMenuTitle.textColor
                objCell.imgMenuIcon.hidden = false
                objCell.imgMenuIconConst.constant = MENUIMAGEWIDTH
            
                if indexPath.row == (objTmpGroup.arrMenus.count - 1){
                    objCell.imgMenuIcon.hidden = true
                    objCell.imgMenuBG.hidden = true
                    objCell.imgMenuIconConst.constant = CGFLOATZERO
                    objCell.backgroundColor = UIColor.clearColor()
                    objCell.lblMenuTitle.textColor = LOGOUTCOLORTHEME
                }
                objCell.layoutIfNeeded()
            }
            else if indexPath.row >= 4 {
               
                objCell.backgroundColor = UIColor.clearColor()
                objCell.imgMenuIconConst.constant = CGFLOATZERO
                objCell.imgMenuIcon.hidden = true
                objCell.imgMenuBG.hidden = true
                if indexPath.row == 4{
                    objCell.imgMenuBG.hidden = false
                }
                objCell.layoutIfNeeded()
            }
        })
        
        objHDTableDataSource.sectionItemBlock = {(objSection:AnyObject!) -> [AnyObject]! in
            
            let objMenuGroup = objSection as! ClsMenuGroup
            return objMenuGroup.arrMenus
        }
        
        objHDTableDataSource.arrSections = self.arrMenuGroups
        tblMenu.dataSource = objHDTableDataSource
        tblMenu.reloadData()
    }
    
    // MARK: - Tableview Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (indexPath.row < 4)
        {
            tableView.cellForRowAtIndexPath(NSIndexPath(forRow: intSelectedRow, inSection: INTZERO))?.backgroundColor = kCellSelectionLabelColor
            tableView.cellForRowAtIndexPath(indexPath)!.backgroundColor = kCellSelectionColor
        }
        if (indexPath.row <= 5 || indexPath.row >= 7)
        {
//        if (indexPath.row < 1 || indexPath.row == 5 || indexPath.row >= 8)
//        {
            self.configSelection(indexPath,shouldPerformSelector :true)
            let objTmpGroup = self.arrMenuGroups[indexPath.section] as! ClsMenuGroup
            if indexPath.row != (objTmpGroup.arrMenus.count - 1) && indexPath.row != 7{
                previousSelectedIndex = indexPath
            }
        }
        else{
            self.configSelection(indexPath,shouldPerformSelector :false)
            let alertView = CustomAlert(title: APPNAME, message:TXTFETURECOMINGSOON, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
                self.configSelection(self.previousSelectedIndex,shouldPerformSelector :false)
            }
            alertView.showInView(APPDELEGATE.window!)
        }
        tableView.reloadData()
    }
    
    func configSelection(indexPath : NSIndexPath,shouldPerformSelector : Bool){
        let objMenuGroup = self.arrMenuGroups[0] as! ClsMenuGroup
        
        if indexPath.row != (objMenuGroup.arrMenus.count - 1)
        {
            let objTmpGroup = self.arrMenuGroups[indexPath.section] as! ClsMenuGroup
            let objTmpMenu = objTmpGroup.arrMenus[indexPath.row]

            if indexPath.row != 7{
                
                objSelectedMenu?.isSelected = false
                objSelectedMenu = objTmpMenu
                objSelectedMenu.isSelected = true
                
                intSelectedRow = indexPath.row
                self.tblMenu.reloadData()
            }
            if objTmpMenu.objSelector != nil && self.respondsToSelector(objTmpMenu.objSelector) == true {
                if (indexPath.row != 6 && shouldPerformSelector == true){
                    self.performSelector(objTmpMenu.objSelector)
                }                
            }
        }
        else{
            self.logoutUser()
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.row < 4){
            return MENUCELLWITHIMAGEHEIGHT
        }
        else{
            return MENUCELLNORMALHEIGHT
        }
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.row < 4){
            return MENUCELLWITHIMAGEHEIGHT
        }
        else{
            return MENUCELLNORMALHEIGHT
        }
    }

    
    // MARK: - Menu Events
    
    func showHomeScreen(){
        self.performMenuSegues(false, strSegueIdentifier: SEGUECENTERCONTAINMENT)
    }
    func showSentClink(){
        self.performMenuSegues(false, strSegueIdentifier: SEGUESHOWSENTRECIVEDCLINK)
    }
    func showReceivedClink(){
        self.performMenuSegues(true, strSegueIdentifier: SEGUESHOWSENTRECIVEDCLINK)
    }
    func showSendNewClink(){
        self.performMenuSegues(false, strSegueIdentifier:SEGUESENDNEWCLINK)
    }
    func showMyContacts(){
        self.performMenuSegues(false, strSegueIdentifier:SEGUETOMYCONTACT)
    }
    func showVenues(){
        self.performMenuSegues(false, strSegueIdentifier: SEGUETOVENUE)
    }
    func showAbooutUs(){
//        self.performMenuSegues(false, strSegueIdentifier: SEGUETOABOUTUS)
        USERDEFAULTS.setInteger(screenType.AboutUs.rawValue, forKey:KSELECTEDSCREENTYPE)
        USERDEFAULTS.synchronize()
        self.sideMenuController()?.performSegueWithIdentifier(SEGUETOTERMS, sender: nil)
    }
    func showSupport(){
        USERDEFAULTS.setInteger(screenType.Support.rawValue, forKey:KSELECTEDSCREENTYPE)
         USERDEFAULTS.synchronize()
        self.sideMenuController()?.performSegueWithIdentifier(SEGUETOTERMS, sender: nil)
    }
    
    func showAccount()
    {
        if INCLUDEACCOUNT == true{
            self.closeMenu()
//            self.setMenuSelectedItem(previousSelectedIndex.row)
            
            let storyboard = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERACCOUNTNAVIGATION) as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        else{
            self.showCommingsoonAlert()
        }
    }
    
    func showHelp()
    {
        USERDEFAULTS.setInteger(screenType.Help.rawValue, forKey:KSELECTEDSCREENTYPE)
        USERDEFAULTS.synchronize()
        self.sideMenuController()?.performSegueWithIdentifier(SEGUETOTERMS, sender: nil)
    }
    
    func showTermsCondition(){
        USERDEFAULTS.setInteger(screenType.Privacy.rawValue, forKey:KSELECTEDSCREENTYPE)
        USERDEFAULTS.synchronize()
        self.sideMenuController()?.performSegueWithIdentifier(SEGUETOTERMS, sender: nil)
        
    }
    
    @IBAction func closeMenu(){
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    func logoutUser(){
        
        let alertView = CustomAlert(title: APPNAME, message:ALERTCONFIRMLOGOUT, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNYESTITLE, otherButtonTitle: BTNCANCELTITLE) { (customAlert, buttonIndex) -> Void in
            
            if buttonIndex == INTZERO{
                //self.performSelector("performLogout", withObject: nil, afterDelay:LOGOUTACTIONDELAY)
                
                
                self.callLogoutWS()
            }
        }
        alertView.showInView(APPDELEGATE.window)
    }
    
    func callLogoutWS() {
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.LogoutUser().URLRequest, isBackgroundCall : false,pAlertView : APPDELEGATE.window!, pViewController: self, pCompletionBlock: { (success, result) -> () in
            if (success == true){
                
                print("Logout success \(result)")
                self.performSelector(#selector(SideMenuController.performLogout), withObject: nil, afterDelay:LOGOUTACTIONDELAY)
            }
            else if result != nil{
                Helper.showAlert("Failed to logout user. Please try again.", pobjView: APPDELEGATE.window)
            }
        })
    }
    
    func performLogout(){
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true {
            APPDELEGATE.isContactSyncedOnce = false
            APPDELEGATE.arrMainContacts.removeAll()
            ClinkAuthenticationManager.sharedInstance.logoutFromSocial()
            USERDEFAULTS.removeObjectForKey(kISUSERLOGGEDIN)
            USERDEFAULTS.removeObjectForKey(KUSERDETAILS)
            USERDEFAULTS.removeObjectForKey(KSESSIONID)
            USERDEFAULTS.removeObjectForKey(KCONTACTARRAY)
            USERDEFAULTS.removeObjectForKey(KISFTUHELPSHOWN)
            
            USERDEFAULTS.removeObjectForKey(KPAYMENTDETAILS)
            USERDEFAULTS.removeObjectForKey(KTRANSFERDETAILSDEBITCARD)
            USERDEFAULTS.removeObjectForKey(KTRANSFERDETAILSBANK)
            USERDEFAULTS.synchronize()
            self.setSentNotificationCount("0")
            self.setReceivedNotificationCount("0")
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                let objMenuGroup = self.arrMenuGroups[0] as! ClsMenuGroup
                let objMenuLast = objMenuGroup.arrMenus.last
                objMenuLast!.isSelected = false
                
                let objMenuSelection = objMenuGroup.arrMenus[self.intSelectedRow]
                objMenuSelection.isSelected = false
                
                let objMenu = objMenuGroup.arrMenus.first
                objMenu!.isSelected = true
                self.objSelectedMenu = objMenu
                self.tblMenu.reloadData()
                self.tblMenu.scrollToRowAtIndexPath(NSIndexPath(forRow:INTZERO, inSection: INTZERO), atScrollPosition: .Top, animated: false)
                
                self.showHomeScreen()
            })
        }
        else{
//            Helper.hideHud(APPDELEGATE.window!)
        }
    }
    
    // MARK: - Custom Methods
    func performMenuSegues(boolToSet : Bool , strSegueIdentifier : String){
        USERDEFAULTS.setBool(boolToSet, forKey: KRECEIVEDCLINKSELECTED)
        USERDEFAULTS.synchronize()
        self.sideMenuController()?.performSegueWithIdentifier(strSegueIdentifier, sender: nil)
    }
//    func setSentNotificationCount(){
//        self.getUserProfile()
//    }
    
    func getUserProfile(){
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetUserProfile().URLRequest, isBackgroundCall: true, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, result) -> () in
                if success == true{
                    let dicResult = result as! [String : AnyObject!]
                    let objUserDetails = ClsUserDetails(pdicResponse: dicResult)
                    let dicSession = objUserDetails.dicSession as ClsSessionDetails
                    if let sid = dicSession.id
                    {
                        ClinkDetailsRouter.SessionID = sid
                        USERDEFAULTS.setObject(sid, forKey: KSESSIONID)
                    }
                    USERDEFAULTS.setUserDetailsObject(objUserDetails)
                    USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                    USERDEFAULTS.synchronize()
                    
                    print("Received Count :- \(objUserDetails.intReceivedCount)")
                    
                    self.setReceivedNotificationCount("\(objUserDetails.intReceivedCount)")
                    self.setSentNotificationCount("\(objUserDetails.intSentCount)")
                }
            })
        }
    }
    
    func setSentNotificationCount(strCount:String){
        let objMenuGroup = self.arrMenuGroups[0] as! ClsMenuGroup
        let objMenu = objMenuGroup.arrMenus[3]
        objMenu.intNotificationCount = Int(strCount)!
        self.tblMenu.reloadRowsAtIndexPaths([NSIndexPath(forRow: 3, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
    }
    
//    func setReceivedNotificationCount()
//    {
//        self.getUserProfile()
//    }
    
    func setReceivedNotificationCount(strCount:String){
        let objMenuGroup = self.arrMenuGroups[0] as! ClsMenuGroup
        let objMenu = objMenuGroup.arrMenus[2]
        objMenu.intNotificationCount = Int(strCount)!
        self.tblMenu.reloadRowsAtIndexPaths([NSIndexPath(forRow: 2, inSection: 0)], withRowAnimation: UITableViewRowAnimation.Fade)
        
    }
    func showCommingsoonAlert() {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: APPDELEGATE.window!)
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSELECTMYCONTACT{
            let objContactVC = segue.destinationViewController as! MyContactController
            objContactVC.isContactSelecting = true
        }
        else if segue.identifier == SEGUETOTERMS{
            let objWebViewVC = segue.destinationViewController as! TermsConditionsController
            objWebViewVC.selectedScreenType = selectedScreenType
        }
    }
    @IBAction func showCommingsoonAlert(sender: AnyObject) {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
    }
    // MARK: - Memory Management    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
