//
//  SendNewClinkController.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import Alamofire

enum SharingClinkType : Int{
    case NoSharing = 1, Facebook = 2, Twitter = 3, BOTH = 4
}


class SendNewClinkController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomeCameraDelegate,TextViewHideenDelegate , UICollectionViewDelegate, UICollectionViewDataSource, ACEDrawingViewDelegate,SelectPaymentViewControllerDelegate{
    
    //    MARK:- Gloable Variable
    var imagePicker : UIImagePickerController!
    var objSelectedClinkDetail : ClsClinkDetails!
    var objSelectedTYClinkDetail : ClsClinkDetails!
    var objToContact : ClsContact!
    var objToContact2 : ClsContact!
    var arrCCContacts : [ClsContact]!
    var isToSelected : Bool!
    var isEdited : Bool!
    var amount : Int!
    var isPrivateClink = false
    var objSelectedVenue : ClsVenue!
    var brush :CGFloat!
    var imgLastPoint = CGPoint.zero
    var isFirstTimeLoading = true
    var controllerType  =  ClinkControllerType.Normal
    var previousScreenType : ClinkType!
    //    var arrEmailCC = [AnyObject]()
    var appliedCode :String = " "
    var objPromoDetails : ClsPromotion!
    var imageSourceType : ImageSourceType!
    var objDummyView:UIView!
    var arrThankYouFlow:[String] = Array()
    var arrEditingToolsUsed:[String] = Array()
    var shouldPopToVenue = false
    
    var objPaymentDetail : ClsCreditCardDetails = ClsCreditCardDetails()
    var strPaymentAccountId : String = ""
    
    var arrayPaymentMethodDetails : [ClsCreditCardDetails]!
    
    
    //    MARK:- IBOutlet
    @IBOutlet var editingView : GBEditingView!
    @IBOutlet var colorPickerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var btnTo : UIButton!
    @IBOutlet var btnCC : UIButton!
    @IBOutlet var imgView : UIImageView!
    //    @IBOutlet weak var sliderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ToCCHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editingToolsHeight: NSLayoutConstraint!
    @IBOutlet weak var editingToolsWidth: NSLayoutConstraint!
    @IBOutlet weak var venueHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSendHeight: NSLayoutConstraint!
    
    //    @IBOutlet var labelBrush : UILabel!
    @IBOutlet var imgColorPicker : UIImageView!
    @IBOutlet var btnColorPicker : UIButton!
    @IBOutlet var colorView : UIView!
    
    @IBOutlet var imgViewVenue : UIImageView!
    
    @IBOutlet var btnVenue : UIButton!
    @IBOutlet var btnAmount : UIButton!
    @IBOutlet var btnSendClink : UIButton!
    @IBOutlet var btnSaveImage : UIButton!
    
    @IBOutlet var btntextediting : UIButton!
    @IBOutlet var btnimgepicker : UIButton!
    @IBOutlet var btnquestion : UIButton!
    
    @IBOutlet var bgColorView : UIView!
    
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnClose : UIButton!
    @IBOutlet var btnUndo : UIButton!
    @IBOutlet weak var previewHeight: NSLayoutConstraint!
    
    @IBOutlet var collectionViewEmailCC: UICollectionView!
    @IBOutlet var btnTitle : UIButton!
    
    // MARK: - Viewcontroller Cycle
    
    override func viewWillAppear(animated: Bool) {
        
        //        imgView.backgroundColor = UIColor.clearColor()
        
        SideMenuRootController.rightSwipeEnabled = false
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) != true{
            self.showLoginScreen(false)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        self.view.endEditing(true)
        SideMenuRootController.rightSwipeEnabled = true
    }
    
    func getDefaultPayment() {
        
        if self.objPaymentDetail.strAccountType == "bank" {
            ClinkAccountManager.sharedInstance.callWebService( ClinkAccountRouter.GetBank(objPaymentDetail.strId).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) in
                
                if success == true {
                    self.objPaymentDetail = ClsCreditCardDetails(pdictResponse: response as! [String : AnyObject])
                    if self.objPaymentDetail.isDefaultPayment == true {
                        USERDEFAULTS.setDefaultPaymentDetailsObject(self.objPaymentDetail)
                    }
                    
                    if self.objPaymentDetail.isDefaultTransfer == true {
                        USERDEFAULTS.setDefaultTransferDetailsObject(self.objPaymentDetail, forKey: KTRANSFERDETAILSBANK)
                    }
                }
                else {
                    print("Error getting bank detail.")
                }
            }
        }
        else {
            ClinkAccountManager.sharedInstance.callWebService( ClinkAccountRouter.GetCard(objPaymentDetail.strId).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) in
                
                if success == true {
                    self.objPaymentDetail = ClsCreditCardDetails(pdictResponse: response as! [String : AnyObject])
                    
                    if self.objPaymentDetail.isDefaultPayment == true {
                        USERDEFAULTS.setDefaultPaymentDetailsObject(self.objPaymentDetail)
                    }
                    
                    if self.objPaymentDetail.isDefaultTransfer == true {
                        USERDEFAULTS.setDefaultTransferDetailsObject(self.objPaymentDetail, forKey: KTRANSFERDETAILSDEBITCARD)
                    }
                }
                else {
                    print("Error getting credit/debit card detail.")
                }
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NOTIFICATIONCENTER.addObserver(self, selector: #selector(SendNewClinkController.getDefaultPayment), name: GETDEFAULTPAYMENTMETHODDETAILS, object: nil)
        
        self.arrayPaymentMethodDetails = Array()
        
        self.imgViewVenue.backgroundColor = UIColor.clearColor()
        self.imgViewVenue.contentMode = UIViewContentMode.ScaleAspectFit
        self.btnVenue.adjustsImageWhenHighlighted = false
        self.btnVenue.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        
        self.btnVenue.adjustsImageWhenHighlighted = false
        
        SideMenuRootController.rightSwipeEnabled = false
        btnBack.hidden = true
        btnClose.hidden = false
        // Do any additional setup after loading the view.
        imagePicker =  UIImagePickerController()
        
        imagePicker.delegate = self
        
        arrCCContacts = Array()
        isToSelected = false
        amount = INTZERO
        editingView.delegateGB = self
        
        
        
        self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: COLORPICKERANIMATION)
        
        self.setDefaultVenue()
        if objSelectedTYClinkDetail != nil{
            objSelectedClinkDetail = objSelectedTYClinkDetail
        }
        
        if objSelectedClinkDetail == nil && objSelectedTYClinkDetail == nil && objSelectedVenue == nil{
            self.defaultVenueEdge()
        }
        
        if controllerType == ClinkControllerType.ThankYou || controllerType == ClinkControllerType.Gift {
            
            arrCCContacts = objSelectedClinkDetail.arrCCUser
            collectionViewEmailCC.reloadData()
            
            btnTo.userInteractionEnabled = false
            btnCC.userInteractionEnabled = false
            btnAmount.userInteractionEnabled = false
            btnVenue.userInteractionEnabled = false
            
            btnTo.setTitle(objSelectedClinkDetail.strSenderName, forState: .Normal)
            btnAmount.setBackgroundImage(UIImage(named: IMGBTNAMOUNT), forState: .Normal)
            
            
            
            let intAmount = Int(objSelectedClinkDetail.strAmount)
            
            if intAmount > 0 {
                let strAmount = "\(intAmount!)"
                btnAmount.setTitle("$ \(strAmount)", forState: UIControlState.Normal)
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString(btnAmount.titleLabel?.text,withColor: KFONTTHEMENEW), forState: .Normal)
            }
            else {
                btnAmount.setTitle("$ ", forState: UIControlState.Normal)
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString(btnAmount.titleLabel?.text,withColor: KFONTTHEMENEW), forState: .Normal)
            }
            
            objToContact = objSelectedClinkDetail.objSenderUser
            objToContact2 = objSelectedClinkDetail.objSenderUser
            isToSelected = true
            self.setVenueImage()
            btnTitle.setTitle(STRSAYTY, forState: .Normal)
            if objSelectedClinkDetail.objMarchantDetails != nil{
                objSelectedVenue = objSelectedClinkDetail.objMarchantDetails
            }
            else if objSelectedClinkDetail.strMerchantId != nil && objSelectedClinkDetail.strMerchantId.isEmptyString() == false{
                objSelectedClinkDetail.getMarchantDetails(self.view, pViewController: self)
                self.performSelector(#selector(SendNewClinkController.setDefaultVenueImage), withObject: nil, afterDelay: 3)
            }
            
            if controllerType == ClinkControllerType.Gift{
                
                if objSelectedClinkDetail.strSenderId != nil && objSelectedClinkDetail.strSenderId != USERDEFAULTS.getUserDetailsObject().strId {
                    let objContact = ClsContact(pdicResponse: objSelectedClinkDetail.dictFromUser)
                    objContact.isUnRemovable = true
                    arrCCContacts.append(objContact)
                    
                    let arrCCTemp = arrCCContacts
                    
                    arrCCContacts.removeAll()
                    
                    for item in arrCCTemp {
                        if arrCCContacts.contains(item) == false {
                            arrCCContacts.append(item)
                        }
                    }
                }
                
                btnAmount.userInteractionEnabled = true
                btnTitle.setTitle(STRADDTOCLINK, forState: .Normal)
                btnSendClink.setImage(UIImage(named: IMGTXTADDTOCLINK), forState: .Normal)
                amount = INTZERO
                
                btnAmount.setBackgroundImage(UIImage(named: IMGBTNAMOUNT), forState: .Normal)
                
                var intAmount = Int(objSelectedClinkDetail.strAmount)
                if let intGiftAmount = Int(objSelectedClinkDetail.strGiftPlusAmount) {
                    intAmount = intAmount! + intGiftAmount
                }
                
                let strAmount = "\(intAmount!)"
                btnAmount.setTitle("$ \(strAmount) + ", forState:
                    UIControlState.Normal)
                
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(strAmount) + ",withColor: KFONTTHEMENEW), forState: .Normal)
                objToContact = objSelectedClinkDetail.objReceiverUser
                objToContact2 = objSelectedClinkDetail.objReceiverUser
                btnTo.setTitle(objSelectedClinkDetail.strReceiverName, forState: .Normal)
                isToSelected = true
            }
        }
        else{
            
            btnTitle.setTitle(STRSENDCLINK, forState: .Normal)
            btnTo.userInteractionEnabled = true
            self.setDefaultVenueImage()
        }
        
        objDummyView = UIView(frame: self.view.bounds)
        objDummyView.backgroundColor = UIColor.blackColor()
        self.view.addSubview(objDummyView)
        
        self.openCameraOnce()
        collectionViewEmailCC.delegate = self
        collectionViewEmailCC.dataSource = self
        
        self.editingView.newDrawingView.delegate = self
        self.btnUndo.enabled = false
        let ccTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SendNewClinkController.showCCContactPicker))
        self.collectionViewEmailCC.addGestureRecognizer(ccTapGesture)
        
        
        //        self.deleteClink()
    }
    
    func deleteClink() {
        
        let strClinkId = "cda2446508287cd5948559af64d63986"
        
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.DeleteClink(strClinkId).URLRequest, isBackgroundCall: true, pAlertView: self.view, pViewController: self) { (success, response) in
            
            if success == true {
                print("Clink Delete Success")
            }
            else {
                print("Clink Delete Error")
            }
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func setVenueImage(){
        if objSelectedClinkDetail != nil && objSelectedClinkDetail.objMarchantDetails != nil{
            objSelectedVenue = objSelectedClinkDetail.objMarchantDetails
            if let strIcnImgSmall = objSelectedClinkDetail.objMarchantDetails.strImgSmallIcon{
                if let url = NSURL(string:strIcnImgSmall) {
                    if let data = NSData(contentsOfURL: url) {
                        self.btnVenue.setImage(nil, forState: .Normal)
                        self.imgViewVenue.image = UIImage(data: data)
                        self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                    }
                }
            }
            else{
                self.btnVenue.setImage(nil, forState: .Normal)
                self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                self.imgViewVenue.backgroundColor = UIColor.whiteColor()
            }
            if objSelectedClinkDetail.objMarchantDetails.strName.isEqualToIgnoringCase(VENUEDRAMALEAGUE){
                btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                objToContact = self.getDramaLeague()
            }
            else if objSelectedClinkDetail.objMarchantDetails.strName.isEqualToIgnoringCase(VENUEHORACEMANN){
                btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                objToContact = self.getHoraceMann()
            }
            else if objSelectedClinkDetail.objMarchantDetails.strName.isEqualToIgnoringCase(VENUEHOUNDSPUB){
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                btnSendClink.setImage(UIImage(named: IMGTXTSAYTY), forState: .Normal)
            }
            else{
                btnSendClink.setImage(UIImage(named: IMGTXTSENDCLINK), forState: .Normal)
            }
            
        }
        else{
            btnSendClink.setImage(UIImage(named: IMGTXTSAYTY), forState: .Normal)
        }
    }
    
    func setDefaultVenueImage(){
        
        if objSelectedVenue != nil
        {
            if let strIcnImgSmall = objSelectedVenue.strImgSmallIcon{
                self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETZERO
                //                btnVenue.sd_setImageWithURL(NSURL(string: strIcnImgSmall), forState: .Normal, placeholderImage: UIImage(named: "no_image"))
                if let url = NSURL(string:strIcnImgSmall) {
                    if let data = NSData(contentsOfURL: url) {
                        self.imgViewVenue.image = UIImage(data: data)
                        self.btnVenue.setImage(nil, forState: .Normal)
                        self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                    }
                }
            }
            else{
                self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETZERO
                self.btnVenue.setImage(nil, forState: .Normal)
                self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                self.imgViewVenue.backgroundColor = UIColor.whiteColor()
            }
            
            if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEDRAMALEAGUE){
                btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                objToContact = self.getDramaLeague()
            }
            else if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEHORACEMANN){
                btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                objToContact = self.getHoraceMann()
            }
            else if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEHOUNDSPUB){
                self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                btnSendClink.setImage(UIImage(named: IMGTXTSENDCLINK), forState: .Normal)
                objToContact = nil
                btnTo.setTitle(" ", forState: .Normal)
            }
            else{
                //                btnVenue.hidden = true
                btnSendClink.setImage(UIImage(named: IMGTXTSENDCLINK), forState: .Normal)
            }
            //            btnTo.enabled = false
        }
        else if controllerType == ClinkControllerType.ThankYou || controllerType == ClinkControllerType.Gift{
            self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETZERO
            self.btnVenue.setImage(nil, forState: .Normal)
            if objSelectedClinkDetail.strMerchantId == nil || objSelectedClinkDetail.strMerchantId.isEmptyString() == true {
                self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                self.imgViewVenue.backgroundColor = UIColor.whiteColor()
            }
            else{
                self.setVenueImage()
            }
        }
        else{
            self.defaultVenueEdge()
        }
    }
    
    //Setting Edge to venue image
    func defaultVenueEdge()
    {
        self.btnVenue.setImage(UIImage(named: IMGICNLOCATION), forState: .Normal)
        self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETCUSTOME
        self.btnVenue.titleLabel?.font = UIFont(name: FONTGOTHAMMEDIUM, size: FONTSIZE18)
        self.btnVenue.titleEdgeInsets = BTNVENUEUTITLEINSETCUSTOME
        self.btnVenue.setTitle(STRFIND, forState: .Normal)
        self.btnVenue.setTitleColor(KFONTTHEMENEW, forState: .Normal)
    }
    
    // MARK: - CollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCCContacts.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cellIdentifier:String = CELLEMAILCC;
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier,forIndexPath:indexPath) as! EmailCCCell
        
        if indexPath.row < arrCCContacts.count - 1
        {
            cell.lblEmailCCName.text = "\((arrCCContacts[indexPath.row].strFullName)!),"
        }
        else
        {
            cell.lblEmailCCName.text = "\((arrCCContacts[indexPath.row].strFullName)!)"
        }
        
        
        //cell.lblName .sizeToFit()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let str:String = "\((arrCCContacts[indexPath.row].strFullName)!),"
        let calCulateSizze: CGSize = str.sizeWithAttributes([NSFontAttributeName: UIFont (name: FONTGOTHAMMEDIUM, size: FONTSIZE14)!])
        return CGSizeMake(calCulateSizze.width+1,COLLECTIONCCNAMEHEIGHT)
    }
    
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    //    MARK:- Touches Events
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            imgLastPoint = touch.locationInView(self.imgColorPicker)
            self.touchesMoved(touches, withEvent: event)
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // 6
        isFirstTimeLoading = false
        if let touch = touches.first {
            if touch.view == bgColorView{
                self.view.endEditing(true)
                self.editingView.isClrSelecting = false
                self.editingView.isErasing = false
                self.editingView.isDrawing = true
                self.editingView.enableDrawing()
                
                let currentPoint = touch.locationInView(self.imgColorPicker)
                // 7
                
                if currentPoint.x > CGFLOATZERO && currentPoint.y > CGFLOATZERO && currentPoint.x < MAXXPOSCLORPICKER && currentPoint.y < MAXYPOSCLORPICKER
                {
                    imgLastPoint = currentPoint
                    editingView.currentColor = imgColorPicker.getColorOfPoint(imgLastPoint)
                    
                    let image = UIImage(named:IMGICNPENCIL)
                    btnColorPicker.setImage(image?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
                    
                    btnColorPicker.imageView?.tintColor = editingView.currentColor
                    btnColorPicker.tintColor = editingView.currentColor
                    if arrEditingToolsUsed.last != EditingToolTracker.Color.rawValue{
                        arrEditingToolsUsed.append(EditingToolTracker.Color.rawValue)
                    }
                }
            }
            else if touch.view != editingView.txtViewLbl{
                self.view.endEditing(true)
            }
            
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.editingView.newDrawingView.lineColor = self.editingView.currentColor;
        //        self.touchesMoved(touches, withEvent: event)
        self.editingView.newDrawingView.drawTool = ACEDrawingToolTypePen
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSELECTMYCONTACT{
            
            let objContactVC = segue.destinationViewController as! MyContactController
            objContactVC.isContactSelecting = true
            objContactVC.isToSelected = self.isToSelected
            objContactVC.arrCCContacts = self.arrCCContacts
            objContactVC.objToContact = self.objToContact
            self.objToContact2 = self.objToContact
        }
        else if segue.identifier == SEGUETOSELECTVENUE{
            
            if self.objToContact != nil {
                self.objToContact2 = self.objToContact
            }
            
            let objVenueVC = segue.destinationViewController as! VenueListController
            objVenueVC.isVenueSelecting = true
            if objSelectedVenue != nil{
                objVenueVC.objSelectedVenue = self.objSelectedVenue
            }
            if objPromoDetails != nil{
                objVenueVC.objPromoDetails = self.objPromoDetails
            }
        }
        else if segue.identifier == SEGUETOAMOUNT{
            
            if self.objToContact != nil {
                self.objToContact2 = self.objToContact
            }
            
            let objAmountVC = segue.destinationViewController as! AmountInputController
            objAmountVC.intAmount = amount
            if controllerType == ClinkControllerType.Gift{
                objAmountVC.isGifting = true
            }
        }
    }
    
    // MARK: - Imagepicker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        objDummyView.removeFromSuperview()
        isFirstTimeLoading = false
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView.contentMode = .ScaleAspectFit
            
            if picker.sourceType == UIImagePickerControllerSourceType.Camera{
                if picker.cameraDevice == UIImagePickerControllerCameraDevice.Front{
                    let image = UIImage(CGImage: pickedImage.CGImage!, scale:pickedImage.scale, orientation: .LeftMirrored)
                    imgView.image = image
                }
                else{
                    imgView.image = pickedImage
                }
                imageSourceType = ImageSourceType.Camera
                self.addTrackThankyouClink(FlowTracker.AddedImageFromCamera.rawValue)
            }
            else{
                imageSourceType = ImageSourceType.Gallery
                imgView.image = pickedImage
                self.addTrackThankyouClink(FlowTracker.AddedImageFromGallery.rawValue)
            }
            
            editingView.originalImage = imgView.image
            editingView.newDrawingView.loadImage(imgView.image)
            self.editingView.setUpMaskingView()
            self.editingView.newDrawingView.delegate = self;
            self.editingView.actionClear()
            
            self.view.endEditing(true)
            editingView.currentColor = UIColor.whiteColor()
            let image = UIImage(named:IMGICNPENCIL)
            btnColorPicker.setImage(image, forState: .Normal)
            
            self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
            editingView.isDrawing = false
            editingView.enableSwipeFilter()
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        if isFirstTimeLoading == true {
            
            if controllerType == ClinkControllerType.ThankYou || controllerType == ClinkControllerType.Gift{
                objDummyView.removeFromSuperview()
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                })
            }
            else{
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                })
            }
        }
        else{
            objDummyView.removeFromSuperview()
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    //MARK: - Custom Camera Delegate
    func didFinishPickingImage(image: UIImage!) {
        
        self.isFirstTimeLoading = false
        if let pickedImage = image {
            imgView.contentMode = .ScaleAspectFit
            imgView.image = pickedImage
            
            editingView.originalImage = imgView.image
            editingView.setUpMaskingView()
            self.editingView.actionClear()
            editingView.txtViewLbl.text = STREMPTY
            editingView.txtViewLbl.hidden = true
            self.view.endEditing(true)
            editingView.currentColor = UIColor.whiteColor()
            let image = UIImage(named:IMGICNPENCIL)
            btnColorPicker.setImage(image, forState: .Normal)
            
            self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
            editingView.isDrawing = false
            editingView.enableSwipeFilter()
            objDummyView.removeFromSuperview()
            imageSourceType = ImageSourceType.Camera
            //            self.addTrackThankyouClink(FlowTracker.AddedImageFromCamera.rawValue)
        }
        
    }
    
    func yCameraControllerDidCancel() {
        
        if isFirstTimeLoading == true {
            
            if shouldPopToVenue == true {
                self.navigationController?.popViewControllerAnimated(false)
            }
            else if controllerType == ClinkControllerType.ThankYou{
                
                objDummyView.removeFromSuperview()
                dismissViewControllerAnimated(false, completion: { () -> Void in
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                })
            }
            else if controllerType == ClinkControllerType.Gift{
                objDummyView.removeFromSuperview()
                if previousScreenType == ClinkType.Sent{
                    APPDELEGATE.objSideMenu.showSentClink()
                    self.isFirstTimeLoading = false
                }
                else if previousScreenType == ClinkType.Received{
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                }
                else{
                    APPDELEGATE.objSideMenu.showHomeScreen()
                    self.isFirstTimeLoading = false
                }
            }
            else{
                
                dismissViewControllerAnimated(false, completion: { () -> Void in
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                })
            }
        }
    }
    
    func yCameraControllerdidSkipped() {
        self.imgView.image = nil
        
    }
    
    
    // MARK: - Button Actions
    
    @IBAction func btnShowImagePicker(sender : UIButton){
        
        self.showImagePicker()
    }
    
    @IBAction func btnShowToContactPicker(sender : UIButton){
        if sender.titleLabel?.text != STRDRAMAEMAIL && sender.titleLabel?.text != STRHMEMAIL
        {
            self.view.endEditing(true)
            isToSelected = true
            self.performSegueWithIdentifier(SEGUETOSELECTMYCONTACT, sender: nil)
        }
    }
    
    @IBAction func btnShowCCContactPicker(sender : UIButton){
        self.addTrackThankyouClink(FlowTracker.OpendContactToaddCCUser.rawValue)
        self.showCCContactPicker()
    }
    
    func showCCContactPicker(){
        self.view.endEditing(true)
        isToSelected = false
        performSegueWithIdentifier(SEGUETOSELECTMYCONTACT, sender: nil)
    }
    
    @IBAction func btnClose(sender : UIButton){
        let alertView = CustomAlert(title: STREMPTY, message:ALERTCONFIRMCLOSINGCLINK, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: STRSTOPCLINKING, otherButtonTitle: STRKEEPCLINKING) { (customAlert, buttonIndex) -> Void in
            
            if buttonIndex == INTZERO{
                for objContact in APPDELEGATE.arrMainContacts {
                    objContact.isSelected = false
                }
                if self.shouldPopToVenue == true{
                    self.navigationController?.popViewControllerAnimated(false)
                }
                else if self.controllerType == ClinkControllerType.ThankYou{
                    
                    self.objDummyView.removeFromSuperview()
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                }
                else if self.controllerType == ClinkControllerType.Gift{
                    self.objDummyView.removeFromSuperview()
                    if self.previousScreenType == ClinkType.Sent{
                        APPDELEGATE.objSideMenu.showSentClink()
                        self.isFirstTimeLoading = false
                    }
                    else if self.previousScreenType == ClinkType.Received{
                        APPDELEGATE.objSideMenu.showReceivedClink()
                        self.isFirstTimeLoading = false
                    }
                    else{
                        APPDELEGATE.objSideMenu.showHomeScreen()
                        self.isFirstTimeLoading = false
                    }
                }
                else{
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                }
            }
        }
        alertView.showInView(APPDELEGATE.window)
        
    }

    @IBAction func btnBack(sender : UIButton){
        self.view.endEditing(true)
        editingToolsHeight.constant = EDITINGTOOLHEIGHT
        venueHeight.constant = VENUEBUTTONHEIGHT
        btnSendHeight.constant = BTNSENDCLINK
        editingToolsWidth.constant = EDITINGTOOLWIDTH
        btnBack.hidden = true
        btnClose.hidden = false
        btnSendClink.hidden = false
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btnSendClink(sender : UIButton){
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        if USERDEFAULTS.getUserDetailsObject().isVerified == false {
            Helper.showAlert("Email verification is required, please click resend if you haven't received the verification email.", cancelButtonTitle: "Cancel", otherButtonTitle: "Resend", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                
                if buttonIndex == 1 {
                    
                    let objUserDetail = USERDEFAULTS.getUserDetailsObject()
                    
                    ClinkAccountManager.sharedInstance.sendVerificationEmail(objUserDetail.strEmail, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
                        
                        if success == true {
                            Helper.showAlert("Verification Email has been successfully sent to your email account. Please check your email.", pobjView: self.view)
                        }
                        else {
                            Helper.showAlert(response as! String, pobjView: self.view)
                        }
                    })
                }
                else {
                    APPDELEGATE.objSideMenu.getUserProfile()
                }
            })
        }
        else {
            
            if objToContact == nil && controllerType != ClinkControllerType.ThankYou && controllerType != ClinkControllerType.Gift{
                Helper.showAlert(ADDATLEASTONERECIPIENT, pobjView: self.view)
            }
            else{
                if controllerType == ClinkControllerType.Gift && amount <= INTZERO{
                    Helper.showAlert(ALERTCONFIRMADDGIFTAMOUNT,cancelButtonTitle: BTNYESTITLE,otherButtonTitle: BTNNOTITLE, pobjView: self.view, completionBock: { (alertObject, index) -> () in
                        
                        if index == 1{
                            if self.amount > 250{
                                // Your CLINK! is over $250
                                Helper.showAlertwithTitle("Your CLINK! is over $250", strMessage: "Is that correct? Please confirm it is addressed correctly as you cannot cancel a CLINK! once sent", cancelButtonTitle: "YES", otherButtonTitle: "NO", pobjView: self.view) { (customAlert, buttonIndex) -> () in
                                    
                                    if buttonIndex == 0 {
                                        if INCLUDEACCOUNT == true{
                                            self.getDefaultPaymentMethod()
                                        }
                                        else{
                                            self.showPrivatePopUp()
                                        }
                                    }
                                }
                            }
                            else{
                                if INCLUDEACCOUNT == true{
                                    self.getDefaultPaymentMethod()
                                }
                                else{
                                    self.showPrivatePopUp()
                                }
                            }
                        }
                        else if index == 0{
                            self.performSegueWithIdentifier(SEGUETOAMOUNT, sender: nil)
                        }
                    })
                }
                else{
                    //            Normal clink
                    if self.amount > 250{
                        // Your CLINK! is over $250
                        Helper.showAlertwithTitle("Your CLINK! is over $250", strMessage: "Is that correct? Please confirm it is addressed correctly as you cannot cancel a CLINK! once sent", cancelButtonTitle: "YES", otherButtonTitle: "NO", pobjView: self.view) { (customAlert, buttonIndex) -> () in
                            
                            if buttonIndex == 0 {
                                if INCLUDEACCOUNT == true{
                                    self.getDefaultPaymentMethod()
                                }
                                else{
                                    self.showPrivatePopUp()
                                }
                            }
                        }
                    }
                    else{
                        if INCLUDEACCOUNT == true{
                            self.getDefaultPaymentMethod()
                        }
                        else{
                            self.showPrivatePopUp()
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Get Default Payment Method
    func getDefaultPaymentMethod() {
        
        if self.amount > 0 {
            if let objPaymentMethod : ClsCreditCardDetails = USERDEFAULTS.getDefaultPaymentDetailsObject() {
                objPaymentDetail = objPaymentMethod
                strPaymentAccountId = objPaymentDetail.strId
            }
            
            if objPaymentDetail.strId == "" {
                self.getAllCreditCard()
            }
            else {
                self.getPaymentDetail()
            }
        }
        else {
            self.showPrivatePopUp()
        }
    }
    
    // MARK: - Get All Credit/Debit Card
    func getAllCreditCard(){
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchCard(["filters[is_default_payment]":"1"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        
                        if objCreditCardDetail.isDefaultPayment == true {
                            arrayCreditCardDetails.append(objCreditCardDetail)
                        }
                    }
                }
                
                if arrayCreditCardDetails.count > 0{
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        self.arrayPaymentMethodDetails.append(objCreditCardDetail)
                    }
                }
                self.getAllBank()
            }
            else{
                self.getAllBank()
            }
        }
    }
    
    // MARK: - Get All Bank
    func getAllBank(){
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchBank(["filters[is_default_payment]":"1"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        if objCreditCardDetail.isDefaultPayment == true {
                            arrayCreditCardDetails.append(objCreditCardDetail)
                        }
                    }
                }
                
                if arrayCreditCardDetails.count > 0{
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        self.arrayPaymentMethodDetails.append(objCreditCardDetail)
                    }
                }
                self.getPaymentDetail()
            }
            else{
                self.getPaymentDetail()
            }
        }
    }
    
    func getPaymentDetail(){
        
        if self.amount > 0 {
            //            if  objSelectedVenue != nil && objSelectedVenue.arrCategoryName.first == KCATEGORYCHARITIES{
            //                self.showPrivatePopUp()
            //            }
            //            else{
            //
            if objPaymentDetail.strId == "" {
                
                if self.arrayPaymentMethodDetails.count <= 0 {
                    Helper.showAlert("You need to enter payment information to send your CLINK!", cancelButtonTitle: "Done", otherButtonTitle: "Continue", pobjView: self.view) { (sender, buttonIndex) -> () in
                        
                        if buttonIndex == 1 {
                            self.pushToSelectPaymentMethodController()
                        }
                    }
                }
                else
                {
                    for objPayment in self.arrayPaymentMethodDetails {
                        
                        objPaymentDetail = objPayment
                        strPaymentAccountId = objPaymentDetail.strId
                        
                        USERDEFAULTS.setDefaultPaymentDetailsObject(objPaymentDetail)
                        USERDEFAULTS.synchronize()
                        
                    }
                    
                    Helper.showAlert("Use default payment method? \n \n Pay from - \(objPaymentDetail.strLastFour!)", cancelButtonTitle: "YES", otherButtonTitle: "NO", pobjView: self.view) { (sender, buttonIndex) -> () in
                        
                        if buttonIndex == 1 {
                            if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
                                Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
                                return
                            }
                            self.pushToSelectPaymentMethodController()
                        }
                        else {
                            
                            if self.objPaymentDetail.isVarified == true {
                                self.showPrivatePopUp()
                            }
                            else {
                                if self.objPaymentDetail.strBankVarifyUrl != nil && self.objPaymentDetail.strBankVarifyUrl != "" {
                                    
                                    Helper.showAlert("You will be redirected to web to verify selected payment method.", pobjView: self.view , completionBock: { (sender , buttonIndex) -> () in
                                        
//                                        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank( self.objPaymentDetail.strId,["is_verified":"1"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
//                                            
//                                            if success == true {
//                                                print("Success")
//                                            }
//                                            else {
//                                                Helper.showAlert(response as! String, pobjView: self.view)
//                                            }
//                                        })
                                        
                                        UIApplication.sharedApplication().openURL(NSURL(string: self.objPaymentDetail.strBankVarifyUrl)!)
                                    })
                                }
                                else{
                                    Helper.showAlert("Unable to proceed request. Verification url not found.", pobjView: self.view)
                                }
                            }
                        }
                    }
                }
            }
            else {
                
                Helper.showAlert("Use default payment method? \n \n Pay from - \(objPaymentDetail.strLastFour!)", cancelButtonTitle: "YES", otherButtonTitle: "NO", pobjView: self.view) { (sender, buttonIndex) -> () in
                    
                    if buttonIndex == 1 {
                        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
                            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
                            return
                        }
                        self.pushToSelectPaymentMethodController()
                    }
                    else {
                        if self.objPaymentDetail.isVarified == true {
                            self.showPrivatePopUp()
                        }
                        else {
                            if self.objPaymentDetail.strBankVarifyUrl != nil && self.objPaymentDetail.strBankVarifyUrl != "" {
                                
                                Helper.showAlert("You will be redirected to web to verify selected payment method.", pobjView: self.view , completionBock: { (sender , buttonIndex) -> () in
                                    
//                                    ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank( self.objPaymentDetail.strId,["is_verified":"1"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
//
//                                        if success == true {
//                                            print("Success")
//                                        }
//                                        else {
//                                            Helper.showAlert(response as! String, pobjView: self.view)
//                                        }
//                                    })
                                    
                                    UIApplication.sharedApplication().openURL(NSURL(string: self.objPaymentDetail.strBankVarifyUrl)!)
                                })
                            }
                            else{
                                Helper.showAlert("Unable to proceed request. Verification url not found.", pobjView: self.view)
                            }
                        }
                    }
                }
            }
            //            }
        }
            //        else if self.amount > 0 && objSelectedVenue.strCategory == KCATEGORYCHARITIES{
            //
            //        }
        else{
            self.showPrivatePopUp()
        }
    }
    
    func pushToSelectPaymentMethodController() {
        let storyboard = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERSELECTPAYMENTMETHOD) as? SelectPaymentViewController
        vc?.delegate = self
        let nav = UINavigationController(rootViewController: vc!) as UINavigationController
        nav.navigationBarHidden = true
        self.presentViewController(nav, animated: true, completion: nil)
    }
    
    func showPrivatePopUp(){
        
        let objViewController = UIStoryboard(name: MAINSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERPRIVATEPOPUP)
        let subView = objViewController.view as! PrivatePopUpView
        subView.subView.layer.cornerRadius = PRIVATEPOPUPCORNERRADIUS
        self.addTrackThankyouClink(FlowTracker.OpenedConfirmationPopup.rawValue)
        subView.showInView(self.view, complitionBlock: { (pIsPrivateClink, pIsShareOnFB, pIsShareOnTwitter,result) -> () in
            
            subView.removeFromSuperview()
            self.isPrivateClink = pIsPrivateClink
            
            if pIsPrivateClink == true {
                Helper.showAlertwithTitle("Your CLINK! is private.", strMessage: "You're CLINK! is marked as private you cannot forward it to new recipients.", pobjView: self.view, completionBock: { (CustomAlert, buttonIndex) -> () in
                    self.sendClink(true , pIsShareOnFB: pIsShareOnFB , pIsShareOnTwitter: pIsShareOnTwitter )
                })
            }
            else{
                self.sendClink(true , pIsShareOnFB: pIsShareOnFB , pIsShareOnTwitter: pIsShareOnTwitter )
            }
        })
    }
    
    func getSharingForClink(pShareOnFB : Bool , pShareOnTwitter : Bool ) -> SharingClinkType{
        if pShareOnFB == true && pShareOnTwitter == false{
            return .Facebook
        }
        else if pShareOnFB == false && pShareOnTwitter == true{
            return .Twitter
        }
        else if pShareOnFB == true && pShareOnTwitter == true{
            return .BOTH
        }
        else{
            return .NoSharing
        }
    }
    
    // MARK: - Send Clink
    
    func sendClink(isPaymentCompleted : Bool , pIsShareOnFB : Bool , pIsShareOnTwitter : Bool )
    {
        let sharingType = self.getSharingForClink(pIsShareOnFB , pShareOnTwitter: pIsShareOnTwitter)
        
        var strReciverId : String
        var isNonClinkUser = false
        var dicClinkData : [String : AnyObject] = ["amount" : self.amount,"is_private" : self.isPrivateClink, "is_sent" : isPaymentCompleted]
        
        if (self.objSelectedVenue != nil) {
            if self.objSelectedVenue.arrCategoryName.first == KCATEGORYCHARITIES {
                dicClinkData["is_sent"] = false
            }
        }
        
        if controllerType == ClinkControllerType.ThankYou {
            strReciverId = self.objSelectedClinkDetail.strSenderId
            dicClinkData[KORIGINALCLINKUUID] = self.objSelectedClinkDetail.strId
        }
        else if controllerType == ClinkControllerType.Gift{
            strReciverId = self.objSelectedClinkDetail.strReceiverId
            dicClinkData[KGIFTPLUSCLINKUUID] = self.objSelectedClinkDetail.strId
        }
        else if self.objToContact.strUserid.isEmptyString() == false{
            strReciverId = self.objToContact.strUserid
        }
        else{
            strReciverId = self.objToContact.strId
            isNonClinkUser = true
        }
        
        if self.arrCCContacts.count > 0{
            var arrCCUser = [String]()
            let arrClinkUsers = arrCCContacts.filter {
                $0.isClinkUser.boolValue == true
            }
            if arrClinkUsers.count > 0{
                arrCCUser = self.getCCUsersArray(arrClinkUsers)
            }
            
            let arrClinkUsers2 = arrCCContacts.filter {
                $0.isUnRemovable.boolValue == true
            }
            
            for objContact in arrClinkUsers2 {
                arrCCUser.append(objContact.strId)
            }
            
            if arrCCUser.count > 0{
                dicClinkData["cc_users"] = arrCCUser
            }
            
            let arrOtherUsers = arrCCContacts.filter {
                $0.isClinkUser.boolValue == false
            }
            
            if arrOtherUsers.count > 0{
                dicClinkData["cc_contacts"] = self.getCCContactsArray(arrOtherUsers)
            }
        }
        
        if (self.objSelectedVenue != nil){
            
            if self.objSelectedVenue.arrCategoryName.first == KCATEGORYCHARITIES{
                
                if self.objSelectedVenue.objDefaultContact.strId.isEmptyString() == false{
                    dicClinkData["to_user_uuid"] = self.objSelectedVenue.objDefaultContact.strId
                    isNonClinkUser = false
                }
                else
                {
                    dicClinkData["to_user_uuid"] = strReciverId
                }
                dicClinkData["merchant_uuid"] = self.objSelectedVenue.id
            }
            else{
                
                if self.objSelectedVenue.id.isEmptyString() == false{
                    dicClinkData["merchant_uuid"] = self.objSelectedVenue.id
                }
                dicClinkData["to_user_uuid"] = strReciverId
            }
        }
        else{
            dicClinkData["to_user_uuid"] = strReciverId
        }
        if isNonClinkUser == true{
            dicClinkData["to_contact_uuid"] = self.objToContact.strId
            dicClinkData.removeValueForKey("to_user_uuid")
            
        }
        
        //        dicClinkData["to_user_uuid"] = self.objSelectedVenue.strDefaultUserId
        
        print("Param :- \(dicClinkData)")
        
        //            var strVenueId = " "
        //            if self.objSelectedClinkDetail.strVenueId != nil{
        //                strVenueId = self.objSelectedClinkDetail.strVenueId
        //            }
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.SendNewClink(dicClinkData).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            if success == true{
                let dicResponse = response as! [String : AnyObject]
                if let strClinkId = dicResponse.getValueIfAvilable(KUUID)
                {
                    self.uploadRelatedImage(strClinkId as! String , pShareTo: sharingType)
                }
            }
            else{
                if let strMessage = response as? String
                {
                    Helper.showAlert(strMessage, pobjView: self.view)
                }
            }
        }
    }
    
    func uploadRelatedImage(clinkUUID : String , pShareTo : SharingClinkType){
        
        //        let image = UIImage(named: "catEye")
        let uuid = clinkUUID
        for objContact in APPDELEGATE.arrMainContacts {
            objContact.isSelected = false
        }
        ClinkDetailsManager.sharedInstance.callUploadService(ClinkDetailsRouter.UploadImage(uuid).URLRequest, imageData: UIImageJPEGRepresentation(self.getEditedImage(false), 0.5)!, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                print("Response :- \(response)")
                
                if let dicResponse = response as? [String : AnyObject] {
                    if let error = dicResponse.getValueIfAvilable(KMESSAGE) {
                        print("Error :- \(error)")
                        return
                    }
                    else{
                        print("wohhaaa!!!!... Image upload success")
                        self.objSelectedClinkDetail = ClsClinkDetails(pdictResponse: dicResponse)
                        if self.isPrivateClink ==  false{
                            
                            switch pShareTo{
                            case .Facebook:
                                dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)){
                                    ClinkDetailsManager.sharedInstance.shareToFacebook(self, strImageUrl: self.objSelectedClinkDetail.strImageUrl, strWebUrl: "http://clink-nyc.com", strUserName : self.objSelectedClinkDetail.strSenderName , strTitle: "CLINK! from \(self.objSelectedClinkDetail.strSenderName) to \(self.objSelectedClinkDetail.strReceiverName)", strDesc: "http://clink-nyc.com")
                                }
                                self.performNavigation()
                                break
                            case .Twitter:
                                dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)){
                                    
                                    ClinkDetailsManager.sharedInstance.shareToTwitter("CLINK! from \(self.objSelectedClinkDetail.strSenderName) to \(self.objSelectedClinkDetail.strReceiverName)" + " " + "http://clink-nyc.com" , image: self.getEditedImage(false))
                                }
                                self.performNavigation()
                                break
                            case .BOTH:
                                
                                dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)){
                                    
                                    ClinkDetailsManager.sharedInstance.shareToTwitter("CLINK! from \(self.objSelectedClinkDetail.strSenderName) to \(self.objSelectedClinkDetail.strReceiverName)" + " " + "http://clink-nyc.com" , image: self.getEditedImage(false))
                                }
                                
                                dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)){
                                    ClinkDetailsManager.sharedInstance.shareToFacebook(self, strImageUrl: self.objSelectedClinkDetail.strImageUrl, strWebUrl: "http://clink-nyc.com", strUserName : self.objSelectedClinkDetail.strSenderName , strTitle: "CLINK! from \(self.objSelectedClinkDetail.strSenderName) to \(self.objSelectedClinkDetail.strReceiverName)", strDesc: "http://clink-nyc.com")
                                }
                                self.performNavigation()
                                break
                            case .NoSharing:
                                self.performNavigation()
                            }
                            
                        }
                        else{
                            self.performNavigation()
                        }
                    }
                }
                
            }
            else{
                let error = response as! String
                Helper.showAlert(error, pobjView: self.view)
            }
        }
        
        if APPDELEGATE.arrTrackerGroups.count > 0 {
            let dicTracker : [String : AnyObject] = ["clink_uuid" : clinkUUID ,"type" : imageSourceType.rawValue]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.first!.strId)
            if APPDELEGATE.arrTrackerGroups.count >= 2{
                let dicToolsTracker : [String : AnyObject] = ["clink_uuid" : clinkUUID ,"Tools" : arrEditingToolsUsed]
                Helper.setTrackerData(dicToolsTracker, strGroupId: APPDELEGATE.arrTrackerGroups[2].strId)
            }
            //            if controllerType == ClinkControllerType.ThankYou && arrThankYouFlow.count > 0 && APPDELEGATE.arrTrackerGroups.count >= 2{
            //                let dicThankyou : [String : AnyObject] = ["clink_uuid" : clinkUUID ,"Thank you flow" : arrThankYouFlow]
            //                Helper.setTrackerData(dicThankyou, strGroupId: APPDELEGATE.arrTrackerGroups.first!.strId)
            //            }
        }
        
    }
    
    // MARK: - Payment
    
    func doDonationPayment(){
        if self.amount > 0 && self.objSelectedVenue.arrCategoryName.first == KCATEGORYCHARITIES{
            if self.objSelectedClinkDetail.strDonationURL != nil{
                
                Helper.showAlert("You will be redirected to web to make donation payment.", pobjView: self.view , completionBock: { (sender , buttonIndex) -> () in
                    
                    UIApplication.sharedApplication().openURL(NSURL(string: self.objSelectedClinkDetail.strDonationURL!+"?account_uuid="+self.objPaymentDetail.strId)!)
                    print(self.objSelectedClinkDetail.strDonationURL!+"?account_uuid="+self.objPaymentDetail.strId)
                    
                    self.redirectToNextVC()
                })
            }
            else{
                Helper.showAlert("Unable to proceed request donation url not found.", pobjView: self.view, completionBock: { (sender, buttonIndex) -> () in
                    self.updateClink(self.objSelectedClinkDetail, isSent: false)
                })
            }
        }
    }
    
    func setSelectedPaymentMethod(objPaymentMethod : ClsCreditCardDetails!,orClinkAccount strClinkAccountId : String){
        
        if objPaymentMethod.strId != "" {
            objPaymentDetail = objPaymentMethod
            strPaymentAccountId = objPaymentDetail.strId
        }
        else if strClinkAccountId != "" {
            strPaymentAccountId = strClinkAccountId
        }
        
        if strClinkAccountId != "" {
            
            let objUserDetails = USERDEFAULTS.getUserDetailsObject()
            let strClinkBalance = objUserDetails.strBalance as String
            
            print("strClinkBalance : - \(Int(strClinkBalance))")
            
            if Int(strClinkBalance) <= Int(self.amount) {
                Helper.showAlert("Insufficient funds in your clink account.", pobjView: self.view)
            }
            else {
                self.showPrivatePopUp()
            }
        }
        else {
            self.showPrivatePopUp()
        }
    }
    
    func doPayment(){
        
        let dictPaymentDetail = ["amount":self.objSelectedClinkDetail.strAmount ,"description":"Clink to \(self.objSelectedClinkDetail.strId)" ,"currency":"usd" ,"clink_uuid":self.objSelectedClinkDetail.strId ,"account_uuid":strPaymentAccountId]
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.MakePayment(dictPaymentDetail).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true {
                Helper.showAlert("Payment successful. Thank you.", pobjView: self.view, completionBock: { (sender, buttonIndex) -> () in
                    self.redirectToNextVC()
                })
            }
            else{
                Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (sender, buttonIndex) -> () in
                    self.updateClink(self.objSelectedClinkDetail, isSent: false)
                })
            }
        }
    }
    
    // MARK: - Update Clink
    
    func updateClink(objClink : ClsClinkDetails , isSent : Bool) {
        
        let dictPaymentDetail : [String : AnyObject] = ["amount":self.objSelectedClinkDetail.strAmount, "is_sent":isSent,"is_private":self.objSelectedClinkDetail.isPrivate]
        
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.UpdateClink(self.objSelectedClinkDetail.strId, dictPaymentDetail).URLRequest, isBackgroundCall: true, pAlertView: self.view, pViewController: self) { (success, response) in
            
            if success == true {
                
                if isSent == true {
                    self.redirectToNextVC()
                }
            }
            else{
                Helper.showAlert("Clink! upload failed. Please try again.", pobjView: self.view)
            }
        }
    }
    
    func getCCContactsArray(arrContact : [ClsContact]) -> [String]
    {
        var arrIds = [String]()
        for objCCContact in arrCCContacts
        {
            if objCCContact.isClinkUser == false && objCCContact.isUnRemovable == false {
                arrIds.append(objCCContact.strId)
            }
        }
        return arrIds
    }
    
    func getCCUsersArray(arrContact : [ClsContact]) -> [String]
    {
        var arrIds = [String]()
        for objCCContact in arrCCContacts
        {
            if objCCContact.isClinkUser == true {
                arrIds.append(objCCContact.strUserid)
            }
        }
        return arrIds
    }
    
    func performNavigation(){
        if INCLUDEACCOUNT == true{
            if self.amount > 0 {
                if self.objSelectedVenue != nil && objSelectedVenue.arrCategoryName.first == KCATEGORYCHARITIES {
                    self.doDonationPayment()
                }
                else{
                    self.doPayment()
                }
            }
            else{
                self.updateClink(self.objSelectedClinkDetail, isSent: true)
                //                self.redirectToNextVC()
            }
        }
        else{
            self.updateClink(self.objSelectedClinkDetail, isSent: true)
            //            self.redirectToNextVC()
        }
    }
    
    func redirectToNextVC(){
        dispatch_async(dispatch_get_main_queue()) {
            USERDEFAULTS.setBool(false, forKey: KRECEIVEDCLINKSELECTED)
            USERDEFAULTS.synchronize()
            if self.controllerType == ClinkControllerType.ThankYou {
                APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
                APPDELEGATE.objSideMenu.showReceivedClink()
            }
            else if self.controllerType != ClinkControllerType.Gift{
                if self.previousScreenType == ClinkType.Sent{
                    APPDELEGATE.objSideMenu.showSentClink()
                    self.isFirstTimeLoading = false
                }
                else if self.previousScreenType == ClinkType.Received{
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                }
                else{
                    APPDELEGATE.objSideMenu.showHomeScreen()
                    self.isFirstTimeLoading = false
                }
            }
            else{
                APPDELEGATE.objSideMenu.setMenuSelectedItem(0)
                APPDELEGATE.objSideMenu.showHomeScreen()
            }
        }
    }
    
    @IBAction func btnHelpTapped(sender: AnyObject) {
        self.view.endEditing(true)
        
        //        let objViewController = UIStoryboard(name: MAINSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERFTUPOPUP)
        //        let subView = objViewController.view as! FTUPopUpView
        //        subView.subView.layer.cornerRadius = FTUPOPUPCORNERRADIUS
        //
        //         self.view.addSubview(objViewController.view)
        //        self.view.bringSubviewToFront(objViewController.view)
        //        subView.showInView(APPDELEGATE.window!) { (result) -> () in
        //            subView.removeFromSuperview()
        //        }
        
        let ftuView = NSBundle.mainBundle().loadNibNamed("FTUPopUpView", owner: self, options: nil)!.first as? FTUPopUpView
        ftuView?.frame = UIScreen.mainScreen().bounds
        ftuView?.subView.layer.cornerRadius = 8
        ftuView?.showInView(self.view, complitionBlock: { (result) -> () in
            ftuView!.removeFromSuperview()
        })
        //        self.view.addSubview(ftuView!)
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        self.view.endEditing(true)
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    @IBAction func btnAmountTapped(sender: AnyObject) {
        self.view.endEditing(true)
        //        if controllerType == ClinkControllerType.Gift && amount <= INTZERO{
        //            Helper.showAlert(ALERTCONFIRMADDGIFTAMOUNT,cancelButtonTitle: BTNYESTITLE,otherButtonTitle: BTNNOTITLE, pobjView: self.view, completionBock: { (alertObject, index) -> () in
        //                if index == 0{
        //                    self.performSegueWithIdentifier(SEGUETOAMOUNT, sender: nil)
        //                }
        //            })
        //        }
        //        else{
        self.performSegueWithIdentifier(SEGUETOAMOUNT, sender: nil)
        //        }
    }
    
    // MARK: - Button Image editor Actions
    
    @IBAction func btnDrawClicked(sender: UIButton) {
        isEdited = true
        if (btnColorPicker.selected == false)
        {
            self.btntextediting.enabled = false
            self.btnimgepicker.enabled = false
            self.btnSaveImage.enabled = false
            self.btnquestion.enabled = false
            self.btnColorPicker.enabled = true
            
            btnColorPicker.selected = true
            self.performAnimation(colorPickerWidthConstraint, value: COLORPICKERWIDTHCONST, duration: ANIMATIONDURATION)
            self.editingView.actionStartDrawing()
            self.addTrackThankyouClink(FlowTracker.UsedDrawingTool.rawValue)
        }
        else
        {
            self.btnColorPicker.enabled = false
            self.enableallButtons()
            let image = UIImage(named:IMGICNPENCIL)
            btnColorPicker.setImage(image, forState: .Normal)
            self.editingView.currentColor = UIColor.whiteColor()
            btnColorPicker.selected = false
            self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
            
            self.editingView.actionStartDrawing()
            self.editingView.enableSwipeFilter()
        }
        self.view.endEditing(true)
        SideMenuRootController.rightSwipeEnabled = false
    }
    
    @IBAction func btnAddTextClicked(sender: UIButton) {
        isEdited = true
        if (btntextediting.selected == false)
        {
            self.btnColorPicker.enabled = false
            self.btnimgepicker.enabled = false
            self.btnSaveImage.enabled = false
            self.btnquestion.enabled = false
            if colorPickerWidthConstraint.constant != CGFLOATZERO{
                self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
            }
            self.addTrackThankyouClink(FlowTracker.AddedText.rawValue)
            self.editingView.actionShowText()
            arrEditingToolsUsed.append(EditingToolTracker.Text.rawValue)
            btntextediting.selected = true
        }
        else{
            self.view.endEditing(true)
            self.enableallButtons()
            self.editingView.currentFeture = FetureType.NAN
            self.editingView.enableSwipeFilter()
            btntextediting.selected = false
        }
    }
    
    @IBAction func btnEraseClicked(sender: AnyObject) {
        isEdited = true
        self.view.endEditing(true)
        self.editingView.actionStartErase()
        let image = UIImage(named:IMGICNPENCIL)
        btnColorPicker.setImage(image, forState: .Normal)
        self.addTrackThankyouClink(FlowTracker.UsedEraseTool.rawValue)
    }
    
    @IBAction func btnClrClicked(sender: AnyObject) {
        isEdited = true
        let alertView = CustomAlert(title: APPNAME, message:ALERTAREYOUSUREYOUWANTTOCLEAR, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNYESTITLE, otherButtonTitle: BTNCANCELTITLE) { (customAlert, buttonIndex) -> Void in
            
            if buttonIndex == INTZERO{
                self.view.endEditing(true)
                self.editingView.currentColor = UIColor.whiteColor()
                let image = UIImage(named:IMGICNPENCIL)
                self.btnColorPicker.setImage(image, forState: .Normal)
                self.editingView.actionClear()
                self.performAnimation(self.colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
                self.enableallButtons()
                self.editingView.enableSwipeFilter()
                self.addTrackThankyouClink(FlowTracker.UsedClearTool.rawValue)
            }
        }
        alertView.showInView(self.view)
    }
    
    @IBAction func btnSaveEditedImage(sender : UIButton){
        self.addTrackThankyouClink(FlowTracker.SavedImage.rawValue)
        btnSaveImage.userInteractionEnabled = false
        self.getEditedImage(true)
    }
    
    func enableallButtons()
    {
        self.btntextediting.enabled = true
        self.btnColorPicker.enabled = true
        self.btnimgepicker.enabled = true
        self.btnSaveImage.enabled = true
        self.btnquestion.enabled = true
        
    }
    
    @IBAction func btnUndoTapped(sender: UIButton) {
        if SHOUDCOMMENT == true{
            Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
        }
        else{
            sender.userInteractionEnabled = false
            self.performSelector(#selector(SendNewClinkController.performUndo(_:)), withObject: sender, afterDelay: UNDOACTIONDELAY)
            sender.userInteractionEnabled = true
        }
    }
    
    func performUndo(sender:UIButton){
        
        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
            
            self.editingView.actionUndo()
            self.updateButtonStatus()
        }
    }
    
    // MARK: - other methods
    
    func openCameraOnce()
    {
        //        self.openCamera()
        self.showImagePicker()
    }
    
    
    func openCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            let storyboard = UIStoryboard(name: MAINSTORYBOARD, bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier(CLSCUSTOMECAMERAVC) as! CustomeCameraVC
            vc.delegate = self
            self.presentViewController(vc, animated: true, completion: nil)
        }
        else
        {
            openGallery()
        }
    }
    
    func showImagePicker(){
        self.view.endEditing(true)
        if controllerType == ClinkControllerType.ThankYou{
            USERDEFAULTS.setValue(STRSAYTY, forKey: KTITLE)
        }
        else if controllerType == ClinkControllerType.Gift
        {
            USERDEFAULTS.setValue(STRADDTOCLINK, forKey: KTITLE)
        }
        else{
            USERDEFAULTS.setValue(STRSENDCLINK, forKey: KTITLE)
        }
        USERDEFAULTS.synchronize()
        let alert:UIAlertController = UIAlertController(title: STRCHOOSEIMAGE, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        
        let cameraAction = UIAlertAction(title: STRCAMERA, style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: STRGALLERY, style: UIAlertActionStyle.Default)
        {
            UIAlertAction in
            self.openGallery()
            
        }
        let cancelAction = UIAlertAction(title: BTNCANCELTITLE, style: UIAlertActionStyle.Cancel)
        {
            UIAlertAction in
            
            if self.isFirstTimeLoading == true {
                if self.shouldPopToVenue == true{
                    self.navigationController?.popViewControllerAnimated(false)
                }
                else if self.controllerType == ClinkControllerType.ThankYou{
                    self.objDummyView.removeFromSuperview()
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                }
                else if self.controllerType == ClinkControllerType.Gift{
                    self.objDummyView.removeFromSuperview()
                    if self.previousScreenType == ClinkType.Sent{
                        APPDELEGATE.objSideMenu.showSentClink()
                        self.isFirstTimeLoading = false
                    }
                    else if self.previousScreenType == ClinkType.Received{
                        APPDELEGATE.objSideMenu.showReceivedClink()
                        self.isFirstTimeLoading = false
                    }
                    else{
                        APPDELEGATE.objSideMenu.showHomeScreen()
                        self.isFirstTimeLoading = false
                    }
                }
                else{
                    
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                }
            }
            else{
                self.objDummyView.removeFromSuperview()
            }
            
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func openGallery()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker!, animated: true, completion: nil)
    }
    
    func performAnimation(constraint : NSLayoutConstraint, value : CGFloat , duration: NSTimeInterval){
        do {
            try UIView.animateWithDuration(duration, animations: { () -> Void in
                constraint.constant = value
                self.view.layoutIfNeeded()
                }, completion: { (success) -> Void in
                    self.view.layoutIfNeeded()
            })
            
        } catch {
            print("Crash saved by try catch")
            // report error
        }
        
    }
    
    func getEditedImage(showAlert : Bool) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(imgView.frame.size, true, CGFLOATZERO)
        imgView.layer.addSublayer(self.editingView.layer)
        imgView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let editedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if showAlert == true{
            UIImageWriteToSavedPhotosAlbum(editedImage!, self, #selector(SendNewClinkController.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        return editedImage! // (editedImage?.crop(imgView.image!.size))!
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        guard error == nil else {
            //Error saving image
            btnSaveImage.userInteractionEnabled = true
            return
        }
        //Image saved successfully
        btnSaveImage.userInteractionEnabled = true
        Helper.showAlert(ALERTIMAGESAVED, pobjView: self.view)
    }
    
    func addTrackThankyouClink(element : String){
        if controllerType == ClinkControllerType.ThankYou{
            arrThankYouFlow.append(element)
        }
    }
    
    // MARK: - Textfield Delegate
    func textViewDidShow(textView: UITextView){
        if colorPickerWidthConstraint.constant != CGFLOATZERO{
            self.performAnimation(colorPickerWidthConstraint, value: CGFLOATZERO, duration: ANIMATIONDURATION)
        }
        self.btntextediting.enabled = true
        self.btnColorPicker.enabled = false
        self.btnimgepicker.enabled = false
        self.btnSaveImage.enabled = false
        self.btnquestion.enabled = false
        self.btntextediting.selected = true
        self.btnColorPicker.selected = false
        let image = UIImage(named:IMGICNPENCIL)
        btnColorPicker.setImage(image, forState: .Normal)
        self.editingView.currentColor = UIColor.whiteColor()
        
        self.editingView.isClrSelecting = false
        self.editingView.isErasing = false
        self.editingView.isDrawing = false
        self.editingView.isTexting = true
        self.editingView.currentFeture = FetureType.Text
        
    }
    
    func textViewDidHide(textView: UITextView){
        
        self.editingView.currentFeture = FetureType.NAN
        self.editingView.isTexting = false
        btntextediting.selected = false
        self.editingView.enableSwipeFilter()
        self.enableallButtons()
    }
    
    func edtingToolUser(strToolName : String){
        
    }
    
    func undoComplete(){
        Helper.hideHud(self.view)
    }
    
    // MARK: - Textfield Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        self.editingView.isTexting = false
        self.editingView.currentFeture = FetureType.NAN
        self.btnColorPicker.enabled = true
        self.btnimgepicker.enabled = true
        self.btnSaveImage.enabled = true
        self.btnquestion.enabled = true
        return true
    }
    
    // MARK: - Unwind Segue
    
    @IBAction func cancelToNewClinkVC(segue:UIStoryboardSegue) {
        SideMenuRootController.rightSwipeEnabled = false
    }
    
    @IBAction func saveToNewClinkVC(segue:UIStoryboardSegue) {
        SideMenuRootController.rightSwipeEnabled = false
        if(segue.sourceViewController.isKindOfClass(MyContactController)){
            
            let objMyContactVC = segue.sourceViewController as! MyContactController
            
            if isToSelected == true {
                self.objToContact = objMyContactVC.objToContact
                self.objToContact2 = objMyContactVC.objToContact
                
                if self.objToContact != nil{
                    
                    let strFullName = self.getFullName(objToContact)
                    
                    if strFullName.isEmptyString() == false{
                        btnTo.setTitle(strFullName, forState: .Normal)
                    }
                    else{
                        btnTo.setTitle(STREMPTYNAME, forState: .Normal)
                    }
                }
                else{
                    btnTo.setTitle(STREMPTY, forState: .Normal)
                }
            }
            else{
                self.arrCCContacts.removeAll()
                if controllerType == ClinkControllerType.ThankYou || controllerType == ClinkControllerType.Gift && objSelectedClinkDetail != nil && objSelectedClinkDetail.arrCCUser.count > 0{
                    
                    for objFixContact in objSelectedClinkDetail.arrCCUser{
                        if objFixContact.isUnRemovable == true{
                            self.arrCCContacts.append(objFixContact)
                        }
                    }
                    
                    if objSelectedClinkDetail.strSenderId != USERDEFAULTS.getUserDetailsObject().strId {
                        let objContact = ClsContact(pdicResponse: objSelectedClinkDetail.dictFromUser)
                        objContact.isUnRemovable = true
                        arrCCContacts.append(objContact)
                    }
                }
                
                for objSelection in objMyContactVC.arrCCContacts {
                    self.arrCCContacts.append(objSelection)
                }
                
                collectionViewEmailCC.reloadData()
            }
        }
        else if(segue.sourceViewController.isKindOfClass(VenueDetailsController))
        {
            
            let objVenueVC = segue.sourceViewController as! VenueDetailsController
            if objVenueVC.objSelectedVenue != nil{
                
                objSelectedVenue = objVenueVC.objSelectedVenue
                self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETZERO
                if let strIcnImgSmall = objSelectedVenue.strImgSmallIcon{
                    //                    btnVenue.sd_setImageWithURL(NSURL(string: strIcnImgSmall), forState: .Normal, placeholderImage: UIImage(named: "no_image"))
                    
                    if let url = NSURL(string:strIcnImgSmall) {
                        if let data = NSData(contentsOfURL: url) {
                            self.imgViewVenue.image = UIImage(data: data)
                            self.btnVenue.setImage(nil, forState: .Normal)
                            
                            self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                        }
                    }
                    
                }
                else{
                    self.btnVenue.setImage(nil, forState: .Normal)
                    self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                    self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                }
                if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEDRAMALEAGUE){
                    btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                    self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                    objToContact = self.getDramaLeague()
                }
                else if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEHORACEMANN){
                    btnSendClink.setImage(UIImage(named: IMGBTNDONATEBIG), forState: .Normal)
                    self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                    objToContact = self.getHoraceMann()
                }
                else{
                    btnSendClink.setImage(UIImage(named: IMGTXTSENDCLINK), forState: .Normal)
                }
                
                if objSelectedVenue.strCategory == KCATEGORYCHARITIES{
                    
                    if objToContact2 != nil {
                        self.arrCCContacts.append(objToContact2)
                        collectionViewEmailCC.reloadData()
                    }
                    
                    btnTo.setTitle(self.objSelectedVenue.objDefaultContact.strFullName, forState: .Normal)
                    btnTo.enabled = false
                    
                }
                else{
                    btnTo.enabled = true
                }
                self.objPromoDetails = nil
            }
        }
        else if(segue.sourceViewController.isKindOfClass(VenueHoundsDetailsController))
        {
            let objVenueVC = segue.sourceViewController as! VenueHoundsDetailsController
            if objVenueVC.objSelectedVenue != nil{
                self.btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETZERO
                objSelectedVenue = objVenueVC.objSelectedVenue
                if let strIcnImgSmall = objSelectedVenue.strImgSmallIcon{
                    if strIcnImgSmall.isEmptyString() == false{
                        //                        btnVenue.sd_setImageWithURL(NSURL(string: strIcnImgSmall), forState: .Normal, placeholderImage: UIImage(named: "no_image"))
                        
                        if let url = NSURL(string:strIcnImgSmall) {
                            if let data = NSData(contentsOfURL: url) {
                                self.imgViewVenue.image = UIImage(data: data)
                                self.btnVenue.setImage(nil, forState: .Normal)
                                self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                            }
                        }
                        
                    }
                    else{
                        self.btnVenue.setImage(nil, forState: .Normal)
                        self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                        self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                    }
                }
                else{
                    self.btnVenue.setImage(nil, forState: .Normal)
                    self.imgViewVenue.image = UIImage(named: IMGLOGOGOLD)
                    self.imgViewVenue.backgroundColor = UIColor.whiteColor()
                }
                if objSelectedVenue.strName.isEqualToIgnoringCase(VENUEHOUNDSPUB){
                    self.btnVenue.setTitle(STREMPTY, forState: .Normal)
                    btnSendClink.setImage(UIImage(named: IMGTXTSENDCLINK), forState: .Normal)
                }
                if self.objToContact?.strEmail == STRHMEMAIL || self.objToContact?.strEmail == STRDRAMAEMAIL
                {
                    objToContact = nil
                    btnTo.setTitle(" ", forState: .Normal)
                }
                
                if objSelectedVenue.strCategory != KCATEGORYCHARITIES{
                    btnTo.enabled = true
                }
            }
            if objVenueVC.objPromoDetails != nil{
                self.objPromoDetails = objVenueVC.objPromoDetails
            }
        }
        else if(segue.sourceViewController.isKindOfClass(AmountInputController)) {
            
            let objAmountInputVC = segue.sourceViewController as! AmountInputController
            
            amount = Int(objAmountInputVC.lblAmount.text!)
            
            if amount > INTZERO {
                btnAmount.setBackgroundImage(UIImage(named: IMGBTNAMOUNT), forState: .Normal)
                btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(objAmountInputVC.lblAmount.text!)" , withColor: KFONTTHEMENEW), forState: .Normal)
                
                if controllerType == ClinkControllerType.Gift && objSelectedClinkDetail != nil{
                    
                    //                    let strAmount = "\(Int(objSelectedClinkDetail.strAmount)!)"
                    
                    var intAmount = Int(objSelectedClinkDetail.strAmount)
                    if let intGiftAmount = Int(objSelectedClinkDetail.strGiftPlusAmount) {
                        intAmount = intAmount! + intGiftAmount
                    }
                    
                    let strAmount = "\(intAmount!)"
                    
                    btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(strAmount) + $ " + objAmountInputVC.lblAmount.text!,withColor: KFONTTHEMENEW), forState: .Normal)
                }
            }
            else{
                btnAmount.setTitle(" ", forState:
                    UIControlState.Normal)
                btnAmount.setAttributedTitle(NSAttributedString(string: "Add $"), forState: .Normal)
                btnAmount.titleLabel?.font = UIFont(name: FONTGOTHAMMEDIUM, size: FONTSIZE18)
                btnAmount.setBackgroundImage(UIImage(named: IMGBTNAMOUNT), forState: .Normal)
                
                if controllerType == ClinkControllerType.Gift && objSelectedClinkDetail != nil {
                    
                    btnAmount.setBackgroundImage(UIImage(named: IMGBTNAMOUNT), forState: .Normal)
                    
                    //                    let strAmount = "\(Int(objSelectedClinkDetail.strAmount)!)"
                    
                    var intAmount = Int(objSelectedClinkDetail.strAmount)
                    if let intGiftAmount = Int(objSelectedClinkDetail.strGiftPlusAmount) {
                        intAmount = intAmount! + intGiftAmount
                    }
                    
                    let strAmount = "\(intAmount!)"
                    
                    btnAmount.setTitle("$ \(strAmount) +", forState:
                        UIControlState.Normal)
                    btnAmount.setAttributedTitle(CommonAttributedString.getFormattedString("$ \(strAmount) +" ,withColor: KFONTTHEMENEW), forState: .Normal)
                }
            }
        }
    }
    
    func getFullName (objContact : ClsContact) -> String{
        var strFullName = STREMPTY
        if objContact.strFirstName.isEmptyString() == false  && objContact.strFirstName.isEmptyString() == false{
            strFullName += objContact.strFirstName
        }
        if objContact.strLastName.isEmptyString() == false && objContact.strLastName != " "{
            strFullName += " " + objContact.strLastName
        }
        return strFullName
    }
    
    func customeVenueEdge(){
        //        btnVenue.imageEdgeInsets = BTNVENUEUIMAGEINSETDEFAULT
    }
    
    @IBAction func btnVenueClicked(sender: AnyObject) {
        self.btnVenue.highlighted = false
    }
    
    // MARK: - slider value changed
    @IBAction func sliderChanged(sender: UISlider) {
        brush = CGFloat(sender.value)
        editingView.brushWidth = brush
    }
    
    func showLoginScreen(animated : Bool){
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: AUTHENTICATIONSTORYBOARD, bundle: nil)
        let loginVC : UIViewController = mainView.instantiateViewControllerWithIdentifier(CONTROLLERLOGIN) as UIViewController
        self.presentViewController(loginVC, animated: animated, completion: nil)
    }
    
    // MARK: - Navigate To Feed
    func showFeedScreen(){
        APPDELEGATE.objSideMenu.setMenuSelectedItem(0)
        APPDELEGATE.objSideMenu.showHomeScreen()
    }
    
    func getHoraceMann() -> ClsContact {
        var objContact = ClsContact()
        
        /*  objContact = ClsContact(pClinkId: STRHORACEID, puserName:STRHMEMAIL , pfirstName: STRHORACE, plastName: STRMANN, pemail: STRHMEMAIL, pphone: STRPHONE, pisClinkUser: true, pisSelected: true, puserid: NOTDEFINED)*/
        //        btnTo.setTitle(objContact.strEmail, forState: .Normal)
        
        // Done by :- Dhruvit - 2016-11-07
        objContact = self.objSelectedVenue.objDefaultContact
        btnTo.setTitle(self.objSelectedVenue.objDefaultContact.strFullName, forState: .Normal)
        return objContact
    }
    
    func getDramaLeague() -> ClsContact {
        var objContact = ClsContact()
        
        /* objContact = ClsContact(pClinkId: STRDLID, puserName:STRDRAMAEMAIL , pfirstName: STRDRAMA, plastName: STRLEAUGE, pemail: STRDRAMAEMAIL, pphone: STRPHONE, pisClinkUser: true, pisSelected: true, puserid: NOTDEFINED)*/
        //        btnTo.setTitle(objContact.strEmail, forState: .Normal)
        
        // Done by :- Dhruvit - 2016-11-07
        objContact = self.objSelectedVenue.objDefaultContact
        btnTo.setTitle(self.objSelectedVenue.objDefaultContact.strFullName, forState: .Normal)
        return objContact
    }
    
    func setDefaultVenue(){
        self.setDefaultVenueImage()
    }
    
    // MARK: - ACEDrawingView Delegate
    func drawingView(view: ACEDrawingView, didEndDrawUsingTool tool: ACEDrawingTool) {
        self.updateButtonStatus()
        
    }
    func updateButtonStatus() {
        let enabled = self.editingView.newDrawingView.canUndo()
        self.btnUndo.enabled = enabled
        self.btnUndo.userInteractionEnabled = enabled
    }
    
    
    @IBAction func undo(sender: AnyObject) {
        self.editingView.newDrawingView.undoLatestStep()
        self.updateButtonStatus()
    }
    
    @IBAction func redo(sender: AnyObject) {
        self.editingView.newDrawingView.redoLatestStep()
        self.updateButtonStatus()
    }
    
    @IBAction func clear(sender: AnyObject) {
        self.editingView.newDrawingView.clear()
        self.updateButtonStatus()
    }
    
}
