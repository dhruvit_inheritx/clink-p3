//
//  SendNewClinkController.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import ParseUI

class SendNewClinkController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CustomeCameraDelegate {
    
    //    MARK:- Gloable Variable
    var imagePicker : UIImagePickerController!
    var objSelectedClinkDetail : ClsClinkDetails!
    var objSelectedTYClinkDetail : ClsClinkDetails!
    var objToContact : ClsContact!
    var arrCCContacts : [ClsContact]!
    var isToSelected : Bool!
    var arrCCName :[String]!
    var arrCCId :[String]!
    var amount : Int!
    var isThankYouClink = false
    var objSelectedVenue : ClsVenue!
    var brush :CGFloat!
    //    MARK:- IBOutlet
    var imgLastPoint = CGPoint.zero
    var isFirstTimeLoading = true
    
    @IBOutlet var editingView : GBEditingView!
    @IBOutlet var colorPickerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var btnTo : UIButton!
    @IBOutlet var btnCC : UIButton!
    @IBOutlet var imgView : UIImageView!
//    @IBOutlet weak var sliderHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var ToCCHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var editingToolsHeight: NSLayoutConstraint!
    @IBOutlet weak var editingToolsWidth: NSLayoutConstraint!
    @IBOutlet weak var venueHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSendHeight: NSLayoutConstraint!
    
//    @IBOutlet var labelBrush : UILabel!
    @IBOutlet var imgColorPicker : UIImageView!
    @IBOutlet var btnColorPicker : UIButton!
     @IBOutlet var colorView : UIView!
    
    @IBOutlet var btnVenue : UIButton!
    @IBOutlet var btnAmount : UIButton!
    @IBOutlet var btnSendClink : UIButton!
    @IBOutlet var btnSaveImage : UIButton!
    
    @IBOutlet var bgColorView : UIView!
    
    @IBOutlet var bottomView : UIView!
    @IBOutlet var bottomViewTQ : UIView!
    
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnClose : UIButton!
    @IBOutlet weak var previewHeight: NSLayoutConstraint!
    
     // MARK: - Viewcontroller Cycle
    
    override func viewWillAppear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = false
        if PFUser.currentUser() == nil{
            self.showLoginScreen(false)
        }
    }
    override func viewWillDisappear(animated: Bool) {
        self.view.endEditing(true)
        SideMenuRootController.rightSwipeEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        btnBack.hidden = true
        btnClose.hidden = false
        // Do any additional setup after loading the view.
        imagePicker =  UIImagePickerController()
    
        imagePicker.delegate = self
        arrCCContacts = Array()
        arrCCName = Array()
        arrCCId  = Array()
        isToSelected = false
        amount = 0
        self.performAnimation(colorPickerWidthConstraint, value: 0, duration: 0.0)
        if objSelectedTYClinkDetail != nil{
           objSelectedClinkDetail = objSelectedTYClinkDetail
        }
        if isThankYouClink == true{
//            ToCCHeightConstraint.constant = 27
//            insideViewHeightConstraint.constant = -27
            btnTo.userInteractionEnabled = !isThankYouClink
            btnCC.userInteractionEnabled = !isThankYouClink
            btnTo.setTitle(objSelectedClinkDetail.strSenderName, forState: .Normal)
            arrCCName = objSelectedClinkDetail.arrCCName as! [String]
            arrCCId = objSelectedClinkDetail.arrCCID  as! [String]
            btnAmount.userInteractionEnabled = !isThankYouClink
            if objSelectedClinkDetail.intAmount != 0 {
                btnAmount.setTitle("$ \(objSelectedClinkDetail.intAmount)", forState: UIControlState.Normal)
                amount = Int(objSelectedClinkDetail.intAmount)
            }
            btnVenue.userInteractionEnabled = !isThankYouClink
            if arrCCName.count > 0{
                btnCC.setTitle(arrCCName.joinWithSeparator(", "), forState: .Normal)
            }
            else{
                ToCCHeightConstraint.constant = 27
            }
            if objSelectedClinkDetail.strVenueName == "DRAMA LEAGUE"{
                btnVenue.setImage(UIImage(named: "btn_venue_dl"), forState: UIControlState.Normal)
//                btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
            }
            else if objSelectedClinkDetail.strVenueName == "Horace Mann School"{
                btnVenue.setImage(UIImage(named: "btn_venue_hm"), forState: UIControlState.Normal)
//                btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
            }
            else if objSelectedClinkDetail.strVenueName == "HOUNDSTOOTH PUB"{
                btnVenue.setImage(UIImage(named: "btn_venue_hp"), forState: UIControlState.Normal)
//                btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
            }
            
//            btnSendClink.setTitle("CLINK! BACK", forState: .Normal)
        }
        else{
            ToCCHeightConstraint.constant = 60
//            insideViewHeightConstraint.constant = -60
            btnTo.userInteractionEnabled = true
//            btnSendClink.setTitle("SEND CLINK!", forState: .Normal)
        }
        self.openCameraOnce()       
        
    }
    override func viewDidAppear(animated: Bool) {
//        if editingView.isLoadedOnce == false{
//            editingView.originalImage = imgView.image
//            editingView.setUpMaskingView()
//        }
    }
    
    // MARK: - Memory Management    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    MARK:- Touches Events
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            imgLastPoint = touch.locationInView(self.imgColorPicker)
            self.touchesMoved(touches, withEvent: event)
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // 6
        isFirstTimeLoading = false
        if let touch = touches.first {
           if touch.view == bgColorView{
                self.view.endEditing(true)
                self.editingView.isClrSelecting = false
                self.editingView.isErasing = false
                self.editingView.isDrawing = true
                self.editingView.enableDrawing()
                
                let currentPoint = touch.locationInView(self.imgColorPicker)
                // 7
                
                if currentPoint.x > 0 && currentPoint.y > 0 && currentPoint.x < 103.5 && currentPoint.y < 19.5
                {
                imgLastPoint = currentPoint
                print("Touch point = \(currentPoint)")
                editingView.currentColor = imgColorPicker.getColorOfPoint(imgLastPoint)
                
                let image = UIImage(named:"icn_pencil")
                btnColorPicker.setImage(image?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
//                btnColorPicker.imageView?.image = image?.imageWithRenderingMode(.AlwaysTemplate)
                btnColorPicker.imageView?.tintColor = editingView.currentColor
                btnColorPicker.tintColor = editingView.currentColor
            }
            }
            else if touch.view != editingView.txtViewLbl{
                self.view.endEditing(true)
            }
            
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
//        self.touchesMoved(touches, withEvent: event)
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSELECTMYCONTACT{
            let objContactVC = segue.destinationViewController as! MyContactController
            objContactVC.isContactSelecting = true
            objContactVC.isToSelected = self.isToSelected
            objContactVC.arrCCContacts = self.arrCCContacts
            objContactVC.objToContact = self.objToContact
        }
        else if segue.identifier == SEGUETOSELECTVENUE{
            let objVenueVC = segue.destinationViewController as! VenueListController
            objVenueVC.isVenueSelecting = true
            if objSelectedVenue != nil{
                objVenueVC.objSelectedVenue = self.objSelectedVenue
            }
        }
        else if segue.identifier == SEGUETOAMOUNT{
            let objAmountVC = segue.destinationViewController as! AmountInputController
            objAmountVC.fAmount = amount
            
        }
    }
    
    // MARK: - Imagepicker Delegate
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        isFirstTimeLoading = false
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView.contentMode = .ScaleAspectFit
            
            if picker.sourceType == UIImagePickerControllerSourceType.Camera{
                if picker.cameraDevice == UIImagePickerControllerCameraDevice.Front{
                    let image = UIImage(CGImage: pickedImage.CGImage!, scale:pickedImage.scale, orientation: .LeftMirrored)
                    imgView.image = image
                }
                else{
                    imgView.image = pickedImage
                }
            }
            else
            {
                imgView.image = pickedImage
            }
            
            let imgSize = pickedImage.size
            print("Size is \(imgSize) window size = \(APPDELEGATE.window?.frame.size)")
            
            editingView.originalImage = imgView.image
            editingView.setUpMaskingView()
            self.editingView.actionClear()
            
            self.view.endEditing(true)
            editingView.currentColor = UIColor.whiteColor()
            let image = UIImage(named:"icn_pencil")
            btnColorPicker.setImage(image, forState: .Normal)
            
            self.performAnimation(colorPickerWidthConstraint, value: 0, duration: 0.3)
            editingView.isDrawing = false
            editingView.enableSwipeFilter()
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        if isFirstTimeLoading == true {
            
            if isThankYouClink == true{
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                })
            }
            else{
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                })
            }            
        }
        else{
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    //MARK: - Custom Camera Delegate
    func didFinishPickingImage(image: UIImage!) {
        
            self.isFirstTimeLoading = false
            if let pickedImage = image {
                imgView.contentMode = .ScaleAspectFit
                imgView.image = pickedImage
                let imgSize = pickedImage.size
                print("Size is \(imgSize) window size = \(APPDELEGATE.window?.frame.size)")
                
                editingView.originalImage = imgView.image
                editingView.setUpMaskingView()
                self.editingView.actionClear()
                
                self.view.endEditing(true)
                editingView.currentColor = UIColor.whiteColor()
                let image = UIImage(named:"icn_pencil")
                btnColorPicker.setImage(image, forState: .Normal)
                
                self.performAnimation(colorPickerWidthConstraint, value: 0, duration: 0.3)
                editingView.isDrawing = false
                editingView.enableSwipeFilter()
            }
        
    }
    
    func yCameraControllerDidCancel() {
        if isFirstTimeLoading == true {
            if isThankYouClink == true{
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    APPDELEGATE.objSideMenu.showReceivedClink()
                    self.isFirstTimeLoading = false
                })
            }
            else{
                dismissViewControllerAnimated(true, completion: { () -> Void in
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                })
            }
        }
//        else{
//            dismissViewControllerAnimated(true, completion: nil)
//        }
    }
    
    func yCameraControllerdidSkipped() {
        self.imgView.image = nil
        
    }
    // MARK: - Button Actions

    @IBAction func btnShowImagePicker(sender : UIButton){
        self.view.endEditing(true)
        let alert:UIAlertController = UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openCamera()
                
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    @IBAction func btnShowToContactPicker(sender : UIButton){
        self.view.endEditing(true)
        isToSelected = true
        self.performSegueWithIdentifier(SEGUETOSELECTMYCONTACT, sender: nil)
    }
    @IBAction func btnShowCCContactPicker(sender : UIButton){
        self.view.endEditing(true)
        isToSelected = false
        performSegueWithIdentifier(SEGUETOSELECTMYCONTACT, sender: nil)
    }
    
    @IBAction func btnClose(sender : UIButton){
        let alertView = CustomAlert(title: APPNAME, message:"Are you sure that you would like to close current CLINK?", delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: "Stop CLINKING", otherButtonTitle: "Keep CLINKING") { (customAlert, buttonIndex) -> Void in
            
            if buttonIndex == 0{
                if self.isThankYouClink == true{
                        APPDELEGATE.objSideMenu.showReceivedClink()
                        self.isFirstTimeLoading = false
                }
                else{
                    self.showFeedScreen()
                    self.isFirstTimeLoading = false
                }
            }            
        }
        alertView.showInView(APPDELEGATE.window)
        
    }
    
    @IBAction func btnBack(sender : UIButton){
        self.view.endEditing(true)
//        ToCCHeightConstraint.constant = 60
        editingToolsHeight.constant = 45
        venueHeight.constant = 43
         btnSendHeight.constant = 50
        editingToolsWidth.constant = 225
         previewHeight.constant = 0
        btnBack.hidden = true
        btnClose.hidden = false
        btnSendClink.hidden = false
        self.view.layoutIfNeeded()
    }
    func btnPrepareToPreview(){
        self.view.endEditing(true)
//        ToCCHeightConstraint.constant = 0
        editingToolsHeight.constant = 0
        venueHeight.constant = 0
        btnSendHeight.constant = 0
        previewHeight.constant = 50
        editingToolsWidth.constant = 0
        self.view.layoutIfNeeded()
        btnBack.hidden = false
        btnClose.hidden = true
        btnSendClink.hidden = true
    }
    
    @IBAction func btnSendClink(sender : UIButton){
        
//        if sender.tag == 111{
//            self.view.endEditing(true)
//            self.btnPrepareToPreview()
//        }
//        else if sender.tag == 222{
//            self.sendClink()
//        }
        self.performSegueWithIdentifier("segueToPreviewClink", sender: nil)
    }
    
    func sendClink(){
        let currentUser = PFUser.currentUser()?.objectId
        var currentUserName = PFUser.currentUser()?.objectForKey("FirstName") as! String
        let strLastName = PFUser.currentUser()?.objectForKey("LastName") as! String
        currentUserName = currentUserName + " " + strLastName
        
        if isThankYouClink == true{
            if objSelectedClinkDetail != nil{
                
                //                This is thank you clink
                
                let alertView = CustomAlert(title: APPNAME, message:"Congratulations! Looks great!", delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: "Yup, send it", otherButtonTitle: "Nope, not yet") { (customAlert, buttonIndex) -> Void in
                    
                    if buttonIndex == 0{
                        
                        Helper.showHud(self.view)
                        
                        var strVenueId = " "
                        if self.objSelectedClinkDetail.strVenueId != nil{
                            strVenueId = self.objSelectedClinkDetail.strVenueId
                        }
                        var strVenueName = " "
                        if self.objSelectedClinkDetail.strVenueId != nil{
                            strVenueName = self.objSelectedClinkDetail.strVenueName
                        }
                        
                        let imgToSend = self.getEditedImage(false)
                        
                        ClinkDetailsManager.sharedInstance.sendClink(currentUser!, pSenderName: currentUserName, pReceiverId: self.objSelectedClinkDetail.strSenderId, pReceiverName: self.objSelectedClinkDetail.strSenderName, pOtherUserIds: self.objSelectedClinkDetail.arrCCID,pOtherUserNames: self.objSelectedClinkDetail.arrCCName, pVenueId : strVenueId,pVenueName: strVenueName, pDescription: "Thank you Clink!", pclinkType: ClinkType.Thankyou, pImage: imgToSend, pAmount: self.amount ,pReferenceClinkId : self.objSelectedClinkDetail.strObjectId) { (success, response) -> () in
                            self.parseClinkResponse(success, response: response)
                        }
                    }
                }
                
                alertView.showInView(APPDELEGATE.window)
                
            }
        }
        else
        {
            if objToContact == nil{
                Helper.showAlert("Add at least one recipient to send Clink!", pobjView: self.view)
            }
            else{
                //                Normal clink
                let alertView = CustomAlert(title: APPNAME, message:"Congratulations! Looks great!", delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: "Yup, send it", otherButtonTitle: "Nope, not yet") { (customAlert, buttonIndex) -> Void in
                    
                    if buttonIndex == 0{
                        
                        Helper.showHud(self.view)
                        
                        var strVenueId = " "
                        var strVenueName = " "
                        if self.objSelectedVenue != nil{
                            strVenueId = self.objSelectedVenue.id
                            strVenueName = self.objSelectedVenue.strName
                        }
                        let imgToSend = self.getEditedImage(false)
                        
                        var strToFullName = self.getFullName(self.objToContact)
                        
                        if strToFullName.isEmptyString() == true{
                            strToFullName = "Empty Name"
                        }
                        
                        
                        ClinkDetailsManager.sharedInstance.sendClink(currentUser!, pSenderName: currentUserName, pReceiverId: self.objToContact.strClinkId, pReceiverName: strToFullName, pOtherUserIds: self.arrCCId,pOtherUserNames: self.arrCCName, pVenueId : strVenueId,pVenueName : strVenueName, pDescription: "This is test Clink!", pclinkType: ClinkType.Sent, pImage: imgToSend, pAmount: self.amount,pReferenceClinkId : "NA") { (success, response) -> () in
                            self.parseClinkResponse(success, response: response)
                        }
                    }
                }
                alertView.showInView(APPDELEGATE.window)
            }
            
        }

    }
    func parseClinkResponse(success : Bool , response : AnyObject){
        
        Helper.hideHud(self.view)
        if success == true{
            USERDEFAULTS.setBool(false, forKey: KRECEIVEDCLINKSELECTED)
            USERDEFAULTS.synchronize()
            if isThankYouClink == true{
                APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
                APPDELEGATE.objSideMenu.showReceivedClink()
            }
            else{
                APPDELEGATE.objSideMenu.setMenuSelectedItem(3)
                APPDELEGATE.objSideMenu.showHomeScreen()
            }
        }
        else{
            let error = response as! NSError
            Helper.showAlert(error.localizedDescription, pobjView: self.view)
        }        
    }
    
    @IBAction func btnHelpTapped(sender: AnyObject) {
        self.view.endEditing(true)
       let objViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("helpPopUp")
        let subView = objViewController.view as! helpPopUpView
        subView.subView.layer.cornerRadius = 5.0
         subView.btnSelection.selected = false
        if USERDEFAULTS.boolForKey(KSHOWHELPOPUP) == true{
            subView.btnSelection.selected = true
        }
        
        self.view.addSubview(objViewController.view)
//         Helper.showAlert("This feature is coming soon", pobjView: self.view)
    }
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        self.view.endEditing(true)
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    @IBAction func btnAmountTapped(sender: AnyObject) {
        self.view.endEditing(true)
        self.performSegueWithIdentifier(SEGUETOAMOUNT, sender: nil)
    }
    // MARK: - Button Image editor Actions
    
    @IBAction func btnDrawClicked(sender: AnyObject) {
        self.view.endEditing(true)
        self.editingView.actionStartDrawing()
        self.manageDrawingViewAnimation()
    }
    
    @IBAction func btnAddTextClicked(sender: AnyObject) {
        if colorPickerWidthConstraint.constant != 0{
            self.performAnimation(colorPickerWidthConstraint, value: 0, duration: 0.3)
        }
        self.editingView.actionShowText()
    }
    
    @IBAction func btnEraseClicked(sender: AnyObject) {
        self.view.endEditing(true)
        self.editingView.actionStartErase()
        let image = UIImage(named:"icn_pencil")
        btnColorPicker.setImage(image, forState: .Normal)
        self.manageSliderViewAnimation(0)
    }
    
    @IBAction func btnClrClicked(sender: AnyObject) {
        let alertView = CustomAlert(title: APPNAME, message:"Are you sure you want to clear the screen?", delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: "Yes", otherButtonTitle: "Cancel") { (customAlert, buttonIndex) -> Void in
            
            if buttonIndex == 0{
                self.view.endEditing(true)
                self.editingView.currentColor = UIColor.whiteColor()
                let image = UIImage(named:"icn_pencil")
                self.btnColorPicker.setImage(image, forState: .Normal)
                self.editingView.actionClear()
                self.performAnimation(self.colorPickerWidthConstraint, value: 0, duration: 0.3)
                self.manageSliderViewAnimation(0)
            }
        }
        alertView.showInView(self.view)
    }
    
    @IBAction func btnSaveEditedImage(sender : UIButton){
        btnSaveImage.userInteractionEnabled = false
        self.getEditedImage(true)
        
    }
    
    // MARK: - other methods
    
    func openCameraOnce()
    {
        self.openCamera()
    }
    
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
//            imagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
//            imagePicker!.cameraDevice = UIImagePickerControllerCameraDevice.Front
//            self .presentViewController(imagePicker!, animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("CustomeCameraVC") as! CustomeCameraVC
            vc.delegate = self
            self.presentViewController(vc, animated: true, completion: nil)
        }
        else
        {
            openGallery()
        }
    }
    func openGallery()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker!, animated: true, completion: nil)
        
    }
    
    func manageDrawingViewAnimation(){
        if editingView.isDrawing == true || editingView.isErasing == true{
            self.manageSliderViewAnimation(0)
            self.performAnimation(colorPickerWidthConstraint, value: 169, duration: 0.3)
        }
        else if editingView.isDrawing == false || editingView.isErasing == false{
            self.manageSliderViewAnimation(40)
            self.performAnimation(colorPickerWidthConstraint, value: 0, duration: 0.3)
        }
    }
    
    func manageSliderViewAnimation(value : CGFloat){
//        self.performAnimation(sliderHeightConstraint, value: value, duration: 0.4)
    }
    
    func performAnimation(constraint : NSLayoutConstraint, value : CGFloat , duration: NSTimeInterval){
        do {
            try UIView.animateWithDuration(duration, animations: { () -> Void in
                constraint.constant = value
                self.view.layoutIfNeeded()
                }, completion: { (success) -> Void in
                    self.view.layoutIfNeeded()
            })
            
        } catch {
            // report error
            print("Something went wrong")
        }
        
    }
    
    
    func getEditedImage(showAlert : Bool) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(imgView.frame.size, true, 0)
        imgView.layer.addSublayer(self.editingView.layer)
        imgView.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let editedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if showAlert == true{
            UIImageWriteToSavedPhotosAlbum(editedImage, self, "image:didFinishSavingWithError:contextInfo:", nil)
        }
//        else{
//            UIImageWriteToSavedPhotosAlbum(editedImage, nil,nil, nil)
//        }
        return editedImage
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        guard error == nil else {
            //Error saving image
            btnSaveImage.userInteractionEnabled = true
            return
        }
        //Image saved successfully
        btnSaveImage.userInteractionEnabled = true
        Helper.showAlert("Image saved in photos.", pobjView: self.view)
    }
    
    // MARK: - Textfield Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // MARK: - Unwind Segue
    
    @IBAction func cancelToNewClinkVC(segue:UIStoryboardSegue) {
        print("Canceled")
    }
    @IBAction func saveToNewClinkVC(segue:UIStoryboardSegue) {
        if(segue.sourceViewController.isKindOfClass(MyContactController))
        {
            arrCCId.removeAll()
            arrCCName.removeAll()
            let objMyContactVC = segue.sourceViewController as! MyContactController
            
            self.arrCCContacts.removeAll()
            self.arrCCContacts = objMyContactVC.arrCCContacts
            if isToSelected == true{
                 self.objToContact = objMyContactVC.objToContact
                if self.objToContact != nil{
                    
                    let strFullName = self.getFullName(objToContact)
                    
                    if strFullName.isEmptyString() == false{
                         btnTo.setTitle(strFullName, forState: .Normal)
                    }
                    else{
                        btnTo.setTitle("Empty Name", forState: .Normal)
                    }
                }
                else{
                    btnTo.setTitle("Select From Contacts", forState: .Normal)
                }
            }
            else{
                
                for objContact in arrCCContacts{
                    
                    if objContact.strClinkId != nil
                    {
                        arrCCId.append(objContact.strClinkId)
                    }
                    let strFullName = self.getFullName(objContact)
                    
                    if strFullName.isEmptyString() == false{
                        arrCCName.append(strFullName)
                    }
                    else{
                        arrCCName.append("Empty Name")
                    }
                }
                if arrCCName.count > 0{
                    btnCC.setTitle(arrCCName.joinWithSeparator(", "), forState: .Normal)
                }
                else{
                    btnCC.setTitle("Select From Contacts", forState: .Normal)
                }
            }
        }
        else if(segue.sourceViewController.isKindOfClass(VenueDetailsController))
        {
            let objVenueVC = segue.sourceViewController as! VenueDetailsController
            if objVenueVC.objSelectedVenue != nil{
                objSelectedVenue = objVenueVC.objSelectedVenue
                
                if objSelectedVenue.strName == "DRAMA LEAGUE"{
                    btnVenue.setImage(UIImage(named: "btn_venue_dl"), forState: UIControlState.Normal)
//                    btnSendClink.setTitle("DONATE ", forState: UIControlState.Normal)
                    btnSendClink.selected = true
                }
                else if objSelectedVenue.strName == "Horace Mann School"{
                    btnVenue.setImage(UIImage(named: "btn_venue_hm"), forState: UIControlState.Normal)
//                    btnSendClink.setTitle("DONATE ", forState: UIControlState.Normal)
                    btnSendClink.selected = true
                }
                else if objSelectedVenue.strName == "HOUNDSTOOTH PUB"{
                    btnVenue.setImage(UIImage(named: "btn_venue_hp"), forState: UIControlState.Normal)
//                    btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
                    btnSendClink.selected = false
                }
                else{
                    btnVenue.setImage(UIImage(named: "btn_venue"), forState: UIControlState.Normal)
//                    btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
                    btnSendClink.selected = false
                }
            }
        }
        else if(segue.sourceViewController.isKindOfClass(VenueHoundsDetailsController))
        {
            let objVenueVC = segue.sourceViewController as! VenueHoundsDetailsController
            if objVenueVC.objSelectedVenue != nil{
                objSelectedVenue = objVenueVC.objSelectedVenue
                
                if objSelectedVenue.strName == "HOUNDSTOOTH PUB"{
                    btnVenue.setImage(UIImage(named: "btn_venue_hp"), forState: UIControlState.Normal)
//                    btnSendClink.setTitle("SEND CLINK!", forState: UIControlState.Normal)
                    btnSendClink.selected = false
                }
                
            }
        }
        else if(segue.sourceViewController.isKindOfClass(AmountInputController))
        {
            let objAmountInputVC = segue.sourceViewController as! AmountInputController
            amount = Int(objAmountInputVC.lblAmount.text!)
            if amount > 0{
                print("Amount Saved \(objAmountInputVC.lblAmount.text)")
                btnAmount.selected = true
                btnAmount.setTitle("$ " + objAmountInputVC.lblAmount.text!, forState:
                    UIControlState.Normal)
            }
            else{
                btnAmount.selected = false
                btnAmount.setTitle("", forState:
                    UIControlState.Normal)
            }
        }
        
    }
    
    func getFullName (objContact : ClsContact) -> String{
        var strFullName = ""
        if objContact.strFirstName != nil  && objContact.strFirstName != " "{
            strFullName += objContact.strFirstName
        }
        if objContact.strLastName != nil && objContact.strLastName != " "{
            strFullName += " " + objContact.strLastName
        }
        return strFullName
    }
    
    // MARK: - slider value changed
    @IBAction func sliderChanged(sender: UISlider) {
        brush = CGFloat(sender.value)
        editingView.brushWidth = brush
//        labelBrush.text = NSString(format: "%.0f", brush.native) as String
    }
    func showLoginScreen(animated : Bool){
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: "Authentication", bundle: nil)
        let loginVC : UIViewController = mainView.instantiateViewControllerWithIdentifier("LoginVC") as UIViewController
        self.presentViewController(loginVC, animated: animated, completion: nil)
        
    }
    // MARK: - Navigate To Feed
    func showFeedScreen(){
        APPDELEGATE.objSideMenu.setMenuSelectedItem(0)
        APPDELEGATE.objSideMenu.showHomeScreen()
//        self.sideMenuController()?.performSegueWithIdentifier("CenterContainment", sender: nil)
    }
}
