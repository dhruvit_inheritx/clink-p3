//
//  AccountsVC.swift
//  Clink
//
//  Created by Gunjan on 20/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource
import Fabric
import TwitterKit
import FBSDKShareKit

class AccountsVC: UIViewController,UITableViewDelegate,UITextFieldDelegate,TWTRComposerViewControllerDelegate {
    
    // MARK: - Variables
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var tblViewAccount: UITableView!
    
    var txtPassword : UITextField!
    var txtDOB : UITextField!
    
    var strSSN : String! = ""
    var strDOB : String! = ""
    var strPhone : String! = ""
    var strEmail : String! = ""
    var strPassword : String! = ""
    var strConfirmPassword : String! = ""
    
    var objUserDetails = ClsUserDetails()
    
    var objHDTableDataSource : HDTableDataSource!
    
    var arrayAccountDetail : [Dictionary<String, AnyObject>] = Array()
    
    var arrayHeaderTitle: [AnyObject] = Array()
    var arrayProfileDetail : [Dictionary<String, AnyObject>] = Array()
    var arrayCreditCard: [AnyObject] = Array()
    var arrayClinkBalance: [AnyObject] = Array()
    var arrayNotifications : [AnyObject] = Array()
    var arrayHistory : [AnyObject] = Array()
    
    var strUserBalance : String! = "0"
    
    let datePickerView  : UIDatePicker = UIDatePicker()
    
    var objSelectedAccountHistoryControllerType : AccountHistoryControllerType! = AccountHistoryControllerType.Redeemed
    
    var addPaymentMethodControllerType : AddPaymentMethodControllerType! = AddPaymentMethodControllerType.Payment
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblViewAccount.tableFooterView = UIView()
        
        objUserDetails = USERDEFAULTS.getUserDetailsObject() as ClsUserDetails
        
        self.strUserBalance = objUserDetails.strBalance
        
        arrayProfileDetail = [[KIMAGE:"icn_email",KPLACEHOLDER:"EMAIL ADDRESS"],[KIMAGE:"icn_phone_number",KPLACEHOLDER:"PHONE NUMBER"],[KIMAGE:"icn_password",KPLACEHOLDER:"PASSWORD"],[KIMAGE:"icn_birthday",KPLACEHOLDER:"OPTIONAL"]]
        
        arrayCreditCard = [[KIMAGE:"icn_add_card",KPLACEHOLDER:"ADD PAYMENT METHOD",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        arrayNotifications = [["title":"Push Notifications","value":""]]
        
        arrayHistory = [[KTITLE:"CLINK! REDEEMED","value":""],[KTITLE:"CLINK! SAVED","value":""],[KTITLE:"CLINK! SENT","value":""],[KTITLE:"DONATIONS","value":""],[KTITLE:"CLINK! ACCOUNT","value":""],[KTITLE:"CASH OUT","value":""]]
        
        arrayClinkBalance = [[KIMAGE:"icn_add_card",KPLACEHOLDER:"ADD CASH OUT ACCOUNT",KDETAIL:"",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KIMAGE:"",KPLACEHOLDER:"CLINK! BALANCE",KDETAIL:strUserBalance,KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        arrayAccountDetail = [[KTITLE:"User Name","value":arrayProfileDetail],[KTITLE:"Send CLINK!:","value":arrayCreditCard],[KTITLE:"CLINK! Balance","value":arrayClinkBalance],[KTITLE:"Notifications","value":arrayNotifications],[KTITLE:"History","value":arrayHistory]]
        
        arrayAccountDetail[0][KTITLE] = objUserDetails.strFirstName + " " + objUserDetails.strLastName
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.reloadTableData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.view.endEditing(true)
    }
    
    func performNavigation(segueIdentifier : String) {
        self.performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == SEGUEACCOUNTHISTORY {
            let vc = segue.destinationViewController as! AccountHistoryController
            vc.accountHistoryControllerType = self.objSelectedAccountHistoryControllerType
        }
        else if segue.identifier == SEGUEPAYMENTMETHOD {
            let vc = segue.destinationViewController as! AddPaymentMethodController
            vc.addPaymentMethodControllerType = self.addPaymentMethodControllerType
        }
    }
    
    func reloadTableData(){
        
        USERDEFAULTS.setDefaultPaymentDetailsObject(ClsCreditCardDetails())
        USERDEFAULTS.setDefaultTransferDetailsObject(ClsCreditCardDetails(), forKey: KTRANSFERDETAILSBANK)
        USERDEFAULTS.setDefaultTransferDetailsObject(ClsCreditCardDetails(), forKey: KTRANSFERDETAILSDEBITCARD)
        
        self.objUserDetails = USERDEFAULTS.getUserDetailsObject() as ClsUserDetails
        
        self.strUserBalance = objUserDetails.strBalance
        
        arrayClinkBalance = [[KIMAGE:"icn_add_card",KPLACEHOLDER:"ADD CASH OUT ACCOUNT",KDETAIL:"",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KIMAGE:"",KPLACEHOLDER:"CLINK! BALANCE",KDETAIL:strUserBalance,KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        arrayAccountDetail[0][KTITLE] = objUserDetails.strFirstName + " " + objUserDetails.strLastName
        
        arrayProfileDetail[0][KTITLE] = objUserDetails.strEmail
        strEmail = objUserDetails.strEmail
        arrayProfileDetail[2][KTITLE] = "12345678"
        strPassword = "12345678"
        strConfirmPassword = "12345678"
        
        datePickerView.datePickerMode = UIDatePickerMode.Time
        datePickerView.addTarget(self, action: #selector(SignUpVC.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.maximumDate = NSDate().getNewUTCDate()
        datePickerView.minimumDate = NSDate(timeIntervalSince1970: 0)
        
        if let date : String = objUserDetails.strDOB {
            
            guard let dateDOB : NSDate = date.getDate("yyyy-MM-dd") else {
                datePickerView.date = NSDate().getNewUTCDate()
                return
            }
            
            datePickerView.date = dateDOB
        }
        else {
            datePickerView.date = NSDate().getNewUTCDate()
        }
        
        datePickerView.datePickerMode = .Date
        
        if objUserDetails.strDOB != nil && objUserDetails.strDOB != "" {
            arrayProfileDetail[3][KTITLE] = objUserDetails.strDOB
            strDOB = objUserDetails.strDOB
        }
        else {
            arrayProfileDetail[3][KTITLE] = ""
            strDOB = ""
        }
        
        strPhone = objUserDetails.strPhone
        
        if strPhone != "" && strPhone != nil {
            if strPhone.characters.count >= 10 {
                
                let num: String = Helper.formatNumber(strPhone)
                let str1 = num.substringToIndex(num.startIndex.advancedBy(3))
                var str2 = num.substringToIndex(num.startIndex.advancedBy(6))
                str2 = str2.substringFromIndex(str2.startIndex.advancedBy(3))
                let str3 = num.substringFromIndex(num.startIndex.advancedBy(6))
                
                strPhone = "("+str1+") "+str2+"-"+str3
                
                arrayProfileDetail[1][KTITLE] = strPhone
            }
            else {
                arrayProfileDetail[1][KTITLE] = objUserDetails.strPhone
                strPhone = objUserDetails.strPhone
            }
        }
        else {
            
            arrayProfileDetail[1][KTITLE] = ""
            strPhone = ""
        }

        arrayAccountDetail[0]["value"] = arrayProfileDetail
        
        self.getAllCreditCard()
    }
    
    // MARK: - Pick date from DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtDOB.text = dateFormatter.stringFromDate(sender.date)
    }
    
    // MARK: - Save Profile
    
    @IBAction func btnSaveProfileCliked(sender : UIButton){
        self.saveProfile()
    }
    
    func saveProfile() {
        
        self.view.endEditing(true)
        
        print("Email :- \(strEmail)")
        print("Phone :- \(strPhone)")
        print("Password :- \(strPassword)")
        
        if strEmail.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKEMAILADDRESS, pobjView: self.view)
        }
        else if strEmail.isValidEmail() == false {
            Helper.showAlert(ALERTINVALIDEMAIL, pobjView: self.view)
        }
        else if strPassword.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKPASSWORD, pobjView: self.view)
        }
        else if strPassword.characters.count < 8 {
            Helper.showAlert(ALERTPASSWORDVALIDATIONDESC, pobjView: self.view)
        }
        else if strPassword == "12345678" {
            self.view.endEditing(true)
            self.updateUserProfile()
        }
        else
        {
            let objViewController = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERCONFIRMPASSWORDPOPUP)
            let subView = objViewController.view as! ConfirmPasswordPopUpView
            subView.subView.layer.cornerRadius = PRIVATEPOPUPCORNERRADIUS
            subView.txtPassword.delegate = self
            self.view.endEditing(true)
            subView.txtPassword.becomeFirstResponder()
            subView.txtPassword.tag = 4
            subView.lblPasswordNotMatch.hidden = true
            subView.showInView(self.view, complitionBlock: { (strPassword,result) -> () in
                
                print("Password is :- \(strPassword)")
                self.strConfirmPassword = strPassword
                
                if self.strConfirmPassword != self.strPassword {
                    subView.lblPasswordNotMatch.hidden = false
                }
                else {
                    subView.lblPasswordNotMatch.hidden = true
                    self.view.endEditing(true)
                    subView.removeFromSuperview()
                    self.updateUserProfile()
                }
            })
        }
    }
    
    // MARK: - Update User Profile
    
    func updateUserProfile(){
        
        print("Confirm Pass :- \(strConfirmPassword)")
        
        if strConfirmPassword.isEmptyString() == true && strPassword != "12345678" {
            Helper.showAlert(ALERTBLANKCONFIRMPASSWORD, pobjView: self.view)
        }
        else if strPassword != strConfirmPassword {
            Helper.showAlert(ALERTPASSWORDMISMATCH, pobjView: self.view)
        }
        else
        {
            objUserDetails = USERDEFAULTS.getUserDetailsObject() as ClsUserDetails
            
            var dictUserDetails = ["firstname":objUserDetails.strFirstName,"lastname":objUserDetails.strLastName,"email":strEmail]
            
            if strDOB != "" {
                dictUserDetails["dob"] = strDOB
            }
            
            if strPassword != "12345678" {
                dictUserDetails["password"] = strPassword
            }
            if strConfirmPassword != "12345678" {
                dictUserDetails["password_confirm"] = strConfirmPassword
            }
            
            if strPhone != nil {
                dictUserDetails["phone"] = Helper.formatNumber(strPhone)
            }
            else {
                dictUserDetails["phone"] = ""
            }
            
            if USERDEFAULTS.valueForKey(KDEVICETOKEN) != nil{
                dictUserDetails[KDEVICETOKEN] = USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String
            }
            else {
                dictUserDetails[KDEVICETOKEN] = ""
            }
            
            print("Param :- \(dictUserDetails)")
            
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.UpdateUserProfile(dictUserDetails).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    let objUserProfileDetail = ClsUserDetails(pdicResponse: response as! [String : AnyObject])
                    USERDEFAULTS.setUserDetailsObject(objUserProfileDetail)
                    Helper.showAlert("Profile Updated Successfully.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                        self.reloadTableData()
                    })
                }
                else if response != nil{
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            }
        }
    }
    
    // MARK: - Get All Bank
    
    func getAllBank(){
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchBank(["search":""]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        arrayCreditCardDetails.append(objCreditCardDetail)
                    }
                }
                
                if arrayCreditCardDetails.count > 0{
                    self.arrayAccountDetail[2]["value"] = nil
                    
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        
                        self.arrayClinkBalance.insert([KIMAGE:"icn_debit_card",KPLACEHOLDER: "Account *",KDETAIL:"",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                        self.arrayCreditCard.insert([KIMAGE:"icn_debit_card",KPLACEHOLDER: "Account *",KDETAIL:"",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                    }
                }
                self.setArrayForAccountDetail()
                self.configureTableView()
            }
            else{
                self.setArrayForAccountDetail()
                self.configureTableView()
            }
        }
    }
    
    // MARK: - Get All Credit Card
    
    func initPaymentAndCashOutArray() {
        self.arrayClinkBalance = Array()
        self.arrayClinkBalance = [[KIMAGE:"icn_add_card",KPLACEHOLDER:"ADD CASH OUT ACCOUNT",KDETAIL:"",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KIMAGE:"",KPLACEHOLDER:"CLINK! BALANCE",KDETAIL:self.strUserBalance.removeZerosAfterDecimal(),KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        self.arrayCreditCard = Array()
        self.arrayCreditCard = [[KIMAGE:"icn_add_card",KPLACEHOLDER:"ADD PAYMENT METHOD",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
    }
    
    func setArrayForAccountDetail() {
        self.arrayAccountDetail[1]["value"] = self.arrayCreditCard
        self.arrayAccountDetail[2]["value"] = self.arrayClinkBalance
    }
    
    func getAllCreditCard(){
        
        self.initPaymentAndCashOutArray()
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchCard(["search":""]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        arrayCreditCardDetails.append(objCreditCardDetail)
                    }
                }
                
                if arrayCreditCardDetails.count > 0{
                    self.arrayAccountDetail[1]["value"] = nil
                    
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        
                        self.arrayCreditCard.insert([KIMAGE:"icn_debit_card",KPLACEHOLDER: "VISA CARD*",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                        
                        if objCreditCardDetail.strAccountType == "debit" {
                            self.arrayClinkBalance.insert([KIMAGE:"icn_debit_card",KPLACEHOLDER: "VISA CARD*",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                        }
                    }
                }
                self.setArrayForAccountDetail()
                self.getAllBank()
            }
            else{
                self.setArrayForAccountDetail()
                self.getAllBank()
            }
        }
    }
    
    // MARK: - TextField Delagate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == txtDOB{
            self.handleDatePicker(datePickerView)
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if textField.tag == 0 {
            strEmail = textField.text
            print("Email " + strEmail)
            arrayProfileDetail[0][KTITLE] = strEmail
        }
        else if textField.tag == 1 {
            strPhone = textField.text
            print("Phone " + strPhone)
            arrayProfileDetail[1][KTITLE] = strPhone
        }
        else if textField.tag == 2 {
            strPassword = textField.text
            print("Password " + strPassword)
            arrayProfileDetail[2][KTITLE] = strPassword
        }
        else if textField.tag == 3 {
            strDOB = textField.text
            txtDOB = textField
            print("DOB " + strDOB)
            arrayProfileDetail[3][KTITLE] = strDOB
        }
        
        self.arrayAccountDetail[0]["value"] = arrayProfileDetail
        //        self.configureTableView()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        
        let newLength = currentCharacterCount + string.characters.count - range.length
        
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        if textField.tag == 1 {
            
            let length: Int = Int(Helper.getLength(textField.text!))
            if length == 10 {
                if range.length == 0 {
                    return false
                }
            }
            if length == 3 {
                let num: String = Helper.formatNumber(textField.text!)
                textField.text = "(\(num)) "
                if range.length > 0 {
                    textField.text = "\(num.substringToIndex(num.startIndex.advancedBy(3)))"
                }
            }
            else if length == 6 {
                let num: String = Helper.formatNumber(textField.text!)
                textField.text = "(\(num.substringToIndex(num.startIndex.advancedBy(3)))) \(num.substringFromIndex(num.startIndex.advancedBy(3)))-"
                if range.length > 0 {
                    textField.text = "(\(num.substringToIndex(num.startIndex.advancedBy(3)))) \(num.substringFromIndex(num.startIndex.advancedBy(3)))"
                }
            }
            return newLength <= 14
        }
        else if textField.tag == 2 {
            return newLength <= 16
        }
        else if textField.tag == 3 {
            return false
        }
        else {
//            return newLength <= 20
            return true
        }
    }
    
    // MARK: - Done Button Clicked
    
    func btnDoneClicked() {
        self.view.endEditing(true)
        arrayProfileDetail[3][KTITLE] = strDOB
        arrayAccountDetail[0]["value"] = arrayProfileDetail
        self.configureTableView()
    }
    
    // MARK: - Cancel Button Clicked
    
    func btnCancelClicked() {
        
        txtDOB.text = strDOB
        self.configureTableView()
        self.view.endEditing(true)
    }
    
    // MARK: - Delete Payment Method
    
    func deletePaymentMethod(objPaymentDetail : ClsCreditCardDetails) {
        
        if objPaymentDetail.strAccountType == "bank" {
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.DeleteBank(objPaymentDetail.strId).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Bank Account Details Deleted Successfully.", pobjView: self.view)
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view)
                }
            }
        }
        else{
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.DeleteCard(objPaymentDetail.strId).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Credit/Debit Card Details Deleted Successfully.", pobjView: self.view)
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view)
                }
            }
        }
    }
    
    // MARK: - TableView Cnfiguration
    
    func configureTableView(){
        
        objHDTableDataSource = HDTableDataSource(items: arrayAccountDetail, multipleCellBlock: { (item, indexPath) -> String! in
            
            return CELLACCOUNTSWITCHICON
            }, configureCellBlock: { (cell, item, indexPath) -> Void in
                
                let objCell = cell as! AccountCell
                
                let objItems = item as! [String : AnyObject!]
                objCell.configureCell(objItems, indexPath: indexPath)
                
                objCell.switchOnOff.onTintColor = COLORTHEME
                objCell.switchOnOff.backgroundColor = UIColor.whiteColor()
                objCell.switchOnOff.layer.cornerRadius = 16
                
                objCell.txtField.userInteractionEnabled = true
                
                if indexPath.section == 0 {
                    objCell.txtField.tag = indexPath.row
                    objCell.txtField.textColor = UIColor.darkGrayColor()
                    objCell.switchOnOff.userInteractionEnabled = true
                    
                    
                    if indexPath.row == 0 {
                        objCell.txtField.keyboardType = .EmailAddress
                        objCell.txtField.returnKeyType = .Default
                        objCell.txtField.inputView = nil
                        objCell.txtField.inputAccessoryView = nil
                        //                        objCell.txtField.enabled = false
                    }
                    else if indexPath.row == 1 {
                        objCell.txtField.keyboardType = .NumberPad
                        objCell.txtField.inputView = nil
                        objCell.txtField.inputAccessoryView = nil
                    }
                    else if indexPath.row == 2 && objCell.btnInfo != nil {
                        objCell.btnInfo.addTarget(self, action: #selector(AccountsVC.btnInfoClicked), forControlEvents: .TouchUpInside)
                        self.txtPassword = objCell.txtField
                        objCell.txtField.inputView = nil
                        objCell.txtField.inputAccessoryView = nil
                    }
                    else if indexPath.row == 3{
                        objCell.txtField.keyboardType = .Default
                        objCell.txtField.inputView = self.datePickerView
                        self.txtDOB = objCell.txtField
                        
                        // datepicker toolbar setup
                        let toolBar = UIToolbar()
                        toolBar.barStyle = UIBarStyle.Default
                        toolBar.tintColor = COLORTHEME
                        toolBar.translucent = true
                        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
                        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AccountsVC.btnCancelClicked))
                        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(AccountsVC.btnDoneClicked))
                        
                        // if you remove the space element, the "done" button will be left aligned
                        // you can add more items if you want
                        toolBar.setItems([cancelButton,space, doneButton], animated: false)
                        toolBar.userInteractionEnabled = true
                        toolBar.sizeToFit()
                        
                        objCell.txtField.inputAccessoryView = toolBar
                    }
                    else {
                        objCell.txtField.inputView = nil
                        objCell.txtField.keyboardType = .Default
                        objCell.txtField.inputAccessoryView = nil
                    }
                }
                else if indexPath.section == 1 {
                    objCell.switchOnOff.tag = indexPath.row
                    objCell.switchOnOff.userInteractionEnabled = !objCell.switchOnOff.on
                }
                else if indexPath.section == 2 {
                    objCell.switchOnOff.tag = indexPath.row
                    objCell.switchOnOff.userInteractionEnabled = !objCell.switchOnOff.on
                }
                else if objCell.switchOnOff != nil {
                    objCell.switchOnOff.tag = indexPath.row
                    objCell.switchOnOff.userInteractionEnabled = true
                }
                
        })
        
        objHDTableDataSource.sectionItemBlock = {(objSection:AnyObject!) -> [AnyObject]! in
            
            let dicSection = objSection as! [String : AnyObject!]
            let arrMContact = dicSection["value"] as! [AnyObject!]
            return arrMContact
        }
        
        objHDTableDataSource.arrSections = arrayAccountDetail
        
        tblViewAccount.dataSource = objHDTableDataSource
        tblViewAccount.delegate = self
        tblViewAccount.reloadData()
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            if indexPath.section == 1 {
                if indexPath.row < arrayCreditCard.count - 1 {
                    let objPaymentDetail = arrayCreditCard[indexPath.row] as! ClsCreditCardDetails
                    self.deletePaymentMethod(objPaymentDetail)
                }
            }
            else if indexPath.section == 2 {
                if indexPath.row < arrayClinkBalance.count - 2 {
                    let objPaymentDetail = arrayClinkBalance[indexPath.row] as! ClsCreditCardDetails
                    self.deletePaymentMethod(objPaymentDetail)
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("\(indexPath.section) \(indexPath.row)")
        
        if indexPath.section == 4
        {
            if indexPath.row == 1 {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.Saved
            }
            else if indexPath.row == 2 {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.Sent
            }
            else if indexPath.row == 3 {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.Donations
            }
            else if indexPath.row == 4 {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.ClinkAccount
            }
            else if indexPath.row == 5 {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.CashOut
            }
            else {
                self.objSelectedAccountHistoryControllerType =  AccountHistoryControllerType.Redeemed
            }
            
            self.performNavigation(SEGUEACCOUNTHISTORY)
            //            Helper.showCommingsoonAlert()
        }
        else if indexPath.section == 1{
            
            if indexPath.row == arrayCreditCard.count - 1{
                self.addPaymentMethodControllerType = AddPaymentMethodControllerType.Payment
                //                self.updateUserProfile(SEGUEPAYMENTMETHOD)
                self.performNavigation(SEGUEPAYMENTMETHOD)
            }
            else{
                //                Helper.showCommingsoonAlert()
            }
        }
        else if indexPath.section == 2
        {
            if indexPath.row == arrayClinkBalance.count - 2
            {
                self.addPaymentMethodControllerType = AddPaymentMethodControllerType.CashOut
                
                //                self.updateUserProfile(SEGUEPAYMENTMETHOD)
                self.performNavigation(SEGUEPAYMENTMETHOD)
            }
            else if indexPath.row == arrayClinkBalance.count - 1 {
                self.performNavigation(SEGUECLINKBALANCE)
            }
            else {
                //                if let obj : ClsCreditCardDetails = arrayClinkBalance[indexPath.row][KPAYMENTMETHODDETAILS] as? ClsCreditCardDetails {
                //                    if let strUrl = obj.strBankVarifyUrl {
                //                        print(strUrl)
                //                        UIApplication.sharedApplication().openURL(NSURL(string: strUrl)!)
                //                    }
                //                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 44
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.groupTableViewBackgroundColor()
        
        if section == 0{
            
            let imgViewLogo = UIImageView()
            imgViewLogo.frame = CGRectMake(10, 10, 75, 60)
            imgViewLogo.image = UIImage(named: IMGLOGOGOLD)
            viewHeader.addSubview(imgViewLogo)
            
            let lblTitle = UILabel()
            lblTitle.frame = CGRectMake(100, 10, 320, 70)
            lblTitle.font = UIFont.boldSystemFontOfSize(FONTSIZE18)
            lblTitle.text = arrayAccountDetail[section].getValueIfAvilable(KTITLE) as? String
            lblTitle.textColor = UIColor.blackColor()
            
            viewHeader.addSubview(lblTitle)
        }
        else{
            
            let lblTitle = UILabel()
            lblTitle.frame = CGRectMake(10, 10, 320, 34)
            lblTitle.font = UIFont.systemFontOfSize(FONTSIZE13)
            lblTitle.text = arrayAccountDetail[section].getValueIfAvilable(KTITLE) as? String
            lblTitle.textColor = UIColor.blackColor()
            
            viewHeader.addSubview(lblTitle)
        }
        
        return viewHeader
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0
        {
            return 80
        }
        else
        {
            return 44
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return arrayHeaderTitle.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return arrayHeaderTitle[section] as? String
    }
    
    // MARK: - ScrollView Delagate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.view.endEditing(true)
        self.configureTableView()
    }
    
    func btnInfoClicked(){
        self.view.endEditing(true)
        Helper.showAlertwithTitle(ALERTTITLEPASSWORDREQUIRENMENTS, strMessage: ALERTPASSWORDVALIDATIONDESC, pobjView: self.view)
    }
    
    // MARK: - Change Default Payment Or CashOut Method
    
    @IBAction func changeDefaultPaymentOrCashOutMethod(sender : UISwitch){
        
        let switchPosition = sender.convertPoint(CGPointZero, toView: tblViewAccount)
        let indexPath = tblViewAccount.indexPathForRowAtPoint(switchPosition)
        
        print("Section :- \(indexPath!.section)" + "\n" + "Row :- \(indexPath!.row)")
        
        var objSelectedMethod : ClsCreditCardDetails = ClsCreditCardDetails()
        
        if indexPath?.section == 1 {
            
            objSelectedMethod = arrayCreditCard[indexPath!.row].valueForKey(KPAYMENTMETHODDETAILS) as! ClsCreditCardDetails
            
            self.updatePaymentMethod(objSelectedMethod, isDefaultPaymentMethod: sender.on, isDefaultCashOutMethod: false, strAccountType: objSelectedMethod.strAccountType,atIndexPath: indexPath!)
        }
        else if indexPath?.section == 2 {
            
            objSelectedMethod = arrayClinkBalance[indexPath!.row].valueForKey(KPAYMENTMETHODDETAILS) as! ClsCreditCardDetails
            
            self.updatePaymentMethod(objSelectedMethod, isDefaultPaymentMethod: false, isDefaultCashOutMethod: sender.on, strAccountType: objSelectedMethod.strAccountType,atIndexPath: indexPath!)
        }
    }
    
    // MARK: - Update Payment Method
    
    func updatePaymentMethod(objSelectedMethod : ClsCreditCardDetails,isDefaultPaymentMethod:Bool, isDefaultCashOutMethod:Bool, strAccountType:String , atIndexPath indexPath : NSIndexPath){
        
        var objSelectedPaymentDetail : ClsCreditCardDetails = objSelectedMethod
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        let dictUpdatedCardDetail = [KISDEFAULTPAYMENT:"\(Int(isDefaultPaymentMethod))",KISDEFAULTTRANSFER:"\(Int(isDefaultCashOutMethod))"]
        
        if strAccountType == "bank" {
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank(objSelectedMethod.strId, dictUpdatedCardDetail).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Bank Account Details Updated Successfully.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        objSelectedPaymentDetail = ClsCreditCardDetails(pdictResponse: response as! [String : AnyObject])
                        
                        var dicTmp  = self.arrayClinkBalance[indexPath.row] as! [String : AnyObject]
                        dicTmp[KPAYMENTMETHODDETAILS] = objSelectedPaymentDetail
                        self.reloadTableData()
                        if objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultPaymentDetailsObject(objSelectedPaymentDetail)
                        }
                        else if objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultTransferDetailsObject(objSelectedPaymentDetail, forKey: KTRANSFERDETAILSBANK)
                        }
                    })
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        //                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        //                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
            }
        }
        else{
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateCard(objSelectedPaymentDetail.strId, dictUpdatedCardDetail).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Card Details Updated Successfully.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        objSelectedPaymentDetail = ClsCreditCardDetails(pdictResponse: response as! [String : AnyObject])
                        
                        var dicTmp  = self.arrayCreditCard[indexPath.row] as! [String : AnyObject]
                        dicTmp[KPAYMENTMETHODDETAILS] = objSelectedPaymentDetail
                        self.reloadTableData()
                        if objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultPaymentDetailsObject(objSelectedPaymentDetail)
                        }
                        else if objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultTransferDetailsObject(objSelectedPaymentDetail, forKey: KTRANSFERDETAILSDEBITCARD)
                        }
                    })
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        //                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        //                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
            }
        }
    }
    
    // MARK: - Back Button Clicked
    
    @IBAction func back_click(sender: UIButton)
    {
        self.view.endEditing(true)
        
        var strPhoneNo : String! = ""

        if strPhone != nil && strPhone != "" {
            strPhone = Helper.formatNumber(strPhone)
        }
        
        if objUserDetails.strPhone != nil && objUserDetails.strPhone != "" {
            strPhoneNo = objUserDetails.strPhone
        }
        
        print("strPhone :- "+strPhone)
        print("strPhone :- "+strPhoneNo)
        
        if objUserDetails.strDOB == nil {
            objUserDetails.strDOB = ""
            print("nil")
        }
        
        
        
        if strEmail != objUserDetails.strEmail || strPhoneNo != strPhone || self.strPassword != "12345678" || self.strDOB != objUserDetails.strDOB
        {
            
//            let objViewController = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERSAVEPROFILEPOPUP)
//            let subView = objViewController.view as! SaveProfilePopUpView
//            subView.subView.layer.cornerRadius = PRIVATEPOPUPCORNERRADIUS
//            subView.showInView(self.view, complitionBlock: { (intIndex,result) -> () in
//                
//                print("intIndex is :- \(intIndex)")
//                
//                if intIndex == 1 {
//                    subView.removeFromSuperview()
//                    self.dismissViewControllerAnimated(true, completion: nil)
//                }
//                else {
//                    subView.removeFromSuperview()
//                    self.saveProfile()
//                }
//            })
            
            Helper.showAlert("There are unsaved changes in your profile. Would you like to save those changes?", cancelButtonTitle: "CANCEL", otherButtonTitle: "SAVE", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                
                if buttonIndex == 0 {
                    
                }
                else {
                    self.saveProfile()
                }
            })
            
        }
        else {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
}
