//
//  DefaultPaymentMethodPopUp.swift
//  Clink
//
//  Created by Dhruvit on 16/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class DefaultPaymentMethodPopUp: UIView {

    typealias DefaultPaymentMethodPopUpCompletionBlock = (Bool,Bool,AnyObject!) -> ()
    
    @IBOutlet var subView : UIView!
    @IBOutlet var switchPayment : UISwitch!
    @IBOutlet var switchCashOut : UISwitch!
    @IBOutlet var lblPayment : UILabel!
    @IBOutlet var lblCashOut : UILabel!
    
    
    var cmplBlck : DefaultPaymentMethodPopUpCompletionBlock!
    var isDefaultPaymentMethod : Bool = false
    var isDefaultCashOutMethod : Bool = false
    var isOn : Bool = false
    
    @IBAction func btnCancelClicked(sender : UIButton){
        self.removeFromSuperview()
        cmplBlck = nil
    }
    
    @IBAction func btnDoneClicked(sender : UIButton){
        cmplBlck(switchPayment.on,switchCashOut.on,nil)
    }
    
    func showInView(mainView : UIView,complitionBlock:DefaultPaymentMethodPopUpCompletionBlock){
        cmplBlck = complitionBlock
        mainView.addSubview(self)
    }
    
}
