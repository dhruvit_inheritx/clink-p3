//
//  ConfirmPasswordPopUpView.swift
//  Clink
//
//  Created by Dhruvit on 02/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ConfirmPasswordPopUpView: UIView,UITextFieldDelegate {
    
    typealias ConfirmPasswordPopUpCompletionBlock = (String,AnyObject?) -> ()
    var cmplBlck : ConfirmPasswordPopUpCompletionBlock!

    @IBOutlet var lblPasswordNotMatch : UILabel!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnOk : UIButton!
    @IBOutlet var subView : UIView!
    @IBOutlet var txtPassword : TextFieldPlaceHolder!

    @IBAction func btnCancel(sender : UIButton){
        self.removeFromSuperview()
        cmplBlck = nil
    }
    
    @IBAction func btnOk(sender : UIButton){
        cmplBlck(txtPassword.text!,nil)
    }

    func showInView(mainView : UIView,complitionBlock:ConfirmPasswordPopUpCompletionBlock){
        cmplBlck = complitionBlock
        mainView.addSubview(self)
    }
}