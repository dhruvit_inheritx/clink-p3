//
//  FTUPopUpView.swift
//  Clink
//
//  Created by Dhruvit on 18/10/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

typealias FTUPopUpCompletionBlock = (AnyObject!) -> ()

class FTUPopUpView: UIView {

    var cmplBlck : FTUPopUpCompletionBlock!
    @IBOutlet var subView : UIView!

    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    

    @IBAction func btnClose(sender : UIButton){
        cmplBlck = nil
        self.removeFromSuperview()
    }
    
    func showInView(mainView : UIView,complitionBlock:FTUPopUpCompletionBlock){
        cmplBlck = complitionBlock
        mainView.addSubview(self)
    }
    
}