//
//  SaveProfilePopUpView.swift
//  Clink
//
//  Created by Dhruvit on 05/01/17.
//  Copyright © 2017 inheritX. All rights reserved.
//

import UIKit

class SaveProfilePopUpView: UIView {

    typealias SaveProfilePopUpCompletionBlock = (Int,AnyObject?) -> ()
    var cmplBlck : SaveProfilePopUpCompletionBlock!
    
    @IBOutlet var lblSaveProfile : UILabel!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnSave : UIButton!
    @IBOutlet var subView : UIView!
    @IBOutlet var btnClose : UIButton!
    
    @IBAction func btnClose(sender : UIButton){
        self.removeFromSuperview()
        cmplBlck = nil
    }

    @IBAction func btnCancel(sender : UIButton){
        self.removeFromSuperview()
        cmplBlck(1,nil)
    }
    
    @IBAction func btnSave(sender : UIButton){
        cmplBlck(2,nil)
    }
    
    func showInView(mainView : UIView,complitionBlock:SaveProfilePopUpCompletionBlock){
        cmplBlck = complitionBlock
        mainView.addSubview(self)
    }

}
