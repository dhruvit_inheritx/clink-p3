//
//  PrivatePopUpView.swift
//  Clink
//
//  Created by Gunjan on 16/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class PrivatePopUpView: UIView {
    typealias PrivatePopUpCompletionBlock = (Bool,Bool,Bool,AnyObject!) -> ()
    @IBOutlet var subView : UIView!
    @IBOutlet var btnPrivate : UIButton!
    @IBOutlet var btnPublic : UIButton!
    @IBOutlet var radioFB : UISwitch!
    @IBOutlet var radioTwitter : UISwitch!
    var cmplBlck : PrivatePopUpCompletionBlock!
    var isPrivate : Bool = false
    var isShareOnFB : Bool = false
    var isShareOnTwitter : Bool = false
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBAction func btnNope(sender : UIButton){
        self.removeFromSuperview()
        cmplBlck = nil
    }
    
    @IBAction func btnYup(sender : UIButton){
        cmplBlck(isPrivate,radioFB.on,radioTwitter.on,nil)
        cmplBlck = nil
    }
    
    @IBAction func btnToggle(sender : UIButton){
        
        switch (sender.tag){
        case 0:
            if !sender.selected{
                btnPublic.selected = sender.selected
                btnPrivate.selected = !sender.selected
            }
            isPrivate = true
            radioFB.enabled = false
            radioTwitter.enabled = false
            radioFB.setOn(false, animated: true)
            radioTwitter.setOn(false, animated: true)
            break
        case 1:
            if !sender.selected{
                btnPrivate.selected = sender.selected
                btnPublic.selected = !sender.selected
            }
            isPrivate = false
            radioFB.enabled = true
            radioTwitter.enabled = true
            radioFB.setOn(false, animated: true)
            radioTwitter.setOn(false, animated: true)
            break
        default:
            break
        }
    }
    
    func showInView(mainView : UIView,complitionBlock:PrivatePopUpCompletionBlock){
        cmplBlck = complitionBlock
        mainView.addSubview(self)
    }
}
