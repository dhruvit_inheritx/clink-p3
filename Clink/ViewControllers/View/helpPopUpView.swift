//
//  helpPopUpView.swift
//  Clink
//
//  Created by Gunjan on 27/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class helpPopUpView: UIView {

    @IBOutlet var subView : UIView!
    @IBOutlet var btnSelection : UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func btnClose(sender : UIButton){
        self.removeFromSuperview()
    }
    
    @IBAction func btnToggle(sender : UIButton){
        if sender.selected == true{
            USERDEFAULTS.setBool(true, forKey: KSHOWHELPOPUP)
            USERDEFAULTS.synchronize()
        }
        else{
            USERDEFAULTS.setBool(false, forKey: KSHOWHELPOPUP)
            USERDEFAULTS.synchronize()
        }
        sender.selected = USERDEFAULTS.boolForKey(KSHOWHELPOPUP)
    }
}
