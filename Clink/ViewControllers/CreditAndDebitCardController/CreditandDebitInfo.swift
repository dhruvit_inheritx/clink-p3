//
//  CreditandDebitInfo.swift
//  Clink
//
//  Created by Gunjan on 20/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class CreditandDebitInfo: UIViewController {

    @IBOutlet var mainView: UIView!
    @IBOutlet var cardDetailView: UIView!
    @IBOutlet var actionView: UIView!
    @IBOutlet var keypadView: UIView!
    @IBOutlet var infoView: UIView!
    @IBOutlet var lblView: UIView!
    @IBOutlet var lbl_accountInfo: UILabel!
    @IBOutlet var backView: UIView!
    @IBOutlet var nextView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet weak var txtBankNumber: TextFieldPlaceHolder!
    @IBOutlet weak var txtBankAccount: TextFieldPlaceHolder!
    
    var txtCurrent : TextFieldPlaceHolder!
    var dictBankInfo : [String : AnyObject]!
    
    var arrayTextField : [TextFieldPlaceHolder] = Array()
    var isFTUUser = false
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        txtBankNumber.tag = 1
        txtBankAccount.tag = 2
        
        txtCurrent = txtBankNumber
        
        arrayTextField = [txtBankAccount,txtBankNumber]
        
//        self.deleteBank("317d8f1d7b4207032bc9c6615a5b497b")

    }
    
    func deleteBank(strID : String){
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.DeleteBank(strID).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                print("Card deleted successfully.")
            }
            else{
                print("Error deleting card.")
            }
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        for txtField in arrayTextField {
            txtField.layer.borderColor = UIColor.clearColor().CGColor
            txtField.layer.borderWidth = 0
        }
        
        textField.layer.borderColor = COLORTHEME.CGColor
        textField.layer.borderWidth = 1
        
        if textField.tag == 1
        {
            txtCurrent = txtBankNumber
        }
        else if textField.tag == 2
        {
            txtCurrent = txtBankAccount
        }
        
        return false
    }
    
    @IBAction func actionInputButton(sender:UIButton){
        
        var inputVal = STREMPTY
        
        switch sender.tag{
            
        case 0 :
            inputVal = STR0
        case 1 :
            inputVal = STR1
        case 2 :
            inputVal = STR2
        case 3 :
            inputVal = STR3
        case 4 :
            inputVal = STR4
        case 5 :
            inputVal = STR5
        case 6 :
            inputVal = STR6
        case 7 :
            inputVal = STR7
        case 8 :
            inputVal = STR8
        case 9 :
            inputVal = STR9
        case 10 :
            inputVal = STRPOINT
        default:
            inputVal = STREMPTY
        }
        
        let strAmount = txtCurrent.text
        
        if txtCurrent == txtBankNumber{
            
            if strAmount!.length < 30
            {
                txtCurrent.text! += inputVal
            }
        }
        else{
            
            if strAmount!.length < 30
            {
                txtCurrent.text! += inputVal
            }
        }
        
    }
    
    @IBAction func actionRemove(sender:UIButton){
        
        if ((txtCurrent.text?.isEmptyString()) == false)
        {
            let strAmount = String(txtCurrent.text!.characters.dropLast()) as String
            txtCurrent.text = strAmount
            
            if ((txtCurrent.text?.isEmptyString()) == true){
                txtCurrent.text = STREMPTY
            }
        }
    }

    @IBAction func back_click(sender: UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func add_click(sender: UIButton)
    {
        if txtBankNumber.text!.isEmpty == true || txtBankAccount.text!.isEmpty == true{
            
            txtBankNumber.setPlaceHolder(txtBankNumber.placeholder!, color: UIColor.redColor())
            txtBankAccount.setPlaceHolder(txtBankAccount.placeholder!, color: UIColor.redColor())
            
            Helper.showAlert(ALERTFILLALLFIELDS, pobjView: self.view)
        }
            //        else if txtBankNumber.text!.length < 9{
            //            Helper.showAlert("Bank Routing Number must be 9 digit number.", pobjView: self.view)
            //        }
            //        else if txtBankAccount.text!.length < 12{
            //            Helper.showAlert("Bank Account Number must be 12 digit number.", pobjView: self.view)
            //        }
        else{
            
            let objUser = USERDEFAULTS.getUserDetailsObject()
            let strUserFullName = objUser.strFirstName + " " + objUser.strLastName
            dictBankInfo = ["routing_number":txtBankNumber.text!,"account_number":txtBankAccount.text!,"type":"checking","name":strUserFullName ,"country":"US","currency":"usd"]
            
            ClinkAuthenticationManager.sharedInstance.callWebService(ClinkAccountRouter.CreateBank(dictBankInfo).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success{
                    
                    Helper.showAlertwithTitle("THANK YOU!", strMessage: "Your account information will be stored in your account profile.", pobjView: self.view, completionBock: { (CustomAlert, buttonIndex) -> () in
                        self.navigateBack()
                    })
                }
                else{
                    let strMessage = response as? String
                    
                    if strMessage == "Account requires verification, please check your email" {
                        
                        Helper.showAlert("Email verification is required, please click resend if you haven't received the verification email.", cancelButtonTitle: "Cancel", otherButtonTitle: "Resend", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                            
                            if buttonIndex == 0 {
                                self.navigateBack()
                            }
                            else {
                                
                                let objUserDetail = USERDEFAULTS.getUserDetailsObject()
                                
                                ClinkAccountManager.sharedInstance.sendVerificationEmail(objUserDetail.strEmail, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
                                    
                                    if success == true {
                                        Helper.showAlert("Verification Email has been successfully sent to your email account.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                                            self.navigateBack()
                                        })
                                    }
                                    else {
                                        Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                                            self.navigateBack()
                                        })
                                    }
                                })
                            }
                        })
                    }
                    else {
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                }
            }
        }
    }
    
    func navigateBack(){
        if self.isFTUUser == true {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKindOfClass(AccountsVC) == true {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
                else if vc.isKindOfClass(SelectPaymentViewController) == true {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    
    func searchBank(){
        
        dictBankInfo = ["search":"abc"]
        
        ClinkAuthenticationManager.sharedInstance.callWebService(ClinkAccountRouter.SearchBank(dictBankInfo).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success{
                
                print("Card Search Successfully.")
            }
            else{
                
                Helper.showAlert((response as? String)!, pobjView: self.view)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
}
