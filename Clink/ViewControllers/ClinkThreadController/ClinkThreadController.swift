//
//  ClinkThreadController.swift
//  Clink
//
//  Created by Gunjan on 30/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

class ClinkThreadController: UIViewController,UITableViewDelegate,HPGrowingTextViewDelegate , UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    @IBOutlet var collectionViewEmailCC: UICollectionView!
    
    var objSelectedClinkDetail : ClsClinkDetails!
    var imgSelected: UIImage!
    
    var objSelectedTYClinkDetail : ClsClinkDetails!
    var imgSelectedTYImage: UIImage!
    
    var selectedClinkType : ClinkType!
    var isUpdateNeeded : Bool!
    var isCommenting = false
    var isFromFeedScreen = false
    var objHDTableDataSource : HDTableDataSource!
    var arrComments : [ClsComment]!
    var currentClinkType : ClinkType!
    var shouldAutoLike : Bool!
    var shouldOpenComment : Bool!
    var controllerType : ClinkControllerType!
    var selectedEmoType : EmoType!
    var arrCCUser = [ClsContact]()
    var intCurrentPage : Int = 1
    var intLastPage : Int! = 0
    var intCurrentIndex : Int! = 0
    
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var imgViewClink: UIImageView!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var badgeLike : UILabel!
    @IBOutlet var badgeComment : UILabel!
    @IBOutlet var imgViewTYClink: UIImageView!
    
    @IBOutlet var viewTYwidthConstraint: NSLayoutConstraint!
    @IBOutlet var lblSayTYwidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var imgisRead : UIImageView!
    @IBOutlet var tblComments: UITableView!
    @IBOutlet var lblNotFound : UILabel!
    
    @IBOutlet var txtViewComment : HPGrowingTextView!
    @IBOutlet var containerView : UIView!
    
    @IBOutlet var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var containerViewBottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var tblCommentBottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet var btnLike : UIButton!
    @IBOutlet var btnComment : UIButton!
    @IBOutlet var btnThankYou : UIButton!
    @IBOutlet var lblGiftAmount : UILabel!
    @IBOutlet var emoDisplayView: UIView!
    @IBOutlet var btnEmoDisplay: UIButton!
    @IBOutlet var imgSelectedEmo: UIImageView!
    
    @IBOutlet var scrlViewTo: UIScrollView!
    @IBOutlet var scrlViewFrom: UIScrollView!
    
    // MARK: - view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromFeedScreen == true
        {
            btnTitle.setImage(UIImage(named: IMGICNLOGO), forState:.Normal)
            btnTitle.setTitle(STRHOME, forState: .Normal)
        }
        else
        {
            btnTitle.setImage(UIImage(named: IMGICNSENTTITLE), forState:.Normal)
            btnTitle.setTitle(STRSENT, forState: .Normal)
        }
        
        arrComments = Array()
        badgeLike.layer.cornerRadius = badgeLike.frame.size.height/2
        badgeComment.layer.cornerRadius = badgeComment.frame.size.height/2
        selectedEmoType = EmoType.NAN
        self.initializeOnce()
        if shouldAutoLike == false{
            self.getComments()
        }
        self.setUpTextView()

        if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil && objSelectedClinkDetail != nil{
            let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.Thread.rawValue ,"ClinkId" : objSelectedClinkDetail.strId]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ClinkThreadController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ClinkThreadController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)

        if objSelectedClinkDetail != nil{
            
            var strNotificationName = STREMPTY
            
            if objSelectedClinkDetail.isRead == false {
                
                let objUserDetail = USERDEFAULTS.getUserDetailsObject()
                
                if objSelectedClinkDetail.strReceiverId == objUserDetail.strId {
                    
                    strNotificationName = UPDATESENTCOUNT
                    let dicIsRead = Dictionary(dictionaryLiteral: ("is_read","1"))
                    ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objSelectedClinkDetail.strId,dicIsRead).URLRequest, isBackgroundCall: true, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                        if (success == true){
                            NOTIFICATIONCENTER.postNotificationName(strNotificationName, object: nil)
                        }
                    })
                }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = false
        if self.shouldAutoLike == true{
            self.btnAddLike(self.btnLike)
        }
        else if self.shouldOpenComment == true{
            self.txtViewComment.becomeFirstResponder()
            self.shouldAutoLike = false
            self.shouldOpenComment = false
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = true
    }
    
    // MARK: - Custom Methods
    func getComments(){
        
        if objSelectedClinkDetail != nil {
            
            // Get All Comment
            let dicParam = [KPERPAGE : "\(RECORDSPERPAGE)", KPAGE : "\(intCurrentPage)"]
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkComment(objSelectedClinkDetail.strId,dicParam).URLRequest, isBackgroundCall:false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true{
//                    print("Responsse :- \(response)")
                    let dicResponse = response as? [String : AnyObject]
                    if let arrCommentDetails = dicResponse!.getValueIfAvilable(KCLINKCOMMENT)  as? [AnyObject]
                    {   var i = 0
                        for objCommentDetails in arrCommentDetails {
                            let objComment = ClsComment(pdictResponse: objCommentDetails as! [String : AnyObject])
                            self.arrComments.insert(objComment, atIndex: i)
                            i += 1
                        }
                        self.intLastPage = dicResponse!.getValueIfAvilable(KLASTPAGE) as? Int
                        _ = dicResponse!.getValueIfAvilable(KTOTALCOUNT) as? Int
//                        self.badgeComment.text = "\(intTotalCount!)"
                        
//                        self.objSelectedClinkDetail.intCommentCount = intTotalCount
                    }
                    self.configureTableView()
                }
                else if response != nil{
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
                
            }
        }
    }
    
    func initializeOnce(){
        
                if objSelectedClinkDetail != nil{
        
//                    lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(objSelectedClinkDetail.strAmount)")
                    
                    if var fltGiftAmount = Float(objSelectedClinkDetail.strGiftPlusAmount){
                        if let fltTotalAmount = Float(objSelectedClinkDetail.strAmount){
                            fltGiftAmount += fltTotalAmount
                        }
                        var strTotalAmount = "\(fltGiftAmount)"
                        strTotalAmount.removeLastCharIfExist("0")
                        strTotalAmount.removeLastCharIfExist(".")
                        lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strTotalAmount)")
                    }
                    else if let fltTotalAmount = Float(objSelectedClinkDetail.strAmount){
                        //            let fltTotalAmount = Float(objSelectedClinkDetail.strAmount)!
                        var strAmount = "\(fltTotalAmount)"
                        strAmount.removeLastCharIfExist("0")
                        strAmount.removeLastCharIfExist(".")
                        lblGiftAmount.attributedText = CommonAttributedString.getGiftFormattedString("$ \(strAmount)")
                    }
                    
                    btnThankYou.userInteractionEnabled = true
                    badgeLike.text = "\(objSelectedClinkDetail.intLikeCount)"
                    badgeComment.text = "\(objSelectedClinkDetail.intCommentCount)"
                    
                    lblFrom.text = "\(objSelectedClinkDetail.strSenderName)"
                    lblTo.text = "\(objSelectedClinkDetail.strReceiverName)"
                    if selectedClinkType == ClinkType.Sent{
                        btnTitle.setImage(UIImage(named: IMGICNSENTTITLE), forState:.Normal)
                        btnTitle.setTitle(STRSENT, forState: .Normal)
                        lblSayTYwidthConstraint.constant = CGFLOATZERO
                        btnThankYou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                        btnThankYou.userInteractionEnabled = false
                    }
                    else if selectedClinkType == ClinkType.Received{
                        if objSelectedClinkDetail.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId
                        {
                            self.btnThankYou.userInteractionEnabled = false
                            self.btnThankYou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                        }
                        else{
                            self.btnThankYou.userInteractionEnabled = true
                            self.btnThankYou.setImage(UIImage(named: IMGBTNTY), forState: .Normal)
                        }
                        
                        btnTitle.setImage(UIImage(named: IMGICNINBOX), forState:.Normal)
                        btnTitle.setTitle(STRRECIEVED, forState: .Normal)
                    }
//                    else if objSelectedClinkDetail.intClinkType == ClinkType.Thankyou{
//                        btnThankYou.userInteractionEnabled = false
//                        lblTFTitleWidthConstraint.constant = LBLTOFROMWIDTH
//                        lblTFTitle.text = STRTO
//                        lblTo.text = "\(objSelectedClinkDetail.strReceiverFirstName)"
//                        btnThankYou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
//                    }
        
                    if isFromFeedScreen == true{
                        btnTitle.setImage(UIImage(named: IMGICNLOGO), forState:.Normal)
                        btnTitle.setTitle(STRHOME, forState: .Normal)
        
                        lblSayTYwidthConstraint.constant = CGFLOATZERO
                        imgisRead.hidden = true
                        btnThankYou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                        btnThankYou.userInteractionEnabled = false
                    }
                    else{
                        if currentClinkType == ClinkType.Sent {
                            imgisRead.hidden = !objSelectedClinkDetail.isRead
                        }
                        else{
                            imgisRead.hidden = objSelectedClinkDetail.isRead
                        }
                    }
                    if imgSelected != nil {
                        imgViewClink.image = imgSelected!
                    }
                    else {
                        if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == false
                        {
                            lblSayTYwidthConstraint.constant = CGFLOATZERO//
                        }
                    }
                    scrlViewTo.contentSize = CGSizeMake(lblTo.intrinsicContentSize().width, LBLTOFROMHEIGHT)
                    scrlViewFrom.contentSize = CGSizeMake(lblFrom.intrinsicContentSize().width, LBLTOFROMHEIGHT)
                    
                    let dateCreated = objSelectedClinkDetail.strCreatedDate.getLocalDate(PHASE3FORMATE)
//                    let dateCreated = objSelectedClinkDetail.strCreatedDate.getDate(PHASE3FORMATE)
                    lblDateTime.text = Helper.getDateDifferenceforDate(dateCreated, andDate: NSDate().getNewUTCDate())
                    if let isLiked = objSelectedClinkDetail.isLiked{
                        btnLike.selected = isLiked
                    }
                    
                    if objSelectedClinkDetail.strImageUrl != nil
                    && objSelectedClinkDetail.strImageUrl != "" {
                        imgViewClink.sd_setImageWithURL(NSURL(string: objSelectedClinkDetail.strImageUrl))
                    }
                }
        
                if objSelectedTYClinkDetail != nil{
        
                    if imgSelectedTYImage != nil{
        
                        imgViewTYClink.image = imgSelectedTYImage!
                        btnThankYou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
                    }
                    lblSayTYwidthConstraint.constant = 0
                }
                else{
                    if currentClinkType == ClinkType.Received {
        
                        if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == false
                        {
        
                        }
                    }
                    else if currentClinkType != ClinkType.Sent{
        
                    }
                    else{
        
                    }
                }
                let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ClinkThreadController.dismissKeyboard))
                self.view.addGestureRecognizer(tap)
//                btnThankYou.userInteractionEnabled = true
        /*
                if objSelectedClinkDetail.strReceiverId != PFUser.currentUser()?.objectId
                {
                    if objSelectedClinkDetail.strRefrenceClinkId == KNOTAVIALABLE
                    {
                        btnThankYou.setImage(UIImage(named: IMGTXTWAITINGON), forState: .Normal)
                        btnThankYou.hidden = false
                        btnThankYou.userInteractionEnabled = false
                    }
                    else{
                        btnThankYou.setImage(UIImage(named: NOIMAGE), forState: .Normal)
                    }
        
                    if objSelectedClinkDetail.strReceiverId != PFUser.currentUser()?.objectId && objSelectedClinkDetail.strReceiverId == KDEFAULTUSERID && objSelectedClinkDetail.strSenderId == PFUser.currentUser()?.objectId
                    {
                        btnThankYou.setImage(UIImage(named: IMGBTNTY), forState: .Normal)
                    }
                }
        */
        if objSelectedClinkDetail != nil{
            self.collectionViewEmailCC.delegate = self
            self.collectionViewEmailCC.dataSource = self
            arrCCUser = objSelectedClinkDetail.arrCCUser
            self.collectionViewEmailCC.reloadData()
        }
        if isFromFeedScreen == true{
            btnThankYou.userInteractionEnabled = false
        }
    }
    
    func configureTableView(){
        
//        arrComments.sortInPlace({ $0.strDateTime.compare($1.strDateTime) == .OrderedAscending })
       /* objHDTableDataSource = HDTableDataSource(items: arrComments, cellIdentifier: CELLCOMMENTT, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
                
                let objCell = cell as! CommentCell
                let objItems = item as! ClsComment
                if objItems.strComment.isEmptyString() == false{
                    objCell.configureCell(objItems)
                }
                else{
                    objCell.configureEmoCell(objItems)
                }
                if (indexPath.row % 2) == INTZERO{
                    objCell.contentView.backgroundColor = KCOMMENTEVENCOLOR
                }
                else{
                    objCell.contentView.backgroundColor = KCOMMENTODDCOLOR
                }
        })*/
        
        objHDTableDataSource = HDTableDataSource(items: arrComments, multipleCellBlock: { (item:AnyObject!, indexPath) -> String! in
            let objItem = item as! ClsComment
            if objItem.strComment.isEmptyString() == false{
                return CELLCOMMENTT
            }
            else{
                return CELLEMOJI
            }
            
            }, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
                
                let objCell = cell as! CommentCell
                let objItems = item as! ClsComment
                if objItems.strComment.isEmptyString() == false{
                    objCell.configureCell(objItems)
                }
                else{
                    objCell.configureEmoCell(objItems)
                }
                if (indexPath.row % 2) == INTZERO{
                    objCell.contentView.backgroundColor = KCOMMENTEVENCOLOR
                }
                else{
                    objCell.contentView.backgroundColor = KCOMMENTODDCOLOR
                }
        })
        
        tblComments.dataSource = objHDTableDataSource
        tblComments.delegate = self
        tblComments.reloadData()
        if arrComments.count > INTZERO{
            self.lblNotFound.hidden = true
            if intCurrentPage <= 1{
                tblComments.scrollToRowAtIndexPath(NSIndexPath(forRow: (arrComments.count-1), inSection: INTZERO), atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
            }
            else{
            self.tblComments.scrollToRowAtIndexPath(NSIndexPath(forRow:arrComments.count - ((intCurrentPage - 1) * RECORDSPERPAGE ) , inSection: INTZERO), atScrollPosition: UITableViewScrollPosition.Top, animated: false)
            }
        }
        else if isCommenting == false{
            self.lblNotFound.hidden = false
        }
    }    
    
    // MARK: - CollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCCUser.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cellIdentifier:String = CELLEMAILCC;
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier,forIndexPath:indexPath) as! EmailCCCell
        if indexPath.row < arrCCUser.count - 1
        {
            cell.lblEmailCCName.text = "\((arrCCUser[indexPath.row].strFullName)!),"
        }
        else
        {
            cell.lblEmailCCName.text = arrCCUser[indexPath.row].strFullName
        }
        //cell.lblName .sizeToFit()
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let str:String = "\((arrCCUser[indexPath.row].strFullName)!),"
        let calCulateSizze: CGSize = str.sizeWithAttributes([NSFontAttributeName: UIFont (name: FONTGOTHAMMEDIUM, size: FONTSIZE14)!])
        return CGSizeMake(calCulateSizze.width+1,22)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getMoreComments(){
        intCurrentPage += 1
        self.getComments()
    }
    // MARK: - tableView delegate
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 && intCurrentPage < intLastPage{
            self.getMoreComments()
        }
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        var cellHeight : CGFloat = COMMENTCELLWITHIMAGEHEIGHT
        let objItems = arrComments[indexPath.row]
        
        let strComment = objItems.strComment
        
        if strComment.isEmptyString() == false{
            var height = strComment.heightWithConstrainedWidth(tableView.frame.size.width - INTCOMMENTWIDTHTOSUBSTRACT, font: UIFont.systemFontOfSize(FONTSIZE14))
            
            if height % COMMENTHEIGHT == CGFLOATZERO{
                height += 3
            }
            else{
                var multiplier : Int = Int(height / COMMENTHEIGHT)
                multiplier += 1
                height = CGFloat(INTCOMMENTHEIGHT * multiplier)
            }
            cellHeight = cellHeight + height
        }
        else{
            cellHeight = 100.0
        }
        return cellHeight
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.dismissKeyboard()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == SEGUETOSENDNEWCLINK{
            let objSendNewClink = segue.destinationViewController as! SendNewClinkController
            objSendNewClink.controllerType = controllerType
            let objSelected = sender as! ClsClinkDetails
            objSendNewClink.previousScreenType = currentClinkType
            objSendNewClink.objSelectedClinkDetail = objSelected
            
        }
        else if segue.identifier == SEGUETOCLINKDETAILS{
            let objClinkDetails = segue.destinationViewController as! ClinkDetailsController
            
            if sender != nil{
                let isobjSelected = sender as! Bool
                if isobjSelected == true{
                    objClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                    if imgSelected != nil{
                        objClinkDetails.imgSelected = imgSelected
                    }
                }
                else if isobjSelected == false{
                    objClinkDetails.objSelectedClinkDetail = objSelectedTYClinkDetail
                    if imgSelectedTYImage != nil{
                        objClinkDetails.imgSelected = imgSelectedTYImage
                    }
                }
                objClinkDetails.selectedClinkType = selectedClinkType
                objClinkDetails.isFromFeedScreen = isFromFeedScreen
                
            }
        }
        else if segue.identifier == SEGUETOADDGIFTVIEW{
            let objGiftClinkDetails = segue.destinationViewController as! AddGiftController
            if self.objSelectedClinkDetail != nil{
                objGiftClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                if imgSelected != nil{
                    objGiftClinkDetails.imgSelected = imgSelected
                }
            }
        }
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackCliked(sender: AnyObject) {
        
        if isCommenting == true{
            self.dismissKeyboard()
        }
        else if isUpdateNeeded != nil && isUpdateNeeded == true{
            self.performSegueWithIdentifier(UNWINDTOUPDATEREADCOUNT, sender: nil)
        }
        else if (isFromFeedScreen == true){
            self.performSegueWithIdentifier(UNWINDTOFEEDSCREEN, sender: nil)
        }
        else{
            self.performSegueWithIdentifier(UNWINDTORELOADDATA, sender: nil)
        }
    }
    
    @IBAction func btnShowDetails(sender: UIButton) {
        sender.userInteractionEnabled = false
        if isCommenting == true{
            self.dismissKeyboard()
        }
        else{
            performSegueWithIdentifier(SEGUETOCLINKDETAILS, sender: true)
        }
        
        sender.userInteractionEnabled = true
    }
    
    @IBAction func btnSendThankyou(sender: UIButton) {
        
        if isFromFeedScreen == false {
            
            sender.userInteractionEnabled = false
            if isCommenting == true{
                self.dismissKeyboard()
            }
            else if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == true
            {
                //                if objSelectedClinkDetail.strReceiverId == PFUser.currentUser()?.objectId
                //                {
                if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == true{
                    controllerType = ClinkControllerType.ThankYou
                    performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: objSelectedClinkDetail)
                }
                //                }
            }
            else{
                performSegueWithIdentifier(SEGUETOCLINKDETAILS, sender: false)
            }
            
            sender.userInteractionEnabled = true
        }
    }
    
    
    @IBAction func btnSendComment(sender: UIButton) {
        self.dismissKeyboard()
        sender.userInteractionEnabled = false
        var strComment = txtViewComment.text.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        )
        
        if strComment.isEmptyString() == true && selectedEmoType == EmoType.NAN{
            strComment = " "
            Helper.showAlert(ALERTBLANKCOMMENTEMO, pobjView: self.view)
        }
        else if objSelectedClinkDetail != nil{
            
            // Clink Comment
            let parameters : [String : AnyObject] = [KCOMMENT:strComment, KEMOJITYPE : selectedEmoType.rawValue]
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.CreateClinkComment(objSelectedClinkDetail.strId, parameters).URLRequest, isBackgroundCall:false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true{
//                    print("Response :- \(response)")
                    self.txtViewComment.text = STREMPTY
                    self.imgSelectedEmo.image = nil
                    self.selectedEmoType = EmoType.NAN
                    self.intCurrentPage = 1
                    self.intLastPage = 0
                    self.arrComments.removeAll()
                    self.getComments()
                    var count = self.objSelectedClinkDetail.intCommentCount!
                    count += 1
                    self.objSelectedClinkDetail.intCommentCount = count
                    self.badgeComment.text = "\(self.objSelectedClinkDetail.intCommentCount)"
                }
                else if response != nil{
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            }
        }
        else{            
            Helper.showAlert(SOMETHINGWENTWRONG, pobjView: self.view)
        }
        self.txtViewComment.text = STREMPTY
//        else{
//            sender.userInteractionEnabled = true
//            self.txtViewComment.text = STREMPTY
//        }
        
        sender.userInteractionEnabled = true
    }
    
    @IBAction func btnComment(sender: UIButton) {
        txtViewComment.becomeFirstResponder()
    }
    
    @IBAction func btnAddLike(sender: UIButton) {
        
        sender.userInteractionEnabled = false        
        shouldAutoLike = false
        shouldOpenComment = false
        self.dismissKeyboard()
        
        if self.objSelectedClinkDetail != nil{
            
            if (self.objSelectedClinkDetail.isLiked != true){
                
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkLike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        print("Like CLink")
                        self.txtViewComment.text = STREMPTY
                        sender.userInteractionEnabled = true
                        self.intCurrentPage = 1
                        self.intLastPage = 0
                        self.arrComments.removeAll()
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getComments()
                        }
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.performSelector(#selector(ClinkThreadController.getClinkDetails), withObject: nil, afterDelay: ANIMATIONDURATION)
                        }
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
            else{
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkUnlike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        
                        print("Unlike CLink")
                        self.txtViewComment.text = STREMPTY
                        sender.userInteractionEnabled = true
                        self.intCurrentPage = 1
                        self.intLastPage = 0
                        self.arrComments.removeAll()
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getComments()
                        }
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.performSelector(#selector(ClinkThreadController.getClinkDetails), withObject: nil, afterDelay: ANIMATIONDURATION)
                        }                        
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
        }
    }
    
    func getClinkDetails()
    {
        if self.objSelectedClinkDetail != nil{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objSelectedClinkDetail.strId,nil).URLRequest, isBackgroundCall:true, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
            
                if success == true{
                    
                    self.txtViewComment.text = STREMPTY
                    
                    if let dicResponse = response as? [String : AnyObject] {
                        if let error = dicResponse.getValueIfAvilable(KMESSAGE) {
                            print("Error :- \(error)")
                            return
                        }
                        else{
                            self.objSelectedClinkDetail = ClsClinkDetails(pdictResponse: dicResponse)
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                //                        self.badgeLike.text = "\(self.objSelectedClinkDetail.intLikeCount)"
                                self.initializeOnce()
                            })
                        }
                    }                   
//                    print("Response :- \(response)")
                }
                else if response != nil{
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            })
        }
    }
    
    @IBAction func btnAddGiftTapped(sender: UIButton) {
        shouldAutoLike = false
        shouldOpenComment = false
        controllerType = ClinkControllerType.Gift
        let objSelClink = objSelectedClinkDetail
        //        if objSelectedClinkDetail.intClinkType == ClinkType.Thankyou{
        //            objSelClink = objSelectedTYClinkDetail
        //        }
        self.performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: objSelClink)
    }
    
    
    @IBAction func showComingsoonAlert(sender: AnyObject) {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
    }
    
    
    @IBAction func btnShowImoClicked(sender: UIButton) {
        
        if SHOUDCOMMENT == true{
            Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
        }
        else{
            emoDisplayView.hidden = sender.selected
            sender.selected = !sender.selected
        }
    }
    
    @IBAction func btnLikeImoClicked(sender: UIButton) {
        
        switch (sender.tag){
        case 1:
            selectedEmoType = EmoType.Rofl
            break
        case 2:
            selectedEmoType = EmoType.Wow
            break
        case 3:
            selectedEmoType = EmoType.Love
            break
        case 4:
            selectedEmoType = EmoType.Cheers
            break
        case 5:
            selectedEmoType = EmoType.Claps
            break
        case 6:
            selectedEmoType = EmoType.NAN
            break
        default:
            selectedEmoType = EmoType.Rofl
            break
        }
        emoDisplayView.hidden = true
        btnEmoDisplay.selected = false
//        if selectedEmoType != EmoType.NAN{
//            imgSelectedEmo.image = sender.imageView?.image
//        }
//        else{
//            imgSelectedEmo.image = nil
//        }
        self.btnSendComment(btnComment)
    }
    
    // MARK: - Other Methods
    
    func setUpTextView(){
        txtViewComment.isScrollable = false
        txtViewComment.contentInset = TXTVIEWCOMMENTINSET
        txtViewComment.minNumberOfLines = 1
        txtViewComment.maxNumberOfLines = 3
        // you can also set the maximum height in points with maxHeight
        txtViewComment.maxHeight = TXTVIEWMAXHEIGHT
        //just as an example
        txtViewComment.keyboardAppearance = .Dark
        txtViewComment.font = UIFont.systemFontOfSize(FONTSIZE15)
        txtViewComment.delegate = self
        txtViewComment.enablesReturnKeyAutomatically = true
        txtViewComment.internalTextView.scrollIndicatorInsets = TXTVIEWCOMMENTSCROLLINSET
        txtViewComment.backgroundColor = UIColor.clearColor()
        txtViewComment.placeholder = STRCOMMENTPLACEHOLDER
        txtViewComment.textColor = kThemeMainLabelColor
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ClinkThreadController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ClinkThreadController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        //        self.view.addGestureRecognizer(tap)
    }
    
    // MARK: - growingTextView delegate
    func growingTextView(growingTextView: HPGrowingTextView, willChangeHeight height: Float)
    {
        if height < TXTVIEWMAXP1HEIGHT{
            containerViewHeightConstraint.constant = CGFloat(height+TXTVIEWMAXEXTRAHEIGHT)
        }
        if height < TXTVIEWMINP1HEIGHT{
            containerViewHeightConstraint.constant = TXTVIEWMINHEIGHTCONST
        }
        
    }
    
    func growingTextViewDidBeginEditing(growingTextView:HPGrowingTextView ){
        emoDisplayView.hidden = true
        btnEmoDisplay.selected = false
    }
    
    func keyboardWillShow(notification: NSNotification) {
        isCommenting = true
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        
        UIView.animateWithDuration(ANIMATIONDURATION, animations: { () -> Void in
            self.containerViewBottomSpaceConstraint.constant =  keyboardRectangle.size.height
            self.tblCommentBottomSpaceConstraint.constant =  self.containerViewHeightConstraint.constant + keyboardRectangle.size.height
            self.view.layoutIfNeeded()
            }) { (success) -> Void in
                if self.arrComments.count > 1{
                    self.tblComments.scrollToRowAtIndexPath(NSIndexPath(forRow: (self.arrComments.count-1), inSection: INTZERO), atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
                }
        }
        self.lblNotFound.hidden = true
    }
    
    func keyboardWillHide(notification: NSNotification) {
        isCommenting = false
        UIView.animateWithDuration(ANIMATIONDURATION, animations: { () -> Void in
            self.containerViewBottomSpaceConstraint.constant = CGFLOATZERO
            self.tblCommentBottomSpaceConstraint.constant =  self.containerViewHeightConstraint.constant
            self.view.layoutIfNeeded()
            
            }) { (success) -> Void in
                if self.arrComments.count > 1 {
                    self.tblComments.scrollToRowAtIndexPath(NSIndexPath(forRow: (self.arrComments.count-1), inSection: INTZERO), atScrollPosition: UITableViewScrollPosition.Bottom, animated: false)
                }
        }
        if arrComments.count > INTZERO{
            self.lblNotFound.hidden = true
        }
        else{
            self.lblNotFound.hidden = false
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
