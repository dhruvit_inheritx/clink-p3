//
//  LoginVC.swift
//  Clink
//
//  Created by Gunjan on 23/02/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import Social
import BSKeyboardControls


class LoginVC: UIViewController,UITextFieldDelegate,BSKeyboardControlsDelegate {
    
    //    MARK:- Gloable variable
    var arrFields:[AnyObject]!
    var objBSKeyboardControls: BSKeyboardControls!
    
    //    MARK:- IBOutlet
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var blurryView: UIVisualEffectView!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //        To reset password
        // Helper.showAlert((USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String)!, pobjView: self.view)
        arrFields = Array()
        arrFields = [txtEmail, txtPassword, txtPromoCode]
        for txtField in arrFields{
            Helper.setTextFieldPadding(txtField as! UITextField, yPosition: 21)
        }
        
        objBSKeyboardControls = BSKeyboardControls(fields: arrFields)
        objBSKeyboardControls.delegate = self
        self.txtPromoCode.hidden = SHOUDCOMMENT
        self.blurryView.hidden = SHOUDCOMMENT
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.view.endEditing(true)
        txtEmail.text = STREMPTY
        txtPassword.text = STREMPTY
        txtPromoCode.text = STREMPTY
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "AUTOFILLDATA", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        if (USERDEFAULTS.objectForKey("AUTOFILLDATA") != nil && USERDEFAULTS.boolForKey("SHOULDAUTOREDIRECT") == true){
            self.showAutoFillData()
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginVC.showAutoFillData), name: "AUTOFILLDATA", object: nil)
    }
    
    func showAutoFillData() {
        self.performSegueWithIdentifier("segueToSignUp", sender: nil)
    }
    
    // MARK: - Memory Management    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Actions
    
    // Show Terms and Conditions
    
    @IBAction func btnShowTerms(sender: AnyObject) {
        let storyboard = UIStoryboard(name: MAINSTORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERTANDC) as! TermsConditionsController
        
        USERDEFAULTS.setInteger(screenType.Privacy.rawValue, forKey:KSELECTEDSCREENTYPE)
        USERDEFAULTS.synchronize()
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    // Perform validation for Sign In
    
    @IBAction func actionSignIn() {
        
        self.view.endEditing(true)
        //        To signin
        if txtEmail.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKEMAILADDRESS, pobjView: self.view)
        }
        else if txtEmail.text?.isValidEmail() == false{
            Helper.showAlert(ALERTINVALIDEMAIL, pobjView: self.view)
        }
        else if txtPassword.text!.isEmptyString() == true{
            Helper.showAlert(ALERTBLANKPASSWORD, pobjView: self.view)
        }
        else
        {
            self.logIn(txtPromoCode.text!)
        }
    }
    
    // Perform Sign In
    
    func logIn(strPromoCode : String) {
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        var strUserName = txtEmail.text!.stringByTrimmingCharactersInSet(NSCharacterSet.decimalDigitCharacterSet())
        strUserName = strUserName.lowercaseString
        var userDetails = [KEMAIL: strUserName, KPASSWORD: txtPassword.text! ]
        
//        if strPromoCode.isEmptyString() == false{
            userDetails["access_code"] = strPromoCode
//        }
        
        
        if USERDEFAULTS.valueForKey(KDEVICETOKEN) != nil{
            userDetails[KDEVICETOKEN] = USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String
        }
        else {
            userDetails[KDEVICETOKEN] = ""
        }
        
        print("User Details : - \(userDetails)")

        ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.LoginUser(userDetails).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController : self, pCompletionBlock :{ (success, strMessage) -> () in
            if success == true{
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    Helper.getTrackerGroups()
                }
                let result = strMessage as! [String : AnyObject!]
                let objUserDetails = ClsUserDetails(pdicResponse: result)
                let dicSession = objUserDetails.dicSession as ClsSessionDetails
                if let sid = dicSession.id
                {
                    ClinkDetailsRouter.SessionID = sid
                    USERDEFAULTS.setObject(sid, forKey: KSESSIONID)
                }
                
                USERDEFAULTS.setUserDetailsObject(objUserDetails)
                USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                USERDEFAULTS.synchronize()
                self.dismissViewControllerAnimated(false, completion:nil)
            }
            else if strMessage != nil{
                Helper.showAlert((strMessage as? String)!, pobjView: self.view)
            }
        })
    }
    
    // Sign In with Facebook
    
    @IBAction func actionSignInWithFacebook(sender: AnyObject) {
        
        // Login PFUser using Facebook
        self.view.endEditing(true)
        ClinkAuthenticationManager.sharedInstance.signInWithFacebook(self, isSigningUp: false) { (success, response) -> () in
            if success == true{
                let result = response as! [String : AnyObject!]
                let objUserDetails = ClsUserDetails(pdicResponse: result)
                let dicSession = objUserDetails.dicSession as ClsSessionDetails
                if let sid = dicSession.id
                {
                    ClinkDetailsRouter.SessionID = sid
                    USERDEFAULTS.setObject(sid, forKey: KSESSIONID)
                }
                USERDEFAULTS.setUserDetailsObject(objUserDetails)
                USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
            }
            else if response != nil{
                USERDEFAULTS.setBool(false, forKey: kISUSERLOGGEDIN)
                Helper.showAlert(response as! String, pobjView: self.view)
            }
            USERDEFAULTS.synchronize()
        }
    }
    
    // Sign In with Twitter
    
    @IBAction func actionSignInWithTwitter(sender: AnyObject) {
        self.view.endEditing(true)
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
/*
//        Helper.showHud(self.view)
        ClinkAuthenticationManager.sharedInstance.signInWithTwitter(self.view, isSigningUp: false) { (success, error) -> () in
            //            Helper.hideHud(self.view)
            if success == true{
                USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
            }
            else if error != nil{
                Helper.showAlert(error as! String , pobjView: self.view)
            }
        }   */
    }
    
    // Perform Forgot Password
    
    @IBAction func actionForgotPassword(sender: AnyObject) {
        
        self.view.endEditing(true)
        txtPassword.text = EMPTYSTRING
        if txtEmail.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKEMAILFORGOT, pobjView: self.view)
        }
        else if txtEmail.text?.isValidEmail() == false{
            Helper.showAlert(ALERTINVALIDEMAIL, pobjView: self.view)
        }
        else{
            let alertView = CustomAlert(title: APPNAME, message:"Password reset link will be sent to \(txtEmail.text!) email address. Are you sure you want to request?", delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNREQUESTTITLE, otherButtonTitle: BTNCANCELTITLE) { (customAlert, buttonIndex) -> Void in
                 let userDetails = [KEMAIL: self.txtEmail.text!]
                if buttonIndex == 0{
                    ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.ForgotPassword(userDetails).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController : self, pCompletionBlock :{ (success, strMessage) -> () in
                        if success == true{
//                            self.dismissViewControllerAnimated(true, completion:nil)
                        }
                        else if strMessage != nil{
                            Helper.showAlert((strMessage as? String)!, pobjView: self.view)
                        }
                    })
                }
            }
            alertView.showInView(self.view)
        }
    }
    
    @IBAction func containtViewtap(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    // MARK: - Textfield Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if(textField == txtEmail){
            txtPassword.becomeFirstResponder()
        }
        else if(textField == txtPassword){
            self.actionSignIn()
        }
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(textField: UITextField) {
        objBSKeyboardControls.activeField = textField
    }
    
    // MARK: - BSKeyboard delegate method
    
    func keyboardControlsDonePressed(keyboardControls: BSKeyboardControls!){
        keyboardControls.activeField?.resignFirstResponder()
    }
    
    func keyboardControls(keyboardControls: BSKeyboardControls!, selectedField field: UIView!, inDirection direction: BSKeyboardControlsDirection){
    }
}
