//
//  VenueListController.swift
//  Clink
//
//  Created by Gunjan on 08/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource
import HDCollectionDataSource

class VenueListController: UIViewController,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //    MARK:- Gloable variable
    var arrAllVenues : [ClsVenue]!
    var arrSubVenues : [ClsVenue]!
    var objHDTableDataSource : HDTableDataSource!
    var isVenueSelecting : Bool!
    var objSelectedVenue : ClsVenue!
    var objHDCollectionDataSource : HDCollectionDataSource!
    var arrVenueCategory : [ClsMerchantCategory]!
//    var intSelectedCategory : Int = INTZERO
    var isAlreadySelected : Bool = false
    var strSelctedVenue : String!
    var objPromoDetails : ClsPromotion!
    var strSelctedCategory : String = STREMPTY
    
    //    MARK:- IBOutlet
    @IBOutlet var tblVenueList : UITableView!
    @IBOutlet var lblNotFound : UILabel!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var btnBack : UIButton!    
    @IBOutlet var collectionVenueCategory : UICollectionView!
    
     // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

//        tblVenueList.rowHeight = UITableViewAutomaticDimension
        
        arrSubVenues = Array()
        arrAllVenues = Array()
        arrVenueCategory = Array()
        
        if isVenueSelecting == nil{
            isVenueSelecting = false
            SideMenuRootController.rightSwipeEnabled = false
        }
        
        
//        tblVenueList.estimatedRowHeight = VENUELISTCELLHEIGHT
        tblVenueList.delegate = self
        tblVenueList.allowsSelection = isVenueSelecting
        btnBack.hidden = !(isVenueSelecting)
        btnMenu.hidden = isVenueSelecting

        if self.objSelectedVenue != nil && self.objSelectedVenue.strCategory != nil && self.objSelectedVenue.strCategory == KCATEGORYCHARITIES
        {
            isAlreadySelected = true
            self.strSelctedVenue = self.objSelectedVenue.strName
            self.strSelctedCategory = self.objSelectedVenue.strCategory
//            self.performSegueWithIdentifier(SEGUETOVENUEDETAILS, sender: nil)
        }
        else if self.objSelectedVenue != nil
        {
            self.strSelctedVenue = self.objSelectedVenue.strName
            self.strSelctedCategory = self.objSelectedVenue.strCategory
            isAlreadySelected = true
//            self.performSegueWithIdentifier(SEGUETOHOUNDSDETAILS, sender: nil)
        }
       
        else{
            isAlreadySelected = false
        }
        self.getCategoryList()
        self.configureCollectionView()
        self.configureTableView()
        self.tblVenueList.tableFooterView = UIView()
    }
    
    override func viewDidAppear(animated: Bool) {
        if isVenueSelecting == true{
            SideMenuRootController.rightSwipeEnabled = false
        }
        else{
            SideMenuRootController.rightSwipeEnabled = true
        }
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) != true{
            ClinkAuthenticationManager.sharedInstance.showLoginScreen(self,alertString: "")
        }
    }
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOVENUEDETAILS{
            let objVenueDetailVC = segue.destinationViewController as! VenueDetailsController
            objVenueDetailVC.objSelectedVenue = self.objSelectedVenue
            objVenueDetailVC.isVenueSelecting = self.isVenueSelecting
            objVenueDetailVC.isAlreadySelected = self.isAlreadySelected
            objVenueDetailVC.strSelctedVenue = self.strSelctedVenue
        }
        else if segue.identifier == SEGUETOHOUNDSDETAILS{
            let objVenueHDetailVC = segue.destinationViewController as! VenueHoundsDetailsController
            objVenueHDetailVC.isVenueSelecting = self.isVenueSelecting
            objVenueHDetailVC.objSelectedVenue = self.objSelectedVenue
            if objPromoDetails != nil{
                objVenueHDetailVC.objPromoDetails = self.objPromoDetails
            }
        }
    }
    
    // MARK: - Custom Methods
   
    func getCategoryList(){
       ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetCategoryList().URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController: self) { (success, response) -> () in
            if success == true
            {
                let arrCategories = response["category"] as! [AnyObject]
                self.arrVenueCategory.removeAll()
                for catResponse in arrCategories{
                    let objCategories = ClsMerchantCategory(pdicCategory: catResponse as! [String : AnyObject!])
                    self.arrVenueCategory.append(objCategories)
                }
                
                if self.arrVenueCategory.count > 0{
                    if self.strSelctedCategory.isEmptyString() == true{
                        self.strSelctedCategory = (self.arrVenueCategory.first?.strName)!
                    }
                    self.getVenueList(self.strSelctedCategory)
                }
                else{
                    self.lblNotFound.hidden = false
                }
                self.configureCollectionView()
            }
            else if response != nil{
                self.lblNotFound.hidden = false
                Helper.showAlert((response as? String)!, pobjView: self.view)
            }
        }
    }
    
    func getVenueList(strCategorName : String){
        self.arrAllVenues.removeAll()
        self.configureTableView()

        if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil{
            let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.Partner.rawValue ,"merchantCategory" : strCategorName]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
        }
        let merchantFilter : [String : AnyObject] = ["filters[category]" :strCategorName]
        strSelctedCategory = strCategorName
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetVenueList(merchantFilter).URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
            if success == true
            {
                
                let dicMerchant = response as? [String:AnyObject]
                self.arrAllVenues.removeAll()
                
                if let arrMerchant = dicMerchant!.getValueIfAvilable("merchant")  as? [AnyObject]
                {
                    for merchantResponse in arrMerchant{
                        var tmpMerchantResponse = merchantResponse as! [String : AnyObject]
                        tmpMerchantResponse = tmpMerchantResponse.nullsRemoved
                        tmpMerchantResponse[KCATEGORYNAME] = strCategorName
                        print(tmpMerchantResponse[KCATEGORYNAME])
                        let objMerchant = ClsVenue(pdictResponse: tmpMerchantResponse)
                        self.arrAllVenues.append(objMerchant)
                    }
                }
                self.configureTableView()
                print("Venues are \(self.arrAllVenues)")
            }
            else if response != nil{
                self.arrAllVenues.removeAll()
                Helper.showAlert((response as? String)!, pobjView: self.view)
                self.configureTableView()
                self.lblNotFound.hidden = false
            }
        })
    }
    
    func configureTableView(){
        
            objHDTableDataSource = HDTableDataSource(items: arrAllVenues, multipleCellBlock: { (item, indexPath) -> String! in
            let objItem = item as? ClsVenue
                        if objItem?.strCategory == KCATEGORYCHARITIES{
                return CELLVENUEDONATECELL
            }
            return CELLVENUEFOODCELL
            }, configureCellBlock: { (cell, item, indexPath) -> Void in
                let objCell = cell as! VenueCell
                let objItems = item as! ClsVenue
                objCell.configureCell(objItems)
                objCell.btnShowDetails.tag = indexPath.row
        })
        
        
        tblVenueList.dataSource = objHDTableDataSource
        tblVenueList.delegate = self
        tblVenueList.reloadData()
        if arrAllVenues.count < 1{
            lblNotFound.hidden = false
            tblVenueList.hidden = true
        }
        else{
            lblNotFound.hidden = true
            tblVenueList.hidden = false
        }
    }
    
    func configureCollectionView(){
        objHDCollectionDataSource = HDCollectionDataSource(items: arrVenueCategory as [ClsMerchantCategory], cellIdentifier: CELLVENUECATEGORYCELL, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! VenueCategoryCell
            let objCategory = item as! ClsMerchantCategory
            objCell.configureCell(objCategory)
            objCell.selectionView.hidden = false
            if self.strSelctedCategory != objCategory.strName{
                objCell.selectionView.hidden = true
                objCell.imgVenueCategory.sd_setImageWithURL(NSURL(string: objCategory.strImageURL), placeholderImage: UIImage(named: "place_holder"))
            }
            else{
                objCell.imgVenueCategory.sd_setImageWithURL(NSURL(string: objCategory.strImageURLSelected), placeholderImage: UIImage(named: "place_holder"))
            }
        })
        
        collectionVenueCategory.dataSource = objHDCollectionDataSource
        collectionVenueCategory.delegate = self
        collectionVenueCategory.reloadData()
    }
    
    // MARK: - tableview delegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return VENUELISTCELLHEIGHT
//        return UITableViewAutomaticDimension
    }
    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
//        return UITableViewAutomaticDimension
//    }
    
    
    // MARK: - collectionview delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let objVenueCategory = arrVenueCategory[indexPath.row]
        if arrVenueCategory.count >= 0 && strSelctedCategory != objVenueCategory.strName{
            self.getVenueList(objVenueCategory.strName)
            self.configureCollectionView()
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let maxWidth = APPDELEGATE.window?.frame.size.width
        let count = arrVenueCategory.count
        let sizew = (maxWidth! - VENUECATEGORYCELLWIDTHSUBS) / CGFloat(count)
        let vz = CGSizeMake(sizew, VENUECATEGORYCELLHEIGHT)
        return vz
    }
    
    
    // MARK: - Button Actions
    @IBAction func btnBackTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    @IBAction func btnshowDetailsTapped(sender: UIButton) {
        if arrAllVenues.count >= sender.tag{
            
            objSelectedVenue = arrAllVenues[sender.tag]
            
            if objSelectedVenue.arrCategoryName[0] == KCATEGORYCHARITIES
            {
                self.performSegueWithIdentifier(SEGUETOVENUEDETAILS, sender: nil)
            }
            else
            {
                self.performSegueWithIdentifier(SEGUETOHOUNDSDETAILS, sender: nil)
            }
//            else
//            {
//                self.performSegueWithIdentifier(SEGUETOVENUEDETAILS, sender: nil)
//            }
        }
        
    }
}
