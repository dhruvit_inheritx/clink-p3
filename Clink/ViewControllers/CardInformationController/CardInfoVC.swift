//
//  CardInfoVC.swift
//  Clink
//
//  Created by Gunjan on 20/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class CardInfoVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var bankDetailView: UIView!
    @IBOutlet var actionView: UIView!
    @IBOutlet var backView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var nextView: UIView!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet var keyPadView: UIView!
    @IBOutlet var infoView: UIView!
    @IBOutlet var lblView: UIView!
    @IBOutlet var lbl_title: UILabel!
    
    @IBOutlet weak var txtCardNumber: TextFieldPlaceHolder!
    @IBOutlet weak var txtName: TextFieldPlaceHolder!
    @IBOutlet weak var txtExpDate: TextFieldPlaceHolder!
    @IBOutlet weak var txtCVC: TextFieldPlaceHolder!
    
    var arrayTextField : [TextFieldPlaceHolder]! = Array()
    
    var txtCurrent : UITextField! = UITextField()
    var dictCardInfo : [String : AnyObject]!
    var isFTUUser = false
    var objCreditCardDetail = ClsCreditCardDetails()
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtCardNumber.tag = 1
        txtName.tag = 2
        txtExpDate.tag = 3
        txtCVC.tag = 4
        
        
        txtName.returnKeyType = .Done
        txtCurrent = txtCardNumber
        
        
        arrayTextField = [txtCardNumber,txtName,txtExpDate,txtCVC]
        
//        self.deleteCard("01b711f91a04d7a44fb7b201be969bad")
    }
    
    func deleteCard(strID : String){
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.DeleteCard(strID).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                print("Card deleted successfully.")
            }
            else{
                print("Error deleting card.")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == 2{
            textField.resignFirstResponder()
//            txtExpDate.becomeFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        for txtField in arrayTextField {
            txtField.layer.borderColor = UIColor.clearColor().CGColor
            txtField.layer.borderWidth = 0
        }
        
        textField.layer.borderColor = COLORTHEME.CGColor
        textField.layer.borderWidth = 1

        if textField.tag == 1
        {
            self.view.endEditing(true)
            txtCurrent = txtCardNumber
            return false
        }
        else if textField.tag == 2
        {
            txtCurrent = txtName
            return true
        }
        else if textField.tag == 3
        {
            self.view.endEditing(true)
            txtCurrent = txtExpDate
            return false
        }
        else
        {
            self.view.endEditing(true)
            txtCurrent = txtCVC
            return false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func actionInputButton(sender:UIButton){
        
        txtName.resignFirstResponder()
        
        var inputVal = STREMPTY
        
        switch sender.tag{
            
        case 0 :
            inputVal = STR0
        case 1 :
            inputVal = STR1
        case 2 :
            inputVal = STR2
        case 3 :
            inputVal = STR3
        case 4 :
            inputVal = STR4
        case 5 :
            inputVal = STR5
        case 6 :
            inputVal = STR6
        case 7 :
            inputVal = STR7
        case 8 :
            inputVal = STR8
        case 9 :
            inputVal = STR9
        case 10 :
            inputVal = STRPOINT
        default:
            inputVal = STREMPTY
        }
        
        let strAmount = txtCurrent.text
        
        if txtCurrent == txtCardNumber{
            
            if strAmount!.length < 19
            {
                let strTemp = txtCurrent.text?.stringByReplacingOccurrencesOfString("-", withString: "")
                
                if strTemp!.length % 4 == 0 && strTemp!.length >= 4{
                    txtCurrent.text! += "-"
                }
                txtCurrent.text! += inputVal
            }
        }
        else if txtCurrent == txtCVC{
            
            if strAmount!.length < 4
            {
                txtCurrent.text! += inputVal
            }
        }
        else if txtCurrent == txtExpDate{
            
            if strAmount!.length < 5
            {
                if txtCurrent.text!.length == 2{
                    txtCurrent.text! += "/"
                }
                txtCurrent.text! += inputVal
            }
        }
        else{
            txtCurrent.text! += inputVal
        }
    }
    
    @IBAction func actionRemove(sender:UIButton){
        
        if ((txtCurrent.text?.isEmptyString()) == false)
        {
            var strAmount = String(txtCurrent.text!.characters.dropLast()) as String
            strAmount.removeLastCharIfExist("-")
            strAmount.removeLastCharIfExist("/")
            
            txtCurrent.text = strAmount
            
            if ((txtCurrent.text?.isEmptyString()) == true){
                txtCurrent.text = STREMPTY
            }
        }
    }
    
    @IBAction func back_click(sender: UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func getCurrentYear() -> String {
        let date = NSDate()
        let cal = NSCalendar.currentCalendar()
        let year = cal.ordinalityOfUnit(NSCalendarUnit.Year, inUnit: NSCalendarUnit.Era, forDate: date)
        let strYear = String(year)
        return strYear
    }

    @IBAction func add_click(sender: UIButton)
    {
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false{
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        let strCardNumber = txtCardNumber.text!.stringByReplacingOccurrencesOfString("-", withString: "")
        let arrayExp = txtExpDate.text?.componentsSeparatedByString("/")
        let strExpMonth = (arrayExp?.first)! as String
        let strCurrentYear = self.getCurrentYear() as String
        let first2 = String(strCurrentYear.characters.prefix(2))
        let strExpYear = first2 + ((arrayExp?.last)! as String)
        
        print(strExpMonth + " " + strExpYear)
        
        if strCardNumber.isEmpty == true || txtCVC.text!.isEmpty == true  || txtExpDate.text!.isEmpty == true || strExpMonth == "" || strExpYear == "" {
            
            txtCardNumber.setPlaceHolder(txtCardNumber.placeholder!, color: UIColor.redColor())
            txtName.setPlaceHolder(txtName.placeholder!, color: UIColor.redColor())
            txtExpDate.setPlaceHolder(txtExpDate.placeholder!, color: UIColor.redColor())
            txtCVC.setPlaceHolder(txtCVC.placeholder!, color: UIColor.redColor())
            
            Helper.showAlert(ALERTFILLALLFIELDS, pobjView: self.view)
        }
        else if strCardNumber.length != 16{
            Helper.showAlert("Card Number must be 16 digit number.", pobjView: self.view)
        }
        else if txtCVC.text!.length <= 2{
            Helper.showAlert("CVC Number must be 3 to 4 digit number.", pobjView: self.view)
        }
        else if strExpMonth == "" || strExpYear == ""{
            Helper.showAlert("Please enter Expiry Month and Year in MM/YY format.", pobjView: self.view)
        }
        else{
            dictCardInfo = ["number":strCardNumber,
                            "exp_month":strExpMonth,"exp_year":strExpYear,"cvc":txtCVC.text!]
            ClinkAuthenticationManager.sharedInstance.callWebService(ClinkAccountRouter.CreateCard(dictCardInfo).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success{
                    
                    Helper.showAlertwithTitle("THANK YOU!", strMessage: "Your account information will be stored in your account profile.", pobjView: self.view, completionBock: { (CustomAlert, buttonIndex) -> () in

                        
                        self.navigateBack()
                    })
                }
                else{
                    
                    let strMessage = response as? String
                    
                    if strMessage == "Account requires verification, please check your email" {
                    
                        Helper.showAlert("Email verification is required, please click resend if you haven't received the verification email.", cancelButtonTitle: "Cancel", otherButtonTitle: "Resend", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                            
                            if buttonIndex == 0 {
                                self.navigateBack()
                            }
                            else {
                                
                                let objUserDetail = USERDEFAULTS.getUserDetailsObject()
                                
                                ClinkAccountManager.sharedInstance.sendVerificationEmail(objUserDetail.strEmail, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
                                    
                                    if success == true {
                                        Helper.showAlert("Verification Email has been successfully sent to your email account.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                                            self.navigateBack()
                                        })
                                    }
                                    else {
                                        Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                                            self.navigateBack()
                                        })
                                    }
                                })
                            }
                        })
                    }
                    else {
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                }
            }
        }
    }
    
    func navigateBack(){
        if self.isFTUUser == true {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            for vc in (self.navigationController?.viewControllers)! {
                if vc.isKindOfClass(AccountsVC) == true {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
                else if vc.isKindOfClass(SelectPaymentViewController) == true {
                    self.navigationController?.popToViewController(vc, animated: true)
                    break
                }
            }
        }
    }
    
    func searchCard(){
        
        dictCardInfo = ["filters[last4]":"3456"]
        
        ClinkAuthenticationManager.sharedInstance.callWebService(ClinkAccountRouter.SearchCard(dictCardInfo).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success{
                
                print("Card Search Successfully.")
            }
            else{
                Helper.showAlert((response as? String)!, pobjView: self.view)
            }
        }
    }
    
}
