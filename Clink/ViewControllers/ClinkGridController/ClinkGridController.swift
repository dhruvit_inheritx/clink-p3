//
//  ClinkGridController.swift
//  Clink
//
//  Created by Gunjan on 29/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class ClinkGridController: UIViewController {
    
    var objSelectedClinkDetails : ClsClinkDetails!
    var arrAssociatedClink : [ClsClinkDetails] = Array()
    var arrAssociatedClinkID : [String] = Array()
    var objSelectedAssociatedClink : ClsClinkDetails!
    var intCurrentIndex : Int = 0
    var selectedClinkType : ClinkType!
    
//    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var collectionClinkDetails : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        if selectedClinkType == ClinkType.Sent{
//            btnTitle.setImage(UIImage(named: IMGICNSENTTITLE), forState:.Normal)
//            btnTitle.setTitle(STRSENT, forState: .Normal)
//        }
//        else if selectedClinkType == ClinkType.Received{
//            btnTitle.setImage(UIImage(named: IMGICNINBOX), forState:.Normal)
//            btnTitle.setTitle(STRRECIEVED, forState: .Normal)
        //        }
        arrAssociatedClinkID = objSelectedClinkDetails.arrAssociatedClinkId
        if objSelectedClinkDetails != nil && objSelectedClinkDetails.strGiftPlusClinkId != nil && objSelectedClinkDetails.strGiftPlusClinkId.isEmptyString() != true {
            arrAssociatedClinkID.insert(objSelectedClinkDetails.strGiftPlusClinkId, atIndex: 0)
        }
        self.manageArray(objSelectedClinkDetails)
        self.getClinkDetails()
    }
    
    func getClinkDetails() {
        var resCount = 0
        Helper.showHud(self.view)
        for strClinkId in arrAssociatedClinkID {
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(strClinkId,nil).URLRequest, isBackgroundCall:true, pAlertView: self.view, pViewController: nil, pCompletionBlock: { (success, response) -> () in
                resCount += 1
                if success == true{
                    self.manageArray(ClsClinkDetails(pdictResponse: response as! [String : AnyObject]))
                }
                
                if resCount == self.arrAssociatedClink.count - 1{
                    Helper.hideHud(self.view)
                }
                
            })
        }
    }
    
    func manageArray(objClinkDetails :ClsClinkDetails) {
        arrAssociatedClink.append(objClinkDetails)
        self.collectionClinkDetails.reloadData()
    }
    
    @IBAction func actionBack(sender : UIButton){
//        self.navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func actionshowDetails(sender : UIButton){
        objSelectedAssociatedClink = arrAssociatedClink[sender.tag]
        self.intCurrentIndex = sender.tag
        self.performSegueWithIdentifier("segueToGridDetails", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueToGridDetails"{
            let objClinkGridDetailsVC = segue.destinationViewController as! ClinkDetailsController
            objClinkGridDetailsVC.objSelectedClinkDetail = self.objSelectedAssociatedClink
            objClinkGridDetailsVC.arrClinkDetails = self.arrAssociatedClink
            objClinkGridDetailsVC.intCurrentIndex = self.intCurrentIndex
            objClinkGridDetailsVC.isFromGridVC = true
            objClinkGridDetailsVC.selectedClinkType = self.selectedClinkType
            
        }
    }
    
    // MARK: - CollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAssociatedClink.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        let cellIdentifier:String = CELLCLINKGRID;
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier,forIndexPath:indexPath) as! ClinkGridCell
        let objCurrentClinkDetails = arrAssociatedClink[indexPath.row]
        cell.activityIndicator.startAnimating()
        if let strImgUrl = objCurrentClinkDetails.strImageUrl {
            cell.imgVwClink.sd_setImageWithURL(NSURL(string: strImgUrl), completed: { (image, error, SDImageCacheType, url) -> Void in
                if (image != nil){
                    cell.activityIndicator.stopAnimating()
                }
            })
        }
        cell.btnDetails.tag = indexPath.row
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        let width = (APPDELEGATE.window?.frame.width)! / 2
        let height = (width * 202) / 160
        return CGSizeMake(width, height)
    }
}
