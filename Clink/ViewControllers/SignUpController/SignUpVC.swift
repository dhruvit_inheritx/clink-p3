//
//  SignUpVC.swift
//  Clink
//
//  Created by Gunjan on 29/02/16.
//  Copyright © 2016 inheritX. All rights reserved.
//
import UIKit
import Social
import BSKeyboardControls


class SignUpVC: UIViewController,UITextFieldDelegate,BSKeyboardControlsDelegate {
    
     //    MARK:- Gloable variable
    var arrFields:[AnyObject]!
    var objBSKeyboardControls: BSKeyboardControls!
    let datePickerView  : UIDatePicker = UIDatePicker()
    
    //    MARK:- IBOutlet
    @IBOutlet weak var txtFName: UITextField!
    @IBOutlet weak var txtLName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtDOB: UITextField!
    
    
    // MARK: - Viewcontroller Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        arrFields = Array()
        arrFields = [txtFName,txtLName,txtEmail,txtMobileNo,txtPassword,txtConfirmPassword]
        for txtField in arrFields{
            Helper.setTextFieldPadding(txtField as! UITextField, yPosition: 21)
        }
        objBSKeyboardControls = BSKeyboardControls(fields: arrFields)
        objBSKeyboardControls.delegate = self
        datePickerView.datePickerMode = UIDatePickerMode.Time
        datePickerView.addTarget(self, action: #selector(SignUpVC.handleDatePicker(_:)), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.maximumDate = NSDate().getNewUTCDate()
        datePickerView.datePickerMode = .Date
        txtDOB.inputView = datePickerView
    }
    
    override func viewWillAppear(animated: Bool) {
        self.autoFillData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SignUpVC.autoFillData), name: "AUTOFILLDATA", object: nil)
    }
    
    func autoFillData() {
        if (USERDEFAULTS.objectForKey("AUTOFILLDATA") != nil){
            let dicUserDetails = USERDEFAULTS.objectForKey("AUTOFILLDATA") as! [String : AnyObject]
            if let email = dicUserDetails.getValueIfAvilable("email"){
                txtEmail.text = email as? String
            }
            if let mobile = dicUserDetails.getValueIfAvilable("mobileno"){
                
                var strPhone = mobile as? String
                
                if strPhone != "" && strPhone != nil {
                    if strPhone!.characters.count >= 10 {
                        
                        let num: String = Helper.formatNumber(strPhone!)
                        let str1 = num.substringToIndex(num.startIndex.advancedBy(3))
                        var str2 = num.substringToIndex(num.startIndex.advancedBy(6))
                        str2 = str2.substringFromIndex(str2.startIndex.advancedBy(3))
                        let str3 = num.substringFromIndex(num.startIndex.advancedBy(6))
                        
                        strPhone = "("+str1+") "+str2+"-"+str3
                        
                        txtMobileNo.text = strPhone
                    }
                    else {
                        txtMobileNo.text = strPhone
                    }
                }
                else {
                    txtMobileNo.text = strPhone
                }
            }
            USERDEFAULTS.removeObjectForKey("ISLAUCHEDUSINGURL")
            USERDEFAULTS.removeObjectForKey("SHOULDAUTOREDIRECT")
            USERDEFAULTS.removeObjectForKey("AUTOFILLDATA")
            USERDEFAULTS.synchronize()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "AUTOFILLDATA", object: nil)
    }
    // MARK: - Button Actions
    
    // Open Password Validation Alert for User Information
    @IBAction func actionPasswordInfo(sender: AnyObject) {
        Helper.showAlertwithTitle(ALERTTITLEPASSWORDREQUIRENMENTS, strMessage: ALERTPASSWORDVALIDATIONDESC, pobjView: self.view)
    }
    
    // Perform validation and Sign Up
    
    @IBAction func actionSignUp() {
//        var arrFields : [String] = Array()        
        self.view.endEditing(true)
        
        if txtFName.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKFNAME, pobjView: self.view)
        }
        else if txtLName.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKLNAME, pobjView: self.view)
        }
        else if txtEmail.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKEMAILADDRESS, pobjView: self.view)
        }
        else if txtMobileNo.text!.isEmptyString() == true {
            Helper.showAlert(ALERTMOBILETENDEGITNUMBER, pobjView: self.view)
        }
        else if txtPassword.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKPASSWORD, pobjView: self.view)
        }
        else if txtConfirmPassword.text!.isEmptyString() == true {
            Helper.showAlert(ALERTBLANKCONFIRMPASSWORD, pobjView: self.view)
        }
//        else if txtDOB.text?.isEmptyString() == true{
//            Helper.showAlert(ALERTBLANKDOB, pobjView: self.view)
//        }
        else if txtEmail.text?.isValidEmail() == false{
            Helper.showAlert(ALERTINVALIDEMAIL, pobjView: self.view)
        }
        else if txtMobileNo.text?.length < 14{
            Helper.showAlert(ALERTMOBILETENDEGITNUMBER, pobjView: self.view)
        }
//        else if txtMobileNo.text?.isValidPhoneNumber() == false{
//            Helper.showAlert(ALERTMOBILETENDEGITNUMBER, pobjView: self.view)
//        }
//        else if txtPassword.text != txtConfirmPassword.text {
//            Helper.showAlert(ALERTPASSWORDMISMATCH, pobjView: self.view)
//        }
        else
        {
            let strUserName = txtEmail.text!.lowercaseString
        
            let strPhoneNumber = Helper.formatNumber(txtMobileNo.text!)
            
            var dicUserDetails : [String : AnyObject] = [KFIRSTNAME : txtFName.text! , KLASTNAME : txtLName.text!, KEMAIL : strUserName, KPASSWORD : txtPassword.text!, KCONFIRMPASS : txtConfirmPassword.text!, KPHONE : strPhoneNumber , KDATEOFBIRTH : txtDOB.text!]
            if USERDEFAULTS.valueForKey(KDEVICETOKEN) != nil{
                dicUserDetails[KDEVICETOKEN] = USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String
            }
            else {
                dicUserDetails[KDEVICETOKEN] = ""
            }

            ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.SignUpUser(dicUserDetails).URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController : self, pCompletionBlock :{ (success, response) -> () in
                
                if success == true{
                    dispatch_async(dispatch_get_main_queue()) { () -> Void in
                        Helper.getTrackerGroups()
                    }
                    let result = response as! [String : AnyObject!]
                    let objUserDetails = ClsUserDetails(pdicResponse: result)
                    let dicSession = objUserDetails.dicSession as ClsSessionDetails
                    if let sid = dicSession.id
                    {
                        ClinkDetailsRouter.SessionID = sid
                        USERDEFAULTS.setObject(sid, forKey: KSESSIONID)
                    }
                    USERDEFAULTS.setUserDetailsObject(objUserDetails)
                    USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                    USERDEFAULTS.synchronize()
                    
                    let alertView = CustomAlert(title: APPNAME, message:ALERTVERIFYACCOUNTEMAIL, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: "OK", otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
                        
                        if buttonIndex == INTZERO {
                            
//                    let storyboard = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil)
//                    let navVC = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERSELECTPAYMENTMETHODFTU) as! UINavigationController
//                    let addPaymentMethodVC = navVC.topViewController! as! AddPaymentMethodController
//                    addPaymentMethodVC.addPaymentMethodControllerType = AddPaymentMethodControllerType.Payment
//                    addPaymentMethodVC.isFTUUser = true
//                    self.navigationController?.pushViewController(addPaymentMethodVC, animated: true)
                            self.dismissViewControllerAnimated(false, completion:nil)
                        }
                    }
                    alertView.showInView(APPDELEGATE.window)
                }
                else if response != nil{
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            })
        }
    }
    
    // Pick date from DatePicker
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        txtDOB.text = dateFormatter.stringFromDate(sender.date)
    }
    
    // Go back to Sign In Screen
    
    @IBAction func actionClose(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true) 
    }
    
    // Sign Up with Facebook
    
    @IBAction func actionSignInWithFacebook(sender: AnyObject) {
        
        // Login PFUser using Facebook
        
        ClinkAuthenticationManager.sharedInstance.signInWithFacebook(self, isSigningUp: true) { (success, response) -> () in
            
            Helper.hideHud(self.view)
            if success == true{
                let result = response as! [String : AnyObject!]
                let objUserDetails = ClsUserDetails(pdicResponse: result)
                let dicSession = objUserDetails.dicSession as ClsSessionDetails
                if let sid = dicSession.id
                {
                    ClinkDetailsRouter.SessionID = sid
                    USERDEFAULTS.setObject(sid, forKey: KSESSIONID)
                }
                USERDEFAULTS.setUserDetailsObject(objUserDetails)
                USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                self.navigationController?.dismissViewControllerAnimated(false, completion: nil)
            }
            else if response != nil{
                
                Helper.showAlert(response as! String, pobjView: self.view)
                /*
                if ClinkDetailsRouter.SessionID != nil{
                    ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.LogoutUser().URLRequest, isBackgroundCall : true, pAlertView : self.view, pViewController: nil, pCompletionBlock: { (success, error) -> () in
                        Helper.hideHud(self.view)
                    })
                }
                else{
                    Helper.hideHud(self.view)
                }*/
            }
        }
    }
    
    // Sign U with Twitter
    
    @IBAction func actionSignInWithTwitter(sender: AnyObject) {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
        /*
        ClinkAuthenticationManager.sharedInstance.signInWithTwitter(self.view, isSigningUp: true) { (success, error) -> () in
            Helper.hideHud(self.view)
            if success == true{
                USERDEFAULTS.setBool(true, forKey: kISUSERLOGGEDIN)
                USERDEFAULTS.synchronize()
                self.dismissViewControllerAnimated(false, completion:nil)
            }
            else if error != nil{
                Helper.showAlert(error as! String, pobjView: self.view)
            }
        }*/
    }
    
    // Show Terms and Conditions
    
    @IBAction func btnShowTerms(sender: AnyObject) {
        let storyboard = UIStoryboard(name: MAINSTORYBOARD, bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(CONTROLLERTANDC) as! TermsConditionsController
        
        USERDEFAULTS.setInteger(screenType.Privacy.rawValue, forKey:KSELECTEDSCREENTYPE)
        USERDEFAULTS.synchronize()
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func containtViewtap(sender: AnyObject) {
        self.view.endEditing(true)
    }
    
    // MARK: - Textfield Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if(textField == txtFName){
            txtLName.becomeFirstResponder()
        }
        else if(textField == txtLName){
            txtEmail.becomeFirstResponder()
        }
        else if(textField == txtEmail){
            txtMobileNo.becomeFirstResponder()
        }
        else if(textField == txtMobileNo){
            txtPassword.becomeFirstResponder()
        }
        else if(textField == txtPassword){
            txtConfirmPassword.becomeFirstResponder()
        }
        else if(textField == txtConfirmPassword){
//            txtDOB.becomeFirstResponder()
//        }
//        else if(textField == txtDOB){
            self.actionSignUp()
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtMobileNo{
            
            let length: Int = Int(Helper.getLength(textField.text!))
            if length == 10 {
                if range.length == 0 {
                    return false
                }
            }
            if length == 3 {
                let num: String = Helper.formatNumber(textField.text!)
                textField.text = "(\(num)) "
                if range.length > 0 {
                    textField.text = "\(num.substringToIndex(num.startIndex.advancedBy(3)))"
                }
            }
            else if length == 6 {
                let num: String = Helper.formatNumber(textField.text!)
                textField.text = "(\(num.substringToIndex(num.startIndex.advancedBy(3)))) \(num.substringFromIndex(num.startIndex.advancedBy(3)))-"
                if range.length > 0 {
                    textField.text = "(\(num.substringToIndex(num.startIndex.advancedBy(3)))) \(num.substringFromIndex(num.startIndex.advancedBy(3)))"
                }
            }

            if  textField.text?.characters.count >= 14 && string != STREMPTY{
                return false
            }
            else if string != STREMPTY && Int(string) == nil{
               return false
            }
        }
        else if textField == txtDOB{
           return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == txtDOB{
            self.handleDatePicker(datePickerView)
        }
        objBSKeyboardControls.activeField = textField
    }
    
    // MARK: - BSKeyboard delegate method
    
    func keyboardControlsDonePressed(keyboardControls: BSKeyboardControls!){
//        myDatePicker.hidden = true
        if keyboardControls.activeField == txtDOB{
            
        }
        keyboardControls.activeField?.resignFirstResponder()
    }
    
    func keyboardControls(keyboardControls: BSKeyboardControls!, selectedField field: UIView!, inDirection direction: BSKeyboardControlsDirection){
    }
    
}
