//
//  TermsConditionsController.swift
//  Clink
//
//  Created by Gunjan on 18/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

enum screenType : Int {
    case Help = 1,Support = 2,Privacy = 3,AboutUs = 5
}
class TermsConditionsController: UIViewController,UIWebViewDelegate {
    
    var selectedScreenType : screenType!
    let ARTICLEBASEURL = "https://qa-web.clink.nyc/article/"
    @IBOutlet var tacWebView : UIWebView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblBannerName : UILabel!
    @IBOutlet var btnMenu : UIButton!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true{
           btnMenu.setImage(UIImage(named:IMGBTNMENU), forState:.Normal)
        }else{
            btnMenu.setImage(UIImage(named: IMGBTNBACK), forState:.Normal)
        }
        // Do any additional setup after loading the view.
        let intScreenType = USERDEFAULTS.integerForKey(KSELECTEDSCREENTYPE)
        
           self.loadHtmlFile(intScreenType)
        tacWebView.delegate = self
    }
    func loadHtmlFile(selectedScreenType : Int)
    {
        var fileName = STREMPTY
        switch selectedScreenType
        {
        case screenType.Help.rawValue:
            fileName = FILEHELP
            lblTitle.text = STRHELP
            break
        case screenType.Support.rawValue:
            fileName = FILESUPPORT
            lblTitle.text = STRSUPPORT
            break
        case screenType.Privacy.rawValue:
            fileName = FILEPRIVACYTERMS
            lblTitle.text = STRTERMSCONDITIONS
            break
        case screenType.AboutUs.rawValue:
            fileName = FILEABOUTUS
            lblTitle.text = STRABOUTUS
            break
        default :
            fileName = FILEABOUTUS
            break

        }
        
        lblTitle.font = UIFont(name: FONTGOTHAMMEDIUM, size: CGFloat(15))

        fileName = ARTICLEBASEURL+fileName
        if let url = NSURL(string: fileName)
        {
            tacWebView.loadRequest(NSURLRequest(URL: url))
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        Helper.showHud(self.view)
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        Helper.hideHud(self.view)
    }    
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        Helper.hideHud(self.view)
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true{
            NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
        }else{
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
}
