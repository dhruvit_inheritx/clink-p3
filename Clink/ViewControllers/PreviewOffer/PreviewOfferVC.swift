//
//  PreviewOfferVC.swift
//  Clink
//
//  Created by Gunjan on 22/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class PreviewOfferVC: UIViewController {

    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var btnReceived: UIButton!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackCliked(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
