//
//  HomeController.swift
//  Clink
//
//  Created by Gunjan on 19/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource
import AddressBookUI
import Alamofire

class HomeController: UIViewController, UITableViewDelegate{
    
    //    MARK:- Gloable variable
    var arrFeedList : [ClsClinkDetails]!
//    var arrEmail : [String]!
    var imgSelectedImage : UIImage!
    var objSelectedClinkDetail : ClsClinkDetails!
    var objHDTableDataSource : HDTableDataSource!
    var reloadData : Bool = true
    var shouldAutoLike : Bool = false
    var shouldOpenComment : Bool = false
    //    var arrMyContacts : [ClsContact]!
    
    var intCurrentIndex : Int!
    var intCurrentPage : Int = 1
    var intLastPage : Int! = 0
    var timer : NSTimer!
    
    //    MARK:- IBOutlet
    @IBOutlet var tblFeedList : UITableView!
    @IBOutlet var lblNotFound : UILabel!
    
    @IBOutlet var badgeReceived : UILabel!
    @IBOutlet var badgeSent : UILabel!
    @IBOutlet var btnSend : UIButton!
    @IBOutlet var btnReceived : UIButton!
    @IBOutlet var btnSent : UIButton!
    @IBOutlet var imgBG : UIImageView!
    
    
    // MARK: - Life cycle
    override func viewWillAppear(animated: Bool) {
        if arrFeedList == nil{
            arrFeedList = Array()
        }
        
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) != true{
            imgBG.hidden = false
            self.view.bringSubviewToFront(imgBG)
        }
        else{
            imgBG.hidden = true
            if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil{
                let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.Home.rawValue ]
                Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
            }
            self.view.sendSubviewToBack(imgBG)
        }
        self.validateCurrentUser()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

//        NOTIFICATIONCENTER.addObserver(self, selector: "getHomeFeedClinks", name: kLoadFeedData, object: nil)
        // Do any additional setup after loading the view.
        
        badgeReceived.layer.cornerRadius = badgeReceived.frame.size.height/2
        badgeSent.layer.cornerRadius = badgeSent.frame.size.height/2
        
        timer = NSTimer.scheduledTimerWithTimeInterval(40, target: self, selector: #selector(HomeController.getUnreadCount), userInfo: nil, repeats: true)
        timer.fire()
        
        if USERDEFAULTS.boolForKey(KNAVIGATETORECEIVEDLIST) == true{
            APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
            USERDEFAULTS.removeObjectForKey(KNAVIGATETORECEIVEDLIST)
            USERDEFAULTS.synchronize()
            APPDELEGATE.objSideMenu.showReceivedClink()
        }
        else if USERDEFAULTS.boolForKey(KNAVIGATETOSENDLIST) == true{
            APPDELEGATE.objSideMenu.setMenuSelectedItem(3)
            
            USERDEFAULTS.removeObjectForKey(KNAVIGATETOSENDLIST)
            USERDEFAULTS.synchronize()
            APPDELEGATE.objSideMenu.showSentClink()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let loginCount = USERDEFAULTS.getUserDetailsObject().intLoginCount
        if loginCount != nil && loginCount == INTZERO  && USERDEFAULTS.boolForKey(KISFTUHELPSHOWN) != true{
            let ftuView = NSBundle.mainBundle().loadNibNamed("FTUPopUpView", owner: self, options: nil)!.first as? FTUPopUpView
            ftuView?.frame = UIScreen.mainScreen().bounds
            ftuView?.subView.layer.cornerRadius = 8
            ftuView?.subView.center = ftuView!.center
            ftuView?.subView.updateConstraintsIfNeeded()
            ftuView?.subView.layoutIfNeeded()
            ftuView?.layoutIfNeeded()
            USERDEFAULTS.setBool(true, forKey: KISFTUHELPSHOWN)
            USERDEFAULTS.synchronize()
            ftuView?.showInView(APPDELEGATE.window!, complitionBlock: { (result) -> () in
                ftuView!.removeFromSuperview()
            })
        }
        timer.fire()
    }
    
    func validateCurrentUser(){
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeController.reloadTableData), name: "RELOADHOMEDATA", object: nil)
        if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) != true{
            imgBG.hidden = false
            self.view.bringSubviewToFront(imgBG)
            
            USERDEFAULTS.setBool(true, forKey: "SHOULDAUTOREDIRECT")
            USERDEFAULTS.synchronize()
            ClinkAuthenticationManager.sharedInstance.showLoginScreen(self,alertString: "")
            
        }
        else if (reloadData == true){
            if USERDEFAULTS.boolForKey("ISLAUCHEDUSINGURL") == true{
                APPDELEGATE.manageAutoFill()
            }
            else{
                imgBG.hidden = true
                self.view.sendSubviewToBack(imgBG)
                intCurrentPage = 1
                intLastPage = 0
                //            arrFeedList.removeAll()
                reloadData = false
                getHomeFeedClinks(RECORDSPERPAGE,intCurrentPage:intCurrentPage)
            }
            
        }
        
    }
    
    func reloadTableData() {
        if arrFeedList.count <= 0{
            intCurrentPage = 1
            intLastPage = 0
            reloadData = false
            getHomeFeedClinks(RECORDSPERPAGE,intCurrentPage:intCurrentPage)
        }
    }
    
    func getUnreadCount(){
        
        badgeReceived.text = "\(USERDEFAULTS.getUserDetailsObject().intReceivedCount)"
        badgeSent.text = "\(USERDEFAULTS.getUserDetailsObject().intSentCount)"
    }
    
    
    func configureTableView(){
        
        //        arrFeedList.sortInPlace({ $0.strDate.compare($1.strDate) == .OrderedDescending })
        
        objHDTableDataSource = HDTableDataSource(items: arrFeedList as [AnyObject], cellIdentifier: KCLINKSENT, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! ClinkListCell
            let objItems = item as! ClsClinkDetails
            
            objCell.configureFeedCell(objItems,pclinkType:ClinkType.Sent)
            objCell.btnShowDetails.tag  = indexPath.row
            objCell.btnLike.tag  = indexPath.row
            objCell.btnComment.tag  = indexPath.row
            objCell.btnGift.tag  = indexPath.row
            objCell.imgisRead.hidden = true
            //objItems.arrCCName
        })
        
        tblFeedList.dataSource = objHDTableDataSource
        tblFeedList.delegate = self
        tblFeedList.reloadData()
    }
    
    // MARK: - Button Actions
    @IBAction func btnClinkGroupClicked(sender: UIButton) {
        
        let btnTag = sender.tag
//                Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
        switch btnTag{
        case 1 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(1)
            APPDELEGATE.objSideMenu.showSendNewClink()
            break
        case 2 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
            APPDELEGATE.objSideMenu.showReceivedClink()
            break
        case 3 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(3)
            APPDELEGATE.objSideMenu.showSentClink()
            break
        default :
            break
        }
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    @IBAction func btnShowClinkDetailsTapped(sender: UIButton) {
        intCurrentIndex = sender.tag
        self.performActionTap(sender,segueID: SEGUETOCLINKDETAILS)
    }
    
    // MARK: - COMMENT CLINLK!

    @IBAction func btnCommentTapped(sender: UIButton) {
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }

        shouldAutoLike = false
        shouldOpenComment = true
        self.performActionTap(sender,segueID: SEGUETOTHREADVIEW)
    }
    
    // MARK: - LIKE CLINLK!
    
    @IBAction func btnLikeTapped(sender: UIButton) {
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        sender.userInteractionEnabled = false

        if arrFeedList.count > sender.tag{
            let indexPath = NSIndexPath(forRow:sender.tag, inSection: 0)
            let cell = tblFeedList.cellForRowAtIndexPath(indexPath) as! ClinkListCell
            objSelectedClinkDetail = arrFeedList[sender.tag]
            
            if cell.imgClink.image != nil{
                imgSelectedImage = cell.imgClink.image!
                tblFeedList.reloadRowsAtIndexPaths(
                    [indexPath], withRowAnimation: .None)
                self.likeClink(sender)
            }
            else{
                Helper.showAlert(ALERTPLEASEWAITTILLLOADINGIMAGE, pobjView: self.view)
            }
        }
    }
    
    func likeClink(sender: UIButton) {
        
        if self.objSelectedClinkDetail != nil{
            
            if (self.objSelectedClinkDetail.isLiked != true){
                
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkLike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        print("Like CLink")
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getClinkDetails(sender)
                        }
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
            else{
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkUnlike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        
                        print("Unlike CLink")
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getClinkDetails(sender)
                        }
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
        }
    }
    
    func getClinkDetails(sender: UIButton)
    {
        if self.objSelectedClinkDetail != nil{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objSelectedClinkDetail.strId,nil).URLRequest, isBackgroundCall:false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                
                if success == true{
                    
                    if let dicResponse = response as? [String : AnyObject] {
                        
                        sender.userInteractionEnabled = true

                        if let error = dicResponse.getValueIfAvilable(KMESSAGE) {
                            print("Error :- \(error)")
                            return
                        }
                        else{
                            self.objSelectedClinkDetail = ClsClinkDetails(pdictResponse: dicResponse)
                            self.arrFeedList[sender.tag] = self.objSelectedClinkDetail
//                            self.arrFeedList.insert(self.objSelectedClinkDetail, atIndex: sender.tag)
                            self.configureTableView()
                        }
                    }
                }
                else if response != nil{
                    sender.userInteractionEnabled = true
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            })
        }
    }
    
    // MARK: - ADD GIFT
    
    @IBAction func btnAddGiftTapped(sender: UIButton) {
        shouldAutoLike = false
        shouldOpenComment = false
        let tmpSelectedClinkDetail = arrFeedList[sender.tag]
        
        
        var arrCCTemp : [ClsContact]!
        arrCCTemp = Array()
        
        var isCCUser : Bool = false
        
        if tmpSelectedClinkDetail.arrCCUser != nil && tmpSelectedClinkDetail.arrCCUser.count > 0 {
            
            arrCCTemp = tmpSelectedClinkDetail.arrCCUser
        }
        
        for item in arrCCTemp {
            if item.strId == USERDEFAULTS.getUserDetailsObject().strId {
                isCCUser = true
                break
            }
        }
        
        if isCCUser == true {
            Helper.showAlert("Can't add gift amount on clink in which you are in cc.", pobjView: self.view)
        }
        else if tmpSelectedClinkDetail.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId {
            self.performActionTap(sender,segueID: SEGUETOADDGIFTVIEW)
        }
        else {
            Helper.showAlert("Unable to add gift amount on your own clink", pobjView: self.view)
        }
    }
    
    
    func performActionTap(sender: UIButton ,segueID : String){
        if arrFeedList.count > sender.tag{
            let indexPath = NSIndexPath(forRow:sender.tag, inSection: 0)
            let cell = tblFeedList.cellForRowAtIndexPath(indexPath) as! ClinkListCell
            objSelectedClinkDetail = arrFeedList[sender.tag]
                        
            if cell.imgClink.image != nil{
                imgSelectedImage = cell.imgClink.image!
                tblFeedList.reloadRowsAtIndexPaths(
                    [indexPath], withRowAnimation: .None)
                intCurrentIndex = sender.tag
                self.performSegueWithIdentifier(segueID, sender: nil)
            }
            else{
                Helper.showAlert(ALERTPLEASEWAITTILLLOADINGIMAGE, pobjView: self.view)
            }
        }
    }
    
    @IBAction func showCommingsoonAlert(sender: AnyObject) {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
    }
    
    @IBAction func btnTitle(sender: UIButton) {
        if arrFeedList.count > INTZERO{
            self.tblFeedList.scrollToRowAtIndexPath(NSIndexPath(forItem: INTZERO, inSection: INTZERO), atScrollPosition: .Top, animated: true)
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOTHREADVIEW{
            let objClinkDetails = segue.destinationViewController as! ClinkThreadController
            if self.objSelectedClinkDetail != nil{
                objClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                if imgSelectedImage != nil{
                    objClinkDetails.imgSelected = imgSelectedImage
                    objClinkDetails.selectedClinkType = ClinkType.Sent
                    objClinkDetails.isFromFeedScreen = true
                    objClinkDetails.shouldAutoLike = shouldAutoLike
                    objClinkDetails.shouldOpenComment = shouldOpenComment
                    if intCurrentIndex != nil{
                        objClinkDetails.intCurrentIndex =  intCurrentIndex
                    }
//                    objClinkDetails.arrEmailCC =  self.objSelectedClinkDetail.arrCCName
                }
            }
        }
        else if segue.identifier == SEGUETOCLINKDETAILS{
            let objClinkDetails = segue.destinationViewController as! ClinkDetailsController
            if self.objSelectedClinkDetail != nil{
                objClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                objClinkDetails.arrClinkDetails = self.arrFeedList
                if imgSelectedImage != nil{
                    objClinkDetails.imgSelected = imgSelectedImage
                    objClinkDetails.selectedClinkType = ClinkType.Sent
                    objClinkDetails.isFromFeedScreen = true
                }
                if intCurrentIndex != nil{
                    objClinkDetails.intCurrentIndex =  intCurrentIndex
                }
            }
        }
        else if segue.identifier == SEGUETOADDGIFTVIEW && segue.destinationViewController.isKindOfClass(SendNewClinkController){
            let objSendNewClink = segue.destinationViewController as! SendNewClinkController
            if self.objSelectedClinkDetail != nil{
                objSendNewClink.objSelectedClinkDetail = objSelectedClinkDetail
                objSendNewClink.controllerType = ClinkControllerType.Gift
            }
        }
    }
    
    // MARK: - TableView Delegate
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let expectedHeight = ((APPDELEGATE.window?.frame.size.height)! - HOMECELLSUBSTRATOR)
        return expectedHeight
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var expectedHeight = ((APPDELEGATE.window?.frame.size.height)! - HOMECELLSUBSTRATOR)
        let objItems = arrFeedList[indexPath.row]
        
        if objItems.strReceiverId == KDEFAULTUSERID || objItems.strReceiverId == KDEFAULTUSERID2 || objItems.strSenderId == KDEFAULTUSERID || objItems.strSenderId == KDEFAULTUSERID2
        {
            if expectedHeight > HOMECELLEXPECTEDHEIGHT{
                expectedHeight = HOMECELLIPHONE6P
            }
            else if expectedHeight != HOMECELLEXPECTEDHEIGHT
            {
                expectedHeight = HOMECELLIPHONE6
            }
        }
        return expectedHeight
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == (arrFeedList.count - 1) && intCurrentPage < intLastPage{
            self.loadMoreClinks()
        }
    }
    
    // MARK: - Custom Methods
    
    func loadMoreClinks(){
        intCurrentPage += 1
        self.getHomeFeedClinks(RECORDSPERPAGE,intCurrentPage:intCurrentPage)
    }
    
    func getHomeFeedClinks(intRecordsPerPage : Int, intCurrentPage : Int){
        
        let currentUser = USERDEFAULTS.boolForKey(kISUSERLOGGEDIN)
        if (currentUser == true) {
            
            let dicParam = [KPERPAGE : "\(intRecordsPerPage)", KPAGE : "\(intCurrentPage)"]
            print("Page to load :- \(dicParam)")
            
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetHomeFeed(dicParam).URLRequest, isBackgroundCall : false,pAlertView: self.view, pViewController : self,  pCompletionBlock: { (success, response) -> () in
                
                if success == true
                {
                    let dicResponse = response as! [String : AnyObject]
                    self.intLastPage = dicResponse.getValueIfAvilable(KLASTPAGE) as? Int
                    if let arrClinks = dicResponse[KCLINK] as? [AnyObject]
                    {
//                        if self.reloadData == false {
//                            self.reloadData = true
//                            self.arrFeedList.removeAll()
//                        }
                        
                        for objClinkDetails in arrClinks {
                            let objClinkDetail = ClsClinkDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                            self.arrFeedList.append(objClinkDetail)
                        }
                        self.lblNotFound.hidden = true
                    }
                    if self.arrFeedList.count <= 0{
                        self.intCurrentPage = 1
                        self.arrFeedList.removeAll()
                        self.lblNotFound.hidden = false
                    }
                    self.configureTableView()
                }
                else{
                    self.intCurrentPage = 1
                    self.arrFeedList.removeAll()
                    self.lblNotFound.hidden = false
                    self.configureTableView()
                }
            })
        }
    }
    
    // MARK: - Show FTU PopUp

    func showFTUPopUp(){
        let ftuView = NSBundle.mainBundle().loadNibNamed("FTUPopUpView", owner: self, options: nil)!.first as? FTUPopUpView
        ftuView?.frame = UIScreen.mainScreen().bounds
        ftuView?.subView.layer.cornerRadius = 8
        ftuView?.showInView(self.view, complitionBlock: { (result) -> () in
            ftuView!.removeFromSuperview()
        })
    }
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    @IBAction func popToFeedScreen(segue:UIStoryboardSegue) {
        reloadData = false
        shouldAutoLike = false
        shouldOpenComment = false
//        if segue.sourceViewController.isKindOfClass(ClinkThreadController) {
//            intCurrentPage = 1
//            intLastPage = 0
//            arrFeedList.removeAll()
//            getHomeFeedClinks()
//        }
        
        if segue.sourceViewController.isKindOfClass(ClinkThreadController) {
            let vc = segue.sourceViewController as! ClinkThreadController
            let objSelectedClinkNew = vc.objSelectedClinkDetail
            let intIndex = vc.intCurrentIndex
            arrFeedList[intIndex] = objSelectedClinkNew
            self.configureTableView()
//            tblFeedList.reloadRowsAtIndexPaths([NSIndexPath.init(forRow: intIndex, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
        }
        
//        let recordsPerPage = intCurrentPage * RECORDSPERPAGE
//        intCurrentPage = 1
//        getHomeFeedClinks(recordsPerPage,intCurrentPage:intCurrentPage)
//        intCurrentPage = recordsPerPage / RECORDSPERPAGE
    }
    
}
