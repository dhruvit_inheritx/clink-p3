//
//  SplashController.swift
//  Clink
//
//  Created by Gunjan on 24/05/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class SplashController: UIViewController {
    
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var imgviewLeadingXposConst : NSLayoutConstraint!
    @IBOutlet var imgviewTrailinhXposConst : NSLayoutConstraint!
    var isAnimationDone = false
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let newWidth = ((APPDELEGATE.window?.frame.size.height)! * 544) / 568
        self.imgView.frame = CGRectMake(0, 0, newWidth, imgView.frame.size.height)
        self.imgviewLeadingXposConst.constant = 0
        self.imgviewTrailinhXposConst.constant = (APPDELEGATE.window?.frame.size.width)! - newWidth
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.imgView.hidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        self.performSelector(#selector(SplashController.performAnimation), withObject: nil, afterDelay: 0.5)
    }
    
    func performAnimation(){
        let xEndPos = (APPDELEGATE.window?.frame.size.width)! - imgView.frame.size.width        
        self.imgView.hidden = false
        UIView.animateWithDuration(SPLASHANIMATIONDURATION, delay: SPLASHDELAY, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.imgviewLeadingXposConst.constant = xEndPos
            self.imgviewTrailinhXposConst.constant = 0
            self.view.layoutIfNeeded()
            self.isAnimationDone = false
            },completion: { (success) -> Void in
                self.isAnimationDone = true
                self.performSelector(#selector(SplashController.hideImageView), withObject: nil, afterDelay: 1)
                self.performSegueWithIdentifier(SEGUELOADCONTROLLERS, sender: nil)
                
        })
    }
    func hideImageView(){
        self.imgView.hidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEGUELOADCONTROLLERS{
           
        }
    }
}
