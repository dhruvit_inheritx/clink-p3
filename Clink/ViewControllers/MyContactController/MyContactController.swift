//
//  MyContactController.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import AddressBookUI
import Contacts
import HDTableDataSource

class MyContactController: UIViewController,UITableViewDelegate,UITextFieldDelegate {
    
    //    MARK:- Gloable variable
    var arrContacts = [ClsContact]()
    var isContactSelecting : Bool!
    var objHDTableDataSource : HDTableDataSource!
    var isToSelected : Bool!
    var objToContact : ClsContact!
    var arrCCContacts : [ClsContact]!
    var arrTmpCCContacts = [ClsContact]()
    var arrMainContacts = [ClsContact]()
    var arrMixAllContacts = [AnyObject]()
    var singleSelectedIndex : NSIndexPath!
    var arrContactIdCheck : [String : AnyObject] = Dictionary()
    
    //    MARK:- IBOutlet
    @IBOutlet var lblNotFound : UILabel!
    @IBOutlet var tblMyContacts : UITableView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnMenu : UIButton!
    @IBOutlet var btnSave : UIButton!
    @IBOutlet var tblContactBottomSpaceConstraint: NSLayoutConstraint!
    @IBOutlet var btnSync : UIButton!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var txtviewsearch : UITextField!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = STRCONTACTS
        
        if isContactSelecting == nil{
            isContactSelecting = false
        }
        else if isContactSelecting == true{
            lblTitle.text = STRADDRESSCLINK
        }
        if isToSelected == nil{
            isToSelected = false
        }
        else if isToSelected == false{
            lblTitle.text = STRCOPYFRIENDS
        }
        btnSave.hidden = !(isContactSelecting)
        btnCancel.hidden = !(isContactSelecting)
        btnMenu.hidden = isContactSelecting
        btnSync.hidden = true
        // Do any additional setup after loading the view.
        if arrCCContacts == nil{
            arrCCContacts = Array()
        }
//        arrEmail = Array()
        arrMixAllContacts = Array()

//        if arrCCContacts.count > 0 {
//            arrTmpCCContacts = arrCCContacts
//        }
//        else if isContactSelecting == false{
//            self.configureContactSource()
//        }
//        else{
            self.configureContactSource()
//        }
        tblMyContacts.delegate = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyContactController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MyContactController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        txtviewsearch.addTarget(self, action: #selector(MyContactController.textFieldTextDidChange(_:)), forControlEvents: UIControlEvents.EditingChanged)
    }
    
    func configureContactSource(){
        
        if APPDELEGATE.isContactSyncedOnce == true && APPDELEGATE.arrMainContacts.count > 0 {
            self.arrContacts = APPDELEGATE.arrMainContacts
            self.arrMainContacts = self.arrContacts
            
            self.performFilteration()
        }
        else{
            let status = ABAddressBookGetAuthorizationStatus()
            if status == .Denied || status == .Restricted || status == .NotDetermined || APPDELEGATE.isContactSyncedOnce == false{
                self.getContactsPermission()
            }
            else{
                self.getPreviousContacts()
            }
        }
    }
    
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Custom Methods
    func getContactsPermission(){
        btnSync.userInteractionEnabled = false
        // make sure user hadn't previously denied access
        
        let status = ABAddressBookGetAuthorizationStatus()
        if status == .Denied || status == .Restricted {
            Helper.showAlert(ALERTCONTACTPERMISIONDNIED, pobjView: self.view)
            // user previously denied, so tell them to fix that in settings
            btnSync.userInteractionEnabled = true
            return
        }
        
        // open it
        
        var error: Unmanaged<CFError>?
        guard let addressBook: ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error)?.takeRetainedValue() else {
            print(error?.takeRetainedValue())
            btnSync.userInteractionEnabled = true
            return
        }
        
        // request permission to use it
        
        ABAddressBookRequestAccessWithCompletion(addressBook) { granted, error in
            if !granted {
                return
            }
            if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == true{
                self.readFromAddressBook(addressBook!)
            }
            else{
                self.btnSync.userInteractionEnabled = true
                Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            }
        }
    }
    
    func readFromAddressBook(addressBook: ABAddressBookRef){
        arrContacts.removeAll()
        Helper.showHud(self.view)
        var newContact: ClsContact?
        let allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        
        for person: ABRecordRef in allPeople{
            
            let firstNameTemp = ABRecordCopyValue(person, kABPersonFirstNameProperty)
            var _firstName: NSObject! = STREMPTY
            if(firstNameTemp != nil){
                _firstName = Unmanaged<NSObject>.fromOpaque(firstNameTemp.toOpaque()).takeRetainedValue()
            }
            
            var _lastName: NSObject! = STREMPTY
            let lastNameTemp = ABRecordCopyValue(person, kABPersonLastNameProperty)
            if(lastNameTemp != nil){
                _lastName = Unmanaged<NSObject>.fromOpaque(lastNameTemp.toOpaque()).takeRetainedValue()
            }
            
            /* Get all the phone numbers this user has */
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones: ABMultiValueRef =
            Unmanaged.fromOpaque(unmanagedPhones.toOpaque()).takeUnretainedValue()
                as NSObject as ABMultiValueRef
            var _phone: String = " "//--
            
            let  numberOfPhoneNumbers : CFIndex = ABMultiValueGetCount(phones)
            for i in 0 ..< numberOfPhoneNumbers {
                if((ABMultiValueCopyLabelAtIndex(phones, i)) != nil){
                    let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, i)
                    _phone = Unmanaged.fromOpaque(
                        unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as! String
                    break
                }
            }
            
            let emailTemp = ABRecordCopyValue(person, kABPersonEmailProperty)
            let emails: ABMultiValueRef = Unmanaged.fromOpaque(emailTemp.toOpaque()).takeRetainedValue() as NSObject as ABMultiValueRef
            
            var _email: String = " "//--
            
            let  numberOfEmails : CFIndex = ABMultiValueGetCount(emails)
            for j in 0 ..< numberOfEmails {
                if((ABMultiValueCopyValueAtIndex(emails, j)) != nil) {
                    let unmanagedEmail = ABMultiValueCopyValueAtIndex(emails, j)
                    _email = Unmanaged.fromOpaque(unmanagedEmail.toOpaque()).takeRetainedValue() as NSObject as! String
                    _email = _email.lowercaseString
                    break
                }
            }
            let strFName = _firstName as! String
            let strLName = _lastName as! String
            if ((!_email.isEmptyString() || !(_phone.isEmptyString())) )
            {
                if (!strFName.isEmptyString() || !strLName.isEmptyString())
                {
//                    let isAlreadySlected = self.validateSelection(_email) 
                    let isAlreadySlected = false
                    newContact = ClsContact(pClinkId:STREMPTY, pfirstName:strFName , plastName: strLName, pemail: _email.lowercaseString as String, pphone: _phone,pisClinkUser : false, pisSelected:isAlreadySlected, puserid: NOTDEFINED,isUnRemovable: false)
                    arrContacts.append(newContact!)
                    
                }
            }
        }
        self.getClinkUsers()
    }
    
    func validateSelection(strId : String) -> Bool{
        
        if(self.isToSelected != nil && self.isToSelected == true && (objToContact != nil)){
            if objToContact.strUserid == strId {
                return true
            }
            else if objToContact.strId == strId {
                return true
            }
        }
        else{
            for objCCContact in arrCCContacts{
                if objCCContact.strUserid == strId {
                    return true
                }
                else if objCCContact.strId == strId {
                    return true
                }
            }
            
        }
        return false
    }
    
    func validateSelectionEmail(strEmail : String? , strPhone : String?) -> Bool{
        
        if strEmail != nil && strEmail!.isEmptyString() == false{
            if(self.isToSelected != nil && self.isToSelected == true && (objToContact != nil)){
                if objToContact.strEmail == strEmail {
                    return true
                }
                else if objToContact.strEmail == strEmail {
                    return true
                }
            }
            else{
                for objCCContact in arrCCContacts{
                    if objCCContact.strEmail == strEmail {
                        return true
                    }
                    else if objCCContact.strEmail == strEmail {
                        return true
                    }
                }
                
            }

        }
        else if strPhone != nil && strPhone!.isEmptyString() == false{
            if(self.isToSelected != nil && self.isToSelected == true && (objToContact != nil)){
                if objToContact.strPhone == strPhone {
                    return true
                }
                else if objToContact.strPhone == strPhone {
                    return true
                }
            }
            else{
                for objCCContact in arrCCContacts{
                    if objCCContact.strPhone == strPhone {
                        return true
                    }
                    else if objCCContact.strPhone == strPhone {
                        return true
                    }
                }
                
            }
        }
        return false
    }

    
    func getClinkUsers(){
        
        let currentUser = USERDEFAULTS.boolForKey(kISUSERLOGGEDIN)
        self.arrContacts.sortInPlace({ (obj1, obj2) -> Bool in
            obj1.strFirstName.compare(obj2.strFirstName) == .OrderedAscending
        })
        
        var arrTmpContact : [AnyObject] = Array()
        if arrContacts.count > 0 {
            
            for objContact in arrContacts{
                var dicContact : [String : AnyObject] = ["email":objContact.strEmail,"firstname":objContact.strFirstName,"lastname":objContact.strLastName]
                if objContact.strPhone != nil && objContact.strPhone.removeSpecialChars("").isEmptyString() == false{
                    dicContact["phone"] = Int(objContact.strPhone.removeSpecialChars(""))
                }
                arrTmpContact.append(dicContact)
            }
        }
        
        if (currentUser == true && arrTmpContact.count > INTZERO) {
            
            
            let dicContactArr : [String : AnyObject] = [ KCONTACT : arrTmpContact]
            let dicQueryParam = [KPERPAGE : "\(arrTmpContact.count)", KPAGE : "1"]
            
            Helper.hideHud(self.view)
            self.deleteContacts({ (success, response) -> () in
                if success == true{
                    self.getMyContacts(dicContactArr,dicQueryParam: dicQueryParam)
                }
                else if response != nil {
                    self.btnSync.userInteractionEnabled = true
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            })
        }
        else{
            btnSync.userInteractionEnabled = true
        }
        
    }
    func getMyContacts(dicContactArr : [String : AnyObject], dicQueryParam : [String : AnyObject]){
        
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.CreateContactList(dicContactArr,dicQueryParam).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
            if success == true{
                self.parseResponse(response as! [String : AnyObject])
            }
            else{
                self.btnSync.userInteractionEnabled = true
                self.lblNotFound.hidden = false
                self.arrMixAllContacts.removeAll()
                self.configureTableView()
            }
        })
    }
    func deleteContacts(pCompletionBlock:ClinkCompletionBlock){
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.DeleteContacts().URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                pCompletionBlock(success,response)
        })
    }
    
    func getPreviousContacts(){
        let dicQueryParam = [KPERPAGE : "\(KINTCONTACTMAX)", KPAGE : "1"]
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetContactList(dicQueryParam).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
            if success == true{
                self.parseResponse(response as! [String : AnyObject])
            }
            else{
                self.lblNotFound.hidden = false
                self.arrMixAllContacts.removeAll()
                self.configureTableView()
            }
        })
    }
    
    func parseResponse(response: [String : AnyObject]){
        arrContacts.removeAll()
        arrMainContacts.removeAll()
        arrMixAllContacts.removeAll()
        if let arrAllContact = response["contact"] as? [AnyObject]
        {
            if arrAllContact.count > 0 {
                for dicContactDetails in arrAllContact{
                    let objContact = ClsContact(pdicResponse: dicContactDetails as! [String : AnyObject])
                    for objCCUser in arrCCContacts{
                        if objCCUser.strId == objContact.strId{
                            objContact.isSelected = true
                            objContact.isUnRemovable = objCCUser.isUnRemovable
                            
                        }
                    }
                    arrContacts.append(objContact)
                }
                APPDELEGATE.isContactSyncedOnce = true
            }
        }
        arrMainContacts = arrContacts
        APPDELEGATE.arrMainContacts = self.arrMainContacts
        USERDEFAULTS.setContactArrayObject(self.arrMainContacts)
        self.performFilteration()
    }
    
    func performFilteration(){
        
        for objCCUser in arrCCContacts{
            if objCCUser.isUnRemovable == true{
                for dicContactDetails in arrContacts{
                    if dicContactDetails.strId == objCCUser.strId || dicContactDetails.strUserid == objCCUser.strId{
                        dicContactDetails.isUnRemovable = objCCUser.isUnRemovable
                    }
                    if objCCUser.strEmail != nil && objCCUser.strEmail.isEmptyString() == false && objCCUser.strEmail == dicContactDetails.strEmail{
                        dicContactDetails.isUnRemovable = objCCUser.isUnRemovable
                    }
                }
            }
        }
        var arrFilteredContactClink = arrContacts.filter {
            $0.isClinkUser.boolValue == true
        }
        
        arrFilteredContactClink = arrFilteredContactClink.filter {
            $0.isUnRemovable.boolValue == false
        }
        
        if objToContact != nil && isToSelected == false && objToContact != nil{
            
            print(objToContact.strEmail)
            
            arrFilteredContactClink = arrFilteredContactClink.filter {
                $0.strEmail != objToContact.strEmail
            }
        }
        
        arrFilteredContactClink = arrFilteredContactClink.filter {
            $0.strUserid != USERDEFAULTS.getUserDetailsObject().strId
        }
        
        arrFilteredContactClink = self.removeDuplicateEmails(arrFilteredContactClink)
        arrFilteredContactClink.sortInPlace({ (obj1, obj2) -> Bool in
            obj1.strFirstName.lowercaseString.compare(obj2.strFirstName.lowercaseString) == .OrderedAscending
        })
        
        arrMixAllContacts.removeAll()
        
        for objContactDetails in arrFilteredContactClink{
            if objContactDetails.strUserid.isEmptyString() == true{
//                objContactDetails.isSelected = self.validateSelection((objContactDetails.strId)!)
                objContactDetails.isSelected = self.validateSelectionEmail(objContactDetails.strEmail, strPhone: objContactDetails.strPhone)
            }
            else{
//                objContactDetails.isSelected = self.validateSelection((objContactDetails.strUserid)!)
                objContactDetails.isSelected = self.validateSelectionEmail(objContactDetails.strEmail, strPhone: objContactDetails.strPhone)
            }
        }
        
        arrMixAllContacts.append(arrFilteredContactClink)
        
        var arrFilteredContactOther = arrContacts.filter {
            $0.isClinkUser.boolValue == false
        }
        
        arrFilteredContactOther = arrFilteredContactOther.filter {
            $0.isUnRemovable.boolValue == false
        }
        
        arrFilteredContactOther.sortInPlace({ (obj1, obj2) -> Bool in
            obj1.strFirstName.lowercaseString.compare(obj2.strFirstName.lowercaseString) == .OrderedAscending
        })
        
        if objToContact != nil && isToSelected == false && objToContact != nil{
            arrFilteredContactOther = arrFilteredContactOther.filter {
                $0.strId != objToContact.strId
            }
        }
        
        for objContactDetails in arrFilteredContactOther{
            if objContactDetails.strUserid.isEmptyString() == true{
//                objContactDetails.isSelected = self.validateSelection((objContactDetails.strId)!)
                objContactDetails.isSelected = self.validateSelectionEmail(objContactDetails.strEmail, strPhone: objContactDetails.strPhone)
            }
            else{
                objContactDetails.isSelected = self.validateSelectionEmail(objContactDetails.strEmail, strPhone: objContactDetails.strPhone)
            }
        }

        self.setCCArray(arrFilteredContactClink)
        self.setCCArray(arrFilteredContactOther)
        
        arrMixAllContacts.append(arrFilteredContactOther)
        self.configureTableView()
        btnSync.userInteractionEnabled = true
        
    }
    
    func removeDuplicateEmails(arrWithDuplicates : [ClsContact]) -> [ClsContact]{
        
        var seen = Set<String>()
        var unique = [ClsContact]()
        for objContact in arrWithDuplicates {
            if !seen.contains(objContact.strEmail!) {
                unique.append(objContact)
                if objContact.strEmail!.isEmptyString() == false{
                seen.insert(objContact.strEmail!)
                }
            }
        }
        return unique
    }
    
    func configureTableView(){
        
        objHDTableDataSource = HDTableDataSource(items: arrMixAllContacts as [AnyObject], cellIdentifier: CELLCONTACTCC, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! ContactCell
            let objItems = item as! ClsContact
            
            objCell.configureCell(objItems)
            if indexPath.section == 0{
                objCell.imgProfile.backgroundColor = kThemeColor
            }
            else if indexPath.section == 1{
                objCell.imgProfile.backgroundColor = UIColor.grayColor()
            }
            if self.isToSelected == true{
                if indexPath.section == 1{
                    if self.objToContact != nil && objItems.strId == self.objToContact.strId {
                        objCell.btnSelection.selected = true
                        self.singleSelectedIndex = indexPath
                    }
                    else{
                        objCell.btnSelection.selected = false
                    }
                }
                else if self.objToContact != nil && objItems.strUserid.isEmptyString() == false && objItems.strUserid == self.objToContact.strUserid{
                    objCell.btnSelection.selected = true
                    self.singleSelectedIndex = indexPath
                }
                else{
                   objCell.btnSelection.selected = false
                }
            }
            else{
                objCell.btnSelection.selected = objItems.isSelected
            }
            objCell.btnSelection.hidden = !(self.isContactSelecting)
        })
        
        objHDTableDataSource.sectionItemBlock = {(objSection:AnyObject!) -> [AnyObject]! in
            let arrMContact = objSection as! [ClsContact]
            return arrMContact
        }
        
        objHDTableDataSource.arrSections = arrMixAllContacts
        tblMyContacts.dataSource = objHDTableDataSource
        tblMyContacts.delegate = self
        tblMyContacts.reloadData()
    }
    
    // MARK: - tableview delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        self.dismissKeyboard()
        
        
        if self.isContactSelecting == true{
            
            let arrContactTmp = self.arrMixAllContacts[indexPath.section] as! [ClsContact]
            let objContact = arrContactTmp[indexPath.row]

                objContact.isSelected = !objContact.isSelected
                if self.isToSelected == true{
                    if self.singleSelectedIndex != nil{
                        let objPreviousContact : ClsContact
                        if singleSelectedIndex.section == indexPath.section{
                            objPreviousContact = arrContactTmp[singleSelectedIndex.row]
                        }
                        else{
                            let arrPreviousContactTmp = self.arrMixAllContacts[singleSelectedIndex.section] as! [ClsContact]
                            objPreviousContact = arrPreviousContactTmp[singleSelectedIndex.row]
                        }
                        objPreviousContact.isSelected = !objPreviousContact.isSelected
                    }
                    self.objToContact = objContact
                    self.singleSelectedIndex = indexPath
                }
                else{
                    self.arrTmpCCContacts.removeAll()
                    self.setCCArray(self.arrMixAllContacts[0] as! [ClsContact])
                    self.setCCArray(self.arrMixAllContacts[1] as! [ClsContact])
                }
                
                tableView.reloadData()
        }
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let viewT = UIView(frame: CGRect(x: CGFLOATZERO, y: CGFLOATZERO, width: APPDELEGATE.window!.frame.size.width, height: CONTACTLISTCELLHEIGHT))
        viewT.backgroundColor = UIColor.whiteColor()
        
        let lblT = UILabel(frame: CGRect(x: CONTACTLISTLABELXPOS, y: CONTACTLISTLABELYPOS, width:(viewT.frame.size.width - CONTACTLISTCELLWIDTHSUBS), height: CONTACTLISTLABELHEIGHT))
        
        if section == 0 {
            let btnSync = UIButton(frame: CGRect(x: APPDELEGATE.window!.frame.size.width - 60, y: CGFLOATZERO + 5 , width: 30, height: 30))
            btnSync.setImage(UIImage(named: "icn_sync")!.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
            
            btnSync.imageView?.tintColor = COLORTHEME
            btnSync.tintColor = COLORTHEME
            btnSync.addTarget(self, action: #selector(MyContactController.btnSyncTapped(_:)), forControlEvents: .TouchUpInside)
            viewT.addSubview(btnSync)
        }
        
        let lineView = UIView(frame: CGRect(x: CONTACTLISTLABELXPOS, y: viewT.frame.size.height - 10, width:(viewT.frame.size.width - CONTACTLISTCELLWIDTHSUBS), height: 1))
        lineView.backgroundColor = kThemeMainLabelColor
        lblT.textColor = kThemeMainLabelColor
        viewT.addSubview(lblT)
        viewT.addSubview(lineView)
        if section == INTZERO{
            lblT.text = STRCLINKUSERSECTIONTITLE
        }
        else{
             lblT.text = STRCONTACTSECTIONTITLE
        }
        lblT.font = UIFont(name: FONTGOTHAMMEDIUM, size: CGFloat(FONTSIZE18))
        return viewT
    }
   
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CONTACTLISTCELLHEIGHT   // Defualt header size
    }
    
    // MARK: - Button Actions
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        self.dismissKeyboard()
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    @IBAction func btnSyncTapped(sender: UIButton) {
        sender.userInteractionEnabled = false
        self.dismissKeyboard()
        self.arrMainContacts.removeAll()
        self.arrMixAllContacts.removeAll()
        self.arrMainContacts.removeAll()
        self.getContactsPermission()
    }
    
    @IBAction func btnCancelTapped(sender: UIButton) {
        self.dismissKeyboard()
        self.arrTmpCCContacts.removeAll()
        
        if self.arrMixAllContacts.count > 0 {
            let arrToReset = (self.arrMixAllContacts[1] as! [ClsContact])
            for objContact in arrToReset{
                objContact.isSelected = false
            }
        }
        
        self.performSegueWithIdentifier(SEGUETOSENDCLINKCANCEL, sender: nil)
    }
    
    @IBAction func btnSaveTapped(sender: UIButton) {
        self.dismissKeyboard()
        if isToSelected == false{
            self.arrCCContacts = self.arrTmpCCContacts
            
            print(self.arrCCContacts)
        }
        self.performSegueWithIdentifier(SEGUETOSAVESENDNEWCLINK, sender: nil)
    }
    
    // MARK: - Other Functions
    func setCCArray(arrContactTmp :[ClsContact]){
        let arrSelection = arrContactTmp.filter {
            $0.isSelected.boolValue == true
        }
        for objToAppend in arrSelection{
            self.arrTmpCCContacts.append(objToAppend)
        }
        
        
    }
    
    // MARK: - UITextField delegate
    func textFieldTextDidChange(textField: UITextField) -> Bool {
        if textField.text!.isEmptyString() == false{
            
            let arrFilteredarr = arrMainContacts.filter {
                $0.strFullName.lowercaseString.containsString(textField.text!.lowercaseString) == true
            }
            arrContacts = arrFilteredarr
            self.performFilteration()
        }
        else{
            arrContacts = arrMainContacts
            self.performFilteration()
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
         self.dismissKeyboard()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.dismissKeyboard()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        
        UIView.animateWithDuration(ANIMATIONDURATION, animations: { () -> Void in
            self.tblContactBottomSpaceConstraint.constant =  keyboardRectangle.size.height
            self.view.layoutIfNeeded()
        })
    }
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animateWithDuration(ANIMATIONDURATION, animations: { () -> Void in
            self.tblContactBottomSpaceConstraint.constant = CGFLOATZERO
            self.view.layoutIfNeeded()
            })
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        self.view.endEditing(true)
    }
}
