//
//  SelectPaymentViewController.swift
//  Clink
//
//  Created by Dhruvit on 11/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

// MARK: - Delegate Methods

protocol SelectPaymentViewControllerDelegate {
    func setSelectedPaymentMethod(objPaymentMethod : ClsCreditCardDetails!,orClinkAccount strClinkAccountId : String)
}

class SelectPaymentViewController: UIViewController,UITableViewDelegate {
    
    var delegate : SelectPaymentViewControllerDelegate?
    
    @IBOutlet weak var tblViewSelectPayment : UITableView!
    
    @IBOutlet weak var btnNext : UIButton!
    
    var objHDTableDataSource : HDTableDataSource!
    var arrayPaymentDetail : [AnyObject] = Array()
    var objSelectedPaymentDetail : ClsCreditCardDetails!
    var previousIndex : NSIndexPath!
    var strClinkBalance : String! = "0"
    var strSegueID : String! = ""
    
    var strClinkAccountId : String = ""
    
    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let objUserDetails : ClsUserDetails! = USERDEFAULTS.getUserDetailsObject()
        strClinkBalance = objUserDetails.strBalance
        
        self.arrayPaymentDetail = [[KTITLE:"Add Debit/Credit Card",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Bank Account",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add PayPal",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Use Clink! Balance",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Other Payment Method",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        tblViewSelectPayment.separatorStyle = .None
        btnNext.enabled = false
        btnNext.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        //        self.getAllCreditCard()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.reloadData()
        NOTIFICATIONCENTER.addObserver(self, selector: #selector(SelectPaymentViewController.reloadData), name: RELOADPAYMENTMETHODS, object: nil)
    }
    
    func reloadData() {
        btnNext.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        btnNext.enabled = false
        self.previousIndex = nil
        tblViewSelectPayment.reloadData()
        
        self.arrayPaymentDetail.removeAll()
        self.arrayPaymentDetail = [[KTITLE:"Add Debit/Credit Card",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Bank Account",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add PayPal",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Use Clink! Balance",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Other Payment Method",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
        
        self.getAllCreditCard()
    }
    
    // MARK: - Get All Bank Accounts

    func getAllBank(){
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchBank(["search":""]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        arrayCreditCardDetails.append(objCreditCardDetail)
                    }
                }
                if arrayCreditCardDetails.count > 0{
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        print("Last4 :- \(objCreditCardDetail.strLastFour)")
                        self.arrayPaymentDetail.insert([KTITLE:"Account *",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                    }
                }
                self.configureTableView()
            }
            else{
                self.arrayPaymentDetail = [[KTITLE:"Add Debit/Credit Card",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Bank Account",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Use Clink Balance",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add PayPal Account",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()],[KTITLE:"Add Other Payment Method",KPAYMENTMETHODDETAILS:ClsCreditCardDetails()]]
                self.configureTableView()
            }
        }
    }
    
    // MARK: - Get All Credit Cards

    func getAllCreditCard(){
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.SearchCard(["search":""]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true{
                
                let dicResponse = response as! [String : AnyObject]
                var arrayCreditCardDetails : [AnyObject] = Array()
                if let arrCreditCards = dicResponse[KACCOUNT] as? [AnyObject]
                {
                    for objClinkDetails in arrCreditCards {
                        let objCreditCardDetail = ClsCreditCardDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        arrayCreditCardDetails.append(objCreditCardDetail)
                    }
                }
                if arrayCreditCardDetails.count > 0{
                    for objCreditCardDetail in arrayCreditCardDetails as! [ClsCreditCardDetails]{
                        print("Last4 :- \(objCreditCardDetail.strLastFour)")
                        self.arrayPaymentDetail.insert([KTITLE:"VISA CARD *",KPAYMENTMETHODDETAILS:objCreditCardDetail], atIndex: 0)
                    }
                }
                self.getAllBank()
                self.configureTableView()
            }
            else{
                self.getAllBank()
            }
        }
    }
    
    // MARK: - Table View Configuration

    func configureTableView(){
        
        objHDTableDataSource = HDTableDataSource(items: arrayPaymentDetail as [AnyObject], cellIdentifier: CELLPAYMENTMETHOD, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! PaymentMethodCell
            let objItems = item as! [String : AnyObject]
            
            objCell.configureCell(objItems)
            objCell.btnAdd.tag  = indexPath.row
            objCell.btnAdd.backgroundColor = UIColor.clearColor()
            objCell.btnAdd.setTitleColor(COLORTHEME, forState: .Normal)

            if self.previousIndex != nil && indexPath == self.previousIndex{
                objCell.btnAdd.backgroundColor = COLORTHEME
                objCell.btnAdd.setTitleColor(UIColor.whiteColor(), forState: .Normal)
            }
            else{
                objCell.btnAdd.backgroundColor = UIColor.clearColor()
                objCell.btnAdd.setTitleColor(COLORTHEME, forState: .Normal)
            }
        })
        
        tblViewSelectPayment.dataSource = objHDTableDataSource
        tblViewSelectPayment.delegate = self
        tblViewSelectPayment.reloadData()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if cell.respondsToSelector(Selector("setSeparatorInset:")){
            cell.separatorInset = UIEdgeInsetsZero
        }
        if cell.respondsToSelector(Selector("setPreservesSuperviewLayoutMargins:")) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.respondsToSelector(Selector("setLayoutMargins:")){
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    // MARK: - Add Payment Method
    
    @IBAction func btnAddPaymentMethod(sender: UIButton) {
        
        switch sender.tag{
        case arrayPaymentDetail.count - 1:
            self.performActionTap(sender,segueID: "segueAddPaymentMethod")
            return
        case arrayPaymentDetail.count - 2:
            self.performActionTap(sender,segueID: "segueAddPayPal")
            return
        case arrayPaymentDetail.count - 3:
            self.strClinkAccountId = USERDEFAULTS.getUserDetailsObject().strClinkAccountId
            self.performActionTap(sender,segueID: "")
            //            self.performActionTap(sender,segueID: "segueClinkBalance")
            return
        case arrayPaymentDetail.count - 4:
            self.performActionTap(sender,segueID: "segueAddBank")
            return
        case arrayPaymentDetail.count - 5:
            self.performActionTap(sender,segueID: "segueAddCreditCard")
            return
        default:
            self.objSelectedPaymentDetail = arrayPaymentDetail[sender.tag][KPAYMENTMETHODDETAILS] as! ClsCreditCardDetails
            self.performActionTap(sender,segueID: "")
            return
        }
    }
    
    func performActionTap(sender: UIButton ,segueID : String){
        
        let indexPath = NSIndexPath(forRow:sender.tag, inSection: 0)
        previousIndex = indexPath
        
        
        if let objItem : ClsCreditCardDetails = arrayPaymentDetail[sender.tag][KPAYMENTMETHODDETAILS] as? ClsCreditCardDetails {
            objSelectedPaymentDetail = objItem
            print(objSelectedPaymentDetail.strAccountType)
        }
        
        tblViewSelectPayment.reloadData()
        self.strSegueID = segueID
        
        btnNext.setTitleColor(COLORTHEME, forState: .Normal)
        btnNext.enabled = true
        
        self.performNavigation()
    }
    
    func performNavigation() {
        
        if self.objSelectedPaymentDetail.strId != "" {
            
//            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank(self.objSelectedPaymentDetail.strId,["is_verified":"0"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
//                
//                if success == true {
//                    print("Success")
//                }
//                else {
//                    Helper.showAlert(response as! String, pobjView: self.view)
//                }
//            })
            
            if objSelectedPaymentDetail.isVarified == true {
                let objViewController = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERDEFAULTPAYMENTPOPUP)
                let subView = objViewController.view as! DefaultPaymentMethodPopUp
                subView.subView.layer.cornerRadius = PRIVATEPOPUPCORNERRADIUS
                
                
                subView.switchPayment.on = objSelectedPaymentDetail.isDefaultPayment
                subView.switchCashOut.on = objSelectedPaymentDetail.isDefaultTransfer
                
                if objSelectedPaymentDetail.strAccountType == "bank" {
                    subView.switchPayment.enabled = true
                    subView.switchPayment.on = false
                    subView.lblPayment.enabled = true
                }
                else if objSelectedPaymentDetail.strAccountType == "credit" {
                    subView.switchCashOut.enabled = false
                    subView.switchCashOut.on = false
                    subView.lblCashOut.enabled = false
                }
                else if objSelectedPaymentDetail.strAccountType == "debit" {
                    subView.switchCashOut.enabled = true
                    subView.switchCashOut.on = false
                    subView.lblCashOut.enabled = true
                }
                
                subView.showInView(self.view, complitionBlock: { (isDefaultPaymentMethod, isDefaultCashOutMethod, result) -> () in
                    
                    print("isDefaultPaymentMethod :- \(isDefaultPaymentMethod)")
                    print("isDefaultCashOutMethod :- \(isDefaultCashOutMethod)")
                    
                    if self.objSelectedPaymentDetail.isDefaultPayment != isDefaultPaymentMethod || self.objSelectedPaymentDetail.isDefaultTransfer != isDefaultCashOutMethod {
                        
                        self.updatePaymentMethod(isDefaultPaymentMethod, isDefaultCashOutMethod: isDefaultCashOutMethod,strAccountType:self.objSelectedPaymentDetail.strAccountType)
                        subView.removeFromSuperview()
                    }
                    else {
                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        subView.removeFromSuperview()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                })
            }
            else {
                Helper.showAlert("The selected payment method requires verification. Please clink verify to complete the verification process.", cancelButtonTitle: "Close", otherButtonTitle: "Verify", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                    
                    if buttonIndex == 0 {
                        
                    }
                    else {
//                        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank(self.objSelectedPaymentDetail.strId,["is_verified":"1"]).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) in
//                            
//                            if success == true {
//                                print("Success")
//                            }
//                            else {
//                                Helper.showAlert(response as! String, pobjView: self.view)
//                            }
//                        })
                        
                        if self.objSelectedPaymentDetail.strBankVarifyUrl != nil && self.objSelectedPaymentDetail.strBankVarifyUrl != "" {
                            
                            Helper.showAlert("You will be redirected to web to verify selected payment method.", pobjView: self.view , completionBock: { (sender , buttonIndex) -> () in
                                UIApplication.sharedApplication().openURL(NSURL(string: self.objSelectedPaymentDetail.strBankVarifyUrl)!)
                                print(self.objSelectedPaymentDetail.strBankVarifyUrl)
                            })
                        }
                        else{
                            Helper.showAlert("Unable to proceed request. Verification url not found.", pobjView: self.view)
                        }
                    }
                })
            }
        }
        else if self.objSelectedPaymentDetail.strId == "" && self.strClinkAccountId != "" {
            delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else if strSegueID == "segueAddPayPal" {
            Helper.showCommingsoonAlert()
        }
        else {
            self.performSegueWithIdentifier(strSegueID, sender: nil)
        }
    }
        
    // MARK: - Back Clicked
    
    @IBAction func back_click(sender: UIButton)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Next Clicked

    @IBAction func next_click(sender: UIButton) {
        
        if self.objSelectedPaymentDetail.strId != "" {
            
            if objSelectedPaymentDetail.isVarified == true {
                let objViewController = UIStoryboard(name: ACCOUNTSTORYBOARD, bundle: nil).instantiateViewControllerWithIdentifier(CONTROLLERDEFAULTPAYMENTPOPUP)
                let subView = objViewController.view as! DefaultPaymentMethodPopUp
                subView.subView.layer.cornerRadius = PRIVATEPOPUPCORNERRADIUS
                
                
                subView.switchPayment.on = objSelectedPaymentDetail.isDefaultPayment
                subView.switchCashOut.on = objSelectedPaymentDetail.isDefaultTransfer
                
                if objSelectedPaymentDetail.strAccountType == "bank" {
                    subView.switchPayment.enabled = true
                    subView.switchPayment.on = false
                    subView.lblPayment.enabled = true
                }
                else if objSelectedPaymentDetail.strAccountType == "credit" {
                    subView.switchCashOut.enabled = false
                    subView.switchCashOut.on = false
                    subView.lblCashOut.enabled = false
                }
                else if objSelectedPaymentDetail.strAccountType == "debit" {
                    subView.switchCashOut.enabled = true
                    subView.switchCashOut.on = false
                    subView.lblCashOut.enabled = true
                }
                
                subView.showInView(self.view, complitionBlock: { (isDefaultPaymentMethod, isDefaultCashOutMethod, result) -> () in
                    
                    print("isDefaultPaymentMethod :- \(isDefaultPaymentMethod)")
                    print("isDefaultCashOutMethod :- \(isDefaultCashOutMethod)")
                    
                    if self.objSelectedPaymentDetail.isDefaultPayment != isDefaultPaymentMethod || self.objSelectedPaymentDetail.isDefaultTransfer != isDefaultCashOutMethod {
                        
                        self.updatePaymentMethod(isDefaultPaymentMethod, isDefaultCashOutMethod: isDefaultCashOutMethod,strAccountType:self.objSelectedPaymentDetail.strAccountType)
                        subView.removeFromSuperview()
                    }
                    else {
                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        subView.removeFromSuperview()
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                })
            }
            else {
                Helper.showAlert("The selected payment method requires verification. Please clink verify to complete the verification process.", cancelButtonTitle: "Close", otherButtonTitle: "Verify", pobjView: self.view, completionBock: { (customAlert, buttonIndex) in
                    
                    if buttonIndex == 0 {
                        
                    }
                    else {
                        if self.objSelectedPaymentDetail.strBankVarifyUrl != nil && self.objSelectedPaymentDetail.strBankVarifyUrl != "" {
                            
                            Helper.showAlert("You will be redirected to web to verify selected payment method.", pobjView: self.view , completionBock: { (sender , buttonIndex) -> () in
                                UIApplication.sharedApplication().openURL(NSURL(string: self.objSelectedPaymentDetail.strBankVarifyUrl)!)
                                print(self.objSelectedPaymentDetail.strBankVarifyUrl)
                            })
                        }
                        else{
                            Helper.showAlert("Unable to proceed request. Verification url not found.", pobjView: self.view)
                        }
                    }
                })
            }
        }
        else if self.objSelectedPaymentDetail.strId == "" && self.strClinkAccountId != "" {
            delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else if strSegueID == "segueAddPayPal" {
            Helper.showCommingsoonAlert()
        }
        else {
            self.performSegueWithIdentifier(strSegueID, sender: nil)
        }
    }
    
    // MARK: - Update Payment Method
    
    func updatePaymentMethod(isDefaultPaymentMethod:Bool, isDefaultCashOutMethod:Bool, strAccountType:String){
        //        delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
        //        self.dismissViewControllerAnimated(true, completion: nil)
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        let dictUpdatedCardDetail = [KISDEFAULTPAYMENT:"\(Int(isDefaultPaymentMethod))",KISDEFAULTTRANSFER:"\(Int(isDefaultCashOutMethod))"]
        
        if strAccountType == "bank" {
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateBank(objSelectedPaymentDetail.strId, dictUpdatedCardDetail).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Bank Account Details Updated Successfully.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        if self.objSelectedPaymentDetail.isDefaultPayment == true {
                            USERDEFAULTS.setDefaultPaymentDetailsObject(self.objSelectedPaymentDetail)
                        }
                        
                        if self.objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultTransferDetailsObject(self.objSelectedPaymentDetail, forKey: KTRANSFERDETAILSBANK)
                        }
                        
                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        //                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        //                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
            }
        }
        else{
            ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.UpdateCard(objSelectedPaymentDetail.strId, dictUpdatedCardDetail).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
                
                if success == true {
                    Helper.showAlert("Card Details Updated Successfully.", pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        self.objSelectedPaymentDetail = ClsCreditCardDetails(pdictResponse: response as! [String : AnyObject])
                        
                        if self.objSelectedPaymentDetail.isDefaultPayment == true {
                            USERDEFAULTS.setDefaultPaymentDetailsObject(self.objSelectedPaymentDetail)
                        }
                        
                        if self.objSelectedPaymentDetail.isDefaultTransfer == true {
                            USERDEFAULTS.setDefaultTransferDetailsObject(self.objSelectedPaymentDetail, forKey: KTRANSFERDETAILSDEBITCARD)
                        }
                        
                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
                else {
                    Helper.showAlert(response as! String, pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                        
                        //                        self.delegate?.setSelectedPaymentMethod(self.objSelectedPaymentDetail, orClinkAccount: self.strClinkAccountId)
                        //                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                }
            }
        }
    }
    
    // MARK: - Memory Warnings
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
}
