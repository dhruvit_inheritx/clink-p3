//
//  CashOutViewController.swift
//  Clink
//
//  Created by Dhruvit on 16/09/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class CashOutViewController: UIViewController {

    @IBOutlet weak var balanceView: UIView!
    @IBOutlet var keypadView: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    
    var strAmount = ""
    
    var objUserDetail : ClsUserDetails!
    var obkTransferDetail : ClsCreditCardDetails = ClsCreditCardDetails()
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        
        objUserDetail = USERDEFAULTS.getUserDetailsObject()
        lblAmount.text = objUserDetail.strBalance//.removeZerosAfterDecimal()
        lblAmount.text = lblAmount.text?.stringByReplacingOccurrencesOfString(",", withString: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    @IBAction func actionInputButton(sender:UIButton){
        
        var inputVal = STREMPTY
        
        switch sender.tag{
            
        case 0 :
            inputVal = STR0
        case 1 :
            inputVal = STR1
        case 2 :
            inputVal = STR2
        case 3 :
            inputVal = STR3
        case 4 :
            inputVal = STR4
        case 5 :
            inputVal = STR5
        case 6 :
            inputVal = STR6
        case 7 :
            inputVal = STR7
        case 8 :
            inputVal = STR8
        case 9 :
            inputVal = STR9
        case 10 :
            inputVal = STRPOINT
        default:
            inputVal = STREMPTY
        }
        
        let strAmount = lblAmount.text
        
//        var strUserBalance = objUserDetail.strBalance
//        strUserBalance = strUserBalance.stringByReplacingOccurrencesOfString(",", withString: "")
//        
//        let floatAmount = Float(lblAmount.text! + inputVal)
//        let maxAmount = Float(strUserBalance)
        
        let arrStr : [String] = (strAmount?.componentsSeparatedByString("."))!
        
        
        var strDecimal  = ""
        let maxDigit : Int = 8
        
//        if floatAmount > maxAmount{
//            return
//        }
        
        if arrStr.count == 2 {
            strDecimal = arrStr.last!
        }
        else if strAmount?.length == maxDigit - 3 {
            lblAmount.text = strAmount! + "."
        }
        
        if strDecimal.characters.count == 2 {
            return
        }
        
        if strAmount!.length < maxDigit
        {
            if lblAmount.text == STR0{
                
                if inputVal == STRPOINT {
                    lblAmount.text! += inputVal
                }
                else {
                    lblAmount.text! = inputVal
                }
            }
            else
            {
                if lblAmount.text!.containsString(".") == true {
                    
                    if inputVal != STRPOINT {
                        lblAmount.text! += inputVal
                    }
                }
                else {
                    lblAmount.text! += inputVal
                }
            }
        }
    }
    
    @IBAction func actionRemove(sender:UIButton){
        
        if ((lblAmount.text?.isEmptyString()) == false)
        {
            let strAmount = String(lblAmount.text!.characters.dropLast()) as String
            lblAmount.text = strAmount
            
            if ((lblAmount.text?.isEmptyString()) == true){
                lblAmount.text = STR0
            }
        }
    }
    
    @IBAction func backClicked(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func sendClicked(sender: AnyObject) {
        
        var strUserBalance = objUserDetail.strBalance//.removeZerosAfterDecimal()
        strUserBalance = strUserBalance.stringByReplacingOccurrencesOfString(",", withString: "")
        var strCashOutBalance = lblAmount.text!
        strCashOutBalance = strCashOutBalance.stringByReplacingOccurrencesOfString(",", withString: "")
        
        let floatUserBalanceAmount = Float(strUserBalance)
        let floatCashOutAmount = Float(strCashOutBalance)

        print("floatUserBalanceAmount :- \(floatUserBalanceAmount)")
        print("floatCashOutAmount :- \(floatCashOutAmount)")
        
        strAmount = String(format: "%.2f", floatCashOutAmount!)
        print("strAmount :- \(strAmount)")

        if floatCashOutAmount <= 0 {
            return
        }
        
        if floatCashOutAmount > floatUserBalanceAmount {
            
            Helper.showAlert("The amount entered can not be cashed out as it exceeds the amount available in your CLINK! balance", pobjView: self.view)
        }
        else{
            Helper.showAlert("Please confirm that $\(strAmount) will being cashed to the primary payment method on file.", cancelButtonTitle: "CANCEL", otherButtonTitle: "OK", pobjView: self.view, completionBock: { (customAlert, buttonIndex) -> () in
                
                if buttonIndex == 1{
                    self.cashOutBalance()
                }
            })
        }
    }
    
    func cashOutBalance() {
        
        let strUserName = objUserDetail.strFirstName + " " + objUserDetail.strLastName
        let dictTransfer = [ "amount":"\(strAmount)" , "description":"\(strUserName) cashed out $\(strAmount) from CLINK! balance." , "from_account_uuid":objUserDetail.strClinkAccountId , "to_account_uuid":obkTransferDetail.strId ]
        
        ClinkAccountManager.sharedInstance.callWebService(ClinkAccountRouter.TransferToAccount(dictTransfer).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self) { (success, response) -> () in
            
            if success == true {
                Helper.showAlert("CLINK! balance cashed out successfully", pobjView: self.view, completionBock: { (CustomAlert, buttonIndex) -> () in
                    
                    for vc in (self.navigationController?.viewControllers)! {
                        if vc.isKindOfClass(AccountsVC) == true {
                            self.navigationController?.popToViewController(vc, animated: true)
                            break
                        }
                    }
                    
                })
                APPDELEGATE.objSideMenu.getUserProfile()
            }
            else{
                Helper.showAlert(response as! String, pobjView: self.view)
            }
        }
        
    }
    
}
