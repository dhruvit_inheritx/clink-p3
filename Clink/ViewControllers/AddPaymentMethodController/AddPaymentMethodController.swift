//
//  AddPaymentMethodController.swift
//  Clink
//
//  Created by Dhruvit on 22/10/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

enum AddPaymentMethodControllerType:Int {
    case Payment = 1, CashOut = 2
}

class AddPaymentMethodController: UIViewController {

    @IBOutlet var btnNext : UIButton!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnNavBack : UIButton!
    @IBOutlet var btnAddCreditCard : UIButton!
    @IBOutlet var btnAddBankAccount : UIButton!
    @IBOutlet var btnAddPayPal : UIButton!
    @IBOutlet var lblSkip : UILabel!
    
    var arrayButtons : [UIButton]!
    var btnSelected : AnyObject!
    
    var addPaymentMethodControllerType : AddPaymentMethodControllerType!
    
    var isFTUUser = false
    
    // MARK: - View Lifecycle
 
    override func viewDidLoad() {
        super.viewDidLoad()

        btnNext.enabled = false
        btnNext.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        arrayButtons = [ btnAddCreditCard , btnAddBankAccount , btnAddPayPal ]
    
        lblSkip.hidden = true
        
        if isFTUUser == true {
            btnBack.setTitle("Skip this step", forState: .Normal)
            lblSkip.hidden = false
        }
        btnNavBack.hidden = isFTUUser
        
        if addPaymentMethodControllerType == AddPaymentMethodControllerType.CashOut {
            self.btnAddCreditCard.setTitle("Add Debit Card", forState: .Normal)
        }
        else {
            self.btnAddCreditCard.setTitle("Add Credit or Debit Card", forState: .Normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    override func viewWillAppear(animated: Bool) {
        for btn in arrayButtons{
            
            btn.backgroundColor = UIColor.clearColor()
            btn.setTitleColor(COLORTHEME, forState: .Normal)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == SEGUECARDINFO {
            let objCardInfoVC = segue.destinationViewController as! CardInfoVC
            objCardInfoVC.isFTUUser = self.isFTUUser
        }
        else if segue.identifier == SEGUEADDBANK {
            let objBankInfoVC = segue.destinationViewController as! CreditandDebitInfo
            objBankInfoVC.isFTUUser = self.isFTUUser
        }
        
    }
    
    // MARK: - Toggle Button
    
    @IBAction func btnToggle(sender : UIButton){
        
        btnNext.setTitleColor(COLORTHEME, forState: .Normal)

        switch (sender.tag){
        case 0:
            btnNext.enabled = true
            break
        case 1:
            btnNext.enabled = true
            break
        case 2:
            btnNext.enabled = true
            break
        default:
            btnNext.enabled = true
            break
        }
        
        for btn in arrayButtons{
            
            btn.backgroundColor = UIColor.clearColor()
            btn.setTitleColor(COLORTHEME, forState: .Normal)
        }
        
        btnSelected = sender
        sender.backgroundColor = COLORTHEME
        sender.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        if btnSelected.tag == 1{
            
            self.performSegueWithIdentifier(SEGUECARDINFO, sender: self)
        }
        else if btnSelected.tag == 2{
            
            self.performSegueWithIdentifier(SEGUEADDBANK, sender: self)
        }
        else if btnSelected.tag == 3{
            Helper.showCommingsoonAlert()
            //            self.performSegueWithIdentifier(SEGUEADDPAYPAL, sender: self)
        }
    }

    // MARK: - Back Clicked

    @IBAction func backClicked(sender: UIButton) {
        
        if isFTUUser == true {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: - Send Clicked

    @IBAction func sendClicked(sender: UIButton) {
        
        if btnSelected.tag == 1{
            
            self.performSegueWithIdentifier(SEGUECARDINFO, sender: self)
        }
        else if btnSelected.tag == 2{
            
            self.performSegueWithIdentifier(SEGUEADDBANK, sender: self)
        }
        else if btnSelected.tag == 3{
            Helper.showCommingsoonAlert()
//            self.performSegueWithIdentifier(SEGUEADDPAYPAL, sender: self)
        }
    }

}
