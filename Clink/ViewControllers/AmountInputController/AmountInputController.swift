//
//  AmountInputController.swift
//  Clink
//
//  Created by Gunjan on 19/03/16.
// Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class AmountInputController: UIViewController {

    //    MARK:- IBOutlet
    @IBOutlet var lblAmount:UILabel!
    @IBOutlet var btnBack:UIButton!
    
    @IBOutlet var btnTitle : UIButton!
    
//    var fAmount : Float!
    var intAmount : Int!
    var isGifting : Bool!
    
    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
//        self.removeExtraChar()
       
        if isGifting == nil{
            isGifting = false
            btnTitle.setTitle(" ", forState: .Normal)
            btnTitle.setTitle(STRSENDCLINK, forState: .Normal)
        }
        else if isGifting == true{
            
             btnTitle.setTitle(STRADDTOCLINK, forState: .Normal)
        }
        else{
            btnTitle.setTitle(STRSAYTY, forState: .Normal)
        }
        if intAmount != nil{
            let strTmp = "\(intAmount!)"
            lblAmount.text = strTmp
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = false
    }
    
    override func viewDidDisappear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Button Actions
    @IBAction func actionBack(sender:UIButton){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func actionInputButton(sender:UIButton){
        
        var inputVal = STREMPTY
        switch sender.tag{
        case 0 :
            inputVal = STR0
        case 1 :
            inputVal = STR1
        case 2 :
            inputVal = STR2
        case 3 :
            inputVal = STR3
        case 4 :
            inputVal = STR4
        case 5 :
            inputVal = STR5
        case 6 :
            inputVal = STR6
        case 7 :
            inputVal = STR7
        case 8 :
            inputVal = STR8
        case 9 :
            inputVal = STR9
        case 10 :
            inputVal = STRPOINT
        default:
            inputVal = STREMPTY
        }
        let strAmount = lblAmount.text
        
        if strAmount!.length < 5
        {
            if lblAmount.text == STR0{
                
                if sender.tag == 10{
                    
                    if ((strAmount?.containsString(".")) == false){
                        lblAmount.text! = STR0+inputVal
                    }
                }
                else{
                    lblAmount.text! = inputVal
                }
            }
            else
            {
                if sender.tag == 10{
                    
                    if ((strAmount?.containsString(".")) == false){
                        lblAmount.text! += inputVal
                    }
                }
                else{
                    lblAmount.text! += inputVal
                }
            }
        }
        
    }
    
    @IBAction func actionRemove(sender:UIButton){
        
        if ((lblAmount.text?.isEmptyString()) == false)
        {
            if lblAmount.text != STR0
            {
                let strAmount = String(lblAmount.text!.characters.dropLast()) as String
                lblAmount.text = strAmount
                
                if ((lblAmount.text?.isEmptyString()) == true){
                    lblAmount.text = STR0
                }
            }
        }
    }
    
    @IBAction func actionAddAmount(sender:UIButton){
        
//        if lblAmount.text!.characters.last == "."{
//            let strAmount = String(lblAmount.text!.characters.dropLast()) as String
//            lblAmount.text = strAmount
//        }
        intAmount = Int(lblAmount.text!)
//        self.removeExtraChar()
        if isGifting == true {
//            self.performSegueWithIdentifier(UNWINDTOGIFT, sender: nil)
            self.performSegueWithIdentifier(UNWINDTOSENDNEWCLINK, sender: nil)
        }
        else{
            self.performSegueWithIdentifier(UNWINDTOSENDNEWCLINK, sender: nil)
        }
    }
    
    /*func removeExtraChar(){
        if fAmount != nil{
            var strTmp = "\(fAmount)"
            strTmp.removeLastCharIfExist("0")
            strTmp.removeLastCharIfExist(".")
            lblAmount.text = strTmp
            fAmount = Float(strTmp)
        }
    }*/
}
