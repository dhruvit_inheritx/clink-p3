
//  ClinkDetailsController.swift
//  Clink
//
//  Created by Gunjan on 04/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

class ClinkDetailsController: UIViewController,UITableViewDelegate ,UICollectionViewDelegate, UICollectionViewDataSource {
    
     //    MARK:- Gloable variable
    var objSelectedClinkDetail : ClsClinkDetails!
    var imgSelected: UIImage!
//    var objSelectedTYClinkDetail : ClsClinkDetails!
//    var imgSelectedTYImage: UIImage!
    var selectedClinkType : ClinkType!
    var isUpdateNeeded : Bool!
    var isFromFeedScreen = false
    var isFromHistoryScreen = false
    var isThankyouClink = false
    var arrClinkDetails : [ClsClinkDetails] = Array()
    var intCurrentIndex : Int = 0
    var isAlertShown = false
    var isFromGridVC = false
    
    //    MARK:- IBOutlet
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var lblTo: UILabel!
    @IBOutlet var lblFrom: UILabel!
    @IBOutlet var lblDateTime: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var imgViewClink: UIImageView!
    @IBOutlet var btnVenue: UIButton!
    @IBOutlet var btnAmount: UIButton!
    @IBOutlet var btnSayTY: UIButton!
    @IBOutlet var lblGift: UILabel!
    @IBOutlet var lblFooter: UILabel!
    @IBOutlet var vwFooter: UIView!
    @IBOutlet var btnSendClinkFTU : UIButton!
    @IBOutlet var imgbtnSendFTUShadow : UIImageView!
    @IBOutlet var collectionViewEmailCC: UICollectionView!
    @IBOutlet var scrlviewTo: UIScrollView!
    @IBOutlet var scrlviewFrom: UIScrollView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var viewMerchant: UIView!
    
    var arrEmailCC = [ClsContact]()

    // MARK: - Viewcontroller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblFooter.text = STREMPTY
        vwFooter.hidden = true
        self.initializeOnce()
        
        self.btnVenue.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
        if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil && objSelectedClinkDetail != nil{
            let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.ClinkDetails.rawValue ,"ClinkId" : objSelectedClinkDetail.strId]
            Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
        }
        
        btnTitle.hidden = isFromGridVC
        if (isFromGridVC == true && arrClinkDetails.count > 1){
            let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(ClinkDetailsController.actionSwipeGesture(_:)))
            swipeLeftGesture.direction = UISwipeGestureRecognizerDirection.Left
            self.view.addGestureRecognizer(swipeLeftGesture)
            
            let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(ClinkDetailsController.actionSwipeGesture(_:)))
            swipeRightGesture.direction = UISwipeGestureRecognizerDirection.Right
            self.view.addGestureRecognizer(swipeRightGesture)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
       
    }
    
    override func viewDidDisappear(animated: Bool) {
        SideMenuRootController.rightSwipeEnabled = true
        
    }
    override func viewDidAppear(animated: Bool) {
         SideMenuRootController.rightSwipeEnabled = false
    }
    
    // MARK: - Custom Methods
    func initializeOnce() {
        
        activityIndicator.hidesWhenStopped = true
        
        if objSelectedClinkDetail != nil {
            
            var strNotificationName = STREMPTY
            
            lblTo.text = " \(objSelectedClinkDetail.strReceiverName)"
            lblFrom.text = " \(objSelectedClinkDetail.strSenderName)"
            
            if objSelectedClinkDetail.isRead == false {
                
                //                    let objUserDetail = USERDEFAULTS.getUserDetailsObject()
                //
                //                    if objSelectedClinkDetail.strReceiverId == objUserDetail.strId {
                
                strNotificationName = UPDATESENTCOUNT
                let dicIsRead = ["is_read":"1"]
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objSelectedClinkDetail.strId,dicIsRead).URLRequest, isBackgroundCall: true, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    if (success == true){
                        NOTIFICATIONCENTER.postNotificationName(strNotificationName, object: nil)
                    }
                })
                //                    }
            }
            
            if selectedClinkType == ClinkType.Sent {
                btnTitle.setImage(UIImage(named: IMGICNSENTTITLE), forState:.Normal)
                btnTitle.setTitle(STRSENT, forState: .Normal)
                btnSayTY.enabled = false
            }
            else if selectedClinkType == ClinkType.Received {
                
                btnTitle.setImage(UIImage(named: IMGICNINBOX), forState:.Normal)
                btnTitle.setTitle(STRRECIEVED, forState: .Normal)
                 
                strNotificationName = UPDATERECEIVEDCOUNT
            }
            
            if isFromFeedScreen == true{
                btnTitle.setImage(UIImage(named: IMGICNLOGO), forState:.Normal)
                btnTitle.setTitle(STRHOME, forState: .Normal)
                btnSayTY.enabled = false
            }
            else if selectedClinkType == ClinkType.Received {
                if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == true
                {
                    btnSayTY.enabled = true
                }
                else{
                    btnSayTY.enabled = false
                }
            }
            else{
                btnSayTY.enabled = false
            }
            
            if imgSelected != nil {
                imgViewClink.image = imgSelected!
                self.activityIndicator.stopAnimating()
            }
            else if let strImgUrl = objSelectedClinkDetail.strImageUrl {
                self.activityIndicator.startAnimating()
                self.activityIndicator.activityIndicatorViewStyle = .White
                self.imgViewClink.sd_setImageWithURL(NSURL(string: strImgUrl), placeholderImage: nil, completed: { (image, error, SDImageCacheType, url) -> Void in
                    if (image != nil){
                        self.imgViewClink.image = image
                        self.imgSelected = image
                        self.activityIndicator.stopAnimating()
                    }
                })
            }
            
            let dateCreated = objSelectedClinkDetail.strCreatedDate.getLocalDate(PHASE3FORMATE)
            lblDateTime.text = Helper.getDateDifferenceforDate(dateCreated, andDate: NSDate())
            
            btnVenue.userInteractionEnabled = false
            
            if objSelectedClinkDetail.strMerchantId != nil && objSelectedClinkDetail.strMerchantId.isEmptyString() == false {
                if objSelectedClinkDetail.objMarchantDetails == nil{
                    getMerchantDetails(objSelectedClinkDetail.strMerchantId)
                }
                else{
                    if let strImageUrl  = objSelectedClinkDetail.objMarchantDetails!.strImgSmallIcon
                    {
                        print("Image Url :- " + strImageUrl)
                        self.btnVenue.sd_setImageWithURL(NSURL(string:strImageUrl), forState: .Normal)
                    }
                    self.setUpBottomBanner()
                }
                
                btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
                
            }
            else{
                btnVenue.setImage(UIImage(named: IMGLOGOGOLD), forState: UIControlState.Normal)
                btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
                vwFooter.hidden = true
            }
            
            
            var strTotalAmount = ""
            
//            if var fltGiftAmount = Float(objSelectedClinkDetail.strGiftPlusAmount){
//                if let fltTotalAmount = Float(objSelectedClinkDetail.strAmount){
//                    fltGiftAmount += fltTotalAmount
//                }
//                strTotalAmount = "\(fltGiftAmount)"
//            }
//            else
            if let fltTotalAmount = Float(objSelectedClinkDetail.strAmount){
                //            let fltTotalAmount = Float(objSelectedClinkDetail.strAmount)!
                strTotalAmount = "\(fltTotalAmount)"
            }
                
            if Float(strTotalAmount) > FLOATZERO{
                var strFltAmount = "\(Float(objSelectedClinkDetail.strAmount!))"
                strFltAmount.removeLastCharIfExist("0")
                strFltAmount.removeLastCharIfExist(".")
                btnAmount.setTitle("$ \(strFltAmount)", forState:
                    UIControlState.Normal)
            }
            
            
            strTotalAmount.removeLastCharIfExist("0")
            strTotalAmount.removeLastCharIfExist(".")
            btnAmount.setTitle("$ \(strTotalAmount)", forState:
                UIControlState.Normal)
            scrlviewTo.contentSize = CGSizeMake(lblTo.intrinsicContentSize().width, LBLTOFROMHEIGHT)
            scrlviewFrom.contentSize = CGSizeMake(lblFrom.intrinsicContentSize().width, LBLTOFROMHEIGHT)
            
            if Int(strTotalAmount) <= 0 {
                btnAmount.setTitle("", forState:
                    UIControlState.Normal)
                if objSelectedClinkDetail.strMerchantId == nil || objSelectedClinkDetail.strMerchantId.isEmptyString() == true{
                    btnAmount.hidden = true
                }
            }
            
            self.collectionViewEmailCC.delegate = self
            self.collectionViewEmailCC.dataSource = self
            
            arrEmailCC = objSelectedClinkDetail.arrCCUser
            self.collectionViewEmailCC.reloadData()
            
            
            if objSelectedClinkDetail.strMerchantId != nil && objSelectedClinkDetail.strMerchantId.isEqualToIgnoringCase(VENUEHOUNDSPUB) && isFromFeedScreen == false && selectedClinkType == ClinkType.Received{
                self.btnVenue.userInteractionEnabled = true
            }
            else{
                self.btnVenue.userInteractionEnabled = false
            }
            
            if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == false || objSelectedClinkDetail.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId
            {
                btnSayTY.enabled = false
            }
            
            if objSelectedClinkDetail.strMerchantId != nil && objSelectedClinkDetail.strMerchantId.isEmptyString() == false {
                if objSelectedClinkDetail.objMarchantDetails == nil{
                    getMerchantDetails(objSelectedClinkDetail.strMerchantId)
                }
                else{
                    if let strImageUrl  = objSelectedClinkDetail.objMarchantDetails!.strImgSmallIcon
                    {
                        print("Image Url :- " + strImageUrl)
                        self.btnVenue.sd_setImageWithURL(NSURL(string:strImageUrl), forState: .Normal)
                    }
                    self.setUpBottomBanner()
                }
                
                btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
                
            }
            else{
                btnVenue.setImage(UIImage(named: IMGLOGOGOLD), forState: UIControlState.Normal)
                btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
                vwFooter.hidden = true
            }
            
            btnSendClinkFTU.userInteractionEnabled = true
        }
    }
    
    func setUpBottomBanner(){
        
        if objSelectedClinkDetail.objMarchantDetails.strName.isEmptyString() == false && selectedClinkType == ClinkType.Received && objSelectedClinkDetail.objMarchantDetails.arrCategoryName[0] != KCATEGORYCHARITIES && objSelectedClinkDetail.strReceiverId == USERDEFAULTS.getUserDetailsObject().strId {
            if isFromFeedScreen == false {
                btnVenue.userInteractionEnabled = true
            }
            btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
            btnVenue.userInteractionEnabled = true
            vwFooter.hidden = false
            let strToAttach = self.getAttributedText("redeem")
            let strStart = NSAttributedString(string:"Tap to ")
            let strLast = NSAttributedString(string:" your Clink!")
            let tmpStr : NSMutableAttributedString = strStart.mutableCopy() as! NSMutableAttributedString
            tmpStr.appendAttributedString(strToAttach)
            tmpStr.appendAttributedString(strLast)
            let finalStr : NSAttributedString = tmpStr.copy() as! NSAttributedString
            lblFooter.attributedText = finalStr
            
        }
        else if isFromFeedScreen == true || isFromHistoryScreen == true {
            
            if objSelectedClinkDetail.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId {
                return
            }
            
            btnSayTY.enabled = true
            btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
            btnVenue.userInteractionEnabled = true
            vwFooter.hidden = false
            let strToAttach = self.getAttributedText("redeem")
            let strStart = NSAttributedString(string:"Tap to ")
            let strLast = NSAttributedString(string:" your Clink!")
            let tmpStr : NSMutableAttributedString = strStart.mutableCopy() as! NSMutableAttributedString
            tmpStr.appendAttributedString(strToAttach)
            tmpStr.appendAttributedString(strLast)
            let finalStr : NSAttributedString = tmpStr.copy() as! NSAttributedString
            lblFooter.attributedText = finalStr
        }
        else if objSelectedClinkDetail.objMarchantDetails.arrCategoryName[0] == KCATEGORYCHARITIES{
            btnSayTY.setImage(UIImage(named: IMGBTNDONATESMALL), forState: UIControlState.Normal)
            lblFooter.text = STREMPTY
            vwFooter.hidden = true
        }
        else{
            
            btnVenue.setImage(UIImage(named: IMGLOGOGOLD), forState: UIControlState.Normal)
            btnSayTY.setImage(UIImage(named: IMGTXTTY), forState: UIControlState.Normal)
            lblFooter.text = STREMPTY
            vwFooter.hidden = true
        }
    }
    
    func getMerchantDetails(strMerchantId : String) -> ClsVenue?{
        var objMerchantDetails : ClsVenue?
        if strMerchantId.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > INTZERO && strMerchantId.isEmptyString() == false{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetVenueWithId(strMerchantId).URLRequest, isBackgroundCall:true, pAlertView: self.view, pViewController: self) { (success, result) -> () in
                if success == true{
                    objMerchantDetails = ClsVenue(pdictResponse: result as! [String : AnyObject])
                    self.objSelectedClinkDetail.objMarchantDetails = objMerchantDetails
                    self.setUpBottomBanner()
                    let strImageUrl : String = objMerchantDetails!.strImgSmallIcon
                    print("Image Url :- " + strImageUrl)
                    self.btnVenue.sd_setImageWithURL(NSURL(string:strImageUrl), forState: .Normal)
                }
            }
        }
        return objMerchantDetails
    }
    
    // MARK: - Swipe Gesture Action
    
    func actionSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped right")
                self.handleSwipe(true)
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped Left")
                self.handleSwipe(false)
            default:
                break
            }
        }
    }
    
    func handleSwipe(isRightSwipe : Bool){
        
        if isAlertShown == false{
            if isRightSwipe == true && intCurrentIndex > 0{
                intCurrentIndex -= 1
                self.setNewCurrentClink()
            }
            else if isRightSwipe == false && arrClinkDetails.count-1 > intCurrentIndex{
                intCurrentIndex += 1
                self.setNewCurrentClink()
            }
            else if isRightSwipe == true {
                self.isAlertShown = true
                Helper.showAlert("You are at the first clink in the list.", pobjView: self.view, completionBock: { (result,buttonIndex) -> () in
                    self.isAlertShown = false
                })
            }
            else if isRightSwipe == false{
                self.isAlertShown = true
                Helper.showAlert("You are at the last clink in the list.", pobjView: self.view, completionBock: { (result,buttonIndex) -> () in
                    self.isAlertShown = false
                })
            }
        }
    }
    
    func setNewCurrentClink(){
        objSelectedClinkDetail = arrClinkDetails[intCurrentIndex]
        self.imgViewClink.image = nil
        self.imgSelected = nil
        self.initializeOnce()
    }
    
    // MARK: - Button Actions
    @IBAction func btnBackCliked(sender: AnyObject) {
        if isUpdateNeeded != nil && isUpdateNeeded == true{
            self.performSegueWithIdentifier(UNWINDTOUPDATEREADCOUNT, sender: nil)
        }
        else if isFromFeedScreen == true{
             self.performSegueWithIdentifier(UNWINDTOFEEDSCREEN, sender: nil)
        }
//        else if isFromGridVC == true{
//            self.dismissViewControllerAnimated(true, completion: nil)
//        }
        else{
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
     @IBAction func btnVenueCliked(sender: UIButton) {
        
        sender.userInteractionEnabled = false
        if objSelectedClinkDetail.objMarchantDetails.strName.isEqualToIgnoringCase(VENUEHOUNDSPUB) && selectedClinkType == ClinkType.Received {
                self.performSegueWithIdentifier(SEGUETOOFFERPREVIEW,sender: nil)
            }
        sender.userInteractionEnabled = true
    }
    @IBAction func btnSendClinkFTU(sender: UIButton) {
        APPDELEGATE.objSideMenu.showSendNewClink()
    }
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    @IBAction func btnSendThankyou(sender: UIButton) {
        
        sender.userInteractionEnabled = false
        
        if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == true && selectedClinkType == ClinkType.Received && objSelectedClinkDetail.strReceiverId == USERDEFAULTS.getUserDetailsObject().strId
        {
            performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: objSelectedClinkDetail)
        }
        else if objSelectedClinkDetail.strOriginalClinkId.isEmptyString() == true && isFromFeedScreen == true && objSelectedClinkDetail.strReceiverId == USERDEFAULTS.getUserDetailsObject().strId
        {
            performSegueWithIdentifier(SEGUETOSENDNEWCLINK, sender: objSelectedClinkDetail)
        }
        
        sender.userInteractionEnabled = true
    }
    
    @IBAction func btnShowGrid(sender: UIButton) {
        if isFromGridVC == false{
            if objSelectedClinkDetail.arrAssociatedClinkId.count > 0 {
                self.performSegueWithIdentifier(SEGUETOGRID, sender: nil)
            }
            else{
//                Helper.showAlert("No associated clinks found", pobjView: self.view)
            }
        }
    }
    
    // MARK: - CollectionView delegate methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrEmailCC.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cellIdentifier:String = CELLEMAILCC;
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier,forIndexPath:indexPath) as! EmailCCCell
        if indexPath.row < arrEmailCC.count - 1
        {
            cell.lblEmailCCName.text = "\((arrEmailCC[indexPath.row].strFullName)!),"
        }
        else
        {
            cell.lblEmailCCName.text = arrEmailCC[indexPath.row].strFullName
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let str:String = "\(arrEmailCC[indexPath.row].strFullName),"
        let calCulateSizze: CGSize = str.sizeWithAttributes([NSFontAttributeName: UIFont (name: FONTGOTHAMMEDIUM, size: FONTSIZE14)!])
        return CGSizeMake(calCulateSizze.width+1,COLLECTIONCCNAMEHEIGHT)
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == SEGUETOSENDNEWCLINK{
            let objSendNewClink = segue.destinationViewController as! SendNewClinkController
            objSendNewClink.controllerType = ClinkControllerType.ThankYou
            let objSelected = sender as! ClsClinkDetails
            objSendNewClink.objSelectedClinkDetail = objSelected
            
        }
        else if segue.identifier == SEGUETOGRID{
            let objNavigationVC = segue.destinationViewController as! UINavigationController
            let objGriedVC = objNavigationVC.topViewController as! ClinkGridController
            objGriedVC.objSelectedClinkDetails = self.objSelectedClinkDetail
            objGriedVC.selectedClinkType = self.selectedClinkType
        }
    }
    
    func getAttributedText(strToConvert : String) -> NSAttributedString{
        let labelFont = UIFont(name: FONTGOTHAMBOLD, size: FONTSIZE17)
        let attributes :[String:AnyObject] = [NSFontAttributeName : labelFont!]
        let attrString = NSAttributedString(string:strToConvert, attributes: attributes)
        return attrString
    }
}
