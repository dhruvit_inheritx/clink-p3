//  ClinkListController.swift
//  Clink
//
//  Created by Gunjan on 04/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import HDTableDataSource

class ClinkListController: UIViewController,UITableViewDelegate {
    
    //    MARK:- Gloable variable
    var arrClinkList : [ClsClinkDetails]!
    var objHDTableDataSource : HDTableDataSource!
    var currentClinkType : ClinkType!
    var objSelectedClinkDetail : ClsClinkDetails!
    var imgSelectedImage : UIImage!
    var objSelectedTYClinkDetail : ClsClinkDetails!
    var imgSelectedTYImage : UIImage!
    var shouldAutoLike : Bool = false
    var shouldOpenComment : Bool = false
    var isThankyouClink : Bool = false
    var controllerType : ClinkControllerType!
    
    var intCurrentIndex : Int!
    var intCurrentPage : Int = 1
    var intLastPage : Int! = 0
    
    //    MARK:- IBOutlet
    @IBOutlet var btnTitle: UIButton!
    @IBOutlet var tblClinkDetails: UITableView!
    @IBOutlet var lblNotFound : UILabel!
    @IBOutlet var btnSendClinkFTU : UIButton!
    
    @IBOutlet var badgeReceived : UILabel!
    @IBOutlet var badgeSent : UILabel!
    @IBOutlet var btnSend : UIButton!
    @IBOutlet var btnReceived : UIButton!
    @IBOutlet var btnSent : UIButton!
    
    // MARK: - Viewcontroller Cycle
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let currentUser = USERDEFAULTS.boolForKey(kISUSERLOGGEDIN)
        if currentUser != true{
            var mainView: UIStoryboard!
            mainView = UIStoryboard(name: AUTHENTICATIONSTORYBOARD, bundle: nil)
            let loginVC : UIViewController = mainView.instantiateViewControllerWithIdentifier(CONTROLLERLOGIN) as UIViewController
            self.presentViewController(loginVC, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initializeOnce()
        
        badgeReceived.layer.cornerRadius = badgeReceived.frame.size.height/2
        badgeSent.layer.cornerRadius = badgeSent.frame.size.height/2
        
        let currentUser = USERDEFAULTS.boolForKey(kISUSERLOGGEDIN)
        if currentUser == true{
            
            let objClinkID = USERDEFAULTS.valueForKey(KFETCHEDCLINKID) as? String
            if objClinkID != nil
            {
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objClinkID!,nil).URLRequest, isBackgroundCall: false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    if success == true{
                        self.objSelectedClinkDetail = ClsClinkDetails(pdictResponse: response as! [String : AnyObject])
                        
                        if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == false{
                            self.currentClinkType = ClinkType.Sent
                        }
                        else {
                            self.currentClinkType = ClinkType.Received
                        }
                        
                        self.shouldAutoLike = false
                        self.shouldOpenComment = false
                        self.performSegueWithIdentifier(SEGUETOTHREADVIEW, sender: nil)
                        SEGUETOCLINKDETAILS
                        USERDEFAULTS.removeObjectForKey(KFETCHEDCLINKID)
                        self.getClinkData(RECORDSPERPAGE,pCurrentPage: self.intCurrentPage)
                    }
                    else{
                        Helper.showAlert(ALERTFAILEDTOGETCLINK, pobjView: APPDELEGATE.window)
                    }
                })
            }
            else{
                self.getClinkData(RECORDSPERPAGE,pCurrentPage: self.intCurrentPage)
            }
        }
        let timer : NSTimer = NSTimer.scheduledTimerWithTimeInterval(40, target: self, selector: #selector(ClinkListController.getUnreadCount), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    // MARK: - Custom Methods
    func initializeOnce(){
        intCurrentPage = 1
        intLastPage = 0
        arrClinkList = Array()
    }
    
    func getUnreadCount(){
        
        badgeReceived.text = "\(USERDEFAULTS.getUserDetailsObject().intReceivedCount)"
        badgeSent.text = "\(USERDEFAULTS.getUserDetailsObject().intSentCount)"
    }
    
    func getClinkData(pRecordsPerPage : Int , pCurrentPage : Int){
        
        var dicClinkFilter : [String : AnyObject] = [KPERPAGE : "\(pRecordsPerPage)", KPAGE : "\(pCurrentPage)"]
        
        if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == false{
            if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil{
                let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.Sent.rawValue]
                Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
            }
            currentClinkType = ClinkType.Sent
            btnTitle.setImage(UIImage(named: IMGICNSENTTITLE), forState:.Normal)
            btnTitle.setTitle(STRSENT, forState: .Normal)
            btnSent.enabled = false
            btnReceived.enabled = true
            dicClinkFilter["filters[type]"] = "sent"
            
        }
        else {
            if APPDELEGATE.arrTrackerGroups.count > 0 && APPDELEGATE.arrTrackerGroups.last!.strId != nil{
                let dicTracker : [String : AnyObject] = ["type" : PageViewTracker.Received.rawValue]
                Helper.setTrackerData(dicTracker, strGroupId: APPDELEGATE.arrTrackerGroups.last!.strId)
            }
            currentClinkType = ClinkType.Received
            btnTitle.setImage(UIImage(named: IMGICNINBOX), forState:.Normal)
            btnTitle.setTitle(STRRECIEVED, forState: .Normal)
            btnReceived.enabled = false
            btnSent.enabled = true
            dicClinkFilter["filters[type]"] = "received"
        }
        
//        self.initializeOnce()
        
        ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkList(dicClinkFilter).URLRequest, isBackgroundCall : false, pAlertView : self.view, pViewController: self) { (success, response) -> () in
            
            if success == true
            {
                Helper.hideHud(self.view)
//                self.arrClinkList.removeAll()
                
                let dicResponse = response as? [String : AnyObject]
                self.intLastPage = dicResponse!.getValueIfAvilable(KLASTPAGE) as? Int
                if let arrClinks = response[KCLINK] as? [AnyObject]
                {
                    for objClinkDetails in arrClinks {
                        let objClinkDetail = ClsClinkDetails(pdictResponse: objClinkDetails as! [String : AnyObject])
                        if objClinkDetail.strOriginalClinkId != nil{                            
                            objClinkDetail.getReplyClink(self.view, pViewController: self)
                        }
                        self.arrClinkList.append(objClinkDetail)
                    }
                    self.lblNotFound.hidden = true
                    
                }
                if self.arrClinkList.count <= 0{
                    self.lblNotFound.hidden = false
                }
                self.configureTableView()
            }
            else if response != nil{
                self.lblNotFound.hidden = false
                Helper.showAlert((response as? String)!, pobjView: self.view)
            }
        }
    }
    
    func configureTableView(){
        
//        arrClinkList.sortInPlace({ $0.strDate.compare($1.strDate) == .OrderedDescending })
        objHDTableDataSource = HDTableDataSource(items: arrClinkList as [AnyObject], cellIdentifier: KCLINKSENT, configureCellBlock: { (cell:AnyObject!, item:AnyObject!,indexPath:NSIndexPath!) -> Void in
            
            let objCell = cell as! ClinkListCell
            let objItems = item as! ClsClinkDetails
            

            objCell.configureCell(objItems,pclinkType:self.currentClinkType)

            objCell.btnShowDetails.tag  = indexPath.row
            objCell.btnLike.tag  = indexPath.row
            objCell.btnComment.tag  = indexPath.row
            objCell.btnGift.tag  = indexPath.row
            objCell.btnSayThankyou.tag = indexPath.row
            
            if objItems.strReceiverId == KDEFAULTUSERID || objItems.strReceiverId == KDEFAULTUSERID2 || objItems.strSenderId == KDEFAULTUSERID || objItems.strSenderId == KDEFAULTUSERID2{
                self.btnSendClinkFTU.hidden = false
            }
        })
        tblClinkDetails.dataSource = objHDTableDataSource
        tblClinkDetails.delegate = self
        tblClinkDetails.reloadData()
    }
    
    // MARK: - TableView Delegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return CLINKLISTCELL
    }
    
    func tableView(tableView: UITableView, didEndDisplayingCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let objCell = cell as! ClinkListCell
        objCell.imgThankyou.image = nil
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == (arrClinkList.count - 1) && intCurrentPage < intLastPage{
            self.loadMoreClinks()
        }
    }
    
    // MARK: - Custom Methods
    func loadMoreClinks(){
        intCurrentPage += 1
        self.getClinkData(RECORDSPERPAGE,pCurrentPage: self.intCurrentPage)
    }
    
    // MARK: - Button Actions
    @IBAction func btnClinkGroupClicked(sender: UIButton) {
        
        let btnTag = sender.tag
        //        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
        switch btnTag{
        case 1 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(1)
            APPDELEGATE.objSideMenu.showSendNewClink()
            break
        case 2 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
            APPDELEGATE.objSideMenu.showReceivedClink()
            break
        case 3 :
            APPDELEGATE.objSideMenu.setMenuSelectedItem(3)
            APPDELEGATE.objSideMenu.showSentClink()
            break
        default :
            break
        }
    }
    
    @IBAction func btnTitle(sender: UIButton) {
        if arrClinkList.count > INTZERO{
            self.tblClinkDetails.scrollToRowAtIndexPath(NSIndexPath(forItem: INTZERO, inSection: INTZERO), atScrollPosition: .Top, animated: true)
        }
    }
    
    @IBAction func btnSideMenuTapped(sender: AnyObject) {
        NSNotificationCenter.defaultCenter().postNotificationName(kToggleSideMenuNotification, object: nil)
    }
    
    func loadTYData(sender: UIButton ,strSegue : String){
        
        if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == false{
            currentClinkType = ClinkType.Sent
        }
        else {
            currentClinkType = ClinkType.Received
        }
        let objClinkList = arrClinkList[sender.tag]
        
        self.objSelectedClinkDetail = objClinkList
        self.objSelectedTYClinkDetail = objClinkList.objReplyClink
        let indexPath = NSIndexPath(forRow:sender.tag, inSection: INTZERO)
        let cell = self.tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
        if cell.imgThankyou.image != nil{
            self.imgSelectedTYImage = cell.imgThankyou.image
            self.imgSelectedImage = cell.imgClink.image
            if strSegue == SEGUETOCLINKDETAILS{
                self.imgSelectedImage = cell.imgThankyou.image
            }
            self.performSegueWithIdentifier(strSegue, sender: nil)
        }
        else{
            Helper.showAlert(ALERTPLEASEWAITTILLLOADINGIMAGE, pobjView: self.view)
        }        
        sender.enabled = true
        
    }
    
    
    @IBAction func btnSendThankyou(sender: UIButton) {
        
        sender.enabled = false
        let objClinkList = arrClinkList[sender.tag]
        
        if objClinkList.strOriginalClinkId.isEmptyString() == false
        {
            let indexPath = NSIndexPath(forRow:sender.tag, inSection: INTZERO)
            let cell = tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
            
            objSelectedClinkDetail = arrClinkList[sender.tag]
            if cell.imgThankyou.image != nil{
                imgSelectedTYImage = cell.imgThankyou.image!
                isThankyouClink = true
                self.loadTYData(sender,strSegue: SEGUETOCLINKDETAILS)
            }
            else{
                Helper.showAlert(ALERTPLEASEWAITTILLLOADINGTYIMAGE, pobjView: self.view)
            }
        }
        else{
             if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == true{
                    currentClinkType = ClinkType.Received
                    controllerType = ClinkControllerType.ThankYou
                    self.manageSelectedClink(sender,pclinkType: ClinkType.Received,segue : SEGUETOSENDNEWCLINK)
                }
            else {
                
            }
            
        }
        sender.enabled = true
    }
    
    func performActionTap(sender: UIButton ,segueID : String){
        isThankyouClink = false
        let objClinkList = arrClinkList[sender.tag]
        intCurrentIndex = sender.tag
        if objClinkList.strOriginalClinkId.isEmptyString() == false
        {
            let indexPath = NSIndexPath(forRow:sender.tag, inSection: INTZERO)
            let cell = tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
            objSelectedClinkDetail = arrClinkList[sender.tag]
            if cell.imgThankyou.image != nil{
                self.loadTYData(sender,strSegue:segueID)
            }
            else{
                Helper.showAlert(ALERTPLEASEWAITTILLLOADINGTYIMAGE, pobjView: self.view)
            }
        }
        else{
            objSelectedTYClinkDetail = nil
            imgSelectedTYImage = nil
            if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == true{
                currentClinkType = ClinkType.Received
            }
            else {
                currentClinkType = ClinkType.Sent
            }
            self.manageSelectedClink(sender,pclinkType: currentClinkType,segue : segueID)
        }
    }
    
    @IBAction func btnShowClinkDetailsTapped(sender: UIButton) {
        sender.enabled = false
        
        let indexPath = NSIndexPath(forRow:sender.tag, inSection: INTZERO)
        let cell = tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
        objSelectedClinkDetail = arrClinkList[sender.tag]
        if cell.imgClink.image != nil{
            imgSelectedImage =  cell.imgClink.image
            intCurrentIndex = sender.tag
            self.performSegueWithIdentifier(SEGUETOCLINKDETAILS, sender: nil)
        }
        else{
            Helper.showAlert(ALERTPLEASEWAITTILLLOADINGTYIMAGE, pobjView: self.view)
        }
        sender.enabled = true
    }
    
    func manageSelectedClink (pSender : UIButton ,pclinkType : ClinkType,segue : String){
        
        let indexPath = NSIndexPath(forRow:pSender.tag, inSection: INTZERO)
        let cell = tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
        
        objSelectedClinkDetail = arrClinkList[pSender.tag]
        if cell.imgClink.image != nil{
            imgSelectedImage = cell.imgClink.image!
            tblClinkDetails.reloadRowsAtIndexPaths(
                [indexPath], withRowAnimation: .None)
            performSegueWithIdentifier(segue, sender: nil)
        }
        else{
            Helper.showAlert(ALERTPLEASEWAITTILLLOADINGIMAGE, pobjView: self.view)
        }
    }
    
    @IBAction func showCommingsoonAlert(sender: AnyObject) {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: self.view)
    }
    
    @IBAction func btnCommentTapped(sender: UIButton) {
        shouldAutoLike = false
        shouldOpenComment = true
        self.performActionTap(sender, segueID: SEGUETOTHREADVIEW)
    }
    
    // MARK: - LIKE CLINLK!

    @IBAction func btnLikeTapped(sender: UIButton) {
//        shouldAutoLike = true
//        shouldOpenComment = false
//        self.performActionTap(sender, segueID: SEGUETOTHREADVIEW)
        
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == false {
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: self.view)
            return
        }
        
        sender.userInteractionEnabled = false
        
        if arrClinkList.count > sender.tag{
            let indexPath = NSIndexPath(forRow:sender.tag, inSection: 0)
            let cell = tblClinkDetails.cellForRowAtIndexPath(indexPath) as! ClinkListCell
            objSelectedClinkDetail = arrClinkList[sender.tag]
            
            if cell.imgClink.image != nil{
                imgSelectedImage = cell.imgClink.image!
                tblClinkDetails.reloadRowsAtIndexPaths(
                    [indexPath], withRowAnimation: .None)
                self.likeClink(sender)
            }
            else{
                Helper.showAlert(ALERTPLEASEWAITTILLLOADINGIMAGE, pobjView: self.view)
            }
        }
    }
    
    func likeClink(sender: UIButton) {
        
        if self.objSelectedClinkDetail != nil{
            
            if (self.objSelectedClinkDetail.isLiked != true){
                
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkLike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        print("Like CLink")
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getClinkDetails(sender)
                        }
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
            else{
                ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.ClinkUnlike(objSelectedClinkDetail.strId).URLRequest, isBackgroundCall : false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                    
                    if success == true{
                        
                        print("Unlike CLink")
                        dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)){
                            self.getClinkDetails(sender)
                        }
                    }
                    else if response != nil{
                        sender.userInteractionEnabled = true
                        Helper.showAlert((response as? String)!, pobjView: self.view)
                    }
                })
            }
        }
    }
    
    func getClinkDetails(sender: UIButton)
    {
        if self.objSelectedClinkDetail != nil{
            ClinkDetailsManager.sharedInstance.callWebService(ClinkDetailsRouter.GetClinkWithId(objSelectedClinkDetail.strId,nil).URLRequest, isBackgroundCall:false, pAlertView: self.view, pViewController: self, pCompletionBlock: { (success, response) -> () in
                
                if success == true{
                    
                    if let dicResponse = response as? [String : AnyObject] {
                        
                        sender.userInteractionEnabled = true
                        
                        if let error = dicResponse.getValueIfAvilable(KMESSAGE) {
                            print("Error :- \(error)")
                            return
                        }
                        else{
                            self.objSelectedClinkDetail = ClsClinkDetails(pdictResponse: dicResponse)
                            self.arrClinkList[sender.tag] = self.objSelectedClinkDetail
//                            self.arrClinkList.insert(self.objSelectedClinkDetail, atIndex: sender.tag)
                            self.configureTableView()
                        }
                    }
                }
                else if response != nil{
                    sender.userInteractionEnabled = true
                    Helper.showAlert((response as? String)!, pobjView: self.view)
                }
            })
        }
    }
    
    @IBAction func btnAddGiftTapped(sender: UIButton) {
        shouldAutoLike = false
        shouldOpenComment = false
        controllerType = ClinkControllerType.Gift
        let objClinkDetails = arrClinkList[sender.tag]
        
        var arrCCTemp : [ClsContact]!
        arrCCTemp = Array()
        
        var isCCUser : Bool = false
        
        if objClinkDetails.arrCCUser != nil && objClinkDetails.arrCCUser.count > 0 {
            
            arrCCTemp = objClinkDetails.arrCCUser
        }
        
        for item in arrCCTemp {
            if item.strId == USERDEFAULTS.getUserDetailsObject().strId {
                isCCUser = true
                break
            }
        }
        
        if isCCUser == true {
            Helper.showAlert("Can't add gift amount on clink in which you are in cc.", pobjView: self.view)
        }
        else if objClinkDetails.strReceiverId != USERDEFAULTS.getUserDetailsObject().strId{
            self.performActionTap(sender,segueID: SEGUETOSENDNEWCLINK)
        }
        else{
            Helper.showAlert("Unable to add gift amount on your own clink", pobjView: self.view)
        }
    }
    
    @IBAction func btnSendClinkFTU(sender: UIButton) {
        APPDELEGATE.objSideMenu.showSendNewClink()
    }
    // MARK: - Memory Management
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == SEGUETOSENDNEWCLINK{
            let objSendNewClink = segue.destinationViewController as! SendNewClinkController
            objSendNewClink.controllerType = controllerType
            objSendNewClink.previousScreenType = currentClinkType
            objSendNewClink.objSelectedClinkDetail = self.objSelectedClinkDetail
            
        }
        else if segue.identifier == SEGUETOTHREADVIEW{
            let objClinkThreadDetails = segue.destinationViewController as! ClinkThreadController
            
            if self.objSelectedClinkDetail != nil{
                objClinkThreadDetails.objSelectedClinkDetail = objSelectedClinkDetail
                objClinkThreadDetails.selectedClinkType = currentClinkType
                objClinkThreadDetails.shouldAutoLike = shouldAutoLike
                objClinkThreadDetails.shouldOpenComment = shouldOpenComment
                objClinkThreadDetails.currentClinkType = currentClinkType
                if currentClinkType == ClinkType.Thankyou
                {
                    objClinkThreadDetails.isUpdateNeeded = false
                }
                else{
                    objClinkThreadDetails.isUpdateNeeded = true
                }
                if imgSelectedImage != nil{
                    objClinkThreadDetails.imgSelected = imgSelectedImage
                }
                
            }
            if self.objSelectedTYClinkDetail != nil{
                objClinkThreadDetails.objSelectedTYClinkDetail = objSelectedTYClinkDetail
                if imgSelectedTYImage != nil{
                    objClinkThreadDetails.imgSelectedTYImage = imgSelectedTYImage
                }
            }
            
            if intCurrentIndex != nil{
                objClinkThreadDetails.intCurrentIndex =  intCurrentIndex
            }
        }
        else if segue.identifier == SEGUETOCLINKDETAILS{
            let objClinkDetails = segue.destinationViewController as! ClinkDetailsController
            if self.objSelectedClinkDetail != nil{
                objClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                objClinkDetails.arrClinkDetails = self.arrClinkList
                objClinkDetails.isThankyouClink = self.isThankyouClink
                if imgSelectedImage != nil{
                    objClinkDetails.imgSelected = imgSelectedImage
                    objClinkDetails.isUpdateNeeded = true
                    if USERDEFAULTS.boolForKey(KRECEIVEDCLINKSELECTED) == true{
                        objClinkDetails.selectedClinkType = ClinkType.Received
                    }
                    else{
                        objClinkDetails.selectedClinkType = ClinkType.Sent
                    }
                    objClinkDetails.isFromFeedScreen = false
                }
                if intCurrentIndex != nil{
                    objClinkDetails.intCurrentIndex =  intCurrentIndex
                }
            }
        }
        else if segue.identifier == SEGUETOADDGIFTVIEW{
            let objGiftClinkDetails = segue.destinationViewController as! AddGiftController
            if self.objSelectedClinkDetail != nil{
                objGiftClinkDetails.objSelectedClinkDetail = objSelectedClinkDetail
                if imgSelectedImage != nil{
                    objGiftClinkDetails.imgSelected = imgSelectedImage
                }
            }
        }
        
    }
    
    // MARK: - Unwind Segue
    @IBAction func updateReadCountClinkListVC(segue:UIStoryboardSegue) {
        if currentClinkType == ClinkType.Received{
            objSelectedClinkDetail.isRead = true
        }
        else if currentClinkType == ClinkType.Sent{
//            objSelectedClinkDetail.hasNewActivity = false
        }
        shouldAutoLike = false
        shouldOpenComment = false
//        arrClinkList.removeAll()

        if segue.sourceViewController.isKindOfClass(ClinkThreadController) {
            let vc = segue.sourceViewController as! ClinkThreadController
            let objSelectedClinkNew = vc.objSelectedClinkDetail
            let intIndex = vc.intCurrentIndex
            arrClinkList[intIndex] = objSelectedClinkNew
            self.configureTableView()
            //            tblFeedList.reloadRowsAtIndexPaths([NSIndexPath.init(forRow: intIndex, inSection: 0)], withRowAnimation: UITableViewRowAnimation.None)
        }
        
        // 2016-12-20
        self.configureTableView()
//        self.getClinkData(intCurrentPage*RECORDSPERPAGE,pCurrentPage: 1)
        
    }
    
    @IBAction func reloadData(segue:UIStoryboardSegue) {
        self.getClinkData(RECORDSPERPAGE,pCurrentPage: self.intCurrentPage)
    }
    
}
