//
//  AddPayPalController.swift
//  Clink
//
//  Created by Dhruvit on 22/10/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

class AddPayPalController: UIViewController {

    @IBOutlet var mainView: UIView!
    @IBOutlet var bankDetailView: UIView!
    @IBOutlet var actionView: UIView!
    @IBOutlet var backView: UIView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var addView: UIView!
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet var keyPadView: UIView!
    @IBOutlet var infoView: UIView!
    @IBOutlet var lblView: UIView!
    @IBOutlet var lbl_title: UILabel!
    
    @IBOutlet weak var txtUserName: TextFieldPlaceHolder!
    @IBOutlet weak var txtPassword: TextFieldPlaceHolder!
    
    var arrayTextField : [TextFieldPlaceHolder]! = Array()

    var txtCurrent : UITextField! = UITextField()

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        txtUserName.tag = 1
        txtPassword.tag = 2
        
        txtCurrent = txtUserName
        
        arrayTextField = [txtUserName,txtPassword]
        
        txtPassword.secureTextEntry = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ClinkDetailsManager.sharedInstance.clearImageCache()
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.tag == 2{
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        for txtField in arrayTextField {
            txtField.layer.borderColor = UIColor.clearColor().CGColor
            txtField.layer.borderWidth = 0
        }
        
        textField.layer.borderColor = COLORTHEME.CGColor
        textField.layer.borderWidth = 1

        if textField.tag == 1
        {
            txtCurrent = txtUserName
            return false
        }
        else
        {
            txtCurrent = txtPassword
            return false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func actionInputButton(sender:UIButton){
        
        var inputVal = STREMPTY
        
        switch sender.tag{
            
        case 0 :
            inputVal = STR0
        case 1 :
            inputVal = STR1
        case 2 :
            inputVal = STR2
        case 3 :
            inputVal = STR3
        case 4 :
            inputVal = STR4
        case 5 :
            inputVal = STR5
        case 6 :
            inputVal = STR6
        case 7 :
            inputVal = STR7
        case 8 :
            inputVal = STR8
        case 9 :
            inputVal = STR9
        case 10 :
            inputVal = STRPOINT
        default:
            inputVal = STREMPTY
        }
        
        if txtCurrent.text == STR0{
            txtCurrent.text! = inputVal
        }
        else
        {
            txtCurrent.text! += inputVal
        }
    }
    
    @IBAction func actionRemove(sender:UIButton){
        
        if ((txtCurrent.text?.isEmptyString()) == false)
        {
            let strAmount = String(txtCurrent.text!.characters.dropLast()) as String
            txtCurrent.text = strAmount
            
            if ((txtCurrent.text?.isEmptyString()) == true){
                txtCurrent.text = STREMPTY
            }
        }
    }
    
    @IBAction func backClicked(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addClick(sender: AnyObject) {
        
        if txtUserName.text!.isEmpty == true || txtPassword.text!.isEmpty == true{
            
            txtUserName.setPlaceHolder(txtUserName.placeholder!, color: UIColor.redColor())
            txtPassword.setPlaceHolder(txtPassword.placeholder!, color: UIColor.redColor())
        }

    }
}
