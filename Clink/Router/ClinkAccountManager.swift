//
//  ClinkAuthenticationManager.swift
//  Clink
//
//  Created by Gunjan on 24/08/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration

enum ClinkAccountRouter: URLRequestConvertible {
    
    static let baseURLString = KBASEURL
    static var OAuthToken: String?
    static var useBasicAuth : Bool?
    static var SessionID: String?
    
    case VarifyEmail([String: AnyObject])
    case CreateBank([String: AnyObject])
    case SearchBank([String: AnyObject])
    case GetBank(String)
    case UpdateBank(String,[String: AnyObject])
    case DeleteBank(String)
    case CreateCard([String: AnyObject])
    case SearchCard([String: AnyObject])
    case GetCard(String)
    case UpdateCard(String,[String: AnyObject])
    case DeleteCard(String)
    case MakePayment([String: AnyObject])
    case TransferToAccount([String: AnyObject])
    case getClinkTransactionHistory([String: AnyObject])
    case getClinkAccountHistory([String: AnyObject])
    case getClinkRedeemHistory([String: AnyObject])
    
    var method: Alamofire.Method {
        switch self {
        case .VarifyEmail:
            return .POST
        case .CreateBank:
            return .POST
        case .SearchBank:
            return .GET
        case .GetBank:
            return .GET
        case .UpdateBank:
            return .PUT
        case .DeleteBank:
            return .DELETE
        case .CreateCard:
            return .POST
        case .SearchCard:
            return .GET
        case .GetCard:
            return .GET
        case .UpdateCard:
            return .PUT
        case .DeleteCard:
            return .DELETE
        case .MakePayment:
            return .POST
        case .TransferToAccount:
            return .POST
        case .getClinkTransactionHistory:
            return .GET
        case .getClinkAccountHistory:
            return .GET
        case .getClinkRedeemHistory:
            return .GET
        }
    }
    
    var path: String {
        switch self {
        case .VarifyEmail:
            return "/v1/auth/requestverifytoken"
        case .CreateBank:
            return "/v1/bank"
        case .SearchBank:
            return "/v1/bank"
        case .GetBank(let bankId):
            return "/v1/bank/\(bankId)"
        case .UpdateBank(let bankId,_):
            return "/v1/bank/\(bankId)"
        case .DeleteBank(let bankId):
            return "/v1/bank/\(bankId)"
        case .CreateCard:
            return "/v1/card"
        case .SearchCard:
            return "/v1/card"
        case .GetCard(let cardId) :
            return "/v1/card/\(cardId)"
        case .DeleteCard(let cardId):
            return "/v1/card/\(cardId)"
        case .UpdateCard(let cardId,_):
            return "/v1/card/\(cardId)"
        case .MakePayment:
            return "/v1/charge"
        case .TransferToAccount:
            return "/v1/transfer"
        case .getClinkTransactionHistory:
            return "/v1/transaction"
        case .getClinkAccountHistory:
            return "/v1/clink"
        case .getClinkRedeemHistory:
            return "/v1/redemption"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        
        let URL = NSURL(string: ClinkDetailsRouter.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path)!)
        mutableURLRequest.HTTPMethod = method.rawValue
        let strDate = NSDate().getNewUTCDate().getUTCFormatedString("yyyyMMdd'T'HHmmss")
        let basicAuthString = Helper.getBasicAuthString(strDate, strMethod: method.rawValue, strPath: path, strNonse: "92345")
        mutableURLRequest.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("92345", forHTTPHeaderField: "nonce")
        mutableURLRequest.setValue(strDate, forHTTPHeaderField: "timestamp")
        
        if let token = ClinkDetailsRouter.OAuthToken
        {
            mutableURLRequest.setValue(token, forHTTPHeaderField: KAUTHORIZATION)
        }
        if let session = ClinkDetailsRouter.SessionID
        {
            mutableURLRequest.setValue(session, forHTTPHeaderField: KSESSIONID)
        }
        else if let session = USERDEFAULTS.objectForKey(KSESSIONID){
            ClinkDetailsRouter.SessionID  = session as? String
            mutableURLRequest.setValue(ClinkDetailsRouter.SessionID , forHTTPHeaderField: KSESSIONID)
        }
        print("Session id is : \(ClinkDetailsRouter.SessionID!)")
        
        switch self {
        case .VarifyEmail(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .CreateBank(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .SearchBank(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .GetBank(_):
            return mutableURLRequest
        case .UpdateBank(_,let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .DeleteBank(_):
            return mutableURLRequest
        case .CreateCard(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .SearchCard(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .GetCard(_):
            return mutableURLRequest
        case .UpdateCard(_,let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .DeleteCard(_):
            return mutableURLRequest
        case .MakePayment(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .TransferToAccount(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .getClinkTransactionHistory(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .getClinkAccountHistory(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .getClinkRedeemHistory(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        }
    }
}


class ClinkAccountManager: NSObject {
    
    class var sharedInstance: ClinkAccountManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ClinkAccountManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            
            let objSharedInstance = ClinkAccountManager()
            Static.instance = objSharedInstance
        }
        return Static.instance!
    }
    
    func sendVerificationEmail(strEmail : String , pAlertView : UIView, pViewController : UIViewController?,pCompletionBlock:ClinkCompletionBlock) {
        self.callWebService(ClinkAccountRouter.VarifyEmail(["email":strEmail]).URLRequest, isBackgroundCall: false, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: pCompletionBlock)
    }
    
    func callWebService(URLRequest:NSMutableURLRequest, isBackgroundCall : Bool , pAlertView : UIView, pViewController : UIViewController?,pCompletionBlock:ClinkCompletionBlock) {
        ClinkAuthenticationManager.sharedInstance.callWebService(URLRequest, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: pCompletionBlock)
    }
}
