//
//  ClinkAuthenticationManager.swift
//  Clink
//
//  Created by Gunjan on 24/08/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import FBSDKLoginKit
import Fabric
import TwitterKit
typealias ClinkCompletionBlock = (Bool,AnyObject!) -> ()

enum AuthenticateRouter: URLRequestConvertible {
    
    static let baseURLString = KBASEURL
    static var OAuthToken: String?
    static var useBasicAuth : Bool?
    
    case SignUpUser([String: AnyObject])
    case SignUpWithToken([String: AnyObject])
    case LoginUser([String: AnyObject])
    case LoginWithToken([String: AnyObject])
    case ForgotPassword([String: AnyObject])
    case ResetPassword([String: AnyObject])
    case VerifyUser([String: AnyObject])
    
    var method: Alamofire.Method {
        switch self {
        case .SignUpUser:
            return .POST
        case .SignUpWithToken:
            return .POST
        case .LoginUser:
            return .POST
        default :
            return .POST
        }
    }
    
    var path: String {
        switch self {
        case .SignUpUser:
            return "/v1/auth/register"
        case .SignUpWithToken:
            return "/v1/auth/registerwithtoken"
        case .LoginUser:
            return "/v1/auth/login"
        case .LoginWithToken:
            return "/v1/auth/loginusertoken"
        case .ForgotPassword:
            return "/v1/auth/forgotpassword"
        case .ResetPassword:
            return "/v1/auth/resetpassword"
        case .VerifyUser:
            return "/v1/auth/verify"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        let URL = NSURL(string: AuthenticateRouter.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path)!)
        mutableURLRequest.HTTPMethod = method.rawValue
        let strDate = NSDate().getNewUTCDate().getUTCFormatedString("yyyyMMdd'T'HHmmss")
        let basicAuthString = Helper.getBasicAuthString(strDate, strMethod: method.rawValue, strPath: path, strNonse: "92345")
        mutableURLRequest.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("92345", forHTTPHeaderField: "nonce")
        mutableURLRequest.setValue(strDate, forHTTPHeaderField: "timestamp")
        
        switch self {
        case .SignUpUser(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .SignUpWithToken(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .LoginUser(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .LoginWithToken(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .ForgotPassword(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .ResetPassword(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .VerifyUser(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0

            //        default:
            //            return mutableURLRequest
        }
    }
}


class ClinkAuthenticationManager: NSObject {
    
    class var sharedInstance: ClinkAuthenticationManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ClinkAuthenticationManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            
            let objSharedInstance = ClinkAuthenticationManager()
            Static.instance = objSharedInstance
        }
        return Static.instance!
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func callWebService(URLRequest:NSMutableURLRequest, isBackgroundCall : Bool , pAlertView : UIView?, pViewController : UIViewController?,pCompletionBlock:ClinkCompletionBlock) {
        if isConnectedToNetwork() == true{
            
            if isBackgroundCall == false {
                Helper.showHud(pAlertView!)
            }

            //            Alamofire.request(URLRequest).responseString(completionHandler: { (strResponse) -> Void in
            //                 Helper.hideHud(pAlertView)
            //                print("Success with JSON: \(strResponse)")
            //            })
            //            println(NSJSONSerialization.JSONObjectWithData(URLRequest.HTTPBody!, options: nil))
            
            print("Request URL : " + URLRequest.URLString)
            if URLRequest.HTTPMethod == "POST"{
                print("Request Param : " + self.getParameterString(URLRequest))
            }
            else if URLRequest.HTTPMethod == "PUT"{
                print("Request Param : " + self.getParameterString(URLRequest))
            }
            
            Alamofire.request(URLRequest).responseJSON(options: .AllowFragments, completionHandler: { (response) -> Void in
                ClinkAuthenticationManager.sharedInstance.parseResponse(response, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (success, result) -> () in
                    pCompletionBlock(success, result)
                })
            })
        }
        else if isBackgroundCall == false {
            pCompletionBlock(false, nil)
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: pAlertView)
        }
    }
    // MARK: - Facebook login
    
    func signInWithFacebook(parentViewController : UIViewController ,isSigningUp : Bool ,pCompletionBlock : ClinkManagerCompletionBlock) {
        let login: FBSDKLoginManager = FBSDKLoginManager()
        login.logOut()
        let permissionsArray : Array = [KPUBLICPROFILE,KEMAIL]
        Helper.showHud(parentViewController.view)
        login.logInWithReadPermissions(permissionsArray, fromViewController: parentViewController, handler: {(result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            
            Helper.hideHud(parentViewController.view)
            if error != nil{
                print(error)
            }
            else if result.isCancelled{
                print("Cancelled")
                pCompletionBlock(false,"Facebook login cancelled by the user.")
            }
            else{
                print("Logged in")
//                let fbloginresult : FBSDKLoginManagerLoginResult = result
//                if(fbloginresult.grantedPermissions.contains(KEMAIL))
//                {
                
                self.getFacebookUserDetails(parentViewController, isSigningUp:isSigningUp , pCompletionBlock: {(success, response) -> () in
                        
                        pCompletionBlock(success,response)
                    })
//                }
//                else{
//                    pCompletionBlock(false,"Failed to get email address of the user.")
//                }
            }
        })
    }
    
    func getFacebookUserDetails(parentViewController : UIViewController, isSigningUp : Bool, pCompletionBlock : ClinkManagerCompletionBlock){
        let requestParameters = ["fields": "id, name,first_name, last_name, email"]
        
        Helper.showHud(parentViewController.view)
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: KMEGRAPHME, parameters: requestParameters).startWithCompletionHandler({ (connection, result, error) -> Void in
                Helper.hideHud(parentViewController.view)
                if (error != nil){
                    pCompletionBlock(false,error?.localizedDescription)
                    print("\(error.localizedDescription)")
                    return
                }
                else if(result != nil)
                {
                    let strUserId : String? = result[KFBUSERID] as? String
                    
                    var userDetails : [String : AnyObject] = [KTOKEN :strUserId!, KSOURCE : KSOURCEFACEBOOK]
                    if USERDEFAULTS.valueForKey(KDEVICETOKEN) != nil{
                        userDetails[KDEVICETOKEN] = USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String
                    }
                    else {
                        userDetails[KDEVICETOKEN] = ""
                    }
                    
                    if isSigningUp == true{
                        let strFirstName:String? = result[KFBFIRSTNAME] as? String
                        let strLastName:String? = result[KFBLASTNAME] as? String
                        let strEmail:String? = result[KEMAIL] as? String
                        
                        userDetails[KFIRSTNAME] = strFirstName!
                        userDetails[KLASTNAME] = strLastName
                        userDetails[KEMAIL] = strEmail
                        
                        ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.SignUpWithToken(userDetails).URLRequest, isBackgroundCall : false, pAlertView : parentViewController.view, pViewController: parentViewController, pCompletionBlock: { (success, response) -> () in
                                pCompletionBlock(success,response)                            
                        })
                    }
                    else{
                        ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.LoginWithToken(userDetails).URLRequest, isBackgroundCall : false, pAlertView : parentViewController.view, pViewController: parentViewController, pCompletionBlock: { (success, response) -> () in
                            pCompletionBlock(success,response)
                        })
                    }
                }
            })
        }
    }
    
    // MARK: - Twitter login
    func signInWithTwitter(viewcontroller : UIViewController ,isSigningUp : Bool, pCompletionBlock:ClinkManagerCompletionBlock) {
        
        Helper.showHud(viewcontroller.view)
        Twitter.sharedInstance().logInWithMethods([.WebBased]) { session, error in            
            Helper.hideHud(viewcontroller.view)
            if error == nil{
                var userDetails : [String : AnyObject] = [KTOKEN : session!.authToken, KSOURCE : KSOURCETWITTER]
                if USERDEFAULTS.valueForKey(KDEVICETOKEN) != nil{
                    userDetails[KDEVICETOKEN] = USERDEFAULTS.valueForKey(KDEVICETOKEN) as? String
                }
                else {
                    userDetails[KDEVICETOKEN] = ""
                }
                
                if isSigningUp == true{
                    let strFname = session!.userName
                    userDetails[KFIRSTNAME] = strFname
                    userDetails[KLASTNAME] = " "
                    ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.SignUpWithToken(userDetails).URLRequest, isBackgroundCall : false, pAlertView : viewcontroller.view, pViewController: viewcontroller, pCompletionBlock: { (success, response) -> () in
                        pCompletionBlock(success,response)
                    })
                }
                else{
                    ClinkAuthenticationManager.sharedInstance.callWebService(AuthenticateRouter.LoginWithToken(userDetails).URLRequest, isBackgroundCall : false, pAlertView : viewcontroller.view, pViewController: viewcontroller, pCompletionBlock: { (success, response) -> () in
                        pCompletionBlock(success,response)
                    })
                }
            }
            else{
                pCompletionBlock(false,error?.localizedDescription)
            }
        }
    }
    
    func getTwitterData(pCompletionBlock:ClinkManagerCompletionBlock){
        /*
        let client = TWTRAPIClient.clientWithCurrentUser()
        let request = client.URLRequestWithMethod("GET",
        URL: "https://api.twitter.com/1.1/account/verify_credentials.json",
        parameters: ["include_email": "true", "skip_status": "true"],
        error: nil)
        
        client.sendTwitterRequest(request) { response, data, connectionError in
        
        if connectionError == nil{
        
        }
        else{
        
        }
        
        }*/
        
    }
    
    func showLoginScreen(viewcontroller : UIViewController,alertString : String)
    {
        var mainView: UIStoryboard!
        mainView = UIStoryboard(name: AUTHENTICATIONSTORYBOARD, bundle: nil)
        let loginVC : UIViewController = mainView.instantiateViewControllerWithIdentifier(CONTROLLERLOGIN) as UIViewController
        viewcontroller.presentViewController(loginVC, animated: false,completion: { () -> Void in
            if alertString.isEmptyString() == false{
                Helper.showAlert(alertString, pobjView: loginVC.view)
            }
        })
        
    }
    
    func parseResponse(response : Response<AnyObject, NSError>, isBackgroundCall : Bool, pAlertView : UIView?, pViewController : UIViewController?, pCompletionBlock:ClinkCompletionBlock){
        
        switch response.result{
        case .Success(let JSON):
            let dicResponse = JSON as! [String : AnyObject]
            print("dicResponse with JSON: \(dicResponse)")
            if isBackgroundCall == false {
                Helper.hideHud(pAlertView!)
            }
            
            let errorMessage = dicResponse[KMESSAGES] as? [AnyObject!]
            let dictJsonData = dicResponse[KDATA] as? [String : AnyObject]
            if(errorMessage!.count <= 0  && dictJsonData != nil && dictJsonData!.count > 0){
                
                pCompletionBlock(true,dictJsonData)
            }
            else if(errorMessage!.count != 0){
                let dicError = errorMessage![0] as! [String : AnyObject]
                let intErrorCode = dicError[KCODE] as! Int
                if intErrorCode == REQUESTSUCCESS{
                    pCompletionBlock(true,dictJsonData)
                }
                else if intErrorCode == REQUESTRESULTNOTFOUND{                    
                    pCompletionBlock(true,dicError)
                }
                else if intErrorCode == SESSIONEXPIRED{
                    USERDEFAULTS.removeObjectForKey(kISUSERLOGGEDIN)
                    USERDEFAULTS.removeObjectForKey(KUSERDETAILS)
                    USERDEFAULTS.removeObjectForKey(KSESSIONID)
                    USERDEFAULTS.synchronize()
                    ClinkAuthenticationManager.sharedInstance.showLoginScreen(pViewController!,alertString: dicError[KMESSAGE] as! String)
                }
                else{
                    pCompletionBlock(false,dicError[KMESSAGE] as! String)
                }
            }
            
        case .Failure(let error):
            print("Request failed with error: \(error)")
            if isBackgroundCall == false {
                Helper.hideHud(pAlertView!)
            }

            pCompletionBlock(false,error.localizedDescription)
        }
    }
    
    func getParameterString(URLRequest: NSMutableURLRequest) -> String{
        if URLRequest.HTTPBody != nil{
            return String(data: URLRequest.HTTPBody!, encoding:NSUTF8StringEncoding)!
        }
         return ""
    }
    
    func logoutFromSocial(){
        self.logoutFromFacebook()
        self.logoutFromTwitter()
    }
    
    func logoutFromFacebook(){
        let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
    }
    
    func logoutFromTwitter(){
        let store = Twitter.sharedInstance().sessionStore
        if let userid = store.session()?.userID {
            Twitter.sharedInstance().sessionStore.logOutUserID(userid)
        }
    }
}
