//
//  ClinkTrackerManager.swift
//  Clink
//
//  Created by Dhruvit on 04/11/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
enum ImageSourceType : String{
    case Camera = "Camera", Gallery = "Gallery"
}

enum FlowTracker : String{
    case AddedImageFromCamera = "Added image from camera"
    case AddedImageFromGallery = "Added image from gallery"
    case UsedDrawingTool = "Used drawing tool"
    case UsedEraseTool = "Used erase tool"
    case UsedClearTool = "Used clear tool"
    case AppliedFilter = "Added filter on image"
    case AddedText = "Added text on image"
    case SavedImage = "Saved image in gallery"
    case OpenedAmountScreen = "Opened amount screen"
    case OpenedMerchantScreen = "Opened merchant screen"
    case AddedAmount = "Added amount on the clink"
    case AddedMerchant = "Added merchant on the clink"
    case OpenedConfirmationPopup = "Opened confirmation popup"
    case OpendContactToaddToUser = "Opend contact to add To User"
    case OpendContactToaddCCUser = "Opend contact to add CC Users"
    
}

enum PageViewTracker : String{
    case Home = "Opened home page"
    case Sent = "Opened sent page"
    case Received = "Opened received page"
    case SendNewClink = "Opened send new page"
    case Contact = "Opened contact page"
    case Partner = "Opened partner page"
    case PartnerDetails = "Opened partner details page"
    case Promotion = "Opened promotion page"
    case Thread = "Opened thread page"
    case ClinkDetails = "Opened Clink details page"
    
}

enum EditingToolTracker : String{
    case Text = "Text"
    case Pen = "Pen"
    case Color = "Color"
    case Filter = "Filter"
}

enum ClinkTrackerRouter: URLRequestConvertible {
    
    static let baseURLString = KBASEURL
    static var OAuthToken: String?
    
    case CreateTrackerGroup([String: AnyObject])
    case SearchTrackerGroup([String: AnyObject])
    case GetTrackerGroup(String)
    case DeleteTrackerGroup(String)
    case CreateTracker([String: AnyObject])
    case SearchTracker([String: AnyObject])
    case GetTracker(String)
    case DeleteTracker(String)
    
    var method: Alamofire.Method {
        switch self {
        case .CreateTrackerGroup:
            return .POST
        case .SearchTrackerGroup:
            return .GET
        case .GetTrackerGroup:
            return .GET
        case .DeleteTrackerGroup:
            return .DELETE
        case .CreateTracker:
            return .POST
        case .SearchTracker:
            return .GET
        case .GetTracker:
            return .GET
        case .DeleteTracker:
            return .DELETE
        }
    }
    
    var path: String {
        switch self {
        case .CreateTrackerGroup:
            return "/v1/trackergroup"
        case .SearchTrackerGroup:
            return "/v1/trackergroup"
        case .GetTrackerGroup(let trackerGroupId):
            return "/v1/trackergroup/\(trackerGroupId)"
        case .DeleteTrackerGroup(let trackerGroupId):
            return "/v1/trackergroup/\(trackerGroupId)"
        case .CreateTracker:
            return "/v1/tracker"
        case .SearchTracker:
            return "/v1/tracker"
        case .GetTracker(let trackerId):
            return "/v1/tracker/\(trackerId)"
        case .DeleteTracker(let trackerId):
            return "/v1/tracker/\(trackerId)"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        let URL = NSURL(string: ClinkDetailsRouter.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path)!)
        mutableURLRequest.HTTPMethod = method.rawValue
        let strDate = NSDate().getNewUTCDate().getUTCFormatedString("yyyyMMdd'T'HHmmss")
        let basicAuthString = Helper.getBasicAuthString(strDate, strMethod: method.rawValue, strPath: path, strNonse: "92345")
        mutableURLRequest.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("92345", forHTTPHeaderField: "nonce")
        mutableURLRequest.setValue(strDate, forHTTPHeaderField: "timestamp")
        
        if let token = ClinkDetailsRouter.OAuthToken
        {
            mutableURLRequest.setValue(token, forHTTPHeaderField: KAUTHORIZATION)
        }
        if let session = ClinkDetailsRouter.SessionID
        {
            mutableURLRequest.setValue(session, forHTTPHeaderField: KSESSIONID)
        }
        else if let session = USERDEFAULTS.objectForKey(KSESSIONID){
            ClinkDetailsRouter.SessionID  = session as? String
            mutableURLRequest.setValue(ClinkDetailsRouter.SessionID , forHTTPHeaderField: KSESSIONID)
        }
        print("Session id is : \(ClinkDetailsRouter.SessionID!)")
        
        switch self {
        case .CreateTrackerGroup(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .SearchTrackerGroup(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .GetTrackerGroup(_):
            return mutableURLRequest
        case .DeleteTrackerGroup(_):
            return mutableURLRequest
        case .CreateTracker(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .SearchTracker(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .GetTracker(_):
            return mutableURLRequest
        case .DeleteTracker(_):
            return mutableURLRequest
        }
    }
}

class ClinkTrackerManager: NSObject {
    
    class var sharedInstance: ClinkTrackerManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ClinkTrackerManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            
            let objSharedInstance = ClinkTrackerManager()
            Static.instance = objSharedInstance
        }
        return Static.instance!
    }
    
    func callWebService(URLRequest:NSMutableURLRequest, pAlertView : UIView?,pViewController : UIViewController?, pCompletionBlock:ClinkCompletionBlock) {
        ClinkAuthenticationManager.sharedInstance.callWebService(URLRequest, isBackgroundCall : true, pAlertView: pAlertView , pViewController: pViewController, pCompletionBlock: pCompletionBlock)
    }
    
}
