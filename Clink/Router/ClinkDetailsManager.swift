//
//  ClinkDetailsManager.swift
//  Clink
//
//  Created by Gunjan on 24/08/16.
//  Copyright © 2016 Administrator. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Fabric
import TwitterKit
import FBSDKShareKit
import FBSDKLoginKit

typealias ClinkManagerCompletionBlock = (Bool , AnyObject!) -> ()

enum ClinkDetailsRouter: URLRequestConvertible {
    
    static let baseURLString = KBASEURL
    static var OAuthToken: String?
    static var SessionID: String?
    
    case GetUserProfile()
    case UpdateUserProfile([String: AnyObject])
    case GetCategoryList()
    case GetClinkList([String: AnyObject])
    case GetVenueList([String: AnyObject])
    case GetVenueWithId(String)
    case GetVenueAllList()
    case GetClinkWithId(String,[String: AnyObject]?)
    case SendNewClink([String: AnyObject])
    case UploadImage(String)
    case GetHomeFeed([String: AnyObject])
    case UpdateClink(String,[String: AnyObject])
    case GetClinkLikeCount(String)
    case ClinkLike(String)
    case ClinkUnlike(String)
    case GetClinkComment(String,[String: AnyObject])
    case CreateClinkComment(String,[String: AnyObject])
    case GetAllPromotion()
    case GetPromotions(String)
    case LogoutUser()
    case CreateContactList([String: AnyObject],[String: AnyObject])
    case GetContactList([String: AnyObject])
    case DeleteComment(String,String)
    case UpdateComment(String,String,[String: AnyObject])
    case CreateClinkCommentLike(String)
    case DeleteClinkCommentLike(String)
    case DeleteClink(String)
    case DeleteContacts()
    case InviteContacts([String: AnyObject])
    
    var method: Alamofire.Method {
        switch self {
        case .GetUserProfile:
            return .GET
        case .UpdateUserProfile:
            return .PUT
        case .GetCategoryList:
            return .GET
        case .GetClinkList:
            return .GET
        case .GetVenueList:
            return .GET
        case .GetVenueWithId:
            return .GET
        case .GetVenueAllList:
            return .GET
        case .GetClinkWithId:
            return .GET
        case .SendNewClink:
            return .POST
        case .UploadImage:
            return .POST
        case GetHomeFeed:
            return .GET
        case .UpdateClink:
            return .PUT
        case .GetClinkLikeCount:
            return .GET
        case .ClinkLike:
            return .POST
        case .ClinkUnlike:
            return .DELETE
        case .GetClinkComment:
            return .GET
        case .CreateClinkComment:
            return .POST
        case .GetAllPromotion:
            return .GET
        case .GetPromotions:
            return .GET
        case .LogoutUser:
            return .POST
        case .GetContactList:
            return .GET
        case .CreateContactList:
            return .POST
        case .DeleteComment:
            return .DELETE
        case .UpdateComment:
            return .PUT
        case .CreateClinkCommentLike:
            return .POST
        case .DeleteClinkCommentLike:
            return .DELETE
        case .DeleteClink:
            return .DELETE
        case .DeleteContacts():
            return .DELETE
        case .InviteContacts:
            return .POST
        }
    }
    
    var path: String {
        switch self {
        case .GetUserProfile():
            return "/v1/profile"
        case .UpdateUserProfile:
            return "/v1/profile"
        case .GetCategoryList():
            return "/v1/category"
        case .GetVenueList(_):
            return "/v1/merchant"
        case .GetVenueAllList():
            return "/v1/merchant"
        case .GetClinkList(_):
            return "/v1/clink"
        case .GetVenueWithId(let merchantId):
            return "/v1/merchant/\(merchantId)"
        case .GetClinkWithId(let clinkId,_):
            return "/v1/clink/\(clinkId)"
        case .SendNewClink(_):
            return "/v1/clink"
        case .UploadImage(let clinkId):
                return "/v1/clink/\(clinkId)/photo"
        case .GetHomeFeed(_):
            return "/v1/clinkactivityfeed"
        case .UpdateClink(let clinkId,_):
            return "/v1/clink/\(clinkId)"
        case .GetClinkLikeCount(let clinkId):
            return "/v1/clink/\(clinkId)/like"
        case .ClinkLike(let clinkId):
            return "/v1/clink/\(clinkId)/like"
        case .ClinkUnlike(let clinkId):
            return "/v1/clink/\(clinkId)/like"
        case .GetClinkComment(let clinkId,_):
            return "/v1/clink/\(clinkId)/comment"
        case .CreateClinkComment(let clinkId,_):
            return "/v1/clink/\(clinkId)/comment"
        case .GetAllPromotion:
            return "/v1/promotion"
        case .GetPromotions(let promotionId):
            return "/v1/promotion/\(promotionId)"
        case .LogoutUser:
            return "/v1/auth/logout"
        case .GetContactList:
            return "/v1/contact"
        case .CreateContactList:
            return "/v1/contact"
        case .DeleteComment(let clinkId,let commentId):
            return "/v1/clink/\(clinkId)/comment/\(commentId)"
        case .UpdateComment(let clinkId,let commentId,_):
            return "/v1/clink/\(clinkId)/comment/\(commentId)"
        case .CreateClinkCommentLike(let commentId):
            return "/v1/clinkcomment/\(commentId)/like"
        case .DeleteClinkCommentLike(let commentId):
            return "/v1/clinkcomment/\(commentId)/like"
        case .DeleteClink(let clinkId):
            return "/v1/clink/\(clinkId)"
        case .DeleteContacts:
            return "/v1/contact"
        case .InviteContacts:
            return "/v1/invite"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        
        let URL = NSURL(string: ClinkDetailsRouter.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path)!)
        mutableURLRequest.HTTPMethod = method.rawValue
        let strDate = NSDate().getNewUTCDate().getUTCFormatedString("yyyyMMdd'T'HHmmss")
        let basicAuthString = Helper.getBasicAuthString(strDate, strMethod: method.rawValue, strPath: path, strNonse: "92345")
        mutableURLRequest.setValue("Basic \(basicAuthString)", forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("92345", forHTTPHeaderField: "nonce")
        mutableURLRequest.setValue(strDate, forHTTPHeaderField: "timestamp")
        
        if let token = ClinkDetailsRouter.OAuthToken
        {
            mutableURLRequest.setValue(token, forHTTPHeaderField: KAUTHORIZATION)
        }
        if let session = ClinkDetailsRouter.SessionID
        {
            mutableURLRequest.setValue(session, forHTTPHeaderField: KSESSIONID)
        }
        else if let session = USERDEFAULTS.objectForKey(KSESSIONID){
            ClinkDetailsRouter.SessionID  = session as? String
            mutableURLRequest.setValue(ClinkDetailsRouter.SessionID , forHTTPHeaderField: KSESSIONID)
        }
        print("Session id is : \(ClinkDetailsRouter.SessionID!)")
        switch self {
        case .GetUserProfile():
            return mutableURLRequest
        case .UpdateUserProfile(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .GetCategoryList():
            return mutableURLRequest
        case .GetVenueAllList():
            return mutableURLRequest
        case .GetVenueWithId(_):
            return mutableURLRequest
        case .GetVenueList(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .SendNewClink(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .GetClinkList(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .GetClinkWithId(_,let parameters):
            if parameters != nil{
                return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
            }
            else{
                return mutableURLRequest
            }
        case .GetHomeFeed(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .UpdateClink(_,let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .GetClinkLikeCount(_):
            return mutableURLRequest
        case .ClinkLike(_):
            return mutableURLRequest
        case .ClinkUnlike(_):
            return mutableURLRequest
        case .GetClinkComment(_,let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .CreateClinkComment(_,let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .GetAllPromotion:
            return mutableURLRequest
        case .GetPromotions(_):
            return mutableURLRequest
        case .LogoutUser():
            return mutableURLRequest
        case .GetContactList(let parameters):
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: parameters).0
        case .CreateContactList(let parameters,let queryParameters):
            let newRequest = Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
            newRequest.URL = NSURL(string:  newRequest.URLString + "?" + ClinkDetailsManager.sharedInstance.getQueryString(queryParameters))
            return newRequest
        case UploadImage(_):
            return mutableURLRequest
        case .DeleteComment(_,_):
            return mutableURLRequest
        case .UpdateComment(_, _, let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        case .CreateClinkCommentLike(_):
            return mutableURLRequest
        case .DeleteClinkCommentLike(_):
            return mutableURLRequest
        case .DeleteClink(_):
            return mutableURLRequest
        case .DeleteContacts():
            return mutableURLRequest
        case .InviteContacts(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0
        }
    }
}

class ClinkDetailsManager: NSObject,FBSDKSharingDelegate {
    
    class var sharedInstance: ClinkDetailsManager {
        
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ClinkDetailsManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            let objSharedInstance = ClinkDetailsManager()
            Static.instance = objSharedInstance
        }
        return Static.instance!
    }
    
    func callWebService(URLRequest:NSMutableURLRequest, isBackgroundCall : Bool, pAlertView : UIView, pViewController : UIViewController?,pCompletionBlock:ClinkCompletionBlock) {
        ClinkAuthenticationManager.sharedInstance.callWebService(URLRequest, isBackgroundCall : isBackgroundCall, pAlertView: pAlertView , pViewController: pViewController, pCompletionBlock: pCompletionBlock)
    }
    
    func callUploadService(URLRequest:NSMutableURLRequest,imageData : NSData , pAlertView : UIView, pViewController : UIViewController?,pCompletionBlock:ClinkCompletionBlock) {
        if ClinkAuthenticationManager.sharedInstance.isConnectedToNetwork() == true{
           URLRequest.addValue("image/png", forHTTPHeaderField: "Content-Type")
            print("Request URL Params : " + URLRequest.URLString)
            Helper.showHud(pAlertView)
            Alamofire.upload(URLRequest, data: imageData).responseJSON(options: .AllowFragments, completionHandler: { (response) -> Void in
                
                ClinkAuthenticationManager.sharedInstance.parseResponse(response,isBackgroundCall: false, pAlertView: pAlertView, pViewController: pViewController, pCompletionBlock: { (success, result) -> () in
                    pCompletionBlock(success, result)
                })
            })
         }
        else{
            Helper.showAlert(ALERTINTERNETCONNECTION, pobjView: pAlertView)
        }
    }
    
    func getQueryString(dicQueryParam : [String : AnyObject]) -> String{
        var strQuery = ""
        for (key,value) in dicQueryParam {
            strQuery += ((key) + "=" + (value as! String)) + "&"
        }
        strQuery.removeLastCharIfExist("&")
        return strQuery
    }
    
    func clearImageCache(){
        SDWebImageManager.sharedManager().imageCache.clearDisk()
        SDWebImageManager.sharedManager().imageCache.clearMemory()
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        
    }
    
    // MARK: - Twitter Share

    func shareToTwitter(status : String , image : UIImage){
        
        let store = Twitter.sharedInstance().sessionStore
        if let userid = store.session()?.userID {
            print("User Id :- \(userid)")
            self.uploadMediaToTwitter(status,image: image)
        }
        else{
            
            Twitter.sharedInstance().logInWithMethods([.WebBased]) { session, error in
                
                if error == nil{
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.uploadMediaToTwitter(status,image: image)
                    })
                }
                else{
                    
                }
            }
        }
    }
    
    func uploadMediaToTwitter(status : String , image : UIImage ){
        
        
        let store = Twitter.sharedInstance().sessionStore
        if let userid = store.session()?.userID {
            let client = TWTRAPIClient(userID: userid)
            let statusesShowEndpoint = "https://upload.twitter.com/1.1/media/upload.json"
            
            let data = UIImagePNGRepresentation(image)
            let strImage = data?.base64EncodedStringWithOptions([])
            
            let params  : [String : AnyObject] = ["media": strImage!]
            var clientError : NSError?
            
            let request = client.URLRequestWithMethod("POST", URL: statusesShowEndpoint, parameters: params, error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                
                if (connectionError == nil) {
                    
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                        print("test tweet :- \(json["media_id_string"])")
                        
                        self.updateTwitterStatusWithMediaID("\(json["media_id_string"])", status: status)
                        
                    } catch {
                        print("error serializing JSON: \(error)")
                    }
                }
                else {
                    ClinkAuthenticationManager.sharedInstance.logoutFromTwitter()
                    print("Error: \(connectionError)")
                }
            }
        }
    }
    
    func updateTwitterStatusWithMediaID( mediaID : String , status : String){
        
        let store = Twitter.sharedInstance().sessionStore
        if let userid = store.session()?.userID {
            let client = TWTRAPIClient(userID: userid)
            let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/update.json"
            let params = ["status": status,"media_ids":mediaID]
            var clientError : NSError?
            
            let request = client.URLRequestWithMethod("POST", URL: statusesShowEndpoint, parameters: params as [NSObject : AnyObject], error: &clientError)
            
            client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
                
                if (connectionError == nil) {
                    
                    do {
                        let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                        print("test tweet :- \(json)")
                        print("wohhaaa!!!!... Twitter Sharing success")
                    } catch {
                        print("error serializing JSON: \(error)")
                    }
                }
                else {
                    print("Error: \(connectionError)")
                }
            }
        }
    }
    
    // MARK: - Facebook Share

    func shareToFacebook(parentViewController : UIViewController , strImageUrl : String , strWebUrl : String , strUserName : String , strTitle : String , strDesc : String){
        
        if FBSDKAccessToken.currentAccessToken() != nil{
            
            print("Already logged in.")
            self.postToFacebook(strImageUrl, strWebUrl: strWebUrl,strUserName : strUserName, strTitle: strTitle, strDesc: strDesc)
        }
        else{
            
            print("Login to Facebook first.")
            
            let fbLoginManager: FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logOut()
            let permissionsArray : Array = [KPUBLISHACTION]
            
            fbLoginManager.logInWithPublishPermissions(permissionsArray, fromViewController: parentViewController, handler: { (result, error) -> Void in
                
                if error != nil{
                    print("Error :- \(error)")
                }
                else if result.isCancelled{
                    print("Cancelled")
                }
                else{
                    print("Logged in")
                    
                    if (result.grantedPermissions.contains(KPUBLISHACTION))
                    {
                        print("publish_actions permission granted")
                    }
                    
                    print(result.grantedPermissions)
                    
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.postToFacebook(strImageUrl, strWebUrl: strWebUrl,strUserName : strUserName, strTitle: strTitle, strDesc: strDesc)
                    })
                }
            })
        }
    }
    
    // MARK: - FBSDKSharingDelegate
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!){
        
        print("sharer results :- \(results.debugDescription)")
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!){
        ClinkAuthenticationManager.sharedInstance.logoutFromFacebook()
        print("sharer error :- \(error.debugDescription)")
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!){
        
        print("sharerDidCancel :- \(sharer.debugDescription)")
    }
    
    func postToFacebook(strImageUrl : String , strWebUrl : String , strUserName : String , strTitle : String , strDesc : String){
        
        let shareApi = FBSDKShareAPI()
        
        if shareApi.canShare() == true{
            let imageUrl = NSURL(string: strImageUrl)
            let webUrl = NSURL(string:strWebUrl)
            
            let shareContent = FBSDKShareLinkContent()
            shareContent.contentTitle = strTitle
            shareContent.contentDescription = strDesc
            shareContent.contentURL = webUrl
            shareContent.imageURL = imageUrl
            
            shareApi.delegate = self
            shareApi.message = strUserName
            shareApi.shareContent = shareContent
            shareApi.share()
        }
        else{
            print("Unable to share on facebook.")
        }
    }
}
