//
//  AppDelegate.swift
//  Clink
//
//  Created by Gunjan on 23/02/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import AddressBookUI
import Contacts
import Fabric
import TwitterKit
import FBSDKCoreKit
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var objSideMenu : SideMenuController!
    var objUserDetails : ClsUserDetails!
    var isContactSyncedOnce = false
    var arrMainContacts = [ClsContact]()
    var arrTrackerGroups = [ClsTrackerGroup]()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        // Initialize FB.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        // [Optional] Track statistics around application opens.
        if self.arrTrackerGroups.count == 0{
            self.arrTrackerGroups = USERDEFAULTS.getTrackerArrayObject()
            if self.arrTrackerGroups.count == 0 && USERDEFAULTS.boolForKey(kISUSERLOGGEDIN){
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    Helper.getTrackerGroups()
                })
            }
        }
        
        // Register for Push Notitications
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.respondsToSelector(Selector("backgroundRefreshStatus"))
            let oldPushHandlerOnly = !self.respondsToSelector(#selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                //                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
        
        if application.respondsToSelector(#selector(UIApplication.registerUserNotificationSettings(_:))) {
            
            let userNotificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
            let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
            
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            
        } else {
            
            //let types: UIRemoteNotificationType = [UIRemoteNotificationType.Badge, UIRemoteNotificationType.Alert, UIRemoteNotificationType.Sound]
            //application.registerForRemoteNotificationTypes(types)
            
            //let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
            //application.registerForRemoteNotificationTypes(types)
        }
        
        
        USERDEFAULTS.setBool(true, forKey: KRECEIVEDCLINKSELECTED)
        USERDEFAULTS.synchronize()
        
        if self.arrMainContacts.count == 0{
            self.arrMainContacts = USERDEFAULTS.getContactArrayObject()
            isContactSyncedOnce = true
        }
        
        
        
        application.setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        let notification = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]
        if (notification != nil) {
            
            if let strNotificationType = notification![KTYPE] as? String {
                
                if strNotificationType == KNOTIFICAITONTYPERECEIVE {
                    
                    if APPDELEGATE.objSideMenu != nil{
                        
                        NOTIFICATIONCENTER.postNotificationName(UPDATERECEIVEDCOUNT, object: nil)
                        USERDEFAULTS.setBool(true, forKey: KRECEIVEDCLINKSELECTED)
                        USERDEFAULTS.setValue(notification![KUUID] as! String, forKey: KFETCHEDCLINKID)
                        APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
                        APPDELEGATE.objSideMenu.showReceivedClink()
                    }
                    else{
                        USERDEFAULTS.setValue(notification![KUUID] as! String, forKey: KFETCHEDCLINKID)
                        USERDEFAULTS.setBool(true, forKey: KRECEIVEDCLINKSELECTED)
                        USERDEFAULTS.setBool(true, forKey: KNAVIGATETORECEIVEDLIST)
                        USERDEFAULTS.setBool(false, forKey: KNAVIGATETOSENDLIST)
                        USERDEFAULTS.synchronize()
                    }
                }
                else if strNotificationType == KNOTIFICATIONTYPEBANKVERIFIED {
                    NOTIFICATIONCENTER.postNotificationName(RELOADPAYMENTMETHODS, object: nil)
                    NOTIFICATIONCENTER.postNotificationName(GETDEFAULTPAYMENTMETHODDETAILS, object: nil)
                }
                else if strNotificationType == KNOTIFICATIONTYPEEMAILVERIFIED {
                    let objUser = USERDEFAULTS.getUserDetailsObject()
                    objUser.isVerified = true
                    USERDEFAULTS.setUserDetailsObject(objUser)
                }
                else {
                    if APPDELEGATE.objSideMenu != nil{
                        NOTIFICATIONCENTER.postNotificationName(UPDATERECEIVEDCOUNT, object: nil)
                        USERDEFAULTS.setValue(notification![KUUID] as! String, forKey: KFETCHEDCLINKID)
                        APPDELEGATE.objSideMenu.setMenuSelectedItem(4)
                        APPDELEGATE.objSideMenu.showSentClink()
                    }
                    else{
                        USERDEFAULTS.setValue(notification![KUUID] as! String, forKey: KFETCHEDCLINKID)
                        USERDEFAULTS.setBool(false, forKey: KNAVIGATETORECEIVEDLIST)
                        USERDEFAULTS.setBool(true, forKey: KNAVIGATETOSENDLIST)
                        USERDEFAULTS.synchronize()
                    }
                }
            }
            
//            // Do your stuff with notification
//            if let refrenceClinkId = notification!["pushClinkId"] as? String {
//                self.getClinkDetails(refrenceClinkId)
//                Helper.showAlert(refrenceClinkId, pobjView: APPDELEGATE.window)
//            }
//            else{
//                USERDEFAULTS.setBool(true, forKey: "navigateToReceivedClinkListVC")
//                USERDEFAULTS.synchronize()
//            }
        }
        else{
            USERDEFAULTS.setBool(false, forKey: "navigateToReceivedClinkListVC")
            USERDEFAULTS.setBool(false, forKey: "navigateToSentClinkListVC")
            USERDEFAULTS.synchronize()
        }
        
        
        
        //        var fontFamilies: [AnyObject] = UIFont.familyNames()
        //        for var i = 0; i < fontFamilies.count; i += 1 {
        //            let fontFamily: String = fontFamilies[i] as! String
        //            let fontNames: [AnyObject] = UIFont.fontNamesForFamilyName(fontFamilies[i] as! String)
        //            NSLog("%@: %@", fontFamily, fontNames)
        //        }
        //        }
        
        let url = launchOptions?[UIApplicationLaunchOptionsURLKey]
        if (url != nil) {
            USERDEFAULTS.setBool(true, forKey: "ISLAUCHEDUSINGURL")
            USERDEFAULTS.synchronize()
//            Helper.showAlert("Launched using url", pobjView: APPDELEGATE.window)
        }
        Fabric.with([Twitter.self])
        
        return true
        
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//        NOTIFICATIONCENTER.postNotificationName(RELOADPAYMENTMETHODS, object: nil)
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        USERDEFAULTS.removeObjectForKey("ISLAUCHEDUSINGURL")
        USERDEFAULTS.removeObjectForKey("SHOULDAUTOREDIRECT")
        USERDEFAULTS.removeObjectForKey("AUTOFILLDATA")
        USERDEFAULTS.synchronize()
    }
    
    // function for FB Connect
    @available(iOS 9.0, *)
    func application(application: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        if Twitter.sharedInstance().application(application, openURL:url, options: options) {
            return true
        }
        else if FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String,
                                                                      annotation: options [UIApplicationOpenURLOptionsAnnotationKey]){
            return true
        }
        else if url.scheme!.isEqualToIgnoringCase("clink") && (url.query?.containsString("&"))!{
            let arrQueryComponents : [String] = (url.query?.componentsSeparatedByString("&"))!
            
            if arrQueryComponents.count > 0{
                var dicAutoFillData = [String : AnyObject]()
                for tmpComponent in arrQueryComponents{
                    let arrParam = tmpComponent.componentsSeparatedByString("=")
                    if arrParam.count == 2 && arrParam.last?.isEmptyString() == false{
                        dicAutoFillData[arrParam.first!] = arrParam.last!
                    }
                }
                dicAutoFillData.nullsRemoved
                if dicAutoFillData.count > 0{
                    
                    USERDEFAULTS.setObject(dicAutoFillData, forKey: "AUTOFILLDATA")
                    USERDEFAULTS.synchronize()
                    if USERDEFAULTS.boolForKey(kISUSERLOGGEDIN) == true {
                        if USERDEFAULTS.boolForKey("ISLAUCHEDUSINGURL") == false{
                            self.manageAutoFill()
                        }
                    }
                    else{
                        NSNotificationCenter.defaultCenter().postNotificationName("AUTOFILLDATA", object: nil, userInfo: nil)
                    }
                }
            }
            return true
        }
        return false
    }
    
    func manageAutoFill() {
        Helper.showAlert("Are you sure you want to logout current user and signup with new user?", cancelButtonTitle: "Cancel", otherButtonTitle: "Ok", pobjView: APPDELEGATE.window, completionBock: { (objAlert, btnIndex) in
            if btnIndex == 1{
                if self.objSideMenu != nil{
                    self.objSideMenu.callLogoutWS()
                }
            }
            else{
                if USERDEFAULTS.boolForKey("ISLAUCHEDUSINGURL") == true{
                    NSNotificationCenter.defaultCenter().postNotificationName("RELOADHOMEDATA", object: nil, userInfo: nil)
                }
                USERDEFAULTS.removeObjectForKey("ISLAUCHEDUSINGURL")
                USERDEFAULTS.removeObjectForKey("SHOULDAUTOREDIRECT")
                USERDEFAULTS.removeObjectForKey("AUTOFILLDATA")
                USERDEFAULTS.synchronize()
            }
        })
    }
    @available(iOS, introduced = 8.0, deprecated = 9.0)
    func application(application: UIApplication,openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        if  FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication!, annotation: annotation) {
            return true
        }
        return false
    }
    
    // function to push notifications
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")
        
        let deviceTokenString: String = (deviceToken.description as NSString)
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString( " ", withString: "") as String
        NSUserDefaults.standardUserDefaults().setObject(deviceTokenString, forKey: KDEVICETOKEN)
        NSUserDefaults.standardUserDefaults().synchronize()
        UIPasteboard.generalPasteboard().string =  "\(deviceTokenString)"
        //        Helper.showAlertwithTitle("DEVICETOKEN", strMessage: "Device token is copied. You can paste the device token if required.", pobjView: APPDELEGATE.window)
        print("DEVICETOKEN: \(deviceTokenString)")
        NSLog("DEVICETOKEN: \(deviceTokenString)")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.", terminator: "")
        } else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error, terminator: "")
        }
        //        Helper.showAlertwithTitle("DEVICETOKEN", strMessage: "Failed to register for device token.", pobjView: APPDELEGATE.window)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        NSLog("Clink Notification : \(userInfo)")
        
        if application.applicationState == UIApplicationState.Inactive {
            
            if let strNotificationType = userInfo[KTYPE] as? String {
                
                if strNotificationType == KNOTIFICAITONTYPERECEIVE {
                    
                    if APPDELEGATE.objSideMenu != nil{
                        
                        NOTIFICATIONCENTER.postNotificationName(UPDATERECEIVEDCOUNT, object: nil)
                        USERDEFAULTS.setBool(true, forKey: KRECEIVEDCLINKSELECTED)
                        USERDEFAULTS.setValue(userInfo[KUUID] as! String, forKey: KFETCHEDCLINKID)
                        APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
                        APPDELEGATE.objSideMenu.showReceivedClink()
                    }
                    else{
                        USERDEFAULTS.setValue(userInfo[KUUID] as! String, forKey: KFETCHEDCLINKID)
                        USERDEFAULTS.setBool(true, forKey: KRECEIVEDCLINKSELECTED)
                        USERDEFAULTS.setBool(true, forKey: KNAVIGATETORECEIVEDLIST)
                        USERDEFAULTS.setBool(false, forKey: KNAVIGATETOSENDLIST)
                        USERDEFAULTS.synchronize()
                    }
                }
                else if strNotificationType == KNOTIFICATIONTYPEBANKVERIFIED {
                    NOTIFICATIONCENTER.postNotificationName(RELOADPAYMENTMETHODS, object: nil)
                    NOTIFICATIONCENTER.postNotificationName(GETDEFAULTPAYMENTMETHODDETAILS, object: nil)
                }
                else if strNotificationType == KNOTIFICATIONTYPEEMAILVERIFIED {
                    let objUser = USERDEFAULTS.getUserDetailsObject()
                    objUser.isVerified = true
                    USERDEFAULTS.setUserDetailsObject(objUser)
                }
                else {
                    if APPDELEGATE.objSideMenu != nil{
                        NOTIFICATIONCENTER.postNotificationName(UPDATERECEIVEDCOUNT, object: nil)
                        USERDEFAULTS.setValue(userInfo[KUUID] as! String, forKey: KFETCHEDCLINKID)
                        APPDELEGATE.objSideMenu.setMenuSelectedItem(4)
                        APPDELEGATE.objSideMenu.showSentClink()
                    }
                    else{
                        USERDEFAULTS.setValue(userInfo[KUUID] as! String, forKey: KFETCHEDCLINKID)
                        USERDEFAULTS.setBool(false, forKey: KNAVIGATETORECEIVEDLIST)
                        USERDEFAULTS.setBool(true, forKey: KNAVIGATETOSENDLIST)
                        USERDEFAULTS.synchronize()
                    }
                }
            }
        }
        else if application.applicationState == UIApplicationState.Active {
            if let strNotificationType = userInfo[KTYPE] as? String {
                
                if strNotificationType == KNOTIFICATIONTYPEBANKVERIFIED {
                    NOTIFICATIONCENTER.postNotificationName(RELOADPAYMENTMETHODS, object: nil)
                    NOTIFICATIONCENTER.postNotificationName(GETDEFAULTPAYMENTMETHODDETAILS, object: nil)
                }
                else if strNotificationType == KNOTIFICATIONTYPEEMAILVERIFIED {
                    let objUser = USERDEFAULTS.getUserDetailsObject()
                    objUser.isVerified = true
                    USERDEFAULTS.setUserDetailsObject(objUser)
                }
            }
        }
    }
    
    func getClinkDetails(refrenceClinkId : String){
        
    }
    
    func navigateToScreen(objClinkDetail : ClsClinkDetails){
        if APPDELEGATE.objSideMenu != nil{
            USERDEFAULTS.setValue(objClinkDetail.strId, forKey: "fetchedClinkId")
            USERDEFAULTS.synchronize()
            NOTIFICATIONCENTER.postNotificationName(UPDATERECEIVEDCOUNT, object: nil)
            NOTIFICATIONCENTER.postNotificationName(UPDATESENTCOUNT, object: nil)
            //            if objClinkDetail.strSenderId == USERDEFAULTS.boolForKey(kISUSERLOGGEDIN){
            //                //                            open sent screen
            //                APPDELEGATE.objSideMenu.setMenuSelectedItem(3)
            //                APPDELEGATE.objSideMenu.showSentClink()
            //            }
            //            else{
            //                //                            open received screen
            //                APPDELEGATE.objSideMenu.setMenuSelectedItem(2)
            //                APPDELEGATE.objSideMenu.showReceivedClink()
            //            }
        }
        else
        {
            USERDEFAULTS.setValue(objClinkDetail.strId, forKey: "fetchedClinkId")
            USERDEFAULTS.synchronize()
            //            if objClinkDetail.strSenderId == USERDEFAULTS.boolForKey(kISUSERLOGGEDIN){
            //                USERDEFAULTS.setBool(false, forKey: "navigateToReceivedClinkListVC")
            //                USERDEFAULTS.setBool(true, forKey: "navigateToSentClinkListVC")
            //            }
            //            else{
            //                USERDEFAULTS.setBool(true, forKey: "navigateToReceivedClinkListVC")
            //                USERDEFAULTS.setBool(false, forKey: "navigateToSentClinkListVC")
            //            }
            USERDEFAULTS.synchronize()
        }
    }
    // MARK: - Other methods
    func getVersionString() -> String{
        let nsObject: AnyObject? = NSBundle.mainBundle().infoDictionary![BUNDLESHORTVERSION]
        let version = nsObject as! String
        let strVersion = "v " + version + BUILD
        return strVersion
    }
}
