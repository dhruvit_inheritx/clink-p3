//
//  UIFont+Common.swift
//  Clink
//
//  Created by Gunjan on 07/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    func sizeOfString (string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRectWithSize(CGSize(width: width, height: DBL_MAX),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: self],
            context: nil).size
    }
}