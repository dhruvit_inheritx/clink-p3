//
//  UITextField+Common.swift
//  Clink
//
//  Created by Gunjan on 07/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

private var maxLengths = [UITextField: Int]()

extension UITextField {
    
    var maxLength: Int {
        get {
            guard let length = maxLengths[self]
                else {
                    return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(
                self,
                action: #selector(UITextField.limitLength(_:)),
                forControlEvents: UIControlEvents.EditingChanged
            )
        }
    }
    
    func setPlaceHolder(text: String, color : UIColor){
        let str = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName:color])
        self.attributedPlaceholder = str
    }
    
    func resizeHeight() -> CGFloat{
        
        let constrainedSize = CGSizeMake(self.frame.size.width, FLOATMAXHEIGHT)
        let finalSize = self.font!.sizeOfString(self.text!, constrainedToWidth: Double(self.frame.size.width))
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, constrainedSize.width, finalSize.height)
        return finalSize.height
    }
    
    func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text
            where prospectiveText.characters.count > maxLength else {
                return
        }
        let selection = selectedTextRange
        text = prospectiveText.substringWithRange(
            Range<String.Index>(prospectiveText.startIndex ..< prospectiveText.startIndex.advancedBy(maxLength))
        )
        selectedTextRange = selection
    }
}
