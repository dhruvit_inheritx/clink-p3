//
//  NSDate+Common.swift
//  Clink
//
//  Created by Gunjan on 18/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

extension NSDate {
    
   
    func getFormatedString(pstrFormat:String)-> String {
        
        let objDateFormatter = NSDateFormatter()
        objDateFormatter.dateFormat = pstrFormat
        let strDate = objDateFormatter.stringFromDate(self)
        return strDate
    }
    func getUTCFormatedString(pstrFormat:String)-> String {
        
        let objDateFormatter = NSDateFormatter()
        objDateFormatter.dateFormat = pstrFormat
        objDateFormatter.timeZone = NSTimeZone(abbreviation: STRUTC)
        let strDate = objDateFormatter.stringFromDate(self)
        return strDate
    }
    
    func getNewUTCDate()-> NSDate{
        let date : NSDate!
        let formatter = NSDateFormatter()
        formatter.dateFormat = UTCFROMFORMATE
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        formatter.timeZone = NSTimeZone(abbreviation: STRUTC)
        let utcTimeZoneStr = formatter.stringFromDate(NSDate())
        date = formatter.dateFromString(utcTimeZoneStr)
        return date
    }
}

extension Array where Element: Equatable {
    
    public func uniq() -> [Element] {
        var arrayCopy = self
        arrayCopy.uniqInPlace()
        return arrayCopy
    }
    
    mutating public func uniqInPlace() {
        var seen = [Element]()
        var index = INTZERO
        for element in self {
            if seen.contains(element) {
                removeAtIndex(index)
            } else {
                seen.append(element)
                index += 1
            }
        }
    }
}
