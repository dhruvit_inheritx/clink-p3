//
//  String+Common.swift
//  Clink
//
//  Created by Gunjan on 18/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation

extension Dictionary where Value: AnyObject {
    
    var nullsRemoved: [Key: Value] {
        let tup = filter { !($0.1 is NSNull) }
        return tup.reduce([Key: Value]()) { (var r, e) in r[e.0] = e.1; return r }
    }
}
extension Dictionary {
    
    func getValueIfAvilable(key: Key) -> Value? {
        // if key not found, replace the nil with
        // the first element of the values collection
        if let val = self[key] {
            return val
        }
        return nil
        // note, this is still an optional (because the
        // dictionary could be empty)
    }
}
