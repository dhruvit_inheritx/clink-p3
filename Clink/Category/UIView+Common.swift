//
//  UIView+Common.swift
//  Clink
//
//  Created by Gunjan on 27/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}




