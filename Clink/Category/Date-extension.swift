//
//  Date-extension.swift
//  Clink
//
//  Created by Gunjan on 11/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

extension NSDate {
    
    static func getClinkFormated() -> String {
        let now = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEEE',' dd MMMM"
        return dateFormatter.stringFromDate(now)
    }
    
}