//
//  UISearchBar+Common.swift
//  Clink
//
//  Created by Gunjan on 6/17/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

extension UISearchBar {
    func changeSearchBarColor(color : UIColor) {
        for subView in self.subviews {
            for subSubView in subView.subviews {
                if subSubView.conformsToProtocol(UITextInputTraits.self) {
                    let textField = subSubView as! UITextField
                    textField.backgroundColor = color
                    break
                }
            }
        }
    }
}