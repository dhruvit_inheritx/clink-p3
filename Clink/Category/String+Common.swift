//
//  String+Common.swift
//  Clink
//
//  Created by Gunjan on 18/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
let INTFIRST : Int = 1
let STREMAILPREDICATE = "SELF MATCHES %@"

enum CryptoAlgorithm {
    case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
    
    var HMACAlgorithm: CCHmacAlgorithm {
        var result: Int = 0
        switch self {
        case .MD5:      result = kCCHmacAlgMD5
        case .SHA1:     result = kCCHmacAlgSHA1
        case .SHA224:   result = kCCHmacAlgSHA224
        case .SHA256:   result = kCCHmacAlgSHA256
        case .SHA384:   result = kCCHmacAlgSHA384
        case .SHA512:   result = kCCHmacAlgSHA512
        }
        return CCHmacAlgorithm(result)
    }
    
    var digestLength: Int {
        var result: Int32 = 0
        switch self {
        case .MD5:      result = CC_MD5_DIGEST_LENGTH
        case .SHA1:     result = CC_SHA1_DIGEST_LENGTH
        case .SHA224:   result = CC_SHA224_DIGEST_LENGTH
        case .SHA256:   result = CC_SHA256_DIGEST_LENGTH
        case .SHA384:   result = CC_SHA384_DIGEST_LENGTH
        case .SHA512:   result = CC_SHA512_DIGEST_LENGTH
        }
        return Int(result)
    }
}

extension String {
    
    func hmac(algorithm: CryptoAlgorithm, key: String) -> String {
        let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
        let strLen = Int(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        let digestLen = algorithm.digestLength
        let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)
        let keyStr = key.cStringUsingEncoding(NSUTF8StringEncoding)
        let keyLen = Int(key.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
        
        CCHmac(algorithm.HMACAlgorithm, keyStr!, keyLen, str!, strLen, result)
        
        let digest = stringFromResult(result, length: digestLen)
        
        result.dealloc(digestLen)
        
        return digest
    }
    
    private func stringFromResult(result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash)
    }

    
    var length: Int {
        return characters.count
    }
    var first: String {
        return String(characters.prefix(INTFIRST))
    }
    var last: String {
        return String(characters.suffix(INTFIRST))
    }
    var uppercaseFirst: String {
        return first.uppercaseString + String(characters.dropFirst())
    }
    
    func getDate(pstrFormat:String) -> NSDate{
        
        let dateString = self
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = pstrFormat
        let dateFromString = dateFormatter.dateFromString(dateString)
        return dateFromString!
    }
    
    func getLocalDate(pstrFormat : String) -> NSDate {
        
        let dateString = self
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = pstrFormat
        dateFormatter.timeZone = NSTimeZone(name: "UTC")
        let date = dateFormatter.dateFromString(dateString)
        
        // change to a readable time format and change to local time zone
        dateFormatter.dateFormat = pstrFormat
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let stringFromDate = dateFormatter.stringFromDate(date!)
        let dateFromString = dateFormatter.dateFromString(stringFromDate)
        
        return dateFromString!
    }
    
    func isValidEmail() -> Bool {
        let emailRegex: String = EMAILREGEX
        let emailTest: NSPredicate = NSPredicate(format: STREMAILPREDICATE, emailRegex)
        return emailTest.evaluateWithObject(self)
    }
    
    
    func isEmptyString() -> Bool {
        let aStrTrimName: NSString = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if aStrTrimName.length == INTZERO {
            return true
        }
        return false
    }
    
    func isEqualToIgnoringCase(var strToCompare:String) -> Bool {
        strToCompare = strToCompare.lowercaseString
        let originalString: NSString = self.lowercaseString
        if originalString == strToCompare {
            return true
        }
        return false
    }
    
    func isValidPhoneNumber() -> Bool {
        
        let charcter  = NSCharacterSet(charactersInString: PHONECHARS).invertedSet
        var filtered:NSString!
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        filtered = inputString.componentsJoinedByString(STREMPTY)
        return  self == filtered
        
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
    
    func removeSpecialChars(strException : String) -> String {
        let strSet = "1234567890".stringByAppendingString(strException)
        let validChars : Set<Character> = Set(strSet.characters)
        return String(self.characters.filter {validChars.contains($0)})
    }
    
    mutating func removeLastCharIfExist(char : Character){
        while self.characters.last == char{
            if self.characters.last == char{
                self = String(self.characters.dropLast())
            }
        }
    }
    
    func removeZerosAfterDecimal() -> String{
       return self.stringByReplacingOccurrencesOfString(".00", withString: "")
    }
    
    
}
