//
//  UILabel+Common.swift
//  Clink
//
//  Created by Gunjan on 18/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func resizeLabelHeight() -> CGFloat{
        
        self.numberOfLines = 0
        self.lineBreakMode = .ByWordWrapping
        let constrainedSize = CGSizeMake(self.frame.size.width, FLOATMAXHEIGHT)
        let finalSize = self.font.sizeOfString(self.text!, constrainedToWidth: Double(self.frame.size.width))
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, constrainedSize.width, finalSize.height)
        return finalSize.height
    }
}