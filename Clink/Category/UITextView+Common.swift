//
//  UITextView+Common.swift
//  Clink
//
//  Created by Gunjan on 14/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    func resizeHeight() -> CGFloat{
        
        let constrainedSize = CGSizeMake(self.frame.size.width, FLOATMAXHEIGHT)
        let finalSize = self.font!.sizeOfString(self.text!, constrainedToWidth: Double(self.frame.size.width))
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, constrainedSize.width, finalSize.height)
        return finalSize.height
    }
}

