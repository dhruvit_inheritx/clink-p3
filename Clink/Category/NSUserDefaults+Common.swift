//
//  NSDate+Common.swift
//  Clink
//
//  Created by Gunjan on 18/03/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

extension NSUserDefaults {
    
   
//    func setClinkDetailsObject(pObject:ClsClinkDetails,pKey : String) {
//        // store the value in aColor in user defaults
//        // as the value for key aKey
//        
//        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
//        USERDEFAULTS.setObject(data, forKey: pKey)
//        USERDEFAULTS.synchronize()
//        
//        
//    }
//    
//    func getClinkDetailsObject(pKey : String)-> ClsClinkDetails{
//        let tmpObject : ClsClinkDetails =  ClsClinkDetails()
//        if let objClinkData = USERDEFAULTS.objectForKey(pKey) as? NSData {
//            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObjectWithData(objClinkData) as? ClsClinkDetails {
//                return userSelectedObject
//            }
//        }
//        return tmpObject
//    }
    
    func setUserDetailsObject(pObject:ClsUserDetails) {
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
        USERDEFAULTS.setObject(data, forKey: KUSERDETAILS)
        USERDEFAULTS.synchronize()
    }
    
    func getUserDetailsObject()-> ClsUserDetails{
        let tmpObject : ClsUserDetails =  ClsUserDetails()
        if let objUserData = USERDEFAULTS.objectForKey(KUSERDETAILS) as? NSData {
            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObjectWithData(objUserData) as? ClsUserDetails {
                return userSelectedObject
            }
        }
        return tmpObject
    }
    
    func setContactArrayObject(pObject:[ClsContact]) {
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
        USERDEFAULTS.setObject(data, forKey: KCONTACTARRAY)
        USERDEFAULTS.synchronize()
    }
    
    func getContactArrayObject()-> [ClsContact]{
        let tmpObject =  [ClsContact]()
        if let objUserData = USERDEFAULTS.objectForKey(KCONTACTARRAY) as? NSData {
            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObjectWithData(objUserData) as? [ClsContact] {
                return userSelectedObject
            }
        }
        return tmpObject
    }
    
    func setTrackerArrayObject(pObject:[ClsTrackerGroup]) {
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
        USERDEFAULTS.setObject(data, forKey: KCONTACTARRAY)
        USERDEFAULTS.synchronize()
    }
    
    func getTrackerArrayObject()-> [ClsTrackerGroup]{
        let tmpObject =  [ClsTrackerGroup]()
        if let objTrackerData = USERDEFAULTS.objectForKey(KCONTACTARRAY) as? NSData {
            if let trackerObject = NSKeyedUnarchiver.unarchiveObjectWithData(objTrackerData) as? [ClsTrackerGroup] {
                return trackerObject
            }
        }
        return tmpObject
    }
    func setDefaultPaymentDetailsObject(pObject:ClsCreditCardDetails) {
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
        USERDEFAULTS.setObject(data, forKey: KPAYMENTDETAILS)
        USERDEFAULTS.synchronize()
    }
    
    func getDefaultPaymentDetailsObject()-> ClsCreditCardDetails{
        let tmpObject : ClsCreditCardDetails =  ClsCreditCardDetails()
        if let objUserData = USERDEFAULTS.objectForKey(KPAYMENTDETAILS) as? NSData {
            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObjectWithData(objUserData) as? ClsCreditCardDetails {
                return userSelectedObject
            }
        }
        return tmpObject
    }
    
    func setDefaultTransferDetailsObject(pObject:ClsCreditCardDetails, forKey key : String) {
        let data : NSData = NSKeyedArchiver.archivedDataWithRootObject(pObject)
        USERDEFAULTS.setObject(data, forKey: key)
        USERDEFAULTS.synchronize()
    }
    
    func getDefaultTransferDetailsObjectForKey(key : String)-> ClsCreditCardDetails{
        let tmpObject : ClsCreditCardDetails =  ClsCreditCardDetails()
        if let objCardDetail = USERDEFAULTS.objectForKey(key) as? NSData {
            if let userSelectedObject = NSKeyedUnarchiver.unarchiveObjectWithData(objCardDetail) as? ClsCreditCardDetails {
                return userSelectedObject
            }
        }
        return tmpObject
    }
}
