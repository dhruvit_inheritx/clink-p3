//
//  CommonAttributedString.h
//  Clink
//
//  Created by Gunjan on 20/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonAttributedString : NSObject

+(NSAttributedString*)getFormattedString:(NSString*)pstrOriginalString withColor:(UIColor*)color;
+(NSAttributedString*)getGiftFormattedString:(NSString*)pstrOriginalString;

@end
