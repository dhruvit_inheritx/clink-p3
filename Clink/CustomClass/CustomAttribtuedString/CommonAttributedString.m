//
//  CommonAttributedString.m
//  Clink
//
//  Created by Gunjan on 20/06/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

#import "CommonAttributedString.h"
#import <UIKit/UIKit.h>

@implementation CommonAttributedString

+(NSAttributedString*)getFormattedString:(NSString*)pstrOriginalString withColor : (UIColor*)color
{
    
    UIFont *fnt = [UIFont fontWithName:@"Helvetica" size:20.0];

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:pstrOriginalString
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:20],NSForegroundColorAttributeName : color}];
    NSRange searchRange = NSMakeRange(0,pstrOriginalString.length);
    NSRange foundRange;
    
    while (searchRange.location < pstrOriginalString.length) {
        searchRange.length = pstrOriginalString.length-searchRange.location;
        
        foundRange = [pstrOriginalString rangeOfString:@"$" options:NSCaseInsensitiveSearch range:searchRange];
        
        if (foundRange.location != NSNotFound) {
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
            [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:15], NSBaselineOffsetAttributeName : @8, NSForegroundColorAttributeName : color} range:foundRange];
        } else {
            // no more substring to find
            break;
        }
    }
    return attributedString;
}

+(NSAttributedString*)getGiftFormattedString:(NSString*)pstrOriginalString
{
    
    UIFont *fnt = [UIFont fontWithName:@"Helvetica" size:10.0];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:pstrOriginalString
                                                                                         attributes:@{NSFontAttributeName: [fnt fontWithSize:10]}];
    NSRange searchRange = NSMakeRange(0,pstrOriginalString.length);
    NSRange foundRange;
    
    while (searchRange.location < pstrOriginalString.length) {
        searchRange.length = pstrOriginalString.length-searchRange.location;
        
        foundRange = [pstrOriginalString rangeOfString:@"$" options:NSCaseInsensitiveSearch range:searchRange];
        
        if (foundRange.location != NSNotFound) {
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location+foundRange.length;
            
            [attributedString setAttributes:@{NSFontAttributeName : [fnt fontWithSize:6], NSBaselineOffsetAttributeName : @5} range:foundRange];
            
            
        } else {
            // no more substring to find
            break;
        }
    }
    return attributedString;
}
@end
