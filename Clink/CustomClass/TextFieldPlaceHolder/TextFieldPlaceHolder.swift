//
//  TextFieldPlaceHolder.swift
//  Clink
//
//  Created by Gunjan on 07/04/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit

@IBDesignable
class TextFieldPlaceHolder: UITextField {
    
    @IBInspectable var placeholderColor: UIColor = UIColor.lightGrayColor()

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func drawRect(rect: CGRect) {
        self.setPlaceHolder(self.placeholder!, color: placeholderColor)
    }
}
