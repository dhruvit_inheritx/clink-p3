//
//  CustomeCameraVC.h
//  CustomCamera
//
//  Created by Gunjan on 06/06/16.
//  Copyright © 2016 inheritx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>

@protocol CustomeCameraDelegate;

@interface CustomeCameraVC : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    
    UIImagePickerController *imgPicker;
    BOOL pickerDidShow;
    
    //Today Implementation
    BOOL FrontCamera;
    BOOL haveImage;
    BOOL initializeCamera, photoFromCam;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
    AVCaptureStillImageOutput *stillImageOutput;
    UIImage *croppedImageWithoutOrientation;
//    NSString *strTitle;
}
@property (nonatomic, readwrite) BOOL dontAllowResetRestaurant;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) NSString *strTitle;
#pragma mark -
@property (nonatomic, strong) IBOutlet UIButton *photoCaptureButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, strong) IBOutlet UIButton *cameraToggleButton;
@property (nonatomic, strong) IBOutlet UIButton *flashToggleButton;
@property (nonatomic, strong) IBOutlet UIView *photoBar;
@property (nonatomic, strong) IBOutlet UIView *topBar;
@property (retain, nonatomic) IBOutlet UIView *imagePreview;
@property (nonatomic, strong) IBOutlet UIButton *btnTitle;

- (IBAction)retakePhoto:(UIButton *)sender;
- (IBAction)donePhotoCapture:(UIButton *)sender;
- (IBAction)cancel:(UIButton *)sender;
- (IBAction)toogleFlash:(UIButton *)sender;
- (IBAction)switchCamera:(UIButton *)sender;
- (IBAction)snapImage:(UIButton *)sender;


@property (retain, nonatomic) IBOutlet UIImageView *captureImage;
@end

@protocol CustomeCameraDelegate
- (void)didFinishPickingImage:(UIImage *)image;
- (void)yCameraControllerDidCancel;
- (void)yCameraControllerdidSkipped;
@end
