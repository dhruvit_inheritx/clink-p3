//
//  GBEditingView.swift
//  Clink
//
//  Created by Gunjan on 17/02/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import GPUImage

private let concurrentFilterQueue = dispatch_queue_create(
    "com.steelproductions.applyFilter", DISPATCH_QUEUE_CONCURRENT)

enum FetureType : Int {
    case Drawing = 1, Overlay = 2, Text = 3 ,NAN = 0
}

enum FilterType : Int {
    case Specia = 1, BlackAndWhite = 2, HighContrast = 3, Congo = 4, DonationX = 5, Birthday = 6, ThankYou = 7,Drama = 8
}

protocol TextViewHideenDelegate : class{
    func textViewDidHide(textView: UITextView)
    func textViewDidShow(textView: UITextView)
    func edtingToolUser(strToolName : String)
}

class GBEditingView: UIView ,UITextViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate {
    
    var currentFeture : FetureType!
    var isErasing : Bool!
    var isDrawing : Bool!
    var isClrSelecting : Bool!
    var isTexting : Bool!
    var selectedImage : UIImage!
    
    var currentColor : UIColor =  UIColor.whiteColor()
    var firstX : CGFloat!
    var firstY : CGFloat!
    var lastRotation : CGFloat = 0.0
    var previousFrame :  CGRect!
    
    //    new
    var swiped = false
    var lastPoint = CGPoint.zero
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var brushWidth: CGFloat = 2.5
    var opacity: CGFloat = 1.0
    
    var txtViewLbl : UITextView!
    weak var delegateGB: TextViewHideenDelegate?
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var newDrawingView: ACEDrawingView!
    
    //  MARK:- MaskingView
    var isLoadedOnce = false
    var objBounds : CGRect!
    var fltScrollOffset : CGFloat!
    var fltConstant : CGFloat!
    
    
    var arrImagViews : NSMutableArray!
    var currentPage : Int!
    var arrImagess : NSMutableArray!
    
    
    var originalImage : UIImage!
    var sepiaImage : UIImage!//1
    var bawImage : UIImage!//2
    var hiContrastImage : UIImage!//3
    var congratulationsImage : UIImage!//4
//    var donationImage : UIImage!//5
    var happybirthdayImage : UIImage!//6
    var thankYouImage : UIImage!//7
    var dramaImage : UIImage!//3
    //    var objbackgroundImage : UIImageView!
    
    var slider : UISlider!
    var previousSliderVal : Float!
    
    var sepiaFilter : GPUImageSepiaFilter!
    var contrastFilter : GPUImageContrastFilter!
    var RGBFilter : GPUImageRGBFilter!
    var GrayFilter : GPUImageGrayscaleFilter!
    
    @IBOutlet var scrollViewFilter : UIScrollView!
    
    var imgViewOriginal : UIImageView!
    var imgViewSpecia : UIImageView!
    var imgViewBAW : UIImageView!
    var imgViewContrast : UIImageView!
    var imgViewCongratulations : UIImageView!
//    var imgViewDonation : UIImageView!
    var imgViewHappyBirthday : UIImageView!
    var imgViewThankyou : UIImageView!
    var imgViewDrama : UIImageView!
    
    //    MARK:- Init methods
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //    MARK:- Other Init Methods
    
    func setUpView()
    {
        self.backgroundColor=UIColor.clearColor()
        currentFeture = FetureType.NAN
        lastRotation = 0.0
        let minYPos = (self.frame.size.height - KEYBOARDMAXHEIGHT)
        if txtViewLbl == nil{
            txtViewLbl = UITextView(frame: CGRectMake(0,minYPos - 36,((UIApplication.sharedApplication().delegate?.window??.bounds)?.size.width)!,36))
            txtViewLbl.delegate = self
            txtViewLbl.userInteractionEnabled = true
            txtViewLbl.textColor = UIColor.whiteColor()
            txtViewLbl.backgroundColor = UIColor.clearColor()
            txtViewLbl.spellCheckingType = UITextSpellCheckingType.No
            txtViewLbl.returnKeyType = UIReturnKeyType.Done
            txtViewLbl.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
            //            txtViewLbl.font = .systemFontOfSize(20)
            txtViewLbl.font = UIFont(name: FONTGOTHAMMEDIUM, size: 16)
            txtViewLbl.textAlignment = .Center
            txtViewLbl.keyboardAppearance = .Dark
            self.addSubview(txtViewLbl)
            self.bringSubviewToFront(txtViewLbl)
            self.setNeedsDisplay()
            let panGesture = UIPanGestureRecognizer(target: self, action: #selector(GBEditingView.handlePanGesture(_:)))
            panGesture.maximumNumberOfTouches = 1
            panGesture.maximumNumberOfTouches = 1
            txtViewLbl.addGestureRecognizer(panGesture)
            txtViewLbl.hidden = true
            
        }
        
//        self.bringSubviewToFront(tempImageView)
        self.bringSubviewToFront(newDrawingView)
    }
    
    //    MARK:- Touches Events
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        super.touchesBegan(touches, withEvent: event)
//    }
//    
//    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        // save all the touches in the path
//        super.touchesMoved(touches, withEvent: event)
//    }
//    
//    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        // make sure a point is recorded
//        super.touchesEnded(touches, withEvent: event)
//    }
//    
//    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
//        // make sure a point is recorded
//        super.touchesCancelled(touches, withEvent: event)
//    }
    //    MARK:- 1. Action Fetures
    
    
    func actionShowDrawing(){
        self.endEditing(true)
        if currentFeture != FetureType.Drawing
        {
            currentFeture = FetureType.Drawing
            isClrSelecting = false
        }
        else {
            currentFeture = FetureType.NAN
            
        }
    }
    
    //    MARK:- 2. Action ShowText
    
    func actionShowText(){
        isDrawing = false
        isErasing = false
        if currentFeture != FetureType.Text
        {
            currentFeture = FetureType.Text
            txtViewLbl.hidden = false
            txtViewLbl.becomeFirstResponder()
            self.bringSubviewToFront(txtViewLbl)
            
        }
        else {
            currentFeture = FetureType.NAN
        }
    }
    
    func actoinHideText(){
        currentFeture = FetureType.NAN
        isTexting = false
        txtViewLbl.resignFirstResponder()
    }
    
    //    MARK:- Actions Draw Feture
    
    func actionStartDrawing(){
        if isDrawing != true
        {
            delegateGB?.edtingToolUser(EditingToolTracker.Pen.rawValue)
            isClrSelecting = false
            isErasing = false
            isDrawing = true
            isTexting = false
            self.enableDrawing()
        }
        else{
            isDrawing = false
            self.enableSwipeFilter()
        }
    }
    func actionSetDrawColor(){
        self.endEditing(true)
        if isClrSelecting == false
        {
            isClrSelecting = true
            self.enableDrawing()
            self.newDrawingView.drawTool = ACEDrawingToolTypePen
        }
        else
        {
            isClrSelecting = false
            self.enableSwipeFilter()
        }
    }
    
    
    
    func actionStartErase(){
        self.endEditing(true)
        if isErasing != true
        {
            isClrSelecting = false
            isErasing = true
            isDrawing = false
            isTexting = false
            self.enableDrawing()
            self.newDrawingView.drawTool = ACEDrawingToolTypeEraser
        }
        else{
            isErasing = false
            self.enableSwipeFilter()
        }
        
    }
    func actionClear(){
        self.tempImageView.image = nil
        isClrSelecting = false
        isErasing = false
        isDrawing = false
        isTexting = false
        self.enableDrawing()
        self.newDrawingView.clear()
    }
    func actionUndo(){
        self.newDrawingView.undoLatestStep()
    }
    
    //    MARK:- TextView delegate
    
    func textViewDidBeginEditing(textView: UITextView) {
        isTexting = true
        previousFrame = textView.frame
        var minYPos = (self.frame.size.height - 290)
        if (previousFrame.origin.y + previousFrame.size.height) > minYPos{
            minYPos -= previousFrame.size.height
        }
        else{
            minYPos = previousFrame.origin.y
        }
        
        UIView.animateWithDuration(0.1) { () -> Void in
            
            textView.frame = CGRectMake(0, minYPos, self.previousFrame.size.width, self.previousFrame.size.height)
        }
        delegateGB?.textViewDidShow(textView)
        textView.hidden = false
    }
    
    func textViewShouldReturn(textView: UITextField) -> Bool {
        isTexting = false
        textView.resignFirstResponder()
        if textView.text == STREMPTY
        {
            currentFeture = FetureType.NAN
            textView.hidden = true
        }
        
        UIView.animateWithDuration(0.1) { () -> Void in
            textView.frame = self.previousFrame
        }
        
        return true
    }
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        isTexting = true
        if(text == "\n") {
            textView.resignFirstResponder()
            if textView.text == STREMPTY
            {
                currentFeture = FetureType.NAN
                textView.hidden = true
            }
            return false
        }
        else if(textView.text.length == 0 && text == " "){
            return false
        }
        else if(textView.text.length > 80 && text != STREMPTY){
            return false
        }
        var minYPos = (self.frame.size.height - 290)
        
        if (previousFrame.origin.y + previousFrame.size.height) > minYPos{
            minYPos -= previousFrame.size.height
        }
        else{
            minYPos = previousFrame.origin.y
        }
        let txtFinal = textView.text + text
        var height : CGFloat = (txtFinal.heightWithConstrainedWidth(previousFrame.size.width, font: textView.font!))
        height += 13
        textView.frame = CGRectMake(textView.frame.origin.x, minYPos, textView.frame.size.width, height)
        
        previousFrame.size.height = textView.frame.size.height
        
        
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        isTexting = false
        if textView.text.isEmptyString() == true
        {
            textView.text = STREMPTY
            textView.hidden = true
            
        }
        else
        {
            UIView.animateWithDuration(0.1) { () -> Void in
                textView.frame = self.previousFrame
            }
        }
        delegateGB?.textViewDidHide(textView)
    }
    
    
    
    //    MARK:- GestureRecognizer Methods
    func handlePanGesture(sender : UIPanGestureRecognizer){
        //        isDrawing == false && isErasing == false && isTexting == false && currentFeture == FetureType.Text
        if isTexting == false{
            
            var translatedPoint = sender.translationInView(self)
            if sender.state == UIGestureRecognizerState.Began {
                firstX = sender.view!.center.x
                firstY = sender.view!.center.y
            }
            let yPos = firstY+translatedPoint.y
            let height = txtViewLbl.frame.size.height
            if (yPos < (self.frame.size.height - (height + 30)) && yPos >= 100)
            {
                translatedPoint = CGPointMake(firstX,yPos)
                sender.view?.center = translatedPoint
                self.assignNewFrame()
            }
            
        }
    }
    
    func assignNewFrame(){
        previousFrame = txtViewLbl.frame
    }
    
    //    MARK:- Save Edited Image
    func saveToPhotos() {
        UIGraphicsBeginImageContext(tempImageView.bounds.size)
        tempImageView.image?.drawInRect(CGRect(x: 0, y: 0,
            width: tempImageView.frame.size.width, height: tempImageView.frame.size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, self,#selector(GBEditingView.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        guard error == nil else {
            //Error saving image
            return
        }
        //Image saved successfully
        Helper.showAlert("Image saved in photos.", pobjView: self)
    }
    
    //    MARK:- MaskingView
    func setUpMaskingView(){
        
        if isLoadedOnce == false{
            
            objBounds = frame
            fltScrollOffset = objBounds.size.width
            fltConstant = objBounds.size.width
            //        slider = UISlider()
            
            self.setImageAndFilter(originalImage, array:[FilterType.HighContrast,FilterType.BlackAndWhite,FilterType.Specia,FilterType.Drama,FilterType.Congo,FilterType.Birthday,FilterType.ThankYou])
            
            arrImagess = NSMutableArray(objects: originalImage,sepiaImage,bawImage,hiContrastImage,dramaImage,congratulationsImage,happybirthdayImage,thankYouImage)
            
            imgViewOriginal = UIImageView()
            imgViewOriginal.image = originalImage
            imgViewOriginal.contentMode = .ScaleAspectFit
            
            imgViewSpecia = UIImageView()
            imgViewSpecia.image = sepiaImage
            
            imgViewSpecia.contentMode = .ScaleAspectFit
            
            imgViewBAW = UIImageView()
            imgViewBAW.image = bawImage
            imgViewBAW.contentMode = .ScaleAspectFit
            
            imgViewContrast = UIImageView()
            imgViewContrast.image = hiContrastImage
            imgViewContrast.contentMode = .ScaleAspectFit
            
            
            imgViewDrama = UIImageView()
            imgViewDrama.image = dramaImage
            imgViewDrama.contentMode = .ScaleAspectFit
            
            imgViewCongratulations = UIImageView()
            imgViewCongratulations.image = congratulationsImage
            imgViewCongratulations.contentMode = .ScaleAspectFit
            
//            imgViewDonation = UIImageView()
//            imgViewDonation.image = donationImage
//            imgViewDonation.contentMode = .ScaleAspectFit
            
            imgViewHappyBirthday = UIImageView()
            imgViewHappyBirthday.image = happybirthdayImage
            imgViewHappyBirthday.contentMode = .ScaleAspectFit
            
            arrImagViews = NSMutableArray(objects:imgViewOriginal,imgViewSpecia,imgViewBAW,imgViewContrast,imgViewDrama,imgViewCongratulations,imgViewHappyBirthday)
            /*
             arrImagViews = NSMutableArray(objects:imgViewOriginal,imgViewSpecia,imgViewBAW,imgViewContrast,imgViewDrama,imgViewCongratulations,imgViewDonation,imgViewHappyBirthday)
             */
            //        img.contentMode = .ScaleAspectFit
            scrollViewFilter.delegate = self;
            scrollViewFilter.backgroundColor = UIColor.clearColor()
            scrollViewFilter.pagingEnabled = true;
            scrollViewFilter.contentSize = CGSizeMake(objBounds.size.width * CGFloat(arrImagess.count), objBounds.size.height);
            currentPage = 0
            
            var scrollWidth : CGFloat = 0
            for someImage in arrImagViews
            {
                let img : UIImageView = someImage as! UIImageView
                img.frame = CGRectMake(0, 0, objBounds.size.width, objBounds.size.height);
                
                self.addSubview(img)
                scrollWidth += objBounds.size.width
            }
            
            self.imgMasking()
            
            scrollViewFilter.contentSize = CGSizeMake(scrollWidth+objBounds.size.width, objBounds.size.height);
            self.bringSubviewToFront(scrollViewFilter)
            self.setUpView()
            imgViewCongratulations.image = originalImage
            isLoadedOnce = true
        }
        else{
            self.setImageAndFilter(originalImage, array:[FilterType.HighContrast,FilterType.BlackAndWhite,FilterType.Specia,FilterType.Drama,FilterType.Congo,FilterType.Birthday,FilterType.ThankYou])
            
            arrImagess.removeAllObjects()
            arrImagess = nil
            arrImagess = NSMutableArray(objects: originalImage,sepiaImage,bawImage,hiContrastImage,dramaImage,congratulationsImage,happybirthdayImage,thankYouImage)
            
            imgViewOriginal.image = originalImage
            imgViewSpecia.image = sepiaImage
            imgViewBAW.image = bawImage
            imgViewContrast.image = hiContrastImage
            imgViewDrama.image = dramaImage
            imgViewCongratulations.image = congratulationsImage
//            imgViewDonation.image = donationImage
            imgViewHappyBirthday.image = happybirthdayImage
            
            imgViewCongratulations.image = arrImagess.objectAtIndex(currentPage) as? UIImage
            currentPage = 0
            scrollViewFilter.contentOffset = CGPointMake(0, 0)
            imgViewDrama.image = arrImagess.objectAtIndex(currentPage) as? UIImage
        }
    }
    
    func imgMasking(){
        
        let maskLayer : CAShapeLayer = CAShapeLayer()
        let maskRect : CGRect = CGRectMake(0, 0, 0,0)
        // Create a path with the rectangle in it.
        let path : CGPathRef = CGPathCreateWithRect(maskRect, nil)
        // Set the path to the mask layer.
        maskLayer.path = path
        // Release the path since it's not covered by ARC.
        // Set the mask of the view.
        
        imgViewCongratulations.layer.mask = maskLayer
        imgViewHappyBirthday.layer.mask = maskLayer
    }
    //    MARK:- scrollView delegate
    func scrollViewDidScroll(scrollViewz: UIScrollView)
    {
        if isTexting == false && currentFeture != FetureType.Text
        {
            let maskLayer : CAShapeLayer = CAShapeLayer()
            let maskRect : CGRect = CGRectMake(fltScrollOffset - scrollViewz.contentOffset.x, 0, scrollViewz.contentOffset.x,imgViewBAW.frame.size.height)
            
            // Create a path with the rectangle in it.
            let path : CGPathRef = CGPathCreateWithRect(maskRect, nil)
            
            // Set the path to the mask layer.
            maskLayer.path = path
            
            // Set the mask of the view.
            imgViewCongratulations.layer.mask = maskLayer
            
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView)
    {
        if isTexting == false && currentFeture != FetureType.Text{
            let xPos : Int = Int(scrollView.contentOffset.x)
            let intPageNo : Int =  xPos / Int(objBounds.width)
            if scrollView.panGestureRecognizer.translationInView(scrollView.superview).x > 0
            {
                //scrolling rightwards
                if currentPage <= 0 {
                    return
                }
                else
                {
                    fltScrollOffset = CGFloat(Int(objBounds.width) * Int(currentPage))
                }
                
                let maskLayer : CAShapeLayer = CAShapeLayer()
                let maskRect : CGRect = CGRectMake(fltScrollOffset, 0, objBounds.width ,objBounds.height)
                // Create a path with the rectangle in it.
                let path : CGPathRef = CGPathCreateWithRect(maskRect, nil)
                
                // Set the path to the mask layer.
                maskLayer.path = path
                // Set the mask of the view.
                
                imgViewCongratulations.layer.mask = maskLayer
                imgViewDrama.image = arrImagess.objectAtIndex(currentPage-1) as? UIImage
                imgViewCongratulations.image = arrImagess.objectAtIndex(currentPage) as? UIImage
                
            }
            else
            {
                //scrolling leftwards
                if (currentPage >= arrImagess.count-1) {
                    return
                }
                fltScrollOffset = fltConstant + scrollView.contentOffset.x ;
                self.imgMasking()
                
                imgViewDrama.image = arrImagess.objectAtIndex(currentPage)  as? UIImage
                imgViewCongratulations.image = arrImagess.objectAtIndex(currentPage+1)  as? UIImage
                currentPage = intPageNo
            }
        }
         delegateGB?.edtingToolUser(EditingToolTracker.Filter.rawValue)
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        if isTexting == false && currentFeture != FetureType.Text{
            let xPos : Int = Int(scrollView.contentOffset.x)
            let intPageNo : Int =  xPos / Int(objBounds.width)
            currentPage = intPageNo
            if scrollView.panGestureRecognizer.translationInView(scrollView.superview).x > 0
            {
                //            if currentPage >= 3
                //            {
                //                imgViewCongratulations.image = arrImagess.objectAtIndex(0)  as? UIImage
                //            }
            }
            else{
                //            if currentPage >= 3
                //            {
                //                imgViewCongratulations.image = arrImagess.objectAtIndex(0)  as? UIImage
                //            }
            }
        }
    }
    
    func setImageAndFilter(image : UIImage , array: [FilterType]){
        for filter in array
        {
            switch filter{
            case .Specia :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
                    self.sepiaImage = self.getSepiaImage(image,intensity :1.1)
                })
                break
            case .BlackAndWhite :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
                    self.bawImage = self.imageBlackAndWhite(image)
                })
                break
            case .HighContrast :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
                    self.hiContrastImage = self.getHighContrastImage(image,intensity :5.0)
                })
                break
            case .Drama :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
               self.dramaImage = self.getOverlappedImage(UIImage(named: "sticker_drama")!,yDivider: 1.0 ,hDivider: 1.0)
                })
                break
            case .Birthday :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
               self.happybirthdayImage = self.getOverlappedImage(UIImage(named: "sticker_happy_birthday")!,yDivider: 1.0 ,hDivider: 1.0)
                })
                break
            case .Congo :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
                    self.congratulationsImage = self.getOverlappedImage(UIImage(named: "sticker_congratulation")!,yDivider: 1.0 ,hDivider: 1.0)
                })
                break
            case .DonationX :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
//                    self.donationImage = self.getOverlappedImage(UIImage(named: "sticker_donation")!,yDivider: 1.0 ,hDivider: 1.0)
                })
                break
            case .ThankYou :
                dispatch_sync(concurrentFilterQueue, { () -> Void in
                    self.thankYouImage = self.getOverlappedImage(UIImage(named: "sticker_thankyou")!,yDivider: 1.0 ,hDivider: 1.0)
                })
                break
            }
        }
        self.layoutSubviews()
    }
    //    MARK:- other methods
    func getSepiaImage(image : UIImage , intensity : CGFloat) -> UIImage{
        sepiaFilter = GPUImageSepiaFilter()
        sepiaFilter.intensity = intensity
        
        let outputImage = sepiaFilter.imageByFilteringImage(image)! as UIImage
        return outputImage
    }
    
    func getHighContrastImage(image : UIImage, intensity : CGFloat) -> UIImage{
        contrastFilter = GPUImageContrastFilter()
        contrastFilter.contrast = intensity
        
        let outputImage = contrastFilter.imageByFilteringImage(image)! as UIImage
        return outputImage
    }
    
    func imageBlackAndWhite(image : UIImage) -> UIImage{
        GrayFilter = GPUImageGrayscaleFilter()
        let outputImage = GrayFilter.imageByFilteringImage(image)! as UIImage
        return outputImage
    }
    
    func sliderValChanged(sender: UISlider) {
        
        switch currentPage
        {
        case 0:
            var currentSliderVal = sender.value as Float
            let strVal = String(format: "%.1f", currentSliderVal) as String
            currentSliderVal = Float(strVal)!
            let beginImage : UIImage = UIImage(named: "secondImage.jpg")!
            self.sepiaImage = nil
            self.sepiaImage = self.getSepiaImage(beginImage,intensity :CGFloat(currentSliderVal))
            
            previousSliderVal = currentSliderVal
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.arrImagess.replaceObjectAtIndex(self.currentPage, withObject: self.sepiaImage)
            })
            self.imgMasking()
            self.imgViewSpecia.image = self.arrImagess.objectAtIndex(self.currentPage)  as? UIImage
            //            }
            
            
            break
        case 2:
            let currentSliderVal = sender.value as Float
            let beginImage : UIImage = UIImage(named: "secondImage.jpg")!
            self.bawImage = nil
            self.bawImage =  self.getHighContrastImage(beginImage,intensity :CGFloat(currentSliderVal) + CGFloat(0.1))
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.arrImagess.replaceObjectAtIndex(self.currentPage, withObject: self.bawImage)
            })
            let maskLayer : CAShapeLayer = CAShapeLayer()
            let maskRect : CGRect = CGRectMake(fltScrollOffset, 0, objBounds.width ,objBounds.height)
            // Create a path with the rectangle in it.
            let path : CGPathRef = CGPathCreateWithRect(maskRect, nil)
            
            // Set the path to the mask layer.
            maskLayer.path = path
            // Set the mask of the view.
            imgViewSpecia.layer.mask = maskLayer
            imgViewBAW.image = arrImagess.objectAtIndex(currentPage)  as? UIImage
            
            break
        default:
            break
        }
    }
    
    func enableDrawing(){
        scrollViewFilter.hidden = true
        scrollViewFilter.scrollEnabled = false
    }
    
    func enableSwipeFilter(){
        scrollViewFilter.hidden = false
        scrollViewFilter.scrollEnabled = true
        self.bringSubviewToFront(scrollViewFilter)
        self.bringSubviewToFront(txtViewLbl)
        isDrawing = false
        isErasing = false
        isClrSelecting = false
    }
    
    func getOverlappedImage(topImage : UIImage,yDivider : CGFloat ,hDivider : CGFloat ) -> UIImage{
        
        let bottomImage = originalImage
        let topSize = bottomImage.size
//        let size = scrollViewFilter.frame.size
        UIGraphicsBeginImageContext(topSize)
        let bottomAreaSize = CGRect(x: 0, y: 0, width: topSize.width, height: topSize.height)
        bottomImage.drawInRect(bottomAreaSize)
        
//        let hDiv = Int(topImage.size.height / size.height)
        
        if yDivider > 0 && hDivider > 0{
            //        let topAreaSize = CGRect(x: 0, y: yPos + 100.0, width: size.width, height: size.height/hDivider)
            
            let topAreaSize = CGRect(x: 0, y: 0, width: topSize.width, height: topSize.height)
            topImage.drawInRect(topAreaSize, blendMode: CGBlendMode.Normal, alpha:1.0)
        }
        
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
