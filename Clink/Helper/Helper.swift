//
//  Helper.swift
//  Clink
//
//  Created by Gunjan on 04/02/16.
//  Copyright © 2016 inheritX. All rights reserved.
//

import UIKit
import MBProgressHUD

typealias alertComplitionBlock = (AnyObject, NSInteger) -> ()

class Helper: NSObject {
    
    static func showCommingsoonAlert() {
        Helper.showAlert(TXTFETURECOMINGSOON, pobjView: APPDELEGATE.window!)
    }
    
    static func showAlert(strMessage:String, pobjView:UIView!){
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
        let alertView = CustomAlert(title: APPNAME, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            
        }
            alertView.showInView(pobjView)
        }
    }
    
    static func showAlert(strMessage:String, pobjView:UIView!,completionBock : alertComplitionBlock){
        
        let alertView = CustomAlert(title: APPNAME, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert, buttonIndex)
        }
        alertView.showInView(pobjView)
    }
    
    static func showAlert(strMessage:String, cancelButtonTitle:String,otherButtonTitle : String, pobjView:UIView!,completionBock : alertComplitionBlock){
        
        let alertView = CustomAlert(title: APPNAME, message:strMessage, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: cancelButtonTitle, otherButtonTitle: otherButtonTitle) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert, buttonIndex)
        }
        alertView.showInView(pobjView)
    }
    static func showAlertwithTitle(strTitle:String , strMessage:String , pobjView:UIView!,completionBock : alertComplitionBlock){
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert, buttonIndex)
        }
        alertView.showInView(pobjView)
    }
    
    static func showAlertwithTitle(strTitle:String , strMessage:String , cancelButtonTitle:String,otherButtonTitle : String, pobjView:UIView!,completionBock : alertComplitionBlock){
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: cancelButtonTitle, otherButtonTitle: otherButtonTitle) { (customAlert, buttonIndex) -> Void in
            completionBock(customAlert, buttonIndex)
        }
        alertView.showInView(pobjView)
    }
    
    static func showAlertwithTitle(strTitle:String , strMessage:String, pobjView:UIView!){
        
        let alertView = CustomAlert(title: strTitle, message:strMessage.uppercaseFirst, delegate: nil, color:kThemeMainLabelColor , cancelButtonTitle: BTNOKTITLE, otherButtonTitle: nil) { (customAlert, buttonIndex) -> Void in
            
        }
        alertView.showInView(pobjView)
    }
    
    static func showNetworkIndicator(){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    static func hideNetworkIndicator(){
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    static func setTextFieldPadding(textField: UITextField!,yPosition:CGFloat){
        
        let leftView:UIView = UIView(frame: CGRectMake(0, 0, yPosition, textField.frame.size.height))
        leftView.backgroundColor = textField.backgroundColor
        textField.leftView = leftView
        textField.leftViewMode = .Always
    }
    
    static func removeFromUserDefaults(pstrKey:String){
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey(pstrKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    static func showHud(pview : UIView){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            MBProgressHUD.showHUDAddedTo(pview, animated: true)
        }
    }
    
    static func hideHud(pview : UIView){
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            MBProgressHUD.hideHUDForView(pview, animated: true)
        }
    }
    
    static func getDateDifferenceforDate(pobjFirstDate: NSDate, andDate pobjLastDate: NSDate) -> String {
        
        var strTime: String
        let distanceBetweenDates: NSTimeInterval = pobjLastDate.timeIntervalSinceDate(pobjFirstDate)
        let secondsInAnHour: Double = 3600
        let daysBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour) / 24)
        let weekBetweenDate:Int = Int((distanceBetweenDates / secondsInAnHour) / (24*7))
        
        if weekBetweenDate > 48{
            strTime = "\(Int(weekBetweenDate)/4/12) \(STRYEARAGO)"
            return strTime
        }
        if weekBetweenDate >= 4{
            strTime = "\(Int(weekBetweenDate)/4) \(STRMONTHAGO)"
            return strTime
        }
        if weekBetweenDate > 0{
            strTime = "\(Int(weekBetweenDate)) \(STRWEEKAGO)"
            return strTime
        }
        
        if daysBetweenDates > 0  && daysBetweenDates <= 1 {
            strTime = "\(Int(daysBetweenDates)) \(STRDAYAGO)"
            return strTime
        }
        else if daysBetweenDates > 0 {
            strTime = "\(Int(daysBetweenDates)) \(STRDAYSAGO)"
            return strTime
        }
        
        let hoursBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour))
        if hoursBetweenDates > 0 && hoursBetweenDates <= 1 {
            strTime = "\(Int(hoursBetweenDates)) \(STRHOURAGO)"
            return strTime
        }
        else if hoursBetweenDates > 1 {
            strTime = "\(Int(hoursBetweenDates)) \(STRHOURSAGO)"
            return strTime
        }
        
        let minutesBetweenDates: Int = Int((distanceBetweenDates / secondsInAnHour) * 60)
        if minutesBetweenDates > 0 && minutesBetweenDates <= 1{
            strTime = "\(Int(minutesBetweenDates)) \(STRMINUTEAGO)"
            return strTime
        }
        else if minutesBetweenDates > 0 {
            strTime = "\(Int(minutesBetweenDates)) \(STRMINUTESAGO)"
            return strTime
        }
        
        if minutesBetweenDates == 0 {
            strTime = STRFEWSECONDSAGO
            return strTime
        }
        
        return STREMPTY
    }
    
    static func getBasicAuthString(strDate : String , strMethod : String , strPath : String ,strNonse : String) -> String{
        
        let strPassword = strMethod + "+" + "api" + strPath + "+" + strNonse + "+" + strDate
        
        let strShaPassword = strPassword.hmac(.SHA256, key: APISECRET)
        let shaPasswordData: NSData = strShaPassword.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64Password = shaPasswordData.base64EncodedStringWithOptions([])
        let loginString = NSString(format: "\(APIKEY):\(base64Password)")
        let loginData: NSData = loginString.dataUsingEncoding(NSUTF8StringEncoding)!
        let base64LoginString = loginData.base64EncodedStringWithOptions([])
        return base64LoginString
    }
    
    static func getTrackerGroups(){
        
        if APPDELEGATE.arrTrackerGroups.count <= 0{
            
            let dicData : [String : AnyObject] = [KSEARCH : ""]
            ClinkTrackerManager.sharedInstance.callWebService(ClinkTrackerRouter.SearchTrackerGroup(dicData).URLRequest, pAlertView: nil, pViewController: nil, pCompletionBlock: { (success, response) -> () in
                
                if success == true && response != nil{
                    let dicResponse = response as? [String : AnyObject]
                    if let arrTrackerResponse = dicResponse!.getValueIfAvilable(KTRACKERGROUP)  as? [AnyObject]
                    {
                        var arrObjTrackerGroup = [ClsTrackerGroup]()
                        for dicTrackerGroup in arrTrackerResponse{
                            let objTrackerGroup = ClsTrackerGroup(pdictResponse: dicTrackerGroup as! [String : AnyObject])
                            arrObjTrackerGroup.append(objTrackerGroup)
                        }
                        APPDELEGATE.arrTrackerGroups = arrObjTrackerGroup
                        USERDEFAULTS.setTrackerArrayObject(arrObjTrackerGroup)
                    }
                }
            })
        }
    }
    
    static func setTrackerData(dicJson : [String : AnyObject],strGroupId : String){
        let dicData : [String : AnyObject] = [KTRACKERGROUPUUID : strGroupId, KDATA : Helper.generateStrginFromJSON(dicJson)]
        
        dispatch_async(BACKGROUNDQUEUE) { () -> Void in
            ClinkTrackerManager.sharedInstance.callWebService(ClinkTrackerRouter.CreateTracker(dicData).URLRequest, pAlertView: nil, pViewController: nil, pCompletionBlock: { (success, response) -> () in
            })
        }
        
    }
    
    static func formatNumber(mobileNumber: String) -> String {
        var number : String!
        number = mobileNumber.stringByReplacingOccurrencesOfString("(", withString: "")
        number = number.stringByReplacingOccurrencesOfString(")", withString: "")
        number = number.stringByReplacingOccurrencesOfString(" ", withString: "")
        number = number.stringByReplacingOccurrencesOfString("-", withString: "")
        number = number.stringByReplacingOccurrencesOfString("+", withString: "")
        let length: Int = Int(number.characters.count)
        if length > 10 {
            number = mobileNumber.substringFromIndex(mobileNumber.startIndex.advancedBy(length - 10))
        }
        return number
    }
    
    static func getLength(mobileNumber: String) -> Int {
        var number : String!
        number = mobileNumber.stringByReplacingOccurrencesOfString("(", withString: "")
        number = number.stringByReplacingOccurrencesOfString(")", withString: "")
        number = number.stringByReplacingOccurrencesOfString(" ", withString: "")
        number = number.stringByReplacingOccurrencesOfString("-", withString: "")
        number = number.stringByReplacingOccurrencesOfString("+", withString: "")
        let length: Int = Int(number.characters.count)
        return length
    }
    
    static func generateStrginFromJSON(dicJson : [String : AnyObject])-> String{
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(dicJson, options: [])
        return String(data: jsonData, encoding: NSUTF8StringEncoding)!
    }
    
}
